Bayesian Algorithm for Finding Groups and Kinematic Members (BAFGKM)
This is a stupid name...

This is the most updated python version of the Bayesian membership selection algorithm from Rizzuto, Ireland and Robertson (2011). Given a kinematic model for a group of co-moving stars (X,Y,Z,U,V,W and covariance matrix, that can be a function of anything e.g. star position) this will compute the bayes factor for the probability of membership in the group relative to the probability of membership in the general "Field" described by the Galactic thin disk using the prescription in Rizzuto et al. 2011 and 2015. 

Also contains the new suite of clustering models, such as the Single-Link algorithm for Hierarchical clustering, and the Bayesian Infinite Gaussian Mixture Model (using the Dirichlet process) for automatic identification of undiscovered groups of co-moving stars. (Work in progress, but it finds many known groups in the limited Tycho-Gaia dataset which is a good sign this early on). This portion is currently on the back burner until Gaia data release 2 is out (mid-late 2018 at current estimates) where it will potentially be extremely usefull. 


VERSION 1.0
Code chunks for the Rizzuto 2011/2015 algorithm, what most people are probably interested in:

cp_math.py: Everything for finding convergent points from velocities
and rotating velocities/observables into the convergent-point
coordinate frame in different functions.

sample_generation.py: Functions for building/tracing-forward group and field
(X,Y,Z,U,V,W,Age) samples for the MC integrals.

bayes_code.py: Functions for computing P(Data|Model) for input
parameters and star data. Includes both multi-star and single-star versions of the star-wise integral. 
Will in future also contain the overall group probability integral for the MCMC
moving-group search.

helpers.py: Houses functions that help run the membership
selection and group finding codes. Currently just dataset_looper()
which chunks star data into managable subsets for input into
bayes_code.py functions. Potentially will have more functionality in
future for running the mixture models, but that's not needed at the moment.

Also scripts for implementing the code using supercomputer resources like Texas Advanced Computing Center with MPI4py.

### DEPENDENCIES ###
Requires various functions in pyutils. Eventually this will all be packaged together.

### Who do I talk to? ###
Aaron Rizzuto.
If used, please cite Rizzuto 2011 and 2015, and feel free to get in contact with me for help getting things running or retooling for your specific use case.
