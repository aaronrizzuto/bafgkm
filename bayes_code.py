#We are using X in the direction of the galactic centre and U in the direction of the anti-centre

##IMPORTS
import numpy as np
#import matplotlib.pyplot as plt
#import time

##UTILS imports:
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from circular_stats import circmean
from circular_stats import circvar
##BAFGK imports:
from cp_math import find_cp
from cp_math import rotate_pm_to_cp
from cp_math import project_uvw_to_cp
from cp_math import rotate_pm_to_cp_multistar_distanced
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
##ASTROLIB imports:
from ACRastro.glactc import glactc

import pdb


    
## this function has to take star data + pre-generated xyzuvw numbers and computes the integral of 
## P(D/phi)P(phi/Model) where phi are the model parameter distributions
##ra,dec,gl,gb,pmra,sig_pmra,pmdec,sig_pmdec,plx,sig_plx,vr,sig_vr always in stardat

def single_star_integral(stardat,rposvel,galcon,eqcon):
    exp = np.exp
    ##find the convergent points for the sample velocities
    ##galcon,eqcon = find_cp(rposvel[:,3:6],equatorial=True) 
    tester = stardat.shape
    if len(tester)==2:
        stardat=stardat[0,:]
    ##project the velocities into the convergent-point frame
    uexp,rvexp = project_uvw_to_cp(rposvel[:,3:6],galcon,stardat[2:3],stardat[3:4],stardat[0:1],stardat[1:2])

    ##now rotate the stars pmra,pmdec into the convergent point frame
    mu_perp,mu_par,sig_mu_perp,sig_mu_par = rotate_pm_to_cp(stardat[0:1],stardat[1:2],stardat[4:5],stardat[6:7],stardat[5:6],stardat[7:8],stardat[13:14],eqcon)

    ##now turn the traced (or not traced) random positions into distances or plx's
    llll,bbbb,gplx=gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)

    ##now compare everything to calculate the MC integral
    mu_par_exp = uexp*2*np.pi*gplx/29.8
    parallel   = -(mu_par-mu_par_exp)**2/2/sig_mu_par**2
    distance   = -(stardat[8]-gplx)**2/2/stardat[9]**2
    radial     = -(stardat[10]-rvexp)**2/2/stardat[11]**2
    perpendicular = -mu_perp**2/2/sig_mu_perp**2
    samp_pdpar    = exp(parallel)*exp(perpendicular)*exp(distance)*exp(radial)
    samp_pdpar    = samp_pdpar/sig_mu_par/stardat[9]/sig_mu_perp/stardat[11]/np.sqrt(2*np.pi)
    pdpar         = np.mean(samp_pdpar)
    #pdb.set_trace()
    return pdpar
#@profile
def multi_star_integral(stardat,rposvel,galcon,eqcon,rgl,rgb,rgplx,field=False):
    exp = np.exp

    ##project the velocities into the convergent-point frame
    uexp,rvexp = project_uvw_to_cp_multistar(rposvel[:,3:6],galcon,stardat.l,stardat.b,stardat.ra,stardat.dec)

    ##now rotate the star pmra,pmdec into the convergent point frame
    #pdb.set_trace()
    mu_perp,mu_par,sig_mu_perp,sig_mu_par=rotate_pm_to_cp_multistar(stardat.ra,stardat.dec,stardat.pmra,stardat.pmdec,
        stardat.sig_pmra,stardat.sig_pmdec,stardat.pmcov,eqcon)


    ###THIS IS NOW DONE OUTSIDE THE FUNCTION AND ARE INPUTS FOR SPEED REASONS
    ##Turn the traced (or not traced) random positions into distances or plx's
    ##rgl,rgb,gplx=gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
    
    
    ##proper motion towards the convergent point
    mu_par_exp = uexp*rgplx*2*np.pi/29.8
    parallel   = -(mu_par-mu_par_exp)**2/2/sig_mu_par**2

    ##distance 
    plx        = stardat.plx
    ivplx      = 1/stardat.sig_plx**2/2
    distance   = -(plx[:,np.newaxis]-rgplx)**2*ivplx[:,np.newaxis]

    ##radial velocity
    rv         = stardat.rv
    ivrv       = 1/stardat.sig_rv**2/2
    radial     = -(rv[:,np.newaxis]-rvexp)**2*ivrv[:,np.newaxis]

    ##proper motion perpendicular to the convergent point 
    perpendicular = -mu_perp**2/2/sig_mu_perp**2
    
    ##sky positions 
    ##treat them as a separate factor because the data precision of position means that
    ##a ridiculous number of samples would be needed
    gll        = stardat.l
    gbb        = stardat.b
    ##calculate the circular standard deviation and mean
    lmean      = circmean(rgl)
    lsig       = np.sqrt(circvar(rgl))
    bmean      = circmean(rgb)
    bsig       = np.sqrt(circvar(rgb))
    ##make sure bmean is between -90 and 90, can only really end up between 0 and 90 or 
    ##270 and 360 in circmean()
    ##also, l has to be between 0 and 360, and not negative
    if bmean > np.pi/2: bmean =  bmean-2*np.pi
    if lmean < 0.0: lmean+= 2*np.pi
    if lmean > 2*np.pi: lmean = np.mod(lmean,2*np.pi)
        
    if field == False:
        galacticl  = exp(-(gll-lmean)**2/2/lsig**2)/np.sqrt(2*np.pi)/lsig
        galacticb  = exp(-(gbb-bmean)**2/2/bsig**2)/np.sqrt(2*np.pi)/bsig
##in the case of the field integral, this should just be 1.0
    if field == True:
        galacticl = gll*0.0+1.0
        galacticb = gbb*0.0+1.0
    
    #pdb.set_trace()
    samp_pdpar    = exp(parallel)*exp(perpendicular)*exp(distance)*exp(radial)
    srvplx        = 1.0/stardat.sig_rv/stardat.sig_plx
    samp_pdpar    = samp_pdpar/sig_mu_par/sig_mu_perp
    samp_pdpar    = samp_pdpar*srvplx[:,np.newaxis]
    
    pdpar         = np.sum(samp_pdpar,axis=1)/np.array([rposvel.shape[0]],dtype='float')*galacticl*galacticb
    qwe = np.where(np.isnan(pdpar))[0]
    if len(qwe) == 1:pdb.set_trace()
    #pdb.set_trace()
    return pdpar
    
def multi_star_integral_distanced(stardat,rposvel,galcon,eqcon):
    exp = np.exp

    ##project the velocities into the convergent-point frame
    uexp,rvexp = project_uvw_to_cp_multistar(rposvel[:,3:6],galcon,stardat.l,stardat.b,stardat.ra,stardat.dec)
    
    ##now rotate the star pmra,pmdec into the convergent point frame
    ##check the output is in the full size format withh all covariance matrix bits, if not assumes zero plx-propoer motion covariances.
    hasallcovs = np.where(np.array(stardat.dtype.names) == 'plxpmra_cov')[0]
    if len(hasallcovs) == 1: mu_perp,mu_par,sig_mu_perp,sig_mu_par,plxmupar_cov = rotate_pm_to_cp_multistar_distanced(stardat.ra,stardat.dec,stardat.pmra,stardat.pmdec,stardat.sig_pmra,stardat.sig_pmdec,stardat.pmcov,stardat.plxpmra_cov,stardat.plxpmdec_cov,eqcon,stardat.sig_plx)
    else: mu_perp,mu_par,sig_mu_perp,sig_mu_par,plxmupar_cov = rotate_pm_to_cp_multistar_distanced(stardat.ra,stardat.dec,stardat.pmra,stardat.pmdec,stardat.sig_pmra,stardat.sig_pmdec,stardat.pmcov,0.0,0.0,eqcon,stardat.sig_plx)

    ##mu_perp2,mu_par2,sig_mu_perp2,sig_mu_par2,plxmupar_cov2 = rotate_pm_to_cp_multistar_distanced(stardat.ra,stardat.dec,stardat.pmra,stardat.pmdec,stardat.sig_pmra,stardat.sig_pmdec,stardat.pmcov,0.0,0.0,eqcon,stardat.sig_plx)

    ##proper motion towards the convergent point, make sure to include the full covariance here:

    mu_par_exp = uexp*stardat.plx*2*np.pi/29.8
    parallel   = -(mu_par-mu_par_exp)**2/2/(sig_mu_par**2 + (2*np.pi/29.8*uexp*stardat.sig_plx)**2 -2*plxmupar_cov*2*np.pi/29.8*uexp) 
    #pdb.set_trace()
        
    ##distance not used in this method:
    #plx        = stardat.plx
    #ivplx      = 1/stardat.sig_plx**2/2
    #distance   = -(plx[:,np.newaxis]-rgplx)**2*ivplx[:,np.newaxis]

    ##radial velocity
    rv         = stardat.rv
    ivrv       = 1/stardat.sig_rv**2/2
    radial     = -(rv[:,np.newaxis]-rvexp)**2*ivrv[:,np.newaxis]

    ##proper motion perpendicular to the convergent point 
    perpendicular = -mu_perp**2/2/sig_mu_perp**2
    #import pdb
    #pdb.set_trace()
    samp_pdpar    = exp(parallel)*exp(perpendicular)*exp(radial)
    samp_pdpar    = samp_pdpar/sig_mu_par/sig_mu_perp
    srvplx        = 1.0/np.sqrt(stardat.sig_rv**2)
    samp_pdpar    = samp_pdpar*srvplx[:,np.newaxis]
    pdpar         = np.sum(samp_pdpar,axis=1)/np.array([rposvel.shape[0]],dtype='float')
    qwe = np.where(np.isnan(pdpar))[0]
    if len(qwe) == 1: 
        print 'We got a nan' + str(stardat.id[0])
        ffff = open('nanfile.txt','ab')
        ffff.write(str(stardat.id[0]) + ' \n')
        ffff.flush()
        ffff.close()
        import pdb
        pdb.set_trace()
    return pdpar
    
    
    
    
    
    
    
    
    
    
    