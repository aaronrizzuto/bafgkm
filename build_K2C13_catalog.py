import numpy as np
#import pdb
#import matplotlib.pyplot as plt
import pickle
import time

from wtmn  import swtmn
from gcirc import gcirc

    ##hy_ucac ra/dec in radians, alk in degrees

##read hyades ucac + PPMXL
hy_ucac = pickle.load(open("K2C15_ucac4.pkl","rb")) 
#hy_ucac = pickle.load(open("hyades_testset.pkl","rb"))

##read Adams stuff
alk         = pickle.load(open("/Volumes/UT2/UTRAID/K2GO5_ALK/k2c15_alk_recarray.pkl","rb"))
##some simple cuts
qwe = np.where((alk.R2mag < 20.0) & (alk.sig_pm < 10.0))[0]
alk=alk[qwe]


##just cut out the K2C13 range for this run
#c13_range   = np.where((alk.ra <=90) & (alk.ra >= 50) & (alk.Kmag > 0.1) & (alk.Kmag < 20) & (alk.Jmag > 0.1) & (alk.sig_pm < 10.0))[0]
## & (alk.sig_pm < 10.0) 
#alk         = alk[c13_range]
dlim = 5 #arcsec cross-match distance limit
#pdb.set_trace()
##plan: for each star in ALK, see if it's in hy_ucac, if it is, figure out which PM to use based
##on uncertainties. If it's not, save it in some auxilliary array and decide if it should be added 
##based on some limits
alk_new = np.zeros(alk.shape[0],int)
closecount = 0
newcount   = 0
diffcount  = 0
simcount   = 0

ucac_range = np.where((hy_ucac.ra*180/np.pi >= min(alk.ra)-0.001)  & (hy_ucac.ra*180/np.pi <= max(alk.ra)+0.001) &  (hy_ucac.dec*180/np.pi >=min(alk.dec)-0.001) & (hy_ucac.dec*180/np.pi <= max(alk.dec)+0.001))[0]
hy_ucac = hy_ucac[ucac_range]
#pdb.set_trace()
diffstop=0
simstop=0
#pdb.set_trace()
timestart = time.time()
for i in range(alk.shape[0]):
    time1 = time.time()
    region = np.where((np.absolute(hy_ucac.ra*180/np.pi-alk.ra[i])<0.1) & (np.absolute(hy_ucac.dec*180/np.pi-alk.dec[i])<0.1))[0]
    time2 = time.time()
    if len(region) == 0: region=np.array([0,1,2,3,4,5,6,7,8,9,10])
    
    dist = gcirc(alk.ra[i],alk.dec[i],hy_ucac[region].ra*180/np.pi,hy_ucac[region].dec*180/np.pi,format=2)
    #time3 = time.time()
   # pdb.set_trace()
   
    close = np.argmin(dist)
    mindist = dist[close]
    close = region[close]
    #if len(close) > 1: pdb.set_trace()
    if mindist <= dlim:
        alk_new[i] = 0
        pmra_diff  = np.absolute(alk.pmra[i]-hy_ucac.pmra[close])/np.sqrt(alk.sig_pm[i]**2+hy_ucac.sig_pmra[close]**2)
        pmdec_diff = np.absolute(alk.pmdec[i]-hy_ucac.pmdec[close])/np.sqrt(alk.sig_pm[i]**2+hy_ucac.sig_pmdec[close]**2)
    
        closecount+=1
        if (pmra_diff < 5) & (pmdec_diff < 5): ##if the proper motions are not that different combine them
            xpmra = np.array([alk.pmra[i],hy_ucac.pmra[close]]) 
            xspmra = np.array([alk.sig_pm[i],hy_ucac.sig_pmra[close]])
            newpmra,new_sigpmra = swtmn(xpmra,1/xspmra**2)
                
            xpmdec = np.array([alk.pmdec[i],hy_ucac.pmdec[close]]) 
            xspmdec = np.array([alk.sig_pm[i],hy_ucac.sig_pmdec[close]])
            newpmdec,new_sigpmdec = swtmn(xpmdec,1/xspmdec**2)
        
            if simstop == 1:
                pdb.set_trace()
                simstop = 0
        
            hy_ucac[close].pmra = newpmra
            hy_ucac[close].sig_pmra = new_sigpmra
            hy_ucac[close].pmdec = newpmdec
            hy_ucac[close].sig_pmdec = new_sigpmdec
            simcount+=1
        else: ##if the proper motions are really different, use ALKs
            hy_ucac[close].pmra      = alk.pmra[i]
            hy_ucac[close].pmdec     = alk.pmdec[i]
            hy_ucac[close].sig_pmra  = alk.sig_pm[i]
            hy_ucac[close].sig_pmdec = alk.sig_pm[i]        
            if diffstop== 1:
                pdb.set_trace() 
                diffstop=0
            diffcount+=1   
    else: 
        alk_new[i] = 1
        newcount+=1
    if np.mod(i,10000) == 0:
        time_so_far = time.time()-timestart
        time_needed = time_so_far/(i+1)/60.0*(alk.shape[0]-i-1)
        print "up to " + str(i+1) + " out of " +str(alk.shape[0]) + " Time remaining: " + str(time_needed)+ " mins"
        ##write out the current results
        file1 = open("K2C15_alk_new_interim.pkl","wb")
        file2 = open("K2C15_alk_crosscat_interim.pkl","wb")
        pickle.dump(alk[np.where(alk_new == 1)[0]],file1)
        pickle.dump(hy_ucac,file2)
        file1.close()
        file2.close()      

pickle.dump(alk[np.where(alk_new == 1)[0]],open("K2C15_alk_new.pkl","wb"))
pickle.dump(hy_ucac,open("K2C15_alk_crosscat.pkl","wb"))

#pdb.set_trace()
print "Im Here"