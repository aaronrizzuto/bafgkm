import numpy as np
import pdb
import pickle
import matplotlib.pyplot as plt
from readcol import readcol
from ACRastro.glactc import glactc
from full_uvw_to_observable import obs_to_gal_full
from full_uvw_to_observable import obs_to_gal_sample
import sco_models

from ACRastro.gal_uvw import gal_uvw
np.set_printoptions(suppress=True,precision=3)

##Usco planet 1
phot_plx     = 7.1
sig_phot_plx = 1.6 
lstar        = 354.4205
pospm = np.array([242.5614059,-19.3192734,1000.0/145.0,-9.8,-24.2,-6.7])
cpospm = np.array([[0.00001,0,0,0,0,0],[0.,0.00001,0,0,0,0],[0.,0,0.7**2,0,0,0],[0,0,0,1.7**2,0,0],[0.,0,0,0,1.8**2,0],[0.,0,0,0,0,0.15**2]])

#TYC 8241-2652-1
phot_plx     = 7.14
sig_phot_plx = 1.0
lstar        = 296.2
pospm  = np.array([182.25,-51.33,7.14,-34.1,-9.4,15.0])
cpospm = np.array([[0.00001,0.,0.,0.,0.,0.],[0.,0.00001,0.,0.,0.,0.],[0.,0.,1.0,0.,0.,0.],[0.,0.,0.,2.1**2,0.,0.],[0.,0.,0.,0.,2.0**2,0.,],[0.,0.,0.,0.,0.,1.0]])

rposvel= obs_to_gal_sample(pospm,cpospm)

posvel,cposvel = obs_to_gal_full(pospm,cpospm)
sig_uvw        = np.sqrt(np.array([cposvel[3,3],cposvel[4,4],cposvel[5,5]]))
uvw            = posvel[3:]

uvw_me,sig_int,rg_bar,sig_rg_bar = sco_models.rizzuto_model(lstar)
uvw_chen = sco_models.chen_model(3)


sig_uvw = np.sqrt(np.array([cposvel[3,3],cposvel[4,4],cposvel[5,5]]))






pdb.set_trace()
print 'Im Here'
