## central script to run the new bayes codes.
## test data will be hipparcos catalog

import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time
import emcee

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp

from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp

dpi = np.pi

## So far I am assuming the data catalog is a numpy redarray where each part of the array
## is a numpy array. I guess the basic data input must contain ra, dec, plx, sig_plx
## pmra,sig_pmra,pmdec,sig_pmdec,rv, sig_rv and some ID number/name for the stars
##General rule: ALL ANGLES ASSUMED IN RADIANS!!!!!!!!


file = open("hip_main.pkl","rb")
datacat = pickle.load(file)

##unpack the data to make things general:
id        = datacat.hip
ra        = datacat.ra
dec       = datacat.dec
pmra      = datacat.pmra
pmdec     = datacat.pmdec
sig_pmra  = datacat.sig_pmra
sig_pmdec = datacat.sig_pmdec
plx       = datacat.plx
sig_plx   = datacat.sig_plx
#vr        = datacat.vr
#sig_vr    = datacat.sig_vr
##make up and RV array for the hipparcos data (note this is temporary)
vr        = ra*0.0
sig_vr    = ra*0.0+300.0  
##Calculate target galactic l and b:
##Can probably run this in fk4 for speed as it shouldn't matter a whole lot
##for the calculations to follow
gl,gb     = glactc(ra*180/dpi,dec*180/dpi,2000,degree=True,fk4=True)

##Next: Cutting the inputs to some range 
##
##ing       = np.where((gl>343) & (gb>10) & (gb<30) & (pmra<-3) & (pmdec<-13) & (plx>3) & (sig_plx<3))
ing       = np.where((gl>343) & (gb>10) & (gb<30) & (plx>0.1))

##Make the sample that is wanted
id        = id[ing]
ra        = ra[ing]
dec       = dec[ing]
pmra      = pmra[ing]
pmdec     = pmdec[ing]
sig_pmra  = sig_pmra[ing]
sig_pmdec = sig_pmdec[ing]
plx       = plx[ing]
sig_plx   = sig_plx[ing]
vr        = vr[ing]
sig_vr    = sig_vr[ing]
##make these two into radians for the remainder of the code
gl        = gl[ing]*np.pi/180
gb        = gb[ing]*np.pi/180 
pmcov     = sig_pmra*0.0
##ideally we turn this into an array with shape (nstars,12) 
## here we have to assume that the array is ordered for each star as:
##ra,dec,gl,gb,pmra,sig_pmra,pmdec,sig_pmdec,plx,sig_plx,vr,sig_vr always
stardat = np.transpose(np.array([ra,dec,gl,gb,pmra,sig_pmra,pmdec,sig_pmdec,plx,sig_plx,vr,sig_vr,id,pmcov]))


##now a sample is defined and ready to go:

##CURRENTLY TESTING HERE:

##generate field distance PDF:
dist = 1000.0/stardat[:,8]
nfsamp = 100000
hist,binedges = np.histogram(dist,bins=50,range=(0.0,1000.0),density=True)
fdist         = rejsamp(binedges[0:50],hist,nfsamp)
fplx          = 1000.0/fdist
#pdb.set_trace()

#now make a field uvw sample
uvw_field = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = fdist
galconf,eqconf = find_cp(rposvel_field[:,3:6],equatorial=True) 
rglf,rgbf,rgplxf=gal_xyz(rposvel_field[:,0],rposvel_field[:,1],rposvel_field[:,2],plx=True,reverse=True)

##define a set of group parameters for testing, make it an upper sco test:
##parameters are like [X,Y,Z,U,V,W,Age] and the covar is a 6x6 on the first 6 parameters (not age)
#We are using X in the direction of the galactic centre and U in the direction of the anti-centre

inx,iny,inz = gal_xyz(352.00,20.00,6.9,radec=False,plx=True,reverse=False)
inu,inv,inw = gal_uvw(distance=None, lsr=None, ra=238.625490, dec=-27.3385, pmra=-12.07, pmdec=-26.17, vrad=-1.3, plx=6.32)
gpars = np.array([inx,iny,inz,inu,inv,inw,0.0])

#pdb.set_trace()
gcov  = np.array([[15,0,0,0,0,0],[0,15,0,0,0,0],[0,0,15,0,0,0],[0,0,0,3,0,0],[0,0,0,0,3,0],[0,0,0,0,0,3.]],dtype='float')**2

#pdb.set_trace()
this_star = np.where(stardat[:,12] == 77900)
#this_star = np.where(stardat[:,12] == 77955)
this_star = np.array([87, 192, 282, 362, 627, 682])

##first generate the random group values
dude = time.clock()
nsamp=10000
rposvel = build_sample_group(gpars[0:6],gcov,0.0,nsamp,trace_forward=True,observables=False)
rgl,rgb,rgplx=gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
gentime = time.clock() - dude

##now find the convergent-points (only need to do this once for each group model)
dude = time.clock()
galcon,eqcon = find_cp(rposvel[:,3:6],equatorial=True) 
cptime = time.clock() - dude


##now lets test this with the dataset_looper function
#rundat = stardat[this_star,:]
rundat = stardat

##field:
ft1 =time.clock()
pdmodf_loop = dataset_looper(rundat,rposvel_field,chunksize=900,field=True,verbose=True,doall=True)
fltime = time.clock()-ft1

gt1  =time.clock()
pdmodg_loop = dataset_looper(rundat,rposvel,chunksize=900,field=False,verbose=True,doall=True)
gltime = time.clock()-gt1

bfact = pdmodg_loop/pdmodf_loop
hn  = np.where(bfact != bfact)
bfact[hn] = 0.0

prob = 0.08*bfact/(1+0.08*bfact)





pdb.set_trace()
print "Im here"