import numpy as np
import pickle 
import pdb
import matplotlib.pyplot as plt
from readcol import readcol
from ACRastro.glactc import glactc

##my imports
from full_uvw_to_observable import obs_to_gal_full
from mahalanobis import mahalanobis_dist
from mahalanobis import mahalanobis_dist_vect
from SLINK import SLINK
from SLINK import assign_clusters
from SLINK import SLINK_plot
from SLINK import cluster_pops
from SLINK import pointer_to_packed
from clusterDPGMM_covar import DPMM
import scipy.cluster.hierarchy as sch
np.set_printoptions(suppress=True,precision=3)

##already done and saved:
infile   = '/Users/arizz/code/BAFsco_RV/final_rvs.csv'
rvs      = readcol(infile,fsep=',',asRecArray=True,names=True)
ok = np.where(rvs.sig_rv > 0)[0]
#pdb.set_trace()
rvs = rvs[ok]


hip_main = pickle.load(open("../hip_main.pkl","rb"))
##get the hipparcos information
ra        = np.zeros(len(rvs))
dec       = np.zeros(len(rvs))
pmra      = np.zeros(len(rvs))
sig_pmra  = np.zeros(len(rvs))
pmdec     = np.zeros(len(rvs))
sig_pmdec = np.zeros(len(rvs))
plx       = np.zeros(len(rvs))
sig_plx   = np.zeros(len(rvs))
#pdb.set_trace()

store_match = np.zeros(len(rvs.hip),int)


for i,hip in enumerate(rvs.hip):
    match        = np.where(hip_main.hip == hip)[0]
    store_match[i] = match
    #pdb.set_trace()
    ra[i]        = hip_main[match[0]].ra
    dec[i]       = hip_main[match[0]].dec
    pmra[i]      = hip_main[match[0]].pmra
    pmdec[i]     = hip_main[match[0]].pmdec
    sig_pmra[i]  = hip_main[match[0]].sig_pmra
    sig_pmdec[i] = hip_main[match[0]].sig_pmdec
    plx[i]       = hip_main[match[0]].plx
    sig_plx[i]   = hip_main[match[0]].sig_plx

gl,gb     = glactc(ra*180/np.pi,dec*180/np.pi,2000,degree=True,fk4=True)

these_hips = hip_main[store_match]
#pdb.set_trace()

#now turn the numbers into xyzuvw
posvel = np.zeros((len(rvs),6))
cposvel = np.zeros((len(rvs),6,6))
for i in range(0,len(rvs)):
    pospm = np.array([ra[i]*180/np.pi,dec[i]*180/np.pi,plx[i],pmra[i],pmdec[i],rvs.rv[i]])
    cpospm = np.array(np.eye(6)*np.array([1e-7,1e-7,sig_plx[i]**2,sig_pmra[i]**2,sig_pmdec[i]**2,rvs.sig_rv[i]**2]))
    thisposvel,thiscposvel = obs_to_gal_full(pospm,cpospm)
    posvel[i] = thisposvel
    cposvel[i] = thiscposvel
    print i+1

ids = rvs.hip
pickle.dump(these_hips,open("BAFsco_hip.pkl","wb"))
pickle.dump(posvel,open("BAFsco_posvel.pkl","wb"))
pickle.dump(ids,open("BAFsco_ids.pkl","wb"))
pickle.dump(cposvel,open("BAFsco_cposvel.pkl","wb"))


pdb.set_trace()

X  = pickle.load(open("BAFsco_posvel.pkl","rb"))
ids     = pickle.load(open("BAFsco_ids.pkl","rb"))
var_X = pickle.load(open("BAFsco_cposvel.pkl","rb"))

S_mat     = np.median(var_X,axis=0)
S_inverse = np.linalg.inv(S_mat)

mPI,mL = SLINK(X,mahalanobis_dist_vect,S_inverse)
mL[-1] = max((max(mL[:-1]),1000))
mZ     = pointer_to_packed(mPI,mL)

pdb.set_trace()
print "up to post processing"
##cut in steps of 2 "sigma", from 100 do
cutlim = 100
cut = np.arange(0,cutlim/2)*2+1
#cut = (13, 15, 17, 19, 21, 23, 25, 27)
#cut=(50,40,30,20,10,8,6,5,4,2,1)
##define group max and min limits
gmin = 1
gmax = 10000


groups = ()
gmeans = ()
gnums  = ()
cutlevel = ()
for thiscut in cut:
    print 'cutting at level ' + str(thiscut)
    assigns = assign_clusters(mPI,mL,thiscut)
    cmem    = cluster_pops(assigns,maxgroup=gmax,mingroup=gmin)
    ok      = np.where(cmem > 0)[0]
    for j in ok:
        ggg       = (np.where(assigns == j+1)[0],)
        groups    = groups+ggg
        gnums     = gnums+(len(ggg[0]),)
        cutlevel = cutlevel+(thiscut,)
        #pdb.set_trace()
        thismeans = np.mean(X[ggg[0],:],axis=0)
        gmeans    = gmeans + (thismeans,)  
    #pdb.set_trace()
gmeans = np.asarray(gmeans)
gnums = np.asarray(gnums)
cutlevel = np.asarray(cutlevel)
##now sort through the means and figure out which are different and which are not
gtype = np.zeros(len(gmeans),int)
current_number = 0
numsig = 10.0
for i in range(len(gmeans)):
    if gtype[i] == 0:
        current_number += 1
        ddd = mahalanobis_dist_vect(gmeans[i,:],gmeans,S_inverse)
        same = np.where(ddd <= numsig)[0]
        gtype[same] = current_number

##now condense "similar groups" into individual groups and make 
##a cut on the number of C levels a group appears in
unique_groups = np.unique(gtype)
uselist = ()
grp = ()
grp_mult = ()
gmult_cut = 2
for thisgtype in unique_groups:
    thisblock = np.where(gtype == thisgtype)[0]
    if len(thisblock) > gmult_cut:
        cutdiff   = np.absolute(cutlevel[thisblock]-np.ceil(np.max(cutlevel[thisblock])))
        usethis   = np.where(cutdiff == min(cutdiff))[0][0]
        uselist   = uselist+(thisblock[usethis],)
        grp       = grp+(groups[thisblock[usethis]],)
        grp_mult = grp_mult + (len(thisblock),)

uselist = np.asarray(uselist)
grp = np.asarray(grp)
gm  = gmeans[uselist,:]
gn  = gnums[uselist]
clv = cutlevel[uselist]
gmult = np.asarray(grp_mult)

pdb.set_trace()
dpmm_cutlevel = 10
dpmm_input_assigns = assign_clusters(mPI,mL,dpmm_cutlevel)
#ccc = SLINK_plot(X,dpmm_input_assigns,3,4,maxgroup=10000,mingroup=1)
dpmm = DPMM(n_components=-2,epsilon=0.1,max_iter=1000,alpha=1)
dpmm.fit_collapsed_Gibbs(X,var_X,assigns=dpmm_input_assigns,save_iterations=True)
pdb.set_trace()
assigns = dpmm.predict(X)
means   = dpmm.means_
covars  = dpmm._get_covars()
fin = SLINK_plot(X,assigns,3,4,maxgroup=10000,mingroup=5)
#	tc = np.where(assigns == i)[0]
# 	plt.plot(X[tc,0],X[tc,4],'.')	


pdb.set_trace()
print "Im here"

