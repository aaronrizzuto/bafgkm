#basics
import numpy as np
import pickle 
import pdb
import matplotlib.pyplot as plt

##my imports
from mahalanobis import mahalanobis_dist
from mahalanobis import mahalanobis_dist_vect
from ACRastro.glactc import glactc


##get hipparcos
outstr = "fullhip"
outfile0 = "outputs/slink_X_" + outstr+".pkl"
outfile4 = "outputs/slink_varX_"+outstr+".pkl"
X        = pickle.load(open(outfile0,"rb"))
var_X    = pickle.load(open(outfile4,"rb"))
ids      = pickle.load(open("outputs/slink_ids_fullhip.pkl","rb"))

gl,gb     = glactc(X[:,0],X[:,1],2000,degree=True,fk4=True)

urange = np.where((gl > 340) & (gb > 10) & (gb < 30) & (X[:,2]>3))[0]
#urange = np.where((X[:,2]>5))[0]
pdb.set_trace()
X     = X[urange]
var_X = var_X[urange]
ids   = ids[urange]
pdb.set_trace()
#pdb.set_trace()
##mahalonobis distance matrix to use
S_mat     = np.median(var_X,axis=0)
#things within a 5 degrees of each other are so called close
S_mat[0,0] = 4.0
S_mat[1,1] = 4.0
S_inverse = np.linalg.inv(S_mat)

##what to set the nearest neighbours number to? 

knum = 50
mN = np.zeros((len(X),knum),int)-99
mD = np.zeros((len(X),knum))-99
density = np.zeros(len(X))-99
for i in range(len(X)):
    dists      = mahalanobis_dist_vect(X[i],X,S_inverse)
    d_ord      = np.argpartition(dists,knum+1)[0:knum+1] ##+1 to deal with 0 distance point i
    dd         = np.argsort(dists[d_ord])
    mN[i]      = d_ord[dd[1:]]
    mD[i]      = dists[mN[i]]
    density[i] = len(np.where(mD[i]<10)[0])
   # pdb.set_trace()
    if np.mod(i+1,1000) == 0: print 'Up to '+ str(i+1) + ' out of '+ str(len(X))
pdb.set_trace()

#make SNN graph
SNN = np.zeros((len(X),len(X)))
for i in range(len(X)):
    pdb.set_trace()



# nn = 9109
# d1=2
# d2=3
# plt.plot(X[:,d1],X[:,d2],',')
# plt.plot(X[nn,d1],X[nn,d2],'.')
# plt.plot(X[mN[nn],d1],X[mN[nn],d2],'.')
# plt.show()

#pdb.set_trace()
##now find cluster supporting objects, outliers and others
otype = np.zeros(len(X),int)+3
density_threshold = 0.02
for i in range(len(X)):
    test1 = np.where(density[mN[i]] > density[i])[0]
    if len(test1) == 0: otype[i] = 1
    if (len(test1) >0) & (density[i] < density_threshold): otype[i]=2

cso  = np.where(otype == 1)[0]
cso_assign = np.arange(len(cso))
outl = np.where(otype == 2)[0] 
rest = np.where(otype == 3)[0]
pdb.set_trace()
# d1=3
# d2=4
# plt.plot(X[:,d1],X[:,d2],',')
# plt.plot(X[cso,d1],X[cso,d2],'.r')
# plt.show()

##now do the local optimization steps
##initialize
cnum = len(cso)+1
##plus 1 for the outlier group, initialize memberships to all groups equally
mem = np.zeros((len(X),cnum))+1.0/cnum
for i in range(len(cso)):
    mem[cso[i]] = 0.0
    mem[cso[i],cso_assign[i]] = 1.0
#assign the outliers 
mem[outl,cnum-1] = 1.0

##run the optimization:
n_iterations = 50
iteration=0
while iteration < n_iterations:
    iteration+=1
    print "up to iteration " + str(iteration) + " out of " +str(n_iterations)
    for i in range(len(X)):
        ##only update a point that isn't a cso or outlier

        if otype[i] == 3:
            mem[i] = np.dot(np.transpose(mem[mN[i]]),1/mD[i])
            mem[i] = mem[i]/np.sum(mem[i])
         #   if np.sum(abs(mem[i] - mem[0])) > 0.000001: pdb.set_trace()
        
assigns= np.argmin(mem,axis=1)

for i in cso_assign: plt.plot(X[np.where(assigns == i)[0],2],X[np.where(assigns == i)[0],3],'.')
plt.show()

pdb.set_trace()
print "Im Here"
