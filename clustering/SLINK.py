##A Basic algorithm to do SLIKNK clustering, for general inputs

import numpy as np
import pdb
import pickle
import matplotlib.pyplot as plt
import time
from scipy import stats
import sys


##inputs: X=data vector of vectors, dmet = a function that will calculate distance metric
## dmet_extra=extra inputs to the distance metric function
def SLINK(X,dmet,dmet_extra):	
	maxdist = sys.float_info.max
	ndim    = X.shape[0]
	mPI     = np.zeros(ndim,dtype=int)
	mL      = np.zeros(ndim)
	mL[0]   = maxdist
	##Make a row of the distance matrix
	start_time = time.clock()
	for i in range(1,ndim):
		mPI[i]   = i
		mL[i]    = maxdist
	    ##calculate a row of the distance matrix for datapoint i to other datapoint <i
		mD       = dmet(X[i,:],X[0:i,:],dmet_extra)
		for j in range(0,i):
			next = mPI[j]
			if mL[j] < mD[j]:
				mD[next] = min(mD[next],mD[j])
			else:
				mD[next] = min(mL[j],mD[next])
				mPI[j]   = i
				mL[j]    = mD[j]
				
		for j in range(0,i):
		    next = mPI[j]
		    if mL[j] > mL[next]: mPI[j] = i
					
		if np.mod(i,1000) == 0: print "SLINKing "+str(i) + " out of " +str(ndim)
	time_used = (time.clock()-start_time)/60.0/60.0
	print "Time Used " + str(time_used)+ " hours"
	#pdb.set_trace()	   
	return mPI,mL

	
def assign_clusters(mPI,mL,cut):
	mL[-1]          = max(max(mL[:-1]),1000)
	ndim            = len(mPI)
	cspot           = np.where(mL >= cut)[0]
	cnum            = len(cspot)
	current_cluster = cnum+1
	assigns         = np.zeros(ndim,int)
	assigns[0]      = 1
	#pdb.set_trace()
	for i,j in enumerate(mPI[::-1]):
	    i = -i-1
	    #pdb.set_trace()
	    if mL[i] < cut: 
	        assigns[i] = assigns[j]
	    else: 
	        current_cluster -= 1
	        assigns[i]       = current_cluster
	return assigns
	
def cluster_pops(assigns,mingroup=50,maxgroup=1000000):
    cnum = len(np.unique(assigns))
    cmem = np.zeros(cnum,int)-1
    for i in range(cnum):
        cluster = np.where(assigns == i+1)[0]
        #cmem[i] = len(cluster)
        if (len(cluster) > mingroup) and (len(cluster) < maxgroup): cmem[i] = len(cluster)
    return cmem
	        
def SLINK_plot(X,assigns,dim1,dim2,mingroup=50,maxgroup=1000000):
	##plot the clustering:
	##start with the raw data
	cnum = len(np.unique(assigns))
	plt.plot(X[:,dim1],X[:,dim2],',')
	cols = ('r','b','g','k','m','c','y','r','b','g','k','m','c','y')
	cid     = np.zeros(cnum,int)
	cmem    = np.zeros(cnum,int)

	for i in range(cnum):
	    cluster = np.where(assigns == i+1)[0]
	    cid[i] = i+1
	    cmem[i] = len(cluster)
	    if (cmem[i] > mingroup) & (cmem[i] < maxgroup):
	    #pdb.set_trace()
		    plt.plot(X[cluster,dim1],X[cluster,dim2],'.')
	    print "plotting cluster "+str(i)+" out of "+str(cnum)
	plt.show()
	return cmem

def pointer_to_packed(mPI,mL):
  ##go from the SLINK pointer representation to the packed
  ##representation of the dendrogram. Good for making figure 
  ##and looking at the cut levels
    n          = mPI.shape[0]    
    sorted_idx = np.argsort(mL)
    node_ids   = np.arange(n)
    Z          = np.zeros((n-1,4))
    
    for i in range(n - 1):
        current_leaf = sorted_idx[i]
        pi = mPI[current_leaf]
        if node_ids[current_leaf] < node_ids[pi]:
            Z[i, 0] = node_ids[current_leaf]
            Z[i, 1] = node_ids[pi]
        else:
            Z[i, 0] = node_ids[pi]
            Z[i, 1] = node_ids[current_leaf]
        
        Z[i, 2]      = mL[current_leaf]
        node_ids[pi] = n + i
        
    Z[:,3]  = calculate_cluster_sizes(Z)[:-1]
    return Z
    
def calculate_cluster_sizes(Z):
    ##figure out how many things are in each cluster, 
    ##needed for going to the scipy.clustering.hierarchy.linkage 
    ##version of the packed representation (which can be plotted easily
    n = Z.shape[0]+1
    cs = np.zeros((n))
    for i in range(n - 1):
        ##floor the Z[i,0/1] numbers to make them ints
        child_l = np.floor(Z[i, 0])
        child_r = np.floor(Z[i, 1])
        if child_l >= n:
            cs[i] += cs[child_l - n]
        else:
            cs[i] += 1
        if child_r >= n:
            cs[i] += cs[child_r - n]
        else:
            cs[i] += 1
    return cs


	        