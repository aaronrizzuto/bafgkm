##A Basic algorithm to do SLIKNK clustering

import numpy as np
import pdb
import pickle
import matplotlib.pyplot as plt
import time
from scipy import stats
import sys

np.set_printoptions(suppress=True,precision=1)
##make fake data
# Number of samples per component
#n_samples = 100

# Generate random sample, two components
#np.random.seed(0)

# 2, 2-dimensional Gaussians
#C = np.array([[0., -0.1], [1.7, .4]])
#X = np.r_[np.dot(np.random.randn(n_samples, 2), C)+np.array([-1,4]),.7 * np.random.randn(n_samples, 2) + np.array([-6, 3])]

##"Field"
mns1 = np.array([-0.0,-0.0])
cov1 = np.array([[50.0,0.0],[0.0,50.0]])

mns2 = np.array([5.0,5.0])
cov2 = np.array([[1.0,0.0],[0.0,1.0]])
mns3 = np.array([-5.0,-5.0])
cov3 = np.array([[1.0,0.0],[0.0,1.0]])
mns4 = np.array([-5.0,5.0])
cov4 = np.array([[1.0,0.2],[0.2,1.0]])


X1 = np.random.multivariate_normal(mns1,cov1,5000)
X2 = np.random.multivariate_normal(mns2,cov2,1000)
X3 = np.random.multivariate_normal(mns4,cov3,1000)
X4 = np.random.multivariate_normal(mns3,cov4,1000)

C1 = np.zeros(X1.shape[0],int)+1
C2 = np.zeros(X2.shape[0],int)+2
C3 = np.zeros(X3.shape[0],int)+2
C4 = np.zeros(X4.shape[0],int)+2

#pdb.set_trace()
X = np.concatenate((X1,X2,X3,X4),axis=0)
C = np.concatenate((C1,C2,C3,C4),axis=0)
Xx = X[:,0]
Xy = X[:,1]

#pdb.set_trace()
##randomize the ordering of the data 
interim = np.vstack((Xx,Xy,C)).transpose()
permdat = np.random.permutation(interim)
X = permdat[:,0:2]
C = permdat[:,2].astype(int)
##randomize the points in the X array
#.random.
#pdb.set_trace()
##now the SLINK
##initialize everything
ndim = X.shape[0]
maxdist = sys.float_info.max
mPI = np.zeros(ndim,dtype=int)
mL  = np.zeros(ndim)
mL[0] = maxdist

##Make a row of the distance matrix
for i in range(1,ndim):
    mPI[i] = i
    mL[i] = maxdist
    ##calculate a row of the distance matrix for datapoint i to other datapoint <i
    if i == 1: mD = np.array([np.sqrt(np.sum((X[0:i,:]-X[i,:])**2))])
    if i != 1:  mD = np.sqrt(np.sum((X[0:i,:]-X[i,:])**2,axis=1))
    for j in range(0,i):
       # if (i==200) & (j == 199): pdb.set_trace() 
        next = mPI[j]
        if mL[j] < mD[j]:
            mD[next] = min(mD[next],mD[j])
        else:
           # pdb.set_trace()
            mD[next] = min(mL[j],mD[next])
            mPI[j] = i
            mL[j] = mD[j]
    if np.mod(i,1000) == 0: print "up to "+str(i) + " out of " +str(ndim)   

#pdb.set_trace()
##re-label clusters if needed
#for i in range(1,ndim):
#    for j in range(0,i):
#        next = mPI[j]
#        if mL[next] < mL[j]: mPI[j] = i

##set the final distance to the max of the rest of the distances
#mL[ndim-1] = max(mL[0:ndim-1])
mL[ndim-1] = 1000
plt.hist(mL[0:ndim-1],bins=30)
plt.show()
cut = input("Select Cut Value  ")
#pdb.set_trace()
#cut = max(mL[0:ndim-1])
#this makes sure the last object is in the last cluster.
cspot = np.where(mL >= cut)[0]
cnum = len(cspot)

current_cluster = cnum+1
assigns = np.zeros(ndim,int)
assigns[0] = 1
print 'now find clusters'
for i,j in enumerate(mPI[::-1]):
    i=-i-1
    #pdb.set_trace()
   # if i == 2: pdb.set_trace()
    #pdb.set_trace()
    if mL[i] < cut: 
        #assigns[j] = current_cluster
        assigns[i] = assigns[j]
    else: 
        current_cluster -= 1
        assigns[i] = current_cluster
        
        
##plot the clustering:
##start with the raw data
plt.plot(X[:,0],X[:,1],',')
cols = ('r','b','g','k','m','c','y','r','b','g','k','m','c','y')
cid     = np.zeros(cnum,int)
cmem    = np.zeros(cnum,int)

for i in range(cnum):
    cluster = np.where(assigns == i+1)[0]
    cid[i] = i+1
    cmem[i] = len(cluster)
    plt.plot(X[cluster,0],X[cluster,1],'.')
plt.show()



pdb.set_trace()
print 'here'