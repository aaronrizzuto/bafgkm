# -*- coding: utf-8 -*-

import itertools, random
import pdb
import numpy as np
from scipy import linalg
import pylab as pl
import matplotlib.pyplot as plt
import time
from scipy import stats
import pickle
np.set_printoptions(suppress=True,precision=3)

##convergence tolerance and iteration limit
#epsilon = 10e-8
#max_iter = 1000
#max_iter = 1
class Gaussian:
    def __init__(self, X=np.zeros((0,1)),sig_X=np.zeros([0,1]), kappa_0=1.000, nu_0=1.0001, mu_0=None, 
            Psi_0=None): # Psi is also called Lambda or T
        # See http://en.wikipedia.org/wiki/Conjugate_prior 
        # Normal-inverse-Wishart conjugate of the Multivariate Normal
        # or see p.18 of Kevin P. Murphy's 2007 paper:"Conjugate Bayesian 
        # analysis of the Gaussian distribution.", in which Psi = ^
        self.n_points = X.shape[0]
        self.n_var    = X.shape[1]

        self._hash_covar = None
        self._inv_covar  = None
        
        # NIW dist initial cluster mean paramater, should be input with cluster points as  mean of the data
        if mu_0 == None: 
            self._mu_0 = np.zeros((1, self.n_var))
        else:
            self._mu_0 = mu_0
        assert(self._mu_0.shape == (1, self.n_var))

        # NIW dist mean fraction, should be at greater that 0, but numerically you can set it to 
        #zero and it should be fine, default is 1
        self._kappa_0 = kappa_0 

        # degrees of freedom, set it to the dimensionality of the data + 2
        self._nu_0 = nu_0 
        if self._nu_0 < self.n_var:
            self._nu_0 = self.n_var+2

        # NIW covariance matrix, initial for the cluster, should be input as the e.g. the typical 
        # covariance matrix of the data really. 
        if Psi_0 == None:
            self._Psi_0 = 10*np.eye(self.n_var) # TODO this 10 factor should be a prior, ~ dependent on the mean distance between points of the dataset
        else:
            self._Psi_0 = Psi_0
        assert(self._Psi_0.shape == (self.n_var, self.n_var))

        if X.shape[0] > 0:
            self.fit(X,sig_X)
        else:
            self.default()


    def default(self):
        self.mean  = np.matrix(np.zeros((1, self.n_var)))  # TODO init to mean of the dataset
        self.covar = 100.0 * np.matrix(np.eye(self.n_var)) # TODO change 100


    def recompute_params(self):
        """ need to have initial _X, _sum """ 
        self.n_points = self._X.shape[0]
        self.n_var    = self._X.shape[1]
        if self.n_points <= 0:
            self.default()
            return
        kappa_n = self._kappa_0 + self.n_points
        nu      = self._nu_0    + self.n_points 
        ##set the data mean as the variance weighted mean
        mu      = np.matrix(self._sum/self._sum_isig2)
        mu_mu_0 = mu - self._mu_0 ##data mean - mean_0 parameter


        #x_mu    = np.matrix(self._X) - mu  #slow because it makes a new array, and now deprecated
        #big_S   = np.dot(x_mu.transpose(),x_mu) #deprecated into next line for speed
        ##following line deprecated into self.covar calculation for speed because it makes an unnessecary array
        #Psi     = (self._Psi_0 + np.dot(np.transpose(self._X-mu),self._X-mu) + self._kappa_0*self.n_points*mu_mu_0.transpose()*mu_mu_0/(self._kappa_0 + self.n_points))

        ##normal inverse Wishart priored mean for the particular Gaussian component
        self.mean  = ((self._kappa_0*self._mu_0 + self.n_points*mu)/(self._kappa_0 + self.n_points))
        ##normal inverse Wishart priored covar for the particular Gaussian component
        self.covar = ((self._Psi_0 + np.dot((np.matrix(self._X)-mu).transpose(),np.matrix(self._X)-mu) + self._kappa_0*self.n_points*mu_mu_0.transpose()*mu_mu_0/(self._kappa_0 + self.n_points))
 * (kappa_n + 1)) / (kappa_n*(nu - self.n_var + 1))
       
        ##calculate everything without errors for testing
        #normal_mu = np.matrix(self._X.sum(0)/self.n_points)
        #nxmu = np.matrix(self._X)-normal_mu
        #nmmu0 = normal_mu-self._mu_0
        #normcovar = ((self._Psi_0 + np.dot(nxmu.transpose(),nxmu) + self._kappa_0*self.n_points*nmmu0.transpose()*nmmu0/(self._kappa_0 + self.n_points))* (kappa_n + 1)) / (kappa_n*(nu - self.n_var + 1))
       # pdb.set_trace()
        assert(np.linalg.det(self.covar) != 0) ##pretty fast
        #pre-deprecation self.covar line
        #self.covar = (Psi * (kappa_n + 1)) / (kappa_n*(nu - self.n_var + 1))
        
    def inv_covar(self):
        """ memoize the inverse of the covariance matrix """
        if self._hash_covar != hash(self.covar):
            self._hash_covar = hash(self.covar)
            self._inv_covar  = np.linalg.inv(self.covar)
        return self._inv_covar


    def fit(self, X, sig_X):
        """ to add several points at once without recomputing """
        #pdb.set_trace()
        self._X         = X
        self._sig_X     = sig_X
        self._sum       = (X/np.diagonal(self._sig_X,axis1=1,axis2=2)).sum(0)
        self._sum_isig2 = (1/np.diagonal(self._sig_X,axis1=1,axis2=2)).sum(0)
       # pdb.set_trace()
        self.recompute_params()
    
    
    
    def add_point(self, x,sig_x):
        """ add a point to this Gaussian cluster """
        #If this cluster is totally empty (it's a new one), add and initialise
        if self.n_points <= 0:
            self._X           = np.array([x])
            self._sig_X       = np.array([sig_x])
            self._sum         = (self._X/np.diagonal(self._sig_X,axis1=1,axis2=2)).sum(0)
            self._sum_isig2   = 1/np.diagonal(self._sig_X,axis1=1,axis2=2)
        #If this cluster already exists and has other members, add and recompute properties
        else:
            self._X           = np.append(self._X, [x], axis=0)
            self._sig_X       = np.append(self._sig_X,[sig_x],axis=0)
            self._sum        += x/np.diagonal(sig_x)
            self._sum_isig2  += 1/np.diagonal(sig_x)
        #pdb.set_trace()
        self.recompute_params()


    def rm_point(self, x):
        """ remove a point from this Gaussian cluster """
        assert(self._X.shape[0] > 0)
        # Find the indice of the point x in self._X
        #dist              = np.sqrt(np.sum(np.square(self._X-x),axis=1)) ##slowest line, deprecated now
        #spot              = np.argmin(dist) ##as above
        spot = np.argmin(((self._X-x)**2).sum(1)) 
        #tmp               = np.matrix(self._X[spot])
        ##order here is important
        #pdb.set_trace()
       
        self._sum        -= self._X[spot]/np.diagonal(self._sig_X[[spot]],axis1=1,axis2=2)[0]
        self._sum_isig2  -= 1/np.diagonal(self._sig_X[[spot]],axis1=1,axis2=2)[0]
        self._X           = np.delete(self._X, spot, axis=0)
        self._sig_X       = np.delete(self._sig_X, spot, axis=0)                
        self.recompute_params()

        
        
    def pdf(self, x, sig_x):
        """ probability density function for a multivariate Gaussian """
        size = len(x)
        assert(size == self.mean.shape[1])
        assert((size, size) == self.covar.shape)
        ##define the effective covariance matrix as the sum of the data and distribution covariances
        thiscov = sig_x + self.covar
        det     = np.linalg.det(thiscov)
        #if det < 0.00001: pdb.set_trace()
        #thisicov = thiscov.I
        assert(det != 0)
        norm_const = 1.0/((2*np.pi)**(size/2.0)*det**0.5)
        x_mu       = x - self.mean
        result     = np.e**(-0.5 * (x_mu * thiscov.I * x_mu.transpose())[0,0])
       # pdb.set_trace()
        return norm_const*result



"""
Dirichlet process mixture model (for N observations y_1, ..., y_N)
    1) generate a distribution G ~ DP(G_0, α)
    2) generate parameters θ_1, ..., θ_N ~ G
    [1+2) <=> (with B_1, ..., B_N a measurable partition of the set for which 
        G_0 is a finite measure, G(B_i) = θ_i:)
       generate G(B_1), ..., G(B_N) ~ Dirichlet(αG_0(B_1), ..., αG_0(B_N)]
    3) generate each datapoint y_i ~ F(θ_i)
Now, an alternative is:
    1) generate a vector β ~ Stick(1, α) (<=> GEM(1, α))
    2) generate cluster assignments c_i ~ Categorical(β) (gives K clusters)
    3) generate parameters Φ_1, ...,Φ_K ~ G_0
    4) generate each datapoint y_i ~ F(Φ_{c_i})
    for instance F is a Gaussian and Φ_c = (mean_c, var_c)
Another one is:
    1) generate cluster assignments c_1, ..., c_N ~ CRP(N, α) (K clusters)
    2) generate parameters Φ_1, ...,Φ_K ~ G_0
    3) generate each datapoint y_i ~ F(Φ_{c_i})
So we have P(y | Φ_{1:K}, β_{1:K}) = \sum_{j=1}^K β_j Norm(y | μ_j, S_j)
"""
class DPMM:
    ##a function to ask each Gaussian class what it's mean is
    def _get_means(self):
        return np.array([g.mean for g in self.params.itervalues()])

    ##a function to ask each Gaussian class what it's covar is
    def _get_covars(self):
        return np.array([g.covar for g in self.params.itervalues()])

    ##initialise lots of variables
    def __init__(self, n_components=-1, alpha=1.0, epsilon=1e-5, max_iter=1000):
        self.params       = {0: Gaussian()}
        self.n_components = n_components
        self.means_       = self._get_means()
        self.alpha        = alpha*1.0
        self.epsilon      = epsilon*1.0
        self.max_iter     = max_iter
        self.iterations_  = () ##a place foe saving iteration states if wanted, will always at least save the initial state (later)
        
    def fit_collapsed_Gibbs(self, X, sig_X, assigns=0,save_iterations=False):
        #Algorithm 3 of collapsed Gibss sampling in Neal 2000:
        #http://www.stat.purdue.edu/~rdutta/24.PDF
        
        ##mean and spread of the data, used as the prior_predictive for the new clusters
        mean_data    = np.matrix(np.average(X,axis=0,weights=1/np.diagonal(sig_X,axis1=1,axis2=2)))
        spread_data  = np.cov(X,rowvar=0)
        ##the median covariance of the data.
        cov_data     = np.median(sig_X,axis=0)
        #spread_data = cov_data
        dimensionality = X.shape[1] 
        self.epsilon = self.epsilon*dimensionality
        scale_matrix  = spread_data
        self.n_points = X.shape[0]
        self.n_var = X.shape[1]
        self._X = X
        self._sig_X = sig_X
        if self.n_components == -1:
            # initialize with 1 cluster for each datapoint
            self.params         = dict([(i, Gaussian(X=np.array(X[[i]]), sig_X=sig_X[[i]], mu_0=mean_data,Psi_0=scale_matrix)) for i in xrange(X.shape[0])])
            self.z              = dict([(i,i) for i in range(X.shape[0])])
            self.n_components   = X.shape[0]
            previous_means      = 2*self._get_means() ##multiplied by 2 to get the thing started later
            previous_components = self.n_components
        if self.n_components == -2:
            if len(assigns) != len(X): pdb.set_trace()
            ##set the first group number to 0 regardless of input, just for niceness
            assigns           = assigns-min(assigns)
            incluster         = np.unique(assigns)
            self.n_components = len(incluster)
            cls = ()
            for incnum in incluster:
                thiscluster = np.where(assigns == incnum)[0]
                cls         = cls + (thiscluster,)
            #pdb.set_trace()
            ##what if only one thing in a group, indexing will be wrong?, test this later....
            self.params         = dict([(i, Gaussian(X=np.array(X[grp]),sig_X=np.array(sig_X[grp]), mu_0=mean_data,Psi_0=scale_matrix)) for i,grp in enumerate(cls)]) 
            self.z              = dict([(i,j) for i,j in enumerate(assigns)])
            previous_means      = 2*self._get_means() ##multiplied by 2 to get the thing started later
            previous_components = self.n_components

        ##save the starting configuration, whatever it is:
        #pdb.set_trace()
        iterations_save = [self.predict(X)]
        print self.epsilon
        print "Initialized collapsed Gibbs sampling with %i clusters" % (self.n_components)
        n_iter   = 0 # with max_iter hard limit, in case of cluster oscillates
        
        # while the clustering hasn't convereged, continue gibbs sampling loop
        while (n_iter < self.max_iter 
                and (previous_components != self.n_components
                or abs((previous_means - self._get_means()).sum()) > self.epsilon*self.n_components)):
            n_iter += 1
            previous_means      = self._get_means()
            previous_components = self.n_components

            start_itime = time.time()
            for i in xrange(X.shape[0]):
                #remove X[i]'s sufficient statistics from z[i]
                self.params[self.z[i]].rm_point(X[i])
                #if it empties the cluster, remove it and decrease K
                if self.params[self.z[i]].n_points <= 0:
                    self.params.pop(self.z[i])
                    self.n_components -= 1
                
                ##a place to store the prob of putting X[i] in each cluster
                tmp = np.zeros(self.n_components+1)
                ##This is the CRP calculation
                for k, param in enumerate(self.params.values()):
                    #compute P_k(X[i]) = P(X[i] | X[-i] = k)
                    marginal_likelihood_Xi = param.pdf(X[i],sig_X[i])
                    #set N_{k,-i} = dim({X[-i] = k})
                    #compute P(z[i] = k | z[-i], Data) = N_{k,-i}/(α+N-1)
                    mixing_Xi = param.n_points / (self.alpha + self.n_points - 1)
#                    tmp.append(marginal_likelihood_Xi*mixing_Xi)
                #    pdb.set_trace()
                    tmp[k] = marginal_likelihood_Xi*mixing_Xi
                #pdb.set_trace()
                ##This is the CRP new cluster probability
                # compute P*(X[i]) = P(X[i]|λ)
                ##use the spread and mean of the data as the base distribution
                thiscov    = np.matrix(sig_X[i] + scale_matrix/(dimensionality+2 - dimensionality-1))
                det        = np.linalg.det(thiscov)
                assert(det != 0)
                norm_const = 1.0/((2*np.pi)**(X.shape[1]/2.0)*det**0.5)
                x_mu       = X[i] - mean_data
                prior_predictive     = np.e**(-0.5 * (x_mu * thiscov.I * x_mu.transpose())[0,0])*norm_const
                # compute P(z[i] = * | z[-i], Data) = α/(α+N-1)
            #    pdb.set_trace()
                prob_new_cluster = self.alpha/(self.alpha + self.n_points - 1)
                tmp[-1] = prior_predictive*prob_new_cluster
                #tmp.append(prior_predictive*prob_new_cluster)
               # pdb.set_trace()
                
                # normalize P(z[i]) (tmp above)
                tmp = tmp/tmp.sum()
               #s = sum(tmp)
               #tmp = map(lambda e: e/s, tmp)



                # sample z[i] ~ P(z[i])
                #this is the gibbs sampling bit
                rdm   = np.random.rand()
                total = tmp[0]
                k     = 0
                while (rdm > total):
                    k     += 1
                    total += tmp[k]
                # add X[i]'s sufficient statistics to cluster z[i]
                new_key    = max(self.params.keys()) + 1
                if k == self.n_components: # create a new cluster
                    self.z[i]            = new_key
                    self.n_components   += 1
                    self.params[new_key] = Gaussian(X=np.array(X[[i]]),sig_X = np.array(sig_X[[i]]))
                else:
                    self.z[i] = self.params.keys()[k]
                    self.params[self.params.keys()[k]].add_point(X[i],sig_X[i])
                assert(k < self.n_components)

                ##some time testing
                #timegibbs = time.time()-time2
                #timestar = time.time()-s_time
               
               ##print visual progress every thousand datapoints
               #print "up to datapoint " + str(i)+" out of "+str(X.shape[0])
                if np.mod(i+1,1000)==0: print "up to datapoint " + str(i+1)+" out of "+str(X.shape[0])



            ##report some times, for fun
            iter_time = (time.time()-start_itime)/60.0
            print "iteration " +str(n_iter)+ " still sampling, %i clusters currently, with log-likelihood %f" % (self.n_components, self.log_likelihood())
            print "iteration time " + str(iter_time) + " mins"

            ##report the reason for stopping, if stopping occurs
            if n_iter >= self.max_iter: print "!!!HIT ITERATION LIMIT!!!"
            if (previous_components == self.n_components
                and abs((previous_means - self._get_means()).sum()) <= self.epsilon*self.n_components): print "!!! HIT TOLERANCE FOR CONVERGENCE!!!"

            #pdb.set_trace()
            ##if we are saving cluster assignments:
            if save_iterations == True:
                iterations_save.append(self.predict(X))
                
        ##store the final means after converstion
        self.means_ = self._get_means()
        ##store the iterations somewher e they can be grabbed later, always has at least the starting config.
        self.iterations_ = iterations_save

    def predict(self, X):
        """ produces and returns the clustering of the data """
        pdb.set_trace()
        if (X != self._X).any(): ##this line doesn't work btw
            self.fit_collapsed_Gibbs(X)
        #map the cluster id's onto numbers starting from 0
        #mapper = list(set(self.z.values()))
        ##this maps in the order that the cluster means are in 
        mapper = self.params.keys()
        Y      = np.array([mapper.index(self.z[i]) for i in range(X.shape[0])])
        return Y


    def log_likelihood(self):
        # TODO test the values 
        log_likelihood = 0.
        
        #the loop is quite slow, there is a faster way.....but it really doesn't matter
        for n in xrange(self.n_points):
           # pdb.set_trace()
            thiscov = self.params[self.z[n]].covar + self._sig_X[n]
            thisicov = thiscov.I
            dp_prob = self.params[self.z[n]].n_points/(self.params[self.z[n]].n_points+self.alpha-1)
            log_likelihood -= (0.5 * self.n_var * np.log(2.0 * np.pi) + 0.5*np.log(np.linalg.det(thiscov)))
            mean_var        = np.matrix(self._X[n, :] - self.params[self.z[n]]._X.mean(axis=0)) # TODO should compute self.params[self.z[n]]._X.mean(axis=0) less often
            assert(mean_var.shape == (1, self.params[self.z[n]].n_var))
            log_likelihood -= 0.5 * np.dot(np.dot(mean_var, 
                thisicov), mean_var.transpose())
            log_likelihood += np.log(dp_prob)
        return log_likelihood          



# Number of samples per component
#n_samples = 100

# Generate random sample, two components
#np.random.seed(0)

# 2, 2-dimensional Gaussians
#C = np.array([[0., -0.1], [1.7, .4]])
#X = np.r_[np.dot(np.random.randn(n_samples, 2), C)+np.array([-1,4]),.7 * np.random.randn(n_samples, 2) + np.array([-6, 3])]

#sig_X = X*0.0+0.01
#inassigns = np.zeros(2*n_samples,int)
#inassigns[0:n_samples]= 3
#inassigns[n_samples:] = 2
#pdb.set_trace()
#mns1 = np.array([-0.0,-0.0])
#cov1 = np.array([[20.0,0.0],[0.0,20.0]])
#mns2 = np.array([3.0,3.0])
#cov2 = np.array([[1.0,0.0],[0.0,1.0]])

#X1 = np.random.multivariate_normal(mns1,cov1,200)
#X2 = np.random.multivariate_normal(mns2,cov2,200)
#pdb.set_trace()
#X = np.concatenate((X1,X2),axis=0)
#pdb.set_trace()
#pdb.set_trace()
#read in my own version of the data
#X         = pickle.load(open("outputs/slink_data_random_g6.pkl","rb"))
#sig_X     = np.absolute(pickle.load(open("outputs/slink_sigdata_random_g6.pkl","rb")))
#inassigns = pickle.load(open("outputs/slink_assignments_random_g6.pkl","rb"))

#pick = np.where((inassigns == 2) | (inassigns == 3))[0]
#pick = np.where(inassigns != 0)[0]
#pick = np.where(inassigns == 5)[0]
#X = X[pick]
#sig_X = sig_X[pick]
#inassigns = inassigns[pick]
#sigmat = np.zeros((X.shape[0],X.shape[1],X.shape[1],))
#for i in range(0,len(X)):
#    sigmat[i] = np.eye(X.shape[1])*sig_X[i]**2
##sig_X = sigmat
#X[20,3] = 15.
#pdb.set_trace()

#ids   = pickle.load(open("betapic_ids.pkl","rb"))
#X     = pickle.load(open("betapic_posvel.pkl","rb"))
#sig_X = pickle.load(open("betapic_cposvel.pkl","rb"))
#inassigns = np.zeros(X.shape[0])

#X = X[:,3:]
#sig_X = sig_X[:,3:,3:]



#inassigns = np.zeros(X.shape[0])
#inassigns[4]   = 1
#inassigns[11]  = 1
# inassigns[15]  = 1
# inassigns[1:4] = 2
# inassigns[7]   = 2
# inassigns = np.array([1, 2, 2, 2, 3, 4, 4, 4, 4, 4, 6, 5, 6, 6, 6, 6, 6, 6, 8, 8, 7, 8])
# 
# pdb.set_trace()
# dpmm = DPMM(n_components=-2,epsilon=1e-10,max_iter=1000,alpha=1) # -1, 1, 2, 5
# # n_components is the number of initial clusters (at random)
# # -1 means that we initialize with 1 cluster per point
# # -2 means initialize with an input clustering
# dpmm.fit_collapsed_Gibbs(X,sig_X,assigns=inassigns,save_iterations=True)
# 
# ##what is the chain like?
# cluster_nums = np.zeros(len(dpmm.iterations_),'int')
# its = dpmm.iterations_
# for i in range(0,len(its)): cluster_nums[i] = len(np.unique(its[i]))
# 
# 
# assigns = dpmm.predict(X)
# means   = dpmm.means_
# covars  = dpmm._get_covars()
# 
# pdb.set_trace()
# 
# for i in range(0,means.shape[0]):
# 	tc = np.where(assigns == i)[0]
# 	plt.plot(X[tc,0],X[tc,4],'.')	
# plt.savefig("outputs/dpgmm04.png")
# plt.clf()
# 
# for i in range(0,means.shape[0]):
# 	tc = np.where(assigns == i)[0]
# 	plt.plot(X[tc,3],X[tc,4],'.')	
# plt.savefig("outputs/dpgmm34.png")
# plt.clf()
# 
# for i in range(0,means.shape[0]):
# 	tc = np.where(assigns == i)[0]
# 	plt.plot(X[tc,4],X[tc,5],'.')	
# plt.savefig("outputs/dpgmm35.png")
# plt.clf()
# 
# for i in range(0,means.shape[0]):
# 	tc = np.where(assigns == i)[0]
# 	plt.plot(X[tc,2],X[tc,5],'.')	
# plt.savefig("outputs/dpgmm25.png")
# plt.clf()