##simple calculation of the Mahalanobis distance between two point-vectors relative to the parent 
##distribution covariance matrix inverse S_inverse
##should be completely invariant to the scales/units of the dataset dimensions.
##S_inverse doesn't need to actually be and np.matrix it can also be an np.array

import numpy as np
import pdb

def mahalanobis_dist(vect1,vect2,S_inverse,inverted=True):
	if inverted == False: S_inverse = np.linalg.inv(S_inverse)
	d_vect = vect1-vect2
	mdist = np.sqrt(np.dot(np.dot(d_vect, S_inverse), d_vect))
	return mdist

##this version calculates the distance between point-vector vect1 and an array of 
##point-vectors vect2
def mahalanobis_dist_vect(vect1,vect2,S_inverse,inverted=True):
	if inverted == False: S_inverse = np.linalg.inv(S_inverse)
	d_vect = vect2 - vect1
	#pdb.set_trace()
	mdist = np.sqrt(np.sum((np.dot(d_vect, S_inverse)*d_vect),axis=1))
	return mdist