dat= read_csv('betaPic2.csv')

rr = dat.pmra/3600.0*40*10
dd = dat.pmdec/3600.0*40*10

set_plot,'ps'
!p.font=0
plotsym,0,1.5,/fill
device,/helvetica,/encapsulated,/color,filename = 'BetaPic_arrows.eps'
plot, dat.radeg,dat.dedeg,psym=8,yr=[-90,40],charsize=1.2,charthick=1.2,xtitle = 'R.A. (degrees)', ytitle = 'Decl. (degrees)',thick=5,xthick=5,ythick=5
arrow,dat.radeg,dat.dedeg,dat.radeg+rr,dat.dedeg+dd,/data,/solid,hsize=300,thick=5
arrow,[300.],[20.],[300],[20.-100/3600.0*40*10],/data,/solid,thick=5,hsize=300
xyouts,301.5,17,'100 mas',charsize=1.2,charthick=1.2

device,/close
stop
end
