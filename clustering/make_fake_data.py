import numpy as np
import pdb
import emcee
import time
import pickle
import os
import sys
import triangle

from gal_xyz import gal_xyz
from ACRastro.gal_uvw import gal_uvw
from ACRastro.glactc import glactc
import matplotlib.pyplot as plt
##add the bayes code directory to the path
sys.path.insert(0, '/Users/arizz/python/BAFGKM')
from sample_generation import build_uvw_field
from uvwtopm import uvwtopm
from full_uvw_to_observable import gal_to_obs_full
from sample_generation import build_sample_group


##start with the field
##better uncertainties should be of order 0.01 mas
makefield = 0
if makefield == 1:
    nfsamp = 500000
    ra   = (240.,290.,)
    dec  = (-0.,-50.,)
    dist = (10.,1000.,)
    uvw_field = build_uvw_field(nfsamp)
##randomly distribute them over the ra/dec window, and give them distances from a 
##cubic power law
    ra_field = np.random.uniform(ra[0],ra[1],nfsamp)
    de_field = np.random.uniform(dec[0],dec[1],nfsamp)
    d_field  = np.random.power(3,size=nfsamp)*(dist[1]-dist[0])+dist[0]
    plx_field= 1000.0/d_field
    sig_plx_field = np.random.normal(0.1,0.03,size=nfsamp)
    bad = np.where(sig_plx_field < 0.01)
    sig_plx_field[bad] = np.mean(sig_plx_field)

##now convert things to observable space, pretending RV's are not available
    pmvect = uvwtopm(uvw_field,ra_field*np.pi/180,de_field*np.pi/180,plx_field)
    pmra_field = pmvect[:,1]
    pmde_field = pmvect[:,2]
    sig_pmra_field = np.random.normal(0.1,0.03,size=nfsamp)
    bad = np.where(sig_plx_field < 0.01)
    sig_pmra_field[bad] = np.mean(sig_plx_field)

    sig_ra_field = np.random.normal(0.1,0.03,size=nfsamp)
    bad = np.where(sig_plx_field < 0.01)
    sig_pmra_field[bad] = np.mean(sig_plx_field)

    field = np.array([ra_field,de_field,plx_field,pmra_field,pmde_field])
    sig_field = np.array([sig_ra_field,sig_ra_field,sig_plx_field,sig_pmra_field,sig_pmra_field])

##output the fake data
    outname = "synth_data/field_"+str(nfsamp)+"_"+str(ra[0])+"_"+str(ra[1])+"_"+str(dec[0])+"_"+str(dec[1])
    field_output = (field,sig_field,)
    pickle.dump(field_output, open(outname+".pkl", "wb" ))

    pdb.set_trace()
    print "Im here"

makegroup=1
if makegroup == 1:
    nsamp = 100
    #ra    = (240.,290.)
    #dec   = (-0.,-50.)
   # pdb.set_trace()
    ##this is equivalent xyz to 260ra, -25dec,500pc, with ~USco UVEW
    #posvel = np.array([-496.35,1.79,60.32,6.5,-16.0,-7.5])
    ##this is equivalent xyz to 260ra, -25dec,200pc, with ~USco uvw
    #posvel  = np.array([-198.54,0.72,24.13,6.5,-16.0,-7.5])
    #pdb.set_trace()
    ##equivalent to xyz to 280ra, -45dec,800pc, with ~USco uvw
    #posvel = np.array([-754.89,-126.21,-232.83,6.5,-16.0,-7.5])
    ##equivalent to xyz to 240ra, 0dec,800pc, with ~USco uvw
    #pdb.set_trace()
    #posvel = np.array([-705.47,124.36,544.84,6.5,-16.0,-7.5])
    #pdb.set_trace()
    ##equivalent to xyz to 280ra, -27dec,100pc, with ~USco uvw
    posvel = np.array([-97.76,12.77,-16.75,6.5,-16.0,-7.5])
   
    ##equivalent to xyz to 260ra, -40dec,50pc, with ~USco uvw
    #posvel = np.array([-48.86,-10.51,-1.41,6.5,-16.0,-7.5])

    cposvel = np.array([[10.,0.,0.,0.,0.,0.],[0.,10.,0.,0.,0.,0.],[0.,0.,10.,0.,0.,0.],[0.,0.,0.,2.,0.,0.],[0.,0.,0.,0.,2.,0.],[0.,0.,0.,0.,0.,2.]])
    
    
    
    means, covs = gal_to_obs_full(posvel,cposvel)
    
    sample = np.random.multivariate_normal(means,covs,nsamp)
    sample = sample[:,0:5]
    sig_plx = np.random.normal(0.1,0.03,size=nsamp)
    bad = np.where(sig_plx < 0.01)
    sig_plx[bad] = np.mean(sig_plx)
    
    sig_pmra = np.random.normal(0.1,0.03,size=nsamp)
    bad = np.where(sig_plx < 0.01)
    sig_pmra[bad] = np.mean(sig_plx)

    sig_ra = np.random.normal(0.1,0.03,size=nsamp)
    bad = np.where(sig_plx < 0.01)
    sig_pmra[bad] = np.mean(sig_plx)
 
    sig_sample=np.array([sig_ra,sig_ra,sig_plx,sig_pmra,sig_pmra])
    output = (sample,sig_sample,)

    outname = "synth_data/Group_"+str(nsamp)+"_"+str(means[0])+"_"+str(means[1])+"_"+str(0.5)
    pickle.dump(output, open(outname+".pkl", "wb" ))
    
    
    pdb.set_trace()
    print "Im here"








