#basics
import numpy as np
import pickle 
import pdb
import matplotlib.pyplot as plt
import matplotlib
##my imports
from mahalanobis import mahalanobis_dist
from mahalanobis import mahalanobis_dist_vect
from SLINK import SLINK
from SLINK import assign_clusters
from SLINK import SLINK_plot
from SLINK import cluster_pops
from SLINK import pointer_to_packed
from ACRastro.glactc import glactc
from clusterDPGMM_covar import DPMM

import scipy.cluster.hierarchy as sch

#stop the crazy outputs
np.set_printoptions(suppress=True,precision=3)

# hip = pickle.load(open("../hip_main.pkl","rb"))
outstr = "hipsco"

##turn the whole catalog into a clusterable dataset

# X = np.transpose(np.array([hip.ra*180/np.pi,hip.dec*180/np.pi,hip.plx,hip.pmra,hip.pmdec]))
# var_X = np.zeros([len(hip),5,5])
# ids  = np.array(hip.hip)
# 
# for i in range(0,len(hip.ra)):
#     var_X[i] = np.eye(5)*np.array([1e-7,1e-7,hip.sig_plx[i]**2,hip.sig_pmra[i]**2,hip.sig_pmdec[i]**2])
    #pdb.set_trace()
#     
# S_mat     = np.median(var_X,axis=0)
##things a 5 degrees apart are so called close
#S_mat[0,0]=25.0
#S_mat[1,1]=25.0
S_mat = np.array([[0.1,0.0,0.0,0.0,0.],[0.,0.1,0.,0.,0.],[0.,0.,0.01,0,0],[0,0,0,0.01,0],[0,0,0,0,0.01]])
S_inverse = np.linalg.inv(S_mat)
# 
# gl,gb     = glactc(X[:,0],X[:,1],2000,degree=True,fk4=True)
# 
# urange = np.where((gl > 280) & (gb > -10) & (gb < 40) & (X[:,2]>0.1))[0]
# X = X[urange]
# var_X = var_X[urange]
# ids = ids[urange]
# 
# # #pdb.set_trace()
# mPI,mL = SLINK(X,mahalanobis_dist_vect,S_inverse)
# mL[-1] = max((max(mL[:-1]+1),1000))

outfile0 = "outputs/slink_X_" + outstr+".pkl"
outfile1 = "outputs/slink_mPI_"+outstr+".pkl"
outfile2 = "outputs/slink_mL_"+outstr+".pkl"
outfile3 = "outputs/slink_mZ_"+outstr+".pkl"
outfile4 = "outputs/slink_varX_"+outstr+".pkl"
# 
# pickle.dump(X,     open( outfile0, "wb" ))
# pickle.dump(mPI,   open( outfile1, "wb" ))
# pickle.dump(mL,    open( outfile2, "wb" ))
# pickle.dump(var_X, open( outfile4, "wb" ))
# pickle.dump(ids,open("outputs/slink_ids_"+outstr+".pkl","wb"))
# pdb.set_trace()

##Load a run for inspection, rather than redoing it.
mPI  = pickle.load(open(outfile1,"rb"))
mL   = pickle.load(open(outfile2,"rb"))
X    = pickle.load(open(outfile0,"rb"))
var_X= pickle.load(open(outfile4,"rb"))
ids  = pickle.load(open("outputs/slink_ids_"+outstr+".pkl","rb"))
#S_mat     = np.median(var_X,axis=0)
#S_inverse = np.linalg.inv(S_mat)
gl,gb     = glactc(X[:,0],X[:,1],2000,degree=True,fk4=False)
##convert to the packed representation for plots and inspections
mZ = pointer_to_packed(mPI,mL)
pickle.dump(mZ,  open( outfile3, "wb" ))

pdb.set_trace()
##after this everything else is ~inspection sort of

#pdb.set_trace()
print "up to post processing"
##cut in steps of 2 "sigma", from 100 do
cutlim = 100
cut = np.arange(cutlim*2)/2.+1
#pdb.set_trace()
#pdb.set_trace()
##define group max and min limits
gmin = 50
gmax = 1000


groups = ()
gmeans = ()
gnums  = ()
cutlevel = ()
for thiscut in cut:
    print 'cutting at level ' + str(thiscut)
    assigns = assign_clusters(mPI,mL,thiscut)
    cmem    = cluster_pops(assigns,maxgroup=gmax,mingroup=gmin)
    ok      = np.where(cmem > 0)[0]
    for j in ok:
        ggg       = (np.where(assigns == j+1)[0],)
        groups    = groups+ggg
        gnums     = gnums+(len(ggg[0]),)
        cutlevel = cutlevel+(thiscut,)
        #pdb.set_trace()
        thismeans = np.mean(X[ggg[0],:],axis=0)
        gmeans    = gmeans + (thismeans,)  
    #pdb.set_trace()
gmeans = np.asarray(gmeans)
gnums = np.asarray(gnums)
cutlevel = np.asarray(cutlevel)
##now sort through the means and figure out which are different and which are not
gtype = np.zeros(len(gmeans),int)
current_number = 0
numsig = 20.0
#pdb.set_trace()
for i in range(len(gmeans)):
    if gtype[i] == 0:
        current_number += 1
        ddd = mahalanobis_dist_vect(gmeans[i,:],gmeans,S_inverse)
        same = np.where(ddd <= numsig)[0]
        gtype[same] = current_number

##now condense "similar groups" into individual groups and make 
##a cut on the number of C levels a group appears in
unique_groups = np.unique(gtype)
uselist = ()
grp = ()
grp_mult = ()
gmult_cut = 1
for thisgtype in unique_groups:
    thisblock = np.where(gtype == thisgtype)[0]
    if len(thisblock) > gmult_cut:
        cutdiff   = np.absolute(cutlevel[thisblock]-np.ceil(np.max(cutlevel[thisblock])))
        usethis   = np.where(cutdiff == min(cutdiff))[0][0]
        uselist   = uselist+(thisblock[usethis],)
        grp_mult = grp_mult + (len(thisblock),)
        gg = groups[thisblock[0]]
        #grp       = grp+(groups[thisblock[usethis]],)
        if len(thisblock) > 1:
            #pdb.set_trace()
            for ggg in thisblock[1:]: gg = np.concatenate((gg,groups[ggg])) 
            grp = grp+(np.unique(gg),)
        else: grp = grp +(groups[thisblock[usethis]],)
     #   pdb.set_trace()
     #   grp       = grp+(groups[thisblock[usethis]],)


plxcut = 3.0
#pdb.set_trace()
uselist = np.asarray(uselist)
pc      = np.where(gmeans[uselist,2]>plxcut)[0]
uselist = uselist[pc]
grp = np.asarray(grp)[pc]
gmult   = np.asarray(grp_mult)[pc]
gm  = gmeans[uselist,:]
gn  = gnums[uselist]
clv = cutlevel[uselist]


#pdb.set_trace()
##now build an array of group assignments to output
outassigns = np.zeros(X.shape[0],int)
for gid, gg in enumerate(grp): 
    outassigns[gg] = gid+1
gts = np.unique(outassigns[np.where(outassigns != 0)[0]])
for i in range(len(gts)):
    qwe = np.where(outassigns == gts[i])[0]
    outassigns[qwe] = i+1
pdb.set_trace()


dummy = outfile0.split('.pkl')
newoutfile = dummy[0] + 'assignments' +".pkl"
pickle.dump(outassigns,open(newoutfile, "wb" ))

plt.clf()

au = X[:,3]/3600.0*40.0*10.0
av = X[:,4]/3600.0*40.0*10.0
ugl,ugb = glactc(X[:,0]+au,X[:,1]+av,2000,degree=True,fk4=False)
ugl = ugl-gl
ugb = ugb-gb
pdb.set_trace()
ooo = np.argsort(clv)[::-1]
plt.plot(gl,gb,',k')
plt.ylim([-10,30])
for gg in grp[ooo]: plt.plot(gl[gg],gb[gg],'o')
for gg in grp[ooo]: plt.quiver(gl[gg],gb[gg],ugl[gg],ugb[gg],scale=1,units='xy',width=0.15,headlength=3,headwidth=3)
plt.xlabel('l  (degrees)')
plt.ylabel('b (degrees)')
plt.title('Automatic Heirarchical Group Finding')
plt.savefig('SLINK_output_sky_' + outstr)
plt.clf()
plt.plot(X[:,3],X[:,4],',')
for gg in grp[ooo]: plt.plot(X[gg,3],X[gg,4],'.')
plt.xlabel('pmra (mas/yr)')
plt.ylabel('pmdec (mas/yr)')
plt.savefig('SLINK_output_pm_'+outstr)
plt.clf()
plt.plot(X[:,2],X[:,3],',')
for gg in grp[ooo]: plt.plot(X[gg,2],X[gg,3],'.')
plt.xlabel('Parallax (mas)')
plt.ylabel('pmra (mas/yr)')
plt.savefig('SLINK_output_dpm_'+outstr)
plt.clf()

##and zoomed in a bit
plt.plot(X[:,3],X[:,4],',')
for gg in grp[ooo]: plt.plot(X[gg,3],X[gg,4],'.')
plt.xlim([-50,50])
plt.ylim([-50,50])
plt.xlabel('pmra (mas/yr)')
plt.ylabel('pmdec (mas/yr)')
plt.savefig('SLINK_output_pm_zoom_'+outstr)
plt.clf()
plt.plot(X[:,2],X[:,3],',')
for gg in grp[ooo]: plt.plot(X[gg,2],X[gg,3],'.')
plt.ylim([-50,20])
plt.xlim([1,20])
plt.xlabel('Parallax (mas)')
plt.ylabel('pmra (mas/yr)')
plt.savefig('SLINK_output_dpm_zoom_'+outstr)
plt.clf()

pdb.set_trace()
run = np.where(outassigns != 0)[0]
#dpmm = DPMM(n_components=-2,epsilon=0.01,max_iter=1000,alpha=1)
#dpmm.fit_collapsed_Gibbs(X[run],var_X[run],assigns=outassigns[run],save_iterations=False)
pdb.set_trace()


#assigns = dpmm.predict(X[run])
#means   = dpmm.means_
#covars  = dpmm._get_covars()

#fin = SLINK_plot(X[run],assigns+1,0,1,maxgroup=10000,mingroup=5)
#pickle.dump(assigns,open("slink-dpmm_hipsco_assigns.pkl","wb"))
assigns = pickle.load(open("slink-dpmm_hipsco_assigns.pkl","rb"))
aaa = np.unique(assigns)
bbb = np.where(((gl < 284.6) | (gl > 296) | (gb > 27.5) | (gb < 24.6)))[0]
plt.plot(gl[bbb],gb[bbb],',k')
plt.ylim([-10,30])
for gg in aaa: 
    this = np.where(assigns == gg)
    plt.plot(gl[run[this]],gb[run[this]],'o')    
qqq = plt.quiver(gl[run],gb[run],ugl[run],ugb[run],scale=1,units='xy',width=0.15,headlength=3,headwidth=3)
plt.quiverkey(qqq, 290,25,20.0/3600.0*40.0*10.0,"20 mas/yr",coordinates='data')
plt.xlabel('l  (degrees)')
plt.ylabel('b (degrees)')
plt.title('Automatic Bayesian Group Finding')
plt.savefig('SLINK-dpmm_output_sky_' + outstr)
plt.clf()


pdb.set_trace()
print "Im Here"
