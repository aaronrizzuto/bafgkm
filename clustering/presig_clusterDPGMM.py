# -*- coding: utf-8 -*-

import itertools, random
import pdb
import numpy as np
from scipy import linalg
import pylab as pl
import matplotlib.pyplot as plt
import time
from scipy import stats


##convergence tolerance and iteration limit
epsilon = 10e-8
#max_iter = 1000
#max_iter = 1
class Gaussian:
    def __init__(self, X=np.zeros((0,1)),sig_X=np.zeros([0,1]), kappa_0=1.000, nu_0=1.0001, mu_0=None, 
            Psi_0=None): # Psi is also called Lambda or T
        # See http://en.wikipedia.org/wiki/Conjugate_prior 
        # Normal-inverse-Wishart conjugate of the Multivariate Normal
        # or see p.18 of Kevin P. Murphy's 2007 paper:"Conjugate Bayesian 
        # analysis of the Gaussian distribution.", in which Psi = ^
        self.n_points = X.shape[0]
        self.n_var    = X.shape[1]

        self._hash_covar = None
        self._inv_covar  = None
        
        # NIW dist initial cluster mean paramater, should be input with cluster points as  mean of the data
        if mu_0 == None: 
            self._mu_0 = np.zeros((1, self.n_var))
        else:
            self._mu_0 = mu_0
        assert(self._mu_0.shape == (1, self.n_var))

        # mean fraction, should be at greater that 0, but numerically you can set it to 
        #zero and it should be fine, default is 1
        self._kappa_0 = kappa_0 

        # degrees of freedom, set it to the dimensionality of the data
        self._nu_0 = nu_0 
        if self._nu_0 < self.n_var:
            self._nu_0 = self.n_var

        # NIW covariance matrix, initial for the cluster, should be input as the e.g. the typical 
        # covariance matrix of the data really. 
        if Psi_0 == None:
            self._Psi_0 = 10*np.eye(self.n_var) # TODO this 10 factor should be a prior, ~ dependent on the mean distance between points of the dataset
        else:
            self._Psi_0 = Psi_0
        assert(self._Psi_0.shape == (self.n_var, self.n_var))

        if X.shape[0] > 0:
            self.fit(X)
        else:
            self.default()


    def default(self):
        self.mean  = np.matrix(np.zeros((1, self.n_var)))  # TODO init to mean of the dataset
        self.covar = 100.0 * np.matrix(np.eye(self.n_var)) # TODO change 100


    def recompute_params(self):
        """ need to have initial _X, _sum, and _square_sum """ 
        self.n_points = self._X.shape[0]
        self.n_var    = self._X.shape[1]
        if self.n_points <= 0:
            self.default()
            return

        kappa_n = self._kappa_0 + self.n_points
        nu      = self._nu_0    + self.n_points 
        mu      = np.matrix(self._sum)/self.n_points
        mu_mu_0 = mu - self._mu_0
        #pdb.set_trace()
        big_S   = self._square_sum - self.n_points*(mu.transpose()*mu)
        test_S = (self._X - mu).transpose()*(self._X-mu)
        test3 = np.dot(np.transpose(self._X-mu),self._X-mu)
        #pdb.set_trace()
        Psi     = (self._Psi_0 + big_S + self._kappa_0*self.n_points*mu_mu_0.transpose()*mu_mu_0/(self._kappa_0 + self.n_points))
        pdb.set_trace()
        self.mean  = ((self._kappa_0*self._mu_0 + self.n_points*mu)/(self._kappa_0 + self.n_points))
        self.covar = (Psi * (kappa_n + 1)) / (kappa_n*(nu - self.n_var + 1))
        assert(np.linalg.det(self.covar) != 0)


    def inv_covar(self):
        """ memoize the inverse of the covariance matrix """
        if self._hash_covar != hash(self.covar):
            self._hash_covar = hash(self.covar)
            self._inv_covar  = np.linalg.inv(self.covar)
        return self._inv_covar


    def fit(self, X):
        """ to add several points at once without recomputing """
        self._X          = X
        self._sum        = X.sum(0)
        self._square_sum = np.matrix(X).transpose() * np.matrix(X)
        self.recompute_params()

    
    def add_point(self, x):
        """ add a point to this Gaussian cluster """
        #If this cluster is totally empty (it's a new one), add and initialise
        if self.n_points <= 0:
            self._X           = np.array([x])
            self._sum         = self._X.sum(0)
            self._square_sum  = np.matrix(self._X).transpose() * np.matrix(self._X)
        #If this cluster already exists and has other members, add and recompute properties
        else:
            self._X           = np.append(self._X, [x], axis=0)
            self._sum        += x
            self._square_sum += np.matrix(x).transpose() * np.matrix(x)
        self.recompute_params()


    def rm_point(self, x):
        """ remove a point from this Gaussian cluster """
        assert(self._X.shape[0] > 0)
        # Find the indice of the point x in self._X
        dist              = np.sqrt(np.sum(np.square(self._X-x),axis=1))
        spot              = np.argmin(dist)
        tmp               = np.matrix(self._X[spot])
       # pdb.set_trace()
        self._sum        -= self._X[spot]
        self._X           = np.delete(self._X, spot, axis=0)
        self._square_sum -= tmp.transpose()*tmp
        self.recompute_params()


    def pdf(self, x):
        """ probability density function for a multivariate Gaussian """
        size = len(x)
        assert(size == self.mean.shape[1])
        assert((size, size) == self.covar.shape)
        det = np.linalg.det(self.covar)
        assert(det != 0)
        norm_const = 1.0/((2*np.pi)**(size/2.0)*det**0.5)
        x_mu       = x - self.mean
        result     = np.e**(-0.5 * (x_mu * self.covar.I * x_mu.transpose())[0,0])
        return norm_const * result



"""
Dirichlet process mixture model (for N observations y_1, ..., y_N)
    1) generate a distribution G ~ DP(G_0, α)
    2) generate parameters θ_1, ..., θ_N ~ G
    [1+2) <=> (with B_1, ..., B_N a measurable partition of the set for which 
        G_0 is a finite measure, G(B_i) = θ_i:)
       generate G(B_1), ..., G(B_N) ~ Dirichlet(αG_0(B_1), ..., αG_0(B_N)]
    3) generate each datapoint y_i ~ F(θ_i)
Now, an alternative is:
    1) generate a vector β ~ Stick(1, α) (<=> GEM(1, α))
    2) generate cluster assignments c_i ~ Categorical(β) (gives K clusters)
    3) generate parameters Φ_1, ...,Φ_K ~ G_0
    4) generate each datapoint y_i ~ F(Φ_{c_i})
    for instance F is a Gaussian and Φ_c = (mean_c, var_c)
Another one is:
    1) generate cluster assignments c_1, ..., c_N ~ CRP(N, α) (K clusters)
    2) generate parameters Φ_1, ...,Φ_K ~ G_0
    3) generate each datapoint y_i ~ F(Φ_{c_i})
So we have P(y | Φ_{1:K}, β_{1:K}) = \sum_{j=1}^K β_j Norm(y | μ_j, S_j)
"""
class DPMM:
    def _get_means(self):
        return np.array([g.mean for g in self.params.itervalues()])


    def _get_covars(self):
        return np.array([g.covar for g in self.params.itervalues()])


    def __init__(self, n_components=-1, alpha=1.0):
        self.params = {0: Gaussian()}
        self.n_components = n_components
        self.means_ = self._get_means()
        self.alpha = alpha


    def fit_collapsed_Gibbs(self, X,assigns=0):
        """ according to algorithm 3 of collapsed Gibss sampling in Neal 2000:
        http://www.stat.purdue.edu/~rdutta/24.PDF """
        mean_data = np.matrix(X.mean(axis=0))
        self.n_points = X.shape[0]
        self.n_var = X.shape[1]
        self._X = X
        if self.n_components == -1:
            # initialize with 1 cluster for each datapoint
            self.params         = dict([(i, Gaussian(X=np.array(X[i]), mu_0=mean_data)) for i in xrange(X.shape[0])])
            self.z              = dict([(i,i) for i in range(X.shape[0])])
            self.n_components   = X.shape[0]
            previous_means      = 2*self._get_means() ##multiplied by 2 to get the thing started later
            previous_components = self.n_components
            #pdb.set_trace()
        if self.n_components == -2:
            if len(assigns) != len(X): pdb.set_trace()
            ##set the first group number to 0 regardless of input
            assigns           = assigns-min(assigns)
            ##number of groups
            incluster         = np.unique(assigns)
            self.n_components = len(incluster)
            cls = ()
            for incnum in incluster:
                thiscluster = np.where(assigns == incnum)[0]
                cls         = cls + (thiscluster,)
            self.params         = dict([(i, Gaussian(X=np.array(X[grp]), mu_0=mean_data)) for i,grp in enumerate(cls)])
            self.z              = dict([(i,j) for i,j in enumerate(assigns)])
            previous_means      = 2*self._get_means()
            previous_components = self.n_components
           # pdb.set_trace()    
        else:
            # init randomly 
            self.params = dict([(j, Gaussian(X=np.zeros((0, X.shape[1])), mu_0=mean_data)) for j in xrange(self.n_components)])
            self.z      = dict([(i, random.randint(0, self.n_components - 1)) 
                      for i in range(X.shape[0])])
            previous_means      = 2 * self._get_means()
            previous_components = self.n_components
            for i in xrange(X.shape[0]):
                self.params[self.z[i]].add_point(X[i])

        print "Initialized collapsed Gibbs sampling with %i clusters" % (self.n_components)

        n_iter   = 0 # with max_iter hard limit, in case of cluster oscillates
        # while the clusters did not converge 
        #pdb.set_trace()
        max_iter = 1000
        while (n_iter < max_iter 
                and (previous_components != self.n_components
                or abs((previous_means - self._get_means()).sum()) > epsilon)):
            n_iter += 1
            previous_means      = self._get_means()
            previous_components = self.n_components

            for i in xrange(X.shape[0]):
            	s_time = time.time()
                # remove X[i]'s sufficient statistics from z[i]
                self.params[self.z[i]].rm_point(X[i])
                # if it empties the cluster, remove it and decrease K
                if self.params[self.z[i]].n_points <= 0:
                    self.params.pop(self.z[i])
                    self.n_components -= 1
                time1 = time.time()
                tmp = []
                ##This is the CRP calculation
                for k, param in enumerate(self.params.values()):
                    # compute P_k(X[i]) = P(X[i] | X[-i] = k)
                    marginal_likelihood_Xi = param.pdf(X[i])
                    # set N_{k,-i} = dim({X[-i] = k})
                    # compute P(z[i] = k | z[-i], Data) = N_{k,-i}/(α+N-1)
                    mixing_Xi = param.n_points / (self.alpha + self.n_points - 1)
                    tmp.append(marginal_likelihood_Xi * mixing_Xi)
                ##This is the CRP new cluster probability
                # compute P*(X[i]) = P(X[i]|λ)
                #base_distrib     = Gaussian(X=np.zeros((0, X.shape[1])))
                #prior_predictive = base_distrib.pdf(X[i])
                prior_predictive = Gaussian(X=np.zeros((0, X.shape[1]))).pdf(X[i])
                
                # compute P(z[i] = * | z[-i], Data) = α/(α+N-1)
                prob_new_cluster = self.alpha/(self.alpha + self.n_points - 1)
                tmp.append(prior_predictive * prob_new_cluster)

                # normalize P(z[i]) (tmp above)
                time15=time.time()
                s = sum(tmp)
                tmp = map(lambda e: e/s, tmp)
                time2 = time.time()


                # sample z[i] ~ P(z[i])
                #this is the gibbs sampling bit
                rdm   = np.random.rand()
                total = tmp[0]
                k     = 0
                while (rdm > total):
                    k     += 1
                    total += tmp[k]
                # add X[i]'s sufficient statistics to cluster z[i]
                new_key    = max(self.params.keys()) + 1
                if k == self.n_components: # create a new cluster
                    self.z[i]            = new_key
                    self.n_components   += 1
                    self.params[new_key] = Gaussian(X=np.matrix(X[i]))
                else:
                    self.z[i] = self.params.keys()[k]
                    self.params[self.params.keys()[k]].add_point(X[i])
                assert(k < self.n_components)

                ##some time testing
                timegibbs = time.time()-time2
                timestar = time.time()-s_time
                #print "up to datapoint " + str(i)+" out of "+str(X.shape[0])
            #time1=time.clock()
            print "iteration " +str(n_iter)+ " still sampling, %i clusters currently, with log-likelihood %f" % (self.n_components, self.log_likelihood())
        #pdb.set_trace()
        self.means_ = self._get_means()

    def predict(self, X):
        """ produces and returns the clustering of the data """
        if (X != self._X).any():
            self.fit_collapsed_Gibbs(X)
        #map the cluster id's onto numbers starting from 0
        mapper = list(set(self.z.values()))
        Y      = np.array([mapper.index(self.z[i]) for i in range(X.shape[0])])
        return Y


    def log_likelihood(self):
        # TODO test the values 
        log_likelihood = 0.
        
        #the loop is quite slow, there is a faster way.....but it really doesn't matter
        for n in xrange(self.n_points):
            log_likelihood -= (0.5 * self.n_var * np.log(2.0 * np.pi) + 0.5*np.log(np.linalg.det(self.params[self.z[n]].covar)))
            mean_var        = np.matrix(self._X[n, :] - self.params[self.z[n]]._X.mean(axis=0)) # TODO should compute self.params[self.z[n]]._X.mean(axis=0) less often
            assert(mean_var.shape == (1, self.params[self.z[n]].n_var))
            log_likelihood -= 0.5 * np.dot(np.dot(mean_var, 
                self.params[self.z[n]].inv_covar()), mean_var.transpose())
        return log_likelihood
    def log_likelihood_pw_all(self):
        # calculate the log likelihood for each datapoint individually
        log_likelihood = np.zeros(self.n_points)
        for n in xrange(self.n_points):
            log_likelihood[n] = -(0.5 * self.n_var * np.log(2.0 * np.pi) + 0.5 
                        * np.log(np.linalg.det(self.params[self.z[n]].covar)))
            mean_var = np.matrix(self._X[n, :] - self.params[self.z[n]]._X.mean(axis=0))
            log_likelihood[n] -= 0.5 * np.dot(np.dot(mean_var, 
                self.params[self.z[n]].inv_covar()), mean_var.transpose())
        return log_likelihood
    def log_likelihood_pw(self,n):
        # calculate the log likelihood for a particular datapoint individually
        log_likelihood = -(0.5 * self.n_var * np.log(2.0 * np.pi) + 0.5 
                     * np.log(np.linalg.det(self.params[self.z[n]].covar)))
        mean_var = np.matrix(self._X[n, :] - self.params[self.z[n]]._X.mean(axis=0))
        log_likelihood -= 0.5 * np.dot(np.dot(mean_var, 
            self.params[self.z[n]].inv_covar()), mean_var.transpose())
        return log_likelihood[0,0]           



# Number of samples per component
n_samples = 100

# Generate random sample, two components
np.random.seed(0)

# 2, 2-dimensional Gaussians
C = np.array([[0., -0.1], [1.7, .4]])
X = np.r_[np.dot(np.random.randn(n_samples, 2), C)+np.array([-1,4]),
          .7 * np.random.randn(n_samples, 2) + np.array([-6, 3])]
inassigns = np.zeros(2*n_samples,int)
inassigns[0:n_samples]= 3
inassigns[n_samples:] = 2
pdb.set_trace()
#mns1 = np.array([-0.0,-0.0])
#cov1 = np.array([[20.0,0.0],[0.0,20.0]])
#mns2 = np.array([3.0,3.0])
#cov2 = np.array([[1.0,0.0],[0.0,1.0]])

#X1 = np.random.multivariate_normal(mns1,cov1,200)
#X2 = np.random.multivariate_normal(mns2,cov2,200)
#pdb.set_trace()
#X = np.concatenate((X1,X2),axis=0)
#pdb.set_trace()

dpmm = DPMM(n_components=-2) # -1, 1, 2, 5
# n_components is the number of initial clusters (at random)
# -1 means that we initialize with 1 cluster per point
# -2 means initialize with an input clustering
dpmm.fit_collapsed_Gibbs(X,assigns=inassigns)

#dpmm.fit_MH(X)
pdb.set_trace()

assigns = dpmm.predict(X)
means   = dpmm.means_
covars  = dpmm._get_covars()

for i in range(0,means.shape[0]):
	tc = np.where(assigns == i)[0]
	plt.plot(X[tc,0],X[tc,1],'.')

plt.savefig("dpgmm.png")