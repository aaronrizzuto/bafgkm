import numpy as np
import pdb
from full_uvw_to_observable import obs_to_gal_full
from readcol import readcol
import pickle
import idlsave
np.set_printoptions(suppress=True,precision=2)
data = readcol('betapic_fixed.csv',names=True,asRecArray=True,fsep=',')


posvel = np.zeros((data.ra.shape[0],6))
cposvel = np.zeros((data.ra.shape[0],6,6))
for i in range(0,data.ra.shape[0]):
    pospm = np.array([data.ra[i],data.dec[i],data.plx[i],data.pmra[i],data.pmdec[i],data.rv[i]])
    cpospm = np.array(np.eye(6)*np.array([1e-7,1e-7,data.sig_plx[i]**2,data.sig_pmra[i]**2,data.sig_pmdec[i]**2,data.sig_rv[i]**2]))
    if data.name[i] == "HR 7329": pdb.set_trace()
    thisposvel,thiscposvel = obs_to_gal_full(pospm,cpospm)
    posvel[i] = thisposvel
    cposvel[i] = thiscposvel
    print i+1
    
    #pdb.set_trace()
    
ids = data.name

pickle.dump(posvel,open("betapic_posvel.pkl","wb"))
pickle.dump(ids,open("betapic_ids.pkl","wb"))
pickle.dump(cposvel,open("betapic_cposvel.pkl","wb"))



pdb.set_trace()
print "im here"