import numpy as np
from readcol import readcol
import pickle
import os,glob,pdb
from numpy.lib.recfunctions import stack_arrays
#c = stack_arrays((a, b), asrecarray=True, usemask=False)
file1 = 'list.csv'
file2 = 'list_clump.csv'

l1 = readcol(file1,fsep=',',asRecArray=True)
l2 = readcol(file2,fsep=',',asRecArray=True)


full_l = stack_arrays((l1, l2), asrecarray=True, usemask=False)

full_l = full_l[np.unique(full_l.GaiaID,return_index=True)[1]]

rrr = np.argsort(full_l.RA)
full_l = full_l[rrr]

pdb.set_trace()
outfile = open('taulist_gaianew.txt','wb')
for i in range(len(full_l)):
    line = str(i+1) + ' ' + '"'+str(full_l.GaiaID[i])+'" ' + full_l.RA[i] + ' ' + full_l.DEC[i] +  ' 2000.0 0.0 0.0 V=' + str(full_l.Mv[i]) + ' \n'
    outfile.write(line)
    outfile.flush()


outfile.close()
pdb.set_trace()
print 'here'