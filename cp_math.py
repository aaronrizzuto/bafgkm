##Convergent point math functions

##IMPORTS
import numpy as np

##ASTROLIB IMPORTS
from ACRastro.glactc  import glactc
from ACRastro.gal_uvw import gal_uvw

import pdb


def find_cp(vel,equatorial=False):
##input is a numpy array that is Nx3 containing UVW velocity components
##output is a Nx2 array containing gall,galb for each con-point
    lcon = np.arctan2(vel[:,1], -vel[:,0])
    bcon = np.arctan2(vel[:,2],np.sqrt(vel[:,1]**2 + vel[:,0]**2))
    galcon = np.array([lcon,bcon])
    galcon = np.transpose(galcon)
    if equatorial == True:
        racon,deccon = glactc(lcon*180/np.pi,bcon*180/np.pi,2000,degree = True,fk4=True,reverse=True)
        eqcon = np.transpose(np.array([racon,deccon])*np.pi/180.0)
        return galcon,eqcon
    if equatorial == False:
        return galcon
    
    

##assume all input angles are in radians
def rotate_pm_to_cp(ra,dec,pmra,pmdec,sig_pmra,sig_pmdec,pmcov,cp):
    sin=np.sin
    cos=np.cos
    ##pdb.set_trace()
    lamb    = np.arccos(sin(dec)*sin(cp[:,1])+cos(dec)*cos(cp[:,1])*cos(ra-cp[:,0]))
    sinthet = cos(cp[:,1])*sin(np.absolute(ra-cp[:,0]))/sin(lamb)
    costhet = (sin(cp[:,1])-cos(lamb)*sin(dec))/(cos(dec)*sin(lamb))
    theta   = np.arctan2(sinthet,costhet)
    
    ##rotate the proper motions as per normal rotation matrix 
    signalf =  (cp[:,0]-ra)/np.absolute(cp[:,0]-ra)
    mu_perp = -signalf*cos(theta)*pmra + sin(theta)*pmdec
    mu_par  = cos(theta)*pmdec + signalf*sin(theta)*pmra
    ##pdb.set_trace()
    ##now calculate the corresponding errors in the new proper motions
    sig_mu_perp = np.sqrt((sin(theta)**2*sig_pmdec**2+cos(theta)**2*sig_pmra**2+2*cos(theta)*sin(theta)*pmcov))
    sig_mu_par  = np.sqrt((sin(theta)**2*sig_pmra**2+cos(theta)**2*sig_pmdec**2+2*cos(theta)*sin(theta)*pmcov))
    #pdb.set_trace()
    return mu_perp,mu_par,sig_mu_perp,sig_mu_par

##same as rotate_pm_to_cp above, but you can input a set of random convergent points and any number
##of star data sets and it will do everything in a matrix way
def rotate_pm_to_cp_multistar(ra,dec,pmra,pmdec,sig_pmra,sig_pmdec,pmcov,cp):
    sin=np.sin
    cos=np.cos

    #rapluscp = ra[:,np.newaxis] + cp[:,0]
    ramincp  = -ra[:,np.newaxis] + cp[:,0]
    bcdec   = np.outer(dec,cp[:,1]*0.0+1.0)
    lamb    = np.arccos(np.outer(sin(dec),sin(cp[:,1])) + np.outer(cos(dec),cos(cp[:,1]))*cos(-ramincp))
    sinthet = np.outer(dec*0.0+1.0,cos(cp[:,1]))*sin(np.absolute(-ramincp))/sin(lamb)
    costhet= (np.outer(dec*0.0+1.0,sin(cp[:,1]))-cos(lamb)*sin(bcdec))/cos(bcdec)/sin(lamb)
    
    ##rotate the proper motions as per normal rotation matrix 
    signalf =  ramincp/np.absolute(ramincp)
    mu_perp = -signalf*costhet*np.outer(pmra,cp[:,1]*0.0+1.0) + sinthet*np.outer(pmdec,cp[:,1]*0.0+1.0)
    mu_par  = costhet*np.outer(pmdec,cp[:,1]*0.0+1.0) + signalf*sinthet*np.outer(pmra,cp[:,1]*0.0+1.0)

    ##now calculate the corresponding errors in the new proper motions
    sig_mu_perp = np.sqrt(sinthet**2*np.outer(sig_pmdec,cp[:,1]*0.0+1.0)**2+costhet**2*np.outer(sig_pmra,cp[:,1]*0.0+1.0)**2+2*costhet*sinthet*np.outer(pmcov,cp[:,1]*0.0+1.0))
    sig_mu_par  = np.sqrt(sinthet**2*np.outer(sig_pmra,cp[:,1]*0.0+1.0)**2+costhet**2*np.outer(sig_pmdec,cp[:,1]*0.0+1.0)**2+2*costhet*sinthet*np.outer(pmcov,cp[:,1]*0.0+1.0))

    #qwe = np.where(np.isnan(sig_mu_par))[0]
    #if len(qwe) >0: pdb.set_trace()

    return mu_perp,mu_par,sig_mu_perp,sig_mu_par
    
def rotate_pm_to_cp_multistar_distanced(ra,dec,pmra,pmdec,sig_pmra,sig_pmdec,pmcov,plxpmra_cov,plxpmdec_cov,cp,sig_plx):
    
    
    sin=np.sin
    cos=np.cos

    #rapluscp = ra[:,np.newaxis] + cp[:,0]
    ramincp  = -ra[:,np.newaxis] + cp[:,0]
    bcdec   = np.outer(dec,cp[:,1]*0.0+1.0)
    lamb    = np.arccos(np.outer(sin(dec),sin(cp[:,1])) + np.outer(cos(dec),cos(cp[:,1]))*cos(-ramincp))
    sinthet = np.outer(dec*0.0+1.0,cos(cp[:,1]))*sin(np.absolute(-ramincp))/sin(lamb)
    costhet= (np.outer(dec*0.0+1.0,sin(cp[:,1]))-cos(lamb)*sin(bcdec))/cos(bcdec)/sin(lamb)
    
    ##rotate the proper motions as per normal rotation matrix 
    signalf =  ramincp/np.absolute(ramincp)
    mu_perp = -signalf*costhet*np.outer(pmra,cp[:,1]*0.0+1.0) + sinthet*np.outer(pmdec,cp[:,1]*0.0+1.0)
    mu_par  = costhet*np.outer(pmdec,cp[:,1]*0.0+1.0) + signalf*sinthet*np.outer(pmra,cp[:,1]*0.0+1.0)
    ##pdb.set_trace()
    ##now calculate the corresponding errors in the new proper motions
    sig_mu_perp = np.sqrt(sinthet**2*np.outer(sig_pmdec,cp[:,1]*0.0+1.0)**2+costhet**2*np.outer(sig_pmra,cp[:,1]*0.0+1.0)**2+2*costhet*sinthet*np.outer(pmcov,cp[:,1]*0.0+1.0))
    sig_mu_par  = np.sqrt(sinthet**2*np.outer(sig_pmra,cp[:,1]*0.0+1.0)**2+costhet**2*np.outer(sig_pmdec,cp[:,1]*0.0+1.0)**2+2*costhet*sinthet*np.outer(pmcov,cp[:,1]*0.0+1.0))

    plxmupar_cov = costhet*np.outer(plxpmdec_cov,cp[:,1]*0.0+1.0) + sinthet*np.outer(plxpmra_cov,cp[:,1]*0.0+1.0) ##*signalf (this was wrong apparently)
    #qwe = np.where(np.isnan(sig_mu_par))[0]
    #if len(qwe) >0: pdb.set_trace()
    return mu_perp,mu_par,sig_mu_perp,sig_mu_par,plxmupar_cov
    
##takes velocities and convergent points and applies projection for a single star location
##given in gall and galb in radians
def project_uvw_to_cp(vel,galcon,l_star,b_star,ra_star,dec_star):
    sin=np.sin
    cos=np.cos
    
    ##l and b unit vectors for the star
    l_hat_star = np.array([sin(l_star[0]),cos(l_star[0]),0.0])
    b_hat_star = np.array([sin(b_star[0])*cos(l_star[0]),-sin(b_star[0])*sin(l_star[0]),cos(b_star[0])])
    
    ##get the lambda and gamma angle
    lamb      = np.arccos(sin(b_star)*sin(galcon[:,1])+cos(b_star)*cos(galcon[:,1])*cos(l_star-galcon[:,0]))
    sin_gamma = sin(np.absolute(l_star-galcon[:,0]))*cos(galcon[:,1])/sin(lamb)
    cos_gamma = (sin(galcon[:,1])-cos(lamb)*sin(b_star))/(cos(b_star)*sin(lamb))
    gamma     = np.arctan2(sin_gamma,cos_gamma)
    
    ##now get the unit vector in the direction of the convergent points
    sign_l = (galcon[:,0]-l_star)/np.absolute(galcon[:,0]-l_star)
    g_hat  = np.array(vel)
    ##tmp = sign_l*l_hat_star[0]*sin_gamma + cos_gamma*b_hat_star[0]
    g_hat[:,0] = sign_l*l_hat_star[0]*sin_gamma + cos_gamma*b_hat_star[0]
    g_hat[:,1] = sign_l*l_hat_star[1]*sin_gamma + cos_gamma*b_hat_star[1]
    g_hat[:,2] = sign_l*l_hat_star[2]*sin_gamma + cos_gamma*b_hat_star[2]

    ##do the dot product for the projection this is now velocity in the direction
    ##of the convergent point
    Uexp = np.sum(g_hat*vel,axis=1)
    #pdb.set_trace()

    ##now do the RV projection
    uuu,vvv,www = gal_uvw(ra=ra_star*180.0/np.pi, dec=dec_star*180.0/np.pi, pmra=0.0, pmdec=0.0, vrad=1.0, plx=7.0)
    length = np.sqrt(uuu**2 + vvv**2 + www**2)
    r_hat  = np.array([uuu[0],vvv[0],www[0]])/length
    rvexp  = np.dot(vel,r_hat)
    ##pdb.set_trace()
    return Uexp,rvexp


    
    
def project_uvw_to_cp_multistar(vel,galcon,l_star,b_star,ra_star,dec_star):
    sin=np.sin
    cos=np.cos
    
    ##l and b unit vectors for the star
    l_hat_star = np.array([sin(l_star),cos(l_star),l_star*0.0])
    b_hat_star = np.array([sin(b_star)*cos(l_star),-sin(b_star)*sin(l_star),cos(b_star)])
    
    ##get the lambda and gamma angle
    lmincp  = l_star[:,np.newaxis] - galcon[:,0]
    bcbs    = np.outer(b_star,galcon[:,1]*0.0+1.0)

    lamb      = np.arccos(np.outer(sin(b_star),sin(galcon[:,1])) + np.outer(cos(b_star),cos(galcon[:,1]))*cos(lmincp))


    #sin_gamma = sin(np.absolute(l_star-galcon[:,0]))*cos(galcon[:,1])/sin(lamb)
    sin_gamma = np.outer(b_star*0.0+1.0,cos(galcon[:,1]))*sin(np.absolute(lmincp))/sin(lamb)
    #cos_gamma = (sin(galcon[:,1])-cos(lamb)*sin(b_star))/(cos(b_star)*sin(lamb))
    cos_gamma = (np.outer(b_star*0.0+1.0,sin(galcon[:,1]))-cos(lamb)*sin(bcbs))/cos(bcbs)/sin(lamb)

    ##now get the unit vector in the direction of the convergent points
    sign_l = -lmincp/np.absolute(-lmincp)
    vshape = vel.shape
    g_hat  = np.zeros((len(l_star),vshape[0],vshape[1]))

    #g_hat[:,0] = sign_l*l_hat_star[0]*sin_gamma + cos_gamma*b_hat_star[0]
    #g_hat[:,1] = sign_l*l_hat_star[1]*sin_gamma + cos_gamma*b_hat_star[1]
    #g_hat[:,2] = sign_l*l_hat_star[2]*sin_gamma + cos_gamma*b_hat_star[2]
    g_hat[:,:,0] = np.transpose(np.transpose(sin_gamma*sign_l)*l_hat_star[0,:]) + np.transpose(np.transpose(cos_gamma)*b_hat_star[0,:])
    g_hat[:,:,1] = np.transpose(np.transpose(sin_gamma*sign_l)*l_hat_star[1,:] + np.transpose(cos_gamma)*b_hat_star[1,:])
    g_hat[:,:,2] = np.transpose(np.transpose(sin_gamma*sign_l)*l_hat_star[2,:] + np.transpose(cos_gamma)*b_hat_star[2,:])

    ##do the dot product for the projection this is now velocity in the direction
    ##of the convergent point
    Uexp = np.sum(g_hat*vel,axis=2)
    #pdb.set_trace()

    ##now do the RV projection
    uuu,vvv,www = gal_uvw(ra=ra_star*180.0/np.pi, dec=dec_star*180.0/np.pi, pmra=0.0, pmdec=0.0, vrad=ra_star*0.0+1.0, plx=ra_star*0.0+7.0)
    ilength = 1/np.sqrt(uuu**2 + vvv**2 + www**2)
    r_hat  = np.array([uuu,vvv,www])*ilength
    rvexp  = np.transpose(np.dot(vel,r_hat))
    ##pdb.set_trace()
    return Uexp,rvexp

    
    
    