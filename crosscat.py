import numpy as np
from gcirc import gcirc
import pdb
import pickle
import matplotlib.pyplot as plt
from numpy.lib.recfunctions import stack_arrays

#file = open("hyades_ucac4.pkl","rb")
#cat1 = pickle.load(file)

def splitcat(cat,tmploc):
	minra = np.min(cat.ra)
	maxra = np.max(cat.ra)
	mindec = np.min(cat.dec)
	maxdec = np.max(cat.dec)
	width = 2.0
	nra = int(np.ceil((maxra-minra)/width))
	nde = nra
	
	tilera = np.arange(nra+2)*width+minra
	tilede = np.arange(nde+2)*width+mindec
	
	tilegrid = np.meshgrid(tilera,tilede)
	gridra = tilegrid[0]
	gridde = tilegrid[1]
	tgname = tmploc+"tilemap.pkl"
	pickle.dump(tilegrid, open(tgname, "wb" ))
	list_tilename = np.zeros((nra+2,nde+2),object)
	##now loop over the tiles top figure out which 
	for i in range(0,nra+2):
		for j in range(0,nde+2):
			#pdb.set_trace()
			inthis = np.where((cat.ra < gridra[i,j]+width/2) & (cat.ra >= gridra[i,j]-width/2) & (cat.dec < gridde[i,j]+width/2) & (cat.dec >= gridde[i,j]-width/2))[0]
			if len(inthis) != 0:
				tilename = tmploc+"tile_"+str(i)+"_"+str(j)+".pkl"
				list_tilename[i,j] = tilename
				#pdb.set_trace()

				dattoout = cat[inthis]
				pickle.dump(dattoout, open(tilename, "wb" ))
		print "up to "+str(i) +" out of " +str(nra+2)
	pickle.dump(list_tilename, open(tmploc+"tilenamelist.pkl", "wb" ))
	return 1

#file  = open("/Volumes/UTRAID/ppmxl/hyades_ppmxl.pkl","rb")
file  = open("/Volumes/UTRAID/ppmxl/pleiades_ppmxl.pkl","rb")

#file  = open("testdata.pkl","rb")

ppmxl = pickle.load(file)

#pickle.dump(ppmxl[0:1000], open("testdata.pkl", "wb" ))
#pdb.set_trace()

dumms = splitcat(ppmxl,"/Volumes/UTRAID/ppmxl/catsplits/")

#pdb.set_trace()
##clear ppmxl variable as it's gigantic and nopt needed.
ppmxl = 1.0

##load in the ucac4
#file = open("hyades_ucac4.pkl","rb")
file = open("pleiades_ucac4.pkl","rb")
ucac = pickle.load(file)
#ucac = ppmxl.copy()

#outfile = "hyades_corsscat.pkl"
outfile = "pleiades_corsscat.pkl"



##get tile definitions and centre locations
tilegrid = pickle.load(open("/Volumes/UTRAID/ppmxl/catsplits/"+"tilemap.pkl","rb"))
tilename = pickle.load(open("/Volumes/UTRAID/ppmxl/catsplits/"+"tilenamelist.pkl","rb"))
gridra = tilegrid[0]
gridde = tilegrid[1]
#pdb.set_trace()
##go star by star and cross
for i in range(0,ucac.shape[0]):
	if np.mod(i,1000) == 0: print "up to " + str(i) +" of " +str(ucac.shape[0])
	#pdb.set_trace()
	if ucac.sig_pmra[i] > 20.0:
		thisra = ucac.ra[i]*180/np.pi
		thisde = ucac.dec[i]*180/np.pi
		dist = np.sqrt((gridra-thisra)**2 + (gridde-thisde)**2)
		#pdb.set_trace()
		sind = np.array([-1,-1,-1,-1],int)
		for j in range(0,4):
			loc1 = np.argmin(dist)
			loc2 =np.unravel_index(loc1,dist.shape)
			sind[j] = loc1
			dist[loc2[0],loc2[1]]=1e20
		#pdb.set_trace()
		toread = tilename.flat[sind]
		##remove empty tiles	
		toread = toread[np.where(toread != 0)[0]]
		for j in range(0,len(toread)):
			thistile = pickle.load(open(toread[j],"rb"))
		
			if j==0:
				tiledat = thistile.copy()
			if j != 0: tiledat=stack_arrays((tiledat,thistile),asrecarray=True,usemask=False)
		#pdb.set_trace()
		##now the cross matching to ucac4
		gdist = gcirc(thisra,thisde,tiledat.ra,tiledat.dec,format=2)
		close = np.where(gdist == np.min(gdist))[0]
		#pdb.set_trace()
		if len(close) != 1: close = close[0]#pdb.set_trace()
		if gdist[close] < 10.0:
			newpmra = (ucac.pmra[i]/ucac.sig_pmra[i]**2 + tiledat.pmra[close]/tiledat.sig_pmra[close]**2)/(1/ucac.sig_pmra[i]**2 + 1/tiledat.sig_pmra[close]**2)
			newpmde = (ucac.pmdec[i]/ucac.sig_pmdec[i]**2 + tiledat.pmdec[close]/tiledat.sig_pmdec[close]**2)/(1/ucac.sig_pmdec[i]**2 + 1/tiledat.sig_pmdec[close]**2)
			nsig_pmra = np.sqrt(1/(1/ucac.sig_pmra[i]**2 + 1/tiledat.sig_pmra[close]**2))
			nsig_pmde = np.sqrt(1/(1/ucac.sig_pmdec[i]**2 + 1/tiledat.sig_pmdec[close]**2))
			sigdif_ra = np.abs((ucac.pmra[i] - tiledat.pmra[close])/(ucac.sig_pmra[i] + tiledat.sig_pmra[close]))
			sigdif_de =np.abs((ucac.pmdec[i] - tiledat.pmdec[close])/(ucac.sig_pmdec[i] + tiledat.sig_pmdec[close]))
			if (sigdif_de < 5) & (sigdif_ra < 5): 
				ucac.pmra[i] = newpmra
				ucac.pmdec[i] = newpmde
				ucac.sig_pmra[i] = nsig_pmra
				ucac.sig_pmdec[i] = nsig_pmde
			if ucac.sig_pmra[i] > 49.999:
				ucac.pmra[i] = tiledat.pmra[close]
				ucac.pmdec[i] = tiledat.pmdec[close]
				ucac.sig_pmra[i] = tiledat.sig_pmra[close]
				ucac.sig_pmdec[i] = tiledat.sig_pmdec[close]
	
##output the new catalog
pickle.dump(ucac, open(outfile, "wb" ))

#pdb.set_trace()
print "Here"
