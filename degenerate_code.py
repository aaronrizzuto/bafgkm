def single_star_integral(stardat,rposvel,mcmc=False,nsamp=500,nwalk=10,mcthreads=1):
	##two methods here, one using emcee, the other doing a normal MC integral
	##Start with the ordinary MC integral
	exp = np.exp
	if mcmc == False:
	##IN THIS CASE DO EVERYTHING IN THE EQUATORIAL COORDS
		if usecp==False:
			gra,gdec,gll,gbb,gplx,gpmvect=build_sample_group(gpars[0:6],gcov,gpars[6],nsamp,trace_forward=True,observables=True)
			gpmra  = gpmvect[:,1]
			gpmdec = gpmvect[:,2]
			gvrad  = gpmvect[:,0]
			##everything is already in the frame of the input data so directly compare everything
			cpmra   = -(stardat[4]-gpmra)**2/2/stardat[5]**2
			cpmdec  = -(stardat[6]-gpmdec)**2/2/stardat[7]**2
			cvrad   = -(stardat[10]-gvrad)**2/2/stardat[11]**2
			cplx    = -(stardat[8]-gplx)**2/2/stardat[9]**2
			samp_pdpar = exp(cpmra)*exp(cpmdec)*exp(cplx)*exp(cvrad)
			samp_pdpar = samp_pdpar/stardat[5]/stardat[7]/stardat[9]/stardat[11]/np.sqrt(2*np.pi)
			pdpar   = np.mean(samp_pdpar)
			return pdpar
	##IN THIS CASE DO EVERYTHING IN THE CONVERGENT-POINT COORDS
		if usecp==True: 
			##make a sample of nsamp random points from the 6 dimensional gaussian, then trace forward to the given age
			#time1 = time.clock()
			rposvel=build_sample_group(gpars[0:6],gcov,gpars[6],nsamp,trace_forward=True,observables=False)
			#print time.clock()-time1
			##find the convergent points for the sample velocities
			galcon,eqcon = find_cp(rposvel[:,3:6],equatorial=True) 
			##project the velocities into the convergent-point frame
			uexp,rvexp = project_uvw_to_cp(rposvel[:,3:6],galcon,stardat[2:3],stardat[3:4],stardat[0:1],stardat[1:2])
			##now rotate the stars pmra,pmdec into the convergent point frame
			mu_perp,mu_par,sig_mu_perp,sig_mu_par = rotate_pm_to_cp(stardat[0:1],stardat[1:2],stardat[4:5],stardat[6:7],stardat[5:6],stardat[7:8],stardat[13:14],eqcon)
			##now turn the traced (or not traced) random positions into distances or plx's
			llll,bbbb,gplx=gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
			##now compare everything to calculate the integral
			mu_par_exp = uexp*2*np.pi*gplx/29.8
			parallel   = -(mu_par-mu_par_exp)**2/2/sig_mu_par**2
			distance   = -(stardat[8]-gplx)**2/2/stardat[9]**2
			radial     = -(stardat[10]-rvexp)**2/2/stardat[11]**2
			perpendicular = -mu_perp**2/2/sig_mu_perp**2
			samp_pdpar    = exp(parallel)*exp(perpendicular)*exp(distance)*exp(radial)
			samp_pdpar    = samp_pdpar/sig_mu_par/stardat[9]/sig_mu_perp/stardat[11]/np.sqrt(2*np.pi)
			pdpar         = np.mean(samp_pdpar)
			##pdb.set_trace()
			return pdpar		
	
	if mcmc == True:
		ndim=6
		nwalkers = nwalk
		startpos= np.random.multivariate_normal(gpars[0:6],gcov,nwalkers)
		##pdb.set_trace()
		sampler  = emcee.EnsembleSampler(nwalkers,ndim,sample_lnprob,args=[gpars[0:6],gcov,gpars[6]],threads=mcthreads)
		sampler.run_mcmc(startpos,nsamp)			
		rposvel = sampler.flatchain
		rlnprob = sampler.flatlnprobability
		if usecp == True:
			##find the convergent points for the sample velocities
			galcon,eqcon = find_cp(rposvel[:,3:6],equatorial=True) 
			##project the velocities into the convergent-point frame
			uexp,rvexp = project_uvw_to_cp(rposvel[:,3:6],galcon,stardat[2:3],stardat[3:4],stardat[0:1],stardat[1:2])
			##now rotate the stars pmra,pmdec into the convergent point frame
			mu_perp,mu_par,sig_mu_perp,sig_mu_par = rotate_pm_to_cp(stardat[0:1],stardat[1:2],stardat[4:5],stardat[6:7],stardat[5:6],stardat[7:8],stardat[13:14],eqcon)
			##now turn the traced (or not traced) random positions into distances or plx's
			llll,bbbb,gplx=gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
			mu_par_exp = uexp*2*np.pi*gplx/29.8
			parallel   = -(mu_par-mu_par_exp)**2/2/sig_mu_par**2
			distance   = -(stardat[8]-gplx)**2/2/stardat[9]**2
			radial     = -(stardat[10]-rvexp)**2/2/stardat[11]**2
			perpendicular = -mu_perp**2/2/sig_mu_perp**2
			samp_pdpar    = exp(parallel)*exp(perpendicular)*exp(distance)*exp(radial)
			samp_pdpar    = samp_pdpar/sig_mu_par/stardat[9]/sig_mu_perp/stardat[11]/np.sqrt(2*np.pi)
			##pdb.set_trace()
			##samp_pdpar    = samp_pdpar*exp(rlnprob)
			pdpar         = np.mean(samp_pdpar)
			##pdb.set_trace()
			return pdpar
		if usecp == False:
			gmll,gmbb,gmplx  = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
			gmra,gmdec     = glactc(gmll*180/np.pi,gmbb*180/np.pi,2000,degree=True,reverse=True,fk4=True) ##use fk4 for speed (slightly inaccurate but not huge)
			gpmvect     = uvwtopm(rposvel[:,3:6],gmra*np.pi/180,gmdec*np.pi/180,gmplx) ##array with (vrad,pmra,pmdec)
			gpmra       = gpmvect[:,1]
			gpmdec      = gpmvect[:,2]
			gvrad       = gpmvect[:,0]
			##everything is already in the frame of the input data so directly compare everything
			cpmra   = -(stardat[4]-gpmra)**2/2/stardat[5]**2
			cpmdec  = -(stardat[6]-gpmdec)**2/2/stardat[7]**2
			cvrad   = -(stardat[10]-gvrad)**2/2/stardat[11]**2
			cplx    = -(stardat[8]-gmplx)**2/2/stardat[9]**2
			samp_pdpar = exp(cpmra)*exp(cpmdec)*exp(cplx)*exp(cvrad)
			samp_pdpar = samp_pdpar/stardat[5]/stardat[7]/stardat[9]/stardat[11]/np.sqrt(2*np.pi)
			pdpar   = np.mean(samp_pdpar)
			##pdb.set_trace()
			return pdpar	
			
		