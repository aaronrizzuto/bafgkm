#!/bin/bash 
#SBATCH -J US 
#SBATCH -o /home1/03602/arizzuto/BAFGKM/gaiadr2_scripts/ologs/US_1.o%j 
#SBATCH -N 5
#SBATCH -n 120
#SBATCH -p normal 
#SBATCH -t 12:00:00 
#SBATCH -A BAFGKM
#SBATCH --mail-user=arizz@astro.as.utexas.edu 
#SBATCH --mail-type=begin 
#SBATCH --mail-type=end 
module load python 
cd /home1/03602/arizzuto/BAFGKM/gaiadr2_scripts
export MLK_MAX_THREADS=2 
ibrun python tacc_scocen.py US_1 US
