import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from astropy.io import fits
import os,glob,sys,pdb,time,pickle
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
import idlsave
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
from gcirc import gcirc
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from taurus_paramgen import taurus_paramgen
from taurus_paramgen import taurus_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from scipy.special import erf as scipyerf
from scipy.stats import truncnorm
#np.set_printoptions(suppress=True,precision=2)
from readcol import readcol


thedatafile = '/Volumes/UT1/gaia_dr2_cleanparallax.csv'
#sizelim =1000 is 11 lines
print 'reading file'
parallax = readcol(thedatafile,fsep=',',asRecArray=False,names=True,nullval='')[1][:,0]
print 'reading done'
#sig_parallax = data.parallax_.copy()
keep = np.where(parallax > 1)[0]
parallax = parallax[keep]
pdb.set_trace()
nfsamp = int(2e6) ##seems like 1-million samples is the ideal number for less ~1-2% pmem variation (and 100000 for group)
hist,binedges = np.histogram(parallax,bins=200,range=(1.0,100.0),density=True)
fplx          = rejsamp(binedges[0:200],hist,nfsamp)

pickle.dump(fplx,open('gaia_parallax_samples_kepfield.pkl','wb'))




print 'done'

