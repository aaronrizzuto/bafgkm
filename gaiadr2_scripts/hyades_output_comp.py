import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys,time,os,glob,pdb,pickle
from gcirc import gcirc
from readcol import readcol
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
import helpers
from numpy.lib.recfunctions import stack_arrays
import pandas as pd
import bspline_acr as bspline



def read_data(datadir,dirnames,paired_files):
    ##read everything in in the right order:
    
    for i in range(len(dirnames)):
        #thisdata = pickle.load(paired_files[i])
        datafiles = glob.glob(datadir+dirnames[i]+'/*pkl')
        ordering = np.argsort([int(ff.split('/')[-1].split('_')[1]) for ff in datafiles])
        
        for ii in range(len(ordering)):
            file = datafiles[ordering[ii]]
            if (ii==0) & (i==0): 

                stardat,pdmg,pdmf = pickle.load(open(file,'rb'))
            else:
                sd,pg,pf,pg2,pf2 = pickle.load(open(file,'rb'))
                stardat  = stack_arrays((stardat,sd),asrecarray=True,usemask=False)
                pdmg     = np.concatenate((pdmg,pg))
                pdmf     = np.concatenate((pdmf,pf))
                #pdmg_nrv = np.concatenate((pdmg_nrv,pg2))
                #pdmf_nrv = np.concatenate((pdmf_nrv,pf2))
        if i == 0: 
            data = pickle.load(open(paired_files[i],'rb'))
        else:
            data = stack_arrays((data, pickle.load(open(paired_files[i],'rb'))),asrecarray=True,usemask=False)
        
        ##NOW TEST THE EVERYTHING LINES UP CORRECTLY
        if np.sum(data.source_id - stardat.id) != 0: print 'datafile ' + str(i) + ' not matching supposed input'
        #pdb.set_trace()
    return stardat,pdmg,pdmf,data
        
            


##Taurus
#datadir   = '/Volumes/UT2/bafgkm_out_tacc/Taurus/tau_1_20180505-07:33:18/'
#data = readcol('/Volumes/UT1/gaia_dr2_tauaur.csv',fsep=',',asRecArray=True,nullval='') ##note that this will be out of order compared to starat once compiled
#inputfile = open(glob.glob(datadir + '*txt')[0],'rb').readline().split(',')[0]
#known = readcol('/Users/arizz/python/projects/K2pipe/membership/Tau_known_forpy.csv',fsep=',',asRecArray=True)


#USco:
#datadir = os.getenv('HOME')+'/data_local/lonestar_outputs/bafgkm_out/ScoCen/US_1_20180508-19:02:11/'
dfiles = ['hytest_20180704-16:01:20']
datalocation = './bafgkm_out/Hyades/'
pairfiles = ['gaia_dr2_hy_inputformat_20180703pair.pkl']

#data    = pickle.load(open('gaia_dr2_usco_inputformat_20180508pair.pkl','rb'))

stardat,pdmg,pdmf,data = read_data(datalocation,dfiles,pairfiles)
print 'loading done'
#pickle.dump((stardat,pdmg,pdmf,data),open('current_load_hyades.pkl','wb'))
#stardat,pdmg,pdmf,data=pickle.load(open('current_load_hyades.pkl','rb'))
#pdb.set_trace()

print 'dumping done'


flag =np.zeros(len(pdmg),dtype=int)
flyers = np.where((pdmf==0 ) & (pdmg == 0))[0]
pdmf[flyers] = 1.0
pdmg[flyers] = 1.0
flag[flyers] = 1
bfact = pdmg/pdmf*1.0
qwe = np.where(np.isinf(bfact))[0]
bfact[qwe]= 9.64452820e+296
flag[qwe] = 2

reg = np.where((stardat.plx > 10.0))[0]
prior = np.zeros(101)+10.0
for i in range(100): prior[i+1] = np.sum(bfact[reg]*prior[i]/(1+prior[i]*bfact[reg]))/(len(reg))
    #ppp = np.sum(pdmg/pdmf*prior[i]/(1+prior[i]*pdmg/pdmf))
    #prior[i+1] = ppp/len(pdmg)
#prior[100] = 3000.0/len(stardat)

prob = bfact*prior[100]/(bfact*prior[100]+1)
##prior =  0.025
best  = np.where(prob >= 0.5)[0]
best2 = np.where(prob >=0.9)[0]
qwe = np.where(pdmg[best2] < 1e-15)[0]
flag[best2[qwe]] = 3
#hmrv = np.where((data.phot_g_mean_mag[best] < 11.0))[0]
#pickle.dump((stardat[best[hmrv]],data[best[hmrv]],prob[best[hmrv]],prob2[best[hmrv]]),open('hmrv_sco_followup.pkl','wb'))





keep = np.where(prob >= 0.01)[0]
pickle.dump((stardat[keep],data[keep],prob[keep],flag[keep]),open('haydes_20percenters.pkl','wb'))



outfile =open('hyades20percenter_180705.csv','wb')
outfile.write('gid,ra,dec,pmem,flag \n')
for i in range(len(keep)):
    j=keep[i]
    thisline = str(data.source_id[j]) + ',' + str(data.ra[j]) + ',' + str(data.dec[j])+',' + str(prob[j])+',' + str(flag[j]) + ' \n'
    outfile.write(thisline)
outfile.flush()
outfile.close()


#plt.plot(data.phot_bp_mean_mag-data.phot_rp_mean_mag,data.phot_g_mean_mag+5-5*np.log10(1000.0/data.parallax),'k,')
#plt.plot(data.phot_bp_mean_mag[best]-data.phot_rp_mean_mag[best],data.phot_g_mean_mag[best]+5-5*np.log10(1000.0/data.parallax[best]),'rx')
#plt.plot(data.phot_bp_mean_mag[best2]-data.phot_rp_mean_mag[best2],data.phot_g_mean_mag[best2]+5-5*np.log10(1000.0/data.parallax[best2]),'bx')
#plt.plot(data.phot_bp_mean_mag[flyers]-data.phot_rp_mean_mag[flyers],data.phot_g_mean_mag[flyers]+5-5*np.log10(1000.0/data.parallax[flyers]),'go')
# 
#plt.ylim([17,0])




pdb.set_trace()
print 'Done'