import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys,time,os,glob,pdb,pickle
from gcirc import gcirc
from readcol import readcol
from ptcircle import circline 

##set matplotlib global params
mpl.rcParams['lines.linewidth']   =2
mpl.rcParams['axes.linewidth']    = 2
mpl.rcParams['xtick.major.width'] =2
mpl.rcParams['ytick.major.width'] =2
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['axes.labelsize'] = 15
mpl.rcParams['legend.numpoints'] = 1
mpl.rcParams['axes.labelweight']='semibold'
mpl.rcParams['mathtext.fontset']='stix'
mpl.rcParams['font.weight'] = 'semibold'

sys.path.append(os.getenv("HOME")+'/python/BAFGKM')

b10=readcol('bhac_bts_1020.txt',asRecArray=True)
b20=readcol('bhac_bts_20.txt',asRecArray=True)
bh20=b20
b40=readcol('bhac_bts_40.txt',asRecArray=True)


#data2= pickle.load(open('gaia_dr2_usco_inputformat_20180516pair.pkl','rb')) 
#plt.hist(data2.phot_g_mean_mag,bins=30)
#plt.show()
#pdb.set_trace()

stardat,data,prob,prob2,age,sig_age,mass,sig_mass,b40num=pickle.load(open('scocen_1percenters_bhac40.pkl','rb'))

rdata = pickle.load(open('random_subset.pkl','rb'))


absg = data.phot_g_mean_mag+5-5*np.log10(1000.0/data.parallax)
gmag= data.phot_g_mean_mag
qwe= np.where(np.isnan(age))[0]
age[qwe]=1000.0
b20num = np.interp(gmag-data.phot_rp_mean_mag,bh20.G[::-1]-bh20.G_RP[::-1],bh20.G[::-1])
samp= np.where(((age<200.0)  | (((prob>0.05) | (prob2>0.05) )& (age < 200.))) &(gmag > 14) & (gmag <=18))[0]

samp2 = np.where(((prob>0.05) | (prob2>0.05))&(gmag > 13) &(gmag < 17) &(b40num>absg))[0]
samp22 = np.where(((prob>0.05) | (prob2>0.05))&(gmag > 13) &(gmag < 17) &(b20num>absg))[0]
dsamp2 = np.where(((prob>0.5) | (prob2>0.5))&(gmag > 13) &(gmag < 17) &(b40num>absg))[0]

samp3 = np.where((gmag > 13) & (gmag < 17)&(b20num>absg))[0]
fullsamp= np.unique(np.concatenate((samp2,samp3)))
asd=  np.where(age < 50)[0]


##make mass/Teff figure
interp_teff = np.interp(absg[samp2],bh20.G[::-1],bh20.Teff[::-1])
interp_mass = np.interp(absg[samp2],bh20.G[::-1],bh20.Teff[::-1])

fig,ax=plt.subplots()
ax.hist(interp_teff,bins=10,color='b')
ax.set_xlabel('Effective Temperature (K)')
ax2=ax.twiny()
ax.set_xlim([3000.,4000.])
mticks= np.array([3200,3500,3800])
mmticks=  mticks.astype(str)
interp_tt= np.interp(mticks,bh20.Teff,bh20.Mass)
ax2.set_xlim(ax.get_xlim())
ax2.set_xticks(mticks)
ax2.set_xticklabels(np.round(interp_tt,decimals=2).astype(str))
ax2.set_xlabel("Mass "+r'(M$\mathbf{_\odot}$)')
plt.tight_layout()
plt.savefig('histogram_sample.pdf')
plt.show()
#pdb.set_trace()


#pdb.set_trace()
cc=3./2.0
hh,xx,yy,image = plt.hist2d(data.l[dsamp2],data.b[dsamp2],bins=[(360-280.)/cc/2,40./cc/2],cmap='Reds',range=[[280,360],[-10,30]],cmin=0)
plt.clf()

fig,ax=plt.subplots()

#xx=xx+1
#yy=yy+1
orig_hh= hh*1.0
qwe2 = np.where((xx>312) & (xx<340))[0]
mask = xx*0
mask[qwe2]=100
qwe = np.where(mask[0:len(mask)-1]<50)[0]
#pdb.set_trace()
hh[qwe] = 0
qwe2= np.where((yy<5))[0]
hh[:,qwe2]=0

hhh =hh.flatten()
zz = np.argsort(hhh)[::-1]
s2d= np.unravel_index(zz,hh.shape)

ax.plot(data.l[samp3],data.b[samp3],'.r',alpha=0.3,markersize=2)
ax.plot(data.l[samp2],data.b[samp2],'.k',alpha=0.8,markersize=3)
ax.set_xlim([360,295])
ax.set_ylim([-10,30])
ax.set_xlabel('Galactic Longitude '+r'($\mathbf{^\circ}$)')
ax.set_ylabel('Galactic Lattitude '+r'($\mathbf{^\circ}$)')
numfields=35
inpoints= np.zeros(numfields)
cenx = np.zeros(numfields)
ceny= np.zeros(numfields)
for i in range(numfields):
    rad=1.0
    cen1=[xx[s2d[0][i]]+cc,yy[s2d[1][i]]+cc]
    cenx[i]=cen1[0]*1.0
    ceny[i]= cen1[1]*1.0
    xc,yc=circline(rad,cen1)
    ax.plot(xc,yc,'b')
    inpoints[i]=len(np.where(np.sqrt((data.l[samp3]-cen1[0])**2 + (data.b[samp3]-cen1[1])**2)<1)[0])
    print inpoints[i]

hh = orig_hh*1
qwe2 = np.where((xx<312) & (xx>295))[0]
mask = xx*0
mask[qwe2]=100
qwe = np.where(mask[0:len(mask)-1]<50)[0]
#pdb.set_trace()
hh[qwe] = 0
hhh =hh.flatten()
zz = np.argsort(hhh)[::-1]
s2d= np.unravel_index(zz,hh.shape)
numfields=35
inpoints2= np.zeros(numfields)
cenx2 = np.zeros(numfields)
ceny2= np.zeros(numfields)
for i in range(numfields):
    rad=1.0
    cen1=[xx[s2d[0][i]]+cc,yy[s2d[1][i]]+cc]
    cenx2[i]=cen1[0]*1.0
    ceny2[i]= cen1[1]*1.0
    xc,yc=circline(rad,cen1)
    ax.plot(xc,yc,'g')
    inpoints2[i]=len(np.where(np.sqrt((data.l[samp3]-cen1[0])**2 + (data.b[samp3]-cen1[1])**2)<1)[0])
    print inpoints2[i]
    
    
hh = orig_hh*1
qwe2 = np.where((xx<345))[0]
hh[qwe2] = 0
hhh =hh.flatten()
zz = np.argsort(hhh)[::-1]
s2d= np.unravel_index(zz,hh.shape)
numfields=10
inpoints3= np.zeros(numfields)
cenx3 = np.zeros(numfields)
ceny3= np.zeros(numfields)
for i in range(numfields):
    rad=1.0
    cen1=[xx[s2d[0][i]]+cc,yy[s2d[1][i]]+cc]
    cenx3[i]=cen1[0]*1.0
    ceny3[i]= cen1[1]*1.0
    xc,yc=circline(rad,cen1)
    ax.plot(xc,yc,'r')
    inpoints3[i]=len(np.where(np.sqrt((data.l[samp3]-cen1[0])**2 + (data.b[samp3]-cen1[1])**2)<1)[0])
    print inpoints3[i]
    
##and V1062Sco
censco=[15*(16.+38/60.),-39*1-9/60.0]
censcolb = [343.5731755066894,+04.2815962654586]
xc,yc=circline(1.0,censcolb)
ax.plot(xc,yc,'m')
    
from ACRastro.glactc import glactc
from nice_radec import wrap_nice_radec
fout= open('fields_2df.txt','wb')
ra2,de2 = glactc(cenx2,ceny2,reverse=True,degree=True)
ra,de = glactc(cenx,ceny,reverse=True,degree=True)
ra3,de3 = glactc(cenx3,ceny3,reverse=True,degree=True)
nrd2 =wrap_nice_radec(ra2+360,de2,spc=' ',mspc=' & ')
nrd3 =wrap_nice_radec(ra3+360,de3,spc=' ',mspc=' & ')
nrd =wrap_nice_radec(ra+360,de,spc=' ',mspc=' & ')
nnn= wrap_nice_radec(np.array([censco[0]]),np.array([censco[1]]),spc=' ',mspc=' & ')
pdb.set_trace()
for i in range(len(cenx2)):
    fout.write('LCC '+str(i+1) +' & ' +nrd2[i]+' & ' + '13-17 & 1 \\\ \n' )
for i in range(len(cenx)):
    fout.write('UCL '+str(i+1) +' & ' +nrd[i]+' & ' + '13-17 & 1 \\\ \n' )
for i in range(len(cenx3)):
    fout.write('US '+str(i+1) +' & ' +nrd3[i]+' & ' + '13-17 & 1 \\\ \n' )
fout.write('V1062 Sco' +' & ' + nnn[0] +' & ' + '13-17 & 1 \\\ \n') 
fout.close()
plt.tight_layout()
plt.savefig('fieldpos.pdf')
plt.show()
#wrap_nice_radec(ra,dec,spc=' ',mspc=' '):
pdb.set_trace()


##now make HR diagram:
fig,ax=plt.subplots()
grp= rdata.phot_g_mean_mag-rdata.phot_rp_mean_mag
rabg = rdata.phot_g_mean_mag+5-5*np.log10(1000.0/rdata.parallax)
qqq = np.where(np.isnan(grp)==False)[0]
dd,xxx,yyy,imageo=ax.hist2d(grp[qqq],rabg[qqq],cmap='Greys',bins=[50,50],range=[[0.2,1.9],[2.2,12.0]])
ax.plot(data.phot_g_mean_mag[samp3]-data.phot_rp_mean_mag[samp3],data.phot_g_mean_mag[samp3]+5-5*np.log10(1000.0/data.parallax[samp3]),'r.',markersize=1,alpha=0.4)
ax.plot(data.phot_g_mean_mag[samp22]-data.phot_rp_mean_mag[samp22],data.phot_g_mean_mag[samp22]+5-5*np.log10(1000.0/data.parallax[samp22]),'k.',markersize=3)

ax.plot(b20.G-b20.G_RP,b20.G,'b')
ax.set_ylim([12,7.2])
ax.set_xlim([0.9,1.53])
ax.set_xlabel('G - RP (mag)')
ax.set_ylabel(r'M$\mathbf{_G}$ (mag)')
plt.tight_layout()
plt.savefig('2df_hrd.pdf')
plt.show()


pdb.set_trace()
plt.plot(data.phot_g_mean_mag[samp2]-data.phot_rp_mean_mag[samp2],data.phot_g_mean_mag[samp2]+5-5*np.log10(1000.0/data.parallax[samp2]),'k.',markersize=3)
plt.plot(data.phot_g_mean_mag[asd]-data.phot_rp_mean_mag[asd],data.phot_g_mean_mag[asd]+5-5*np.log10(1000.0/data.parallax[asd]),'k,')
plt.plot(data.phot_g_mean_mag[samp2]-data.phot_rp_mean_mag[samp2],data.phot_g_mean_mag[samp2]+5-5*np.log10(1000.0/data.parallax[samp2]),'k.',markersize=3)
plt.plot(b40.G-b40.G_RP,b40.G,'b')
plt.plot(b20.G-b20.G_RP,b20.G,'b')

plt.show()
pdb.set_trace()
print 'DONE'

data2= pickle.load(open('gaia_dr2_usco_inputformat_20180516pair.pkl','rb'))