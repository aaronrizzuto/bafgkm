import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys,time,os,glob,pdb,pickle
from gcirc import gcirc
from readcol import readcol
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
import helpers
from numpy.lib.recfunctions import stack_arrays
import pandas as pd
import bspline_acr as bspline
from uvwtopm import uvwtopm

stardat,data,prob,prob2 = pickle.load(open('scocen_20percenters.pkl','rb'))
best = np.where(prob > 0.8)[0]
print 'Data Loaded'


pdb.set_trace()


sig_rg_bar  = 25.0
posvel = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
cposvel = np.diag([sig_rg_bar**2,0.0,0.0,9.0,9.0,9.0])
distparams = np.array([0.0058992004,-0.0072425234])
uvw_a      = np.array([-0.13389093  ,    0.0089457638   ,     0.019123222])
uvw_b      = np.array([50.863992    ,    -20.810681     ,     -12.262665])
sigbf = 40.0
bslope = 0.20952391
bint = -54.98
sigbslope = -0.097688882
sigbint = 45.612

# 
best_bp = data.phot_bp_mean_mag[best] 
best_rp = data.phot_rp_mean_mag[best]
best_g = data.phot_g_mean_mag[best]
qwe = np.where(np.isnan(best_g+best_bp+best_rp)==False)[0]

# bg   = bspline.iterfit(best_bp[qwe]-best_rp[qwe],best_g[qwe]+5-5*np.log10(1000.0/data.parallax[best[qwe]]),bkspace = 0.5,nord=3,maxiter=2)
# testg,flagsg  = bg[0].value(best_bp[qwe]-best_rp[qwe])
# plt.plot(best_bp[qwe]-best_rp[qwe],best_g[qwe]+5-5*np.log10(1000.0/data.parallax[best[qwe]]),'k.')
# plt.plot(best_bp[qwe]-best_rp[qwe],testg,'r.')
# plt.plot(best_bp[qwe]-best_rp[qwe],testg+1,'r,')
# plt.plot(best_bp[qwe]-best_rp[qwe],testg+2,'r,')
# plt.plot(best_bp[qwe]-best_rp[qwe],testg+0.5,'r,')
# plt.ylim([16,-05])
# plt.show()
# 
# asd = np.where((best_g[qwe]+5-5*np.log10(1000.0/data.parallax[best[qwe]]) <10) & (best_g[qwe]+5-5*np.log10(1000.0/data.parallax[best[qwe]]) < testg+0.5))[0]
# asd2 = np.where((best_g[qwe]+5-5*np.log10(1000.0/data.parallax[best[qwe]]) <10) &  (best_g[qwe]+5-5*np.log10(1000.0/data.parallax[best[qwe]]) >= testg+0.5))[0]
# 
# plt.plot(data.l[best[qwe[asd]]],data.b[best[qwe[asd]]],'k.',alpha=1)
# plt.plot(data.l[best[qwe[asd2]]],data.b[best[qwe[asd2]]],'ro',alpha=0.75)
# plt.xlabel('l (degrees)')
# plt.xlabel('b (degrees)')
# plt.savefig('ScoCen_lowhrd.pdf')
# plt.show()
# 
# plt.clf()
# 
# pdb.set_trace()


exp_pmra  = np.zeros(len(data[best]))
exp_pmdec = np.zeros(len(data[best]))
exp_rv    = np.zeros(len(data[best]))
for i in range(len(data[best])):
    print 'up to ' +str(i+1) + ' out of ' + str(len(best))
    thisuvw = uvw_a*stardat.l[best[i]]*180/np.pi + uvw_b
    pmvect = uvwtopm(thisuvw,stardat.ra[best[i]],stardat.dec[best[i]],stardat.plx[best[i]])
    exp_rv[i]    = pmvect[0]*1.0
    exp_pmra[i]  = pmvect[1]*1.0
    exp_pmdec[i] = pmvect[2]*1.0

plotpmra = np.stack((stardat.pmra[best],exp_pmra),axis=-1)
plotpmdec = np.stack((stardat.pmdec[best],exp_pmdec),axis=-1)

dummy = helpers.arrow_plotter(data.ra[best],data.dec[best],plotpmra[:,0],plotpmdec[:,0],galco=True,outfilename='ScoCen_gdr2_arrows_80percenters.pdf',plotlimits=[[359,290],[-10,30]],cols = ['k'])


pad10 = readcol('pad10myr_gaia.txt',asRecArray=True)
pad20 = readcol('pad20myr_gaia.txt',asRecArray=True)
pad1g = readcol('pad1gyr_gaia.txt',asRecArray=True)

p10br = pad10.G_BP-pad10.G_RP
p10G = pad10.G
p20br = pad20.G_BP-pad20.G_RP
p20G = pad20.G
p1gbr = pad1g.G_BP-pad1g.G_RP
p1gG = pad1g.G


isos = np.array([[p10br,p10G],[p20br,p20G],[p1gbr,p1gG]])


#pdb.set_trace()
dummy = helpers.gaia_HRD(data.phot_bp_mean_mag[best],data.phot_rp_mean_mag[best],data.phot_g_mean_mag[best],data.parallax[best],outfilename='ScoCen_gdr2_HRD_80percenters.pdf',isos=isos,isolabels=['PARSEC 10 Myr','PARSEC 20 Myr','PARSEC 1 Gyr'])
##,outfilename='ScoCen_gdr2_HRD_80percenters.pdf'

#plt.plot(data.phot_bp_mean_mag[best]-data.phot_rp_mean_mag[best],data.phot_g_mean_mag[best]+5-5*np.log10(1000.0/data.parallax[best]),'k,')

#plt.ylim([17,5])




pdb.set_trace()
print 'Done'


