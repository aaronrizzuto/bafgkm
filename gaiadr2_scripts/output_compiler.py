import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys,time,os,glob,pdb,pickle
from gcirc import gcirc
from readcol import readcol
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
import helpers
from numpy.lib.recfunctions import stack_arrays
import pandas as pd
import bspline_acr as bspline

##Taurus
#datadir   = '/Volumes/UT2/bafgkm_out_tacc/Taurus/tau_1_20180505-07:33:18/'
#data = readcol('/Volumes/UT1/gaia_dr2_tauaur.csv',fsep=',',asRecArray=True,nullval='') ##note that this will be out of order compared to starat once compiled
#inputfile = open(glob.glob(datadir + '*txt')[0],'rb').readline().split(',')[0]
#known = readcol('/Users/arizz/python/projects/K2pipe/membership/Tau_known_forpy.csv',fsep=',',asRecArray=True)

#USco:
#datadir = os.getenv('HOME')+'/data_local/lonestar_outputs/bafgkm_out/ScoCen/US_1_20180508-19:02:11/'
datadir = '/Volumes/UT2/bafgkm_out_tacc/ScoCen/US_2_20180510-13:38:41/'
#data    = pickle.load(open('gaia_dr2_usco_inputformat_20180508pair.pkl','rb'))
known = readcol(os.getenv('HOME')+'/python/projects/K2pipe/membership/'+'ScoCenAll_forpython.csv',fsep=',',asRecArray=True)

data=pd.read_csv('/Volumes/UT1/gaia_dr2_usco.csv', sep=',').to_records()
keep = np.where((data.l >280) & (data.b <= 35) & (data.b >= 0.0) & (data.parallax >=4))[0]
print len(keep),len(data)
data = data[keep]
#pdb.set_trace()


#pdb.set_trace()
##combine the output files in the correct ordering
datafiles = glob.glob(datadir+'*pkl') 
indexes = [int(ff.split('/')[-1].split('_')[1]) for ff in datafiles]
ordering = np.argsort(indexes)


print 'Reading ' + datadir
for ii in range(len(ordering)):
    file = datafiles[ordering[ii]]
    if ii == 0: stardat, pdmg,pdmf,pdmg_nrv,pdmf_nrv = pickle.load(open(file,'rb'))
    if ii != 0: 
        sd,pg,pf,pg2,pf2 = pickle.load(open(file,'rb'))
        stardat  = stack_arrays((stardat,sd),asrecarray=True,usemask=False)
        pdmg     = np.concatenate((pdmg,pg))
        pdmf     = np.concatenate((pdmf,pf))
        pdmg_nrv = np.concatenate((pdmg_nrv,pg2))
        pdmf_nrv = np.concatenate((pdmf_nrv,pf2))
print 'Data loaded'
if np.sum(stardat.id - data.source_id) >0:
    print 'in/out mismatch!!??!!'
    pdb.set_trace()

flyers = np.where((pdmg==0) & (pdmf==0))[0]
pdmf[flyers] = 1.0
pdmg[flyers] = 0.0
pdmg_nrv[flyers] = 0.0
pdmf_nrv[flyers] = 1.0

bfact = pdmg/pdmf*1.0
bf2 = pdmg_nrv/pdmf_nrv
reg = np.where((stardat.pmra < 0.0) & (stardat.pmdec < 0.0))[0]
prior = np.zeros(101)+10.0
for i in range(100): prior[i+1] = np.sum(pdmg[reg]/pdmf[reg]*prior[i]/(1+prior[i]*pdmg[reg]/pdmf[reg]))/(len(reg))
    #ppp = np.sum(pdmg/pdmf*prior[i]/(1+prior[i]*pdmg/pdmf))
    #prior[i+1] = ppp/len(pdmg)
#prior[100] = 3000.0/len(stardat)

prob = pdmg/pdmf*prior[100]/(pdmg/pdmf*prior[100]+1)

best  = np.where(prob >= 0.5)[0]
best2 = np.where(prob >=0.9)[0]
asd  = np.where((pdmg < 1e-8) & (prob > 0.9))[0]
qwe = np.where((pdmg > 1e-6) & (prob > 0.9))[0]


# 
# plt.show()
pdb.set_trace()
knownmem = np.zeros(len(stardat),dtype='|S21')
recov    = np.zeros(len(known),dtype=int)
for i in range(len(known)):
    dist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,known.radeg[i],known.decdeg[i])
    close = np.argmin(dist)
    print str(i) + ' / ' + str(len(known))
   # pdb.set_trace()
    if dist[close] < 10.0:
        knownmem[close] = known.mem[i]
        recov[i] = 1
#    pdb.set_trace()
        



pdb.set_trace()

#datafile = 'gaia_dr2_tauaur_inputformat_20180504.pkl'
#(stardatooo,galah_idooo,rvsourceooo,rposvel_field) = pickle.load(open(datafile,'rb'))

##5317
##uvw=(10.05964498792061, -19.226151993492238, -7.1195458163785865)








pdb.set_trace()
print 'Done'