import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from astropy.io import fits
import os,glob,sys,pdb,time,pickle
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
import idlsave
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
from gcirc import gcirc
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from taurus_paramgen import taurus_paramgen
from taurus_paramgen import taurus_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from scipy.special import erf as scipyerf
from scipy.stats import truncnorm
#np.set_printoptions(suppress=True,precision=2)
from readcol import readcol
import pandas as pd

datestamp = time.strftime('%Y%m%d', time.localtime(time.time()))
#datadir = os.getenv("HOME") + '/data_local/'
datadir = '/Volumes/UT1/'
#dfile = 'gaia_dr2_tauaur.csv'
#data = pd.read_csv(datadir + dfile, sep=',').to_records()
#pdb.set_trace()

#dfile = 'gaia_dr2_usco.csv'
#dfile = 'gaia_dr2_ucl.csv'
#dfile = 'gaia_dr2_lcc.csv'
dfile = 'gaia_dr2_100pc.csv'

#outname = dfile.split('.')[0] + '_inputformat_' + datestamp + '.pkl'
#outname2 = dfile.split('.')[0] + '_inputformat_' + datestamp + 'pair.pkl'
outname = 'gaia_dr2_tuchor' + '_inputformat_' + datestamp + '.pkl'
outname2 = 'gaia_dr2_tuchor' + '_inputformat_' + datestamp + 'pair.pkl'
###!!!!NOTE MAKE SURE TO OUTPUT A FULL GAIA DR2 file for the prepared inputs to use for analysis later
##get the main gaia dr2 file 
#data = readcol(datadir + dfile,fsep=',',asRecArray = False,names=True,nullval='')
data=pd.read_csv(datadir+dfile, sep=',').to_records()

#data= np.recfromcsv(datadir+dfile, delimiter=',',filling_values=np.nan)



##apply any further cuts to the main input here:
#pdb.set_trace()
##US and UCL cut out below zero
#for LCC can leave them in?
#keep = np.where((data.l >280) & (data.b <= 35) & (data.b >= -10.0) & (data.parallax >=4))[0]

#keep = np.where((data.l >280) & (data.b <= 35) & (data.b >= 0.0) & (data.parallax >=4))[0]




##keep =np.where((data.ra > 40) & (data.ra < 90) & (data.dec > -10) & (data.dec <50) & (data.parallax/data.parallax_error > 15))[0] ##hyades
keep =np.where((data.ra >= 300) | (data.ra <= 90) & (data.parallax/data.parallax_error > 10))[0] ##tuchor


print len(keep),len(data)
data = data[keep]
#pdb.set_trace()
pickle.dump(data,open(outname2,'wb'))
pdb.set_trace()
##get the galah data currently DR2 catalog
galah =  fits.open(datadir+'GALAH_DR2_catalog.fits')[1].data
##first cross_match everything with Galah to get as many RV's as possible
##can add more things like Teff here in the future
simpcut = np.where((galah.raj2000 >= np.min(data.ra)-0.1) & (galah.raj2000 <= np.max(data.ra)+0.1) & (galah.dej2000 >= np.min(data.dec)-0.1) & (galah.dej2000 <= np.max(data.dec)+0.1))[0]
galah = galah[simpcut]
gl,gb = glactc(galah.raj2000,galah.dej2000,2000)
simpcut = np.where((gl<np.max(data.l)-0.1) & (gl > np.min(data.l)+0.1) & (gb > np.min(data.b)-0.1) & (gb < np.max(data.b)+0.1) )[0]
galah = galah[simpcut]

#pdb.set_trace()
rvg = np.zeros(len(data))-999.999
ervg = np.zeros(len(data))+999.999
galah_id = np.zeros(len(data),'|S16')
if len(simpcut) > 0:
    for i in range(len(simpcut)):
        print 'Galah cross ' + str(i) + ' / ' + str(len(simpcut))

        reg = np.where((np.absolute(data.ra-galah.raj2000[i]) < 0.2) & (np.absolute(data.dec-galah.dej2000[i]) < 0.2))[0]
        if len(reg)  > 0:
            mmm = gcirc(galah.raj2000[i],galah.dej2000[i],data.ra[reg],data.dec[reg])
            close = np.argmin(mmm)
            if mmm[close] < 10.0:
                if np.isnan(galah.rv_obst[i]) == False:
                    rvg[reg[close]]  = galah.rv_obst[i]*1.0
                    ervg[reg[close]] = galah.e_rv_obst[i]*1.0
                    galah_id[reg[close]] = galah.star_id[i]
                   # pdb.set_trace()
                    print 'found one;'
#pdb.set_trace()   
##now take the smallest RV for every object
finalrv  = np.zeros(len(data)) + 0.0
efinalrv = np.zeros(len(data)) + 300.0
rvsource = np.zeros(len(data),int)+0
for i in range(len(data)):
    if ervg[i] < 999:
        if (ervg[i] < data.radial_velocity_error[i]) | (np.isnan(data.radial_velocity_error[i]) == True): 
            finalrv[i]  = rvg[i]*1.0
            efinalrv[i] = ervg[i]*1.0
            rvsource[i]=2
    elif np.isnan(data.radial_velocity[i]) == False:
        finalrv[i]  = data.radial_velocity[i]
        efinalrv[i] = data.radial_velocity_error[i]
        rvsource[i]=1
##now ask for gcrv and pulkovo rv's
gcrv = pickle.load(open(os.getenv("HOME")+'/data_local/catalogs/gcvr/gcrv.pkl','rb'))
pulk = fits.open(os.getenv('HOME')+'/data_local/catalogs/pulkovo_rv/pulkovofull.fits')[1].data

rvcat  = np.zeros(len(data))-999.999
srvcat = np.zeros(len(data))+999.999
for i in range(len(data)):
    print i
    dist = gcirc(data.ra[i],data.dec[i],gcrv.RA,gcrv.DEC)
    close = np.argmin(dist)
    if dist[close]<=10.0:
        if gcrv.SIG_RV[close] < efinalrv[i]:
            finalrv[i] = gcrv.RV[close]
            efinalrv[i] = gcrv.SIG_RV[close]
            rvsource[i] = 2
            #pdb.set_trace()
    coarse = np.where((np.absolute(data.ra[i]-pulk._RA) < 0.2) & (np.absolute(data.dec[i]-pulk._DE) < 0.2))[0]
    if len(coarse) > 0:
        dist = gcirc(data.ra[i],data.dec[i],pulk._RA[coarse],pulk._DE[coarse])
        close = np.argmin(dist)
        if dist[close] <= 10.0:
            if pulk.e_RV[coarse[close]] < efinalrv[i]:
                finalrv[i] = pulk.RV[coarse[close]]
                efinalrv[i] = pulk.e_RV[coarse[close]]
                rvsource[i] = 3
                #pdb.set_trace()
                
jrv = readcol(os.getenv('HOME')+'/python/projects/ScoCen/joint_rvtab.csv' ,fsep=',',asRecArray=True)
for i in range(len(jrv)):
    dist = gcirc(jrv.ra[i],jrv.dec[i],data.ra,data.dec)
    close  = np.argmin(dist)
    if dist[close] <= 10.0:
        if (jrv.binglf[i] != '') | (jrv.sig_finalrv[i] < efinalrv[close]) : 
            finalrv[close] = jrv.finalrv[i]
            efinalrv[close] = jrv.sig_finalrv[i]
            rvsource[close]=4    
#pdb.set_trace()



##now produce the stardat structure for running the mem selection


stardat = np.recarray(len(data.ra),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',int),('pmcov',float),('plxpmra_cov',float),('plxpmdec_cov',float)])
stardat.id        = data.source_id
stardat.ra        = data.ra*np.pi/180
stardat.dec       = data.dec*np.pi/180
stardat.pmra      = data.pmra
stardat.pmdec     = data.pmdec
stardat.sig_pmra  = data.pmra_error
stardat.sig_pmdec = data.pmdec_error
stardat.plx       = data.parallax
stardat.sig_plx   = data.parallax_error
stardat.rv        = finalrv
stardat.sig_rv    = efinalrv
stardat.l         = data.l*np.pi/180.0
stardat.b         = data.b*np.pi/180.0
stardat.pmcov = data.pmra_pmdec_corr*data.pmra_error*data.pmdec_error
stardat.plxpmra_cov = data.parallax_pmra_corr*data.pmra_error*data.parallax_error
stardat.plxpmdec_cov = data.parallax_pmdec_corr*data.pmdec_error*data.parallax_error

##generate Field sample information ##this is bad inside of ~10 parsecs
fplx = pickle.load(open('gaia_parallax_samples_kepfield.pkl','rb'))
zxc = np.where((fplx > 10))[0]
fplx = fplx[zxc]
nfsamp=1000000
hist,binedges = np.histogram(stardat.plx,bins=200,range=(np.min(stardat.plx),np.max(stardat.plx)),density=True)
fplx          = rejsamp(binedges[0:200],hist,nfsamp)
uvw_field     = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = 1000.0/fplx


#pdb.set_trace()




pickle.dump((stardat,galah_id,rvsource,rposvel_field),open(outname,'wb'))
pickle.dump(data,open(outname2,'wb'))
pdb.set_trace()
print 'Done, output file is: ' + outname





