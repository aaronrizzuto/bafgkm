import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys,time,os,glob,pdb,pickle
from gcirc import gcirc
from readcol import readcol
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
import helpers
from numpy.lib.recfunctions import stack_arrays
import pandas as pd
import bspline_acr as bspline



def read_data(datadir,dirnames,paired_files):
    ##read everything in in the right order:
    
    for i in range(len(dirnames)):
        #thisdata = pickle.load(paired_files[i])
        datafiles = glob.glob(datadir+dirnames[i]+'/*pkl')
        ordering = np.argsort([int(ff.split('/')[-1].split('_')[1]) for ff in datafiles])
       # pdb.set_trace()
        for ii in range(len(ordering)):
            file = datafiles[ordering[ii]]
            if (ii==0) & (i==0): 
                stardat,pdmg,pdmf,pdmg_nrv,pdmf_nrv = pickle.load(open(file,'rb'))
            else:
                sd,pg,pf,pg2,pf2 = pickle.load(open(file,'rb'))
                stardat  = stack_arrays((stardat,sd),asrecarray=True,usemask=False)
                pdmg     = np.concatenate((pdmg,pg))
                pdmf     = np.concatenate((pdmf,pf))
                pdmg_nrv = np.concatenate((pdmg_nrv,pg2))
                pdmf_nrv = np.concatenate((pdmf_nrv,pf2))
        if i == 0: 
            data = pickle.load(open(paired_files[i],'rb'))
            qwe = np.random.choice(len(data),size=100000)
            pickle.dump(data[qwe],open('random_subset.pkl','wb'))
           # pdb.set_trace()

           # pdb.set_trace()
        else:
            data = stack_arrays((data, pickle.load(open(paired_files[i],'rb'))),asrecarray=True,usemask=False)

        ##NOW TEST THE EVERYTHING LINES UP CORRECTLY
        if np.sum(data.source_id - stardat.id) != 0: print 'datafile ' + str(i) + ' not matching supposed input'
        
    return stardat,pdmg,pdmf,pdmg_nrv,pdmf_nrv,data
        
            


##Taurus
#datadir   = '/Volumes/UT2/bafgkm_out_tacc/Taurus/tau_1_20180505-07:33:18/'
#data = readcol('/Volumes/UT1/gaia_dr2_tauaur.csv',fsep=',',asRecArray=True,nullval='') ##note that this will be out of order compared to stardat once compiled
#inputfile = open(glob.glob(datadir + '*txt')[0],'rb').readline().split(',')[0]
#known = readcol('/Users/arizz/python/projects/K2pipe/membership/Tau_known_forpy.csv',fsep=',',asRecArray=True)


#USco:
dfiles = ['US_2_20180510-13:38:41','UCL_1_20180511-18:01:34','LCC_1_20180511-18:09:34']
agepfiles= ['../../taurus_project/US_ageprob.pkl','../../taurus_project/UCL_ageprob.pkl','../../taurus_project/LCC_ageprob.pkl']

datalocation = '/Volumes/UT2/bafgkm_out_tacc/ScoCen/'
#datalocation= os.getenv("HOME") +'/data_local/lonestar_outputs/bafgkm_out/ScoCen/'
pairfiles = ['gaia_dr2_usco_inputformat_20180516pair.pkl','gaia_dr2_ucl_inputformat_20180511pair.pkl','gaia_dr2_lcc_inputformat_20180511pair.pkl']

#data    = pickle.load(open('gaia_dr2_usco_inputformat_20180508pair.pkl','rb'))

reload = False
if reload == True:
    print('making new compilation')
    stardat,pdmg,pdmf,pdmg_nrv,pdmf_nrv,data = read_data(datalocation,dfiles,pairfiles)

    import pandas as pd
    pddata = pd.DataFrame.from_records(data)
    pdstardat = pd.DataFrame.from_records(stardat)
    pddata.to_csv('sco_data_compiled_190325.csv')
    pdstardat.to_csv('sco_stardat_compiled_190325.csv')
    pickle.dump((pdmg,pdmf,pdmg_nrv,pdmf_nrv),open('compiled_sco_output_190325.pkl','wb'))

if reload == False:
    print('reading existing compilation')
    import pandas as pd
    data = pd.DataFrame.from_csv('sco_data_compiled_190325.csv').to_records()
    stardat = pd.DataFrame.from_csv('sco_stardat_compiled_190325.csv').to_records()
    pdmg,pdmf,pdmg_nrv,pdmf_nrv = pickle.load(open('compiled_sco_output_190325.pkl','rb'))
    
    

#pickle.dump((stardat,pdmg,pdmf,pdmg_nrv,pdmf_nrv,data),open('current_load_sco.pkl','wb'))
#stardat,pdmg,pdmf,pdmg_nrv,pdmf_nrv,data=pickle.load(open('current_load_sco.pkl','rb'))
#pdb.set_trace()


#pdb.set_trace()


flyers = np.where((pdmg==0) & (pdmf==0))[0]
flyers2 = np.where((pdmg_nrv==0) & (pdmf_nrv==0))[0]
pdmf[flyers] = 1.0
pdmg[flyers] = 0.0
pdmg_nrv[flyers2] = 0.0
pdmf_nrv[flyers2] = 1.0

bfact = pdmg/pdmf*1.0
bf2 = pdmg_nrv/pdmf_nrv





u_param = data.astrometric_chi2_al/(data.astrometric_n_good_obs_al - 5)
b_r = data.phot_bp_mean_mag-data.phot_rp_mean_mag
ef = data.phot_bp_rp_excess_factor
g = data.phot_g_mean_mag
gexp = np.exp(-0.2*(g-19.5))
qwe = np.where(gexp < 1.0)[0]
gexp[qwe] = 1.0
testmask = np.zeros(len(data),int)

goodrow = np.where((u_param < 1.2*gexp) & (ef >1.+0.015*b_r**2) & (ef < 1.3+0.06*b_r**2))[0]
testmask[goodrow] = 1
gr = data.phot_g_mean_mag-data.phot_rp_mean_mag
absg = g+5-5*np.log10(1000.0/data.parallax)
qwe = np.where((absg[goodrow] > 9.6) & (gr[goodrow] < 0.85))[0]
masker = np.zeros(len(goodrow),dtype=int)
masker[qwe] = 1
goodgood = np.where(masker ==0 )[0]
goodrow = goodrow[goodgood]
qwe = np.where((absg > 9.6) & (gr < 0.85))[0]
bfact[qwe] = 1.0

##lets define an emipirical main-sequence:
mmm = 8.5
inter = 2.25
lineval = gr*mmm+inter
use = np.where((u_param < 1.2*gexp) & (ef >1.+0.015*b_r**2) & (ef < 1.3+0.06*b_r**2) & (absg<lineval))[0]
car = np.random.choice(len(use),100000)
ocar = car.copy()
from bspline_acr import iterfit 
for i in range(4):
    bs = iterfit(gr[use[car]],absg[use[car]],bkspace=0.5,nord=3,maxiter=10)
    testg,flagsg = bs[0].value(gr[use[car]])
    rms = np.sqrt(np.mean((testg-absg[use[car]])**2))
    off = (testg-absg[use[car]])/rms
  
    qwe = np.where(off < 1.3)[0]
   # plt.plot(gr[use[car]],absg[use[car]],'k.')
   # plt.plot(gr[use[car[qwe]]],absg[use[car[qwe]]],'r.')
   # plt.plot(gr[use[car]],testg,'r,')
   # plt.show()
    #pdb.set_trace()
    car = car[qwe]
    
testg,flagsg = bs[0].value(gr[use[ocar]])
    
#plt.plot(gr[use[ocar]],absg[use[ocar]],'k.')
#plt.plot(gr[use[ocar]],testg,'r,')
#plt.show()
  
#bg   = bspline.iterfit(gjgj,ggg,bkspace = 0.7,nord=3,maxiter=10)
#    testg,flagsg  = bg[0].value(gjgj)


#pdb.set_trace()



reg = np.where((stardat.pmra < 0.0) )[0]##& (stardat.plx < 20.0) & (stardat.plx > 4.0) & (data.phot_g_mean_mag<18))[0]
#reg=np.where(data.phot_g_mean_mag < 18)[0]
prior = np.zeros(101)+10.0
prior2= np.zeros(101)+10.0
for i in range(100): prior[i+1] = np.sum(pdmg[reg]/pdmf[reg]*prior[i]/(1+prior[i]*pdmg[reg]/pdmf[reg]))/(len(reg))
for i in range(100):prior2 [i+1] = np.sum(pdmg_nrv[reg]/pdmf_nrv[reg]*prior2[i]/(1+prior2[i]*pdmg_nrv[reg]/pdmf_nrv[reg]))/(len(reg))
    #ppp = np.sum(pdmg/pdmf*prior[i]/(1+prior[i]*pdmg/pdmf))
    #prior[i+1] = ppp/len(pdmg)
#prior[100] = 3000.0/len(stardat)
prob = prior[-1]*pdmg/pdmf/(1+pdmg/pdmf*prior[-1])
prob2 = prior[-1]*pdmg_nrv/pdmf_nrv/(1+pdmg_nrv/pdmf_nrv*prior[-1])
best=  np.where(((prob > 0.2) | (prob2>0.2)) & (data.phot_g_mean_mag < 15))[0]


p2 = pd.read_csv('p1g.txt',sep='\s+')
p20 = pd.read_csv('p20m.txt',sep='\s+')

p20gr = p20.Gmag[0:60]-p20.G_RPmag[0:60]
p20g = p20.Gmag[0:60]

p2gr = p2.Gmag[0:60]-p2.G_RPmag[0:60]
p2g = p2.Gmag[0:60]

p2gi = np.interp(gr[best],p2gr[::-1],p2g[::-1])
p20gi=np.interp(gr[best],p20gr[::-1],p20g[::-1])

qwe=  np.where(gr[best] <0.4)[0]##things we don't want to worry about here:

p20d  = -absg[best] + p20gi
p20d[qwe] = -99
p2d  = -absg[best]+p2gi
p2d[qwe] = -100

mmm = np.where(np.absolute(p20d)<np.absolute(p2d))[0]





plt.plot(gr[use[ocar]],absg[use[ocar]],'k.',label= 'Random Field')
plt.plot(gr[best],absg[best],'.',label='P>0.5 low')
plt.plot(gr[best[mmm]],absg[best[mmm]],'.g',label='P>0.5 in')
plt.plot(p2.Gmag[0:60]-p2.G_RPmag[0:60],p2.Gmag[0:60],'r',label='Parsec 1Gyr')
plt.plot(p20.Gmag[0:70]-p20.G_RPmag[0:70],p20.Gmag[0:70],'m',label='Parsec 20Myr')
plt.xlim([-0.45,1.5])
plt.ylim([7.5,-2.5])
plt.legend()
plt.savefig('Sco_G12_50_msd.pdf')
plt.clf()

plt.plot(gr[use[ocar]],absg[use[ocar]],'k.',label= 'Random Field')
plt.plot(gr[best],absg[best],'.',label='P<0.5 low')
plt.plot(gr[best[mmm]],absg[best[mmm]],'.g',label='P>0.5 in')
plt.plot(p2.Gmag[0:60]-p2.G_RPmag[0:60],p2.Gmag[0:60],'r',label='Parsec 1Gyr')
plt.plot(p20.Gmag[0:70]-p20.G_RPmag[0:70],p20.Gmag[0:70],'m',label='Parsec 20Myr')
plt.xlim([0.3,1.1])
plt.ylim([7.5,3.5])
plt.legend()
plt.savefig('Sco_G12_50_msd_zoom.pdf')

#plt.show()

pdb.set_trace()
ffff = open('Sco_G15_20_msd.pkl','wb')
pdb.set_trace()
pickle.dump((prob[best],prob2[best],p20d,p2d),ffff)
pdb.set_trace()
xxx = pd.DataFrame.from_records(stardat[best])
xxx.to_csv('Sco_G15_20_msd_stardat.csv')
xxx= pd.DataFrame.from_records(data[best])
xxx.to_csv('Sco_G15_20_msd.csv')
ffff.close()

pdb.set_trace()


hist,edge = np.histogram(gr,bins=15,range=[-0.2,1.5])
binassign = np.digitize(gr,edge[0:len(edge)-1])
qwe = np.where(binassign >= len(edge)-1)[0]
binassign[qwe] = len(edge)-2
qwe = np.where(binassign <= edge[0])[0]
binassign[qwe] = 0
prior = np.zeros((101,len(edge)-1))+10.0

goodrow = np.where(stardat.id > 0)[0]
for i in range(len(prior)-1):     prior[i+1] = np.histogram(gr,bins=15,range=[-.2,1.5],weights=prior[i,binassign]*bfact/(prior[i,binassign]*bfact+1))[0]/np.histogram(gr[goodrow],bins=15,range=[-.2,1.5])[0]

    #pdb.set_trace()
    #print i
    #pdb.set_trace()
    #theseprobs = prior[i,binassign]*bfact/(prior[i,binassign]*bfact+1)
    #prior[i+1] = np.sum(pdmg[reg]/pdmf[reg]*prior[i]/(1+prior[i]*pdmg[reg]/pdmf[reg]))/(len(reg)-np.sum(pdmg[reg]/pdmf[reg]*prior[i]/(1+prior[i]*pdmg[reg]/pdmf[reg])))

prob = prior[-1,binassign]*bfact/(prior[-1,binassign]*bfact+1)


best = np.where(prob > 0.5)[0]

import pandas as pd 
p2 = pd.read_csv('parsec2gyr.txt',sep='\s+')
p20 = pd.read_csv('parsec20m.txt',sep='\s+')
plt.plot(gr[use[ocar]],absg[use[ocar]],'k.')
plt.plot(gr[best],absg[best],'.')
plt.plot(p2.Gmag-p2.G_RPmag,p2.Gmag,'k')
plt.plot(p20.Gmag-p20.G_RPmag,p20.Gmag,'k')
plt.show()

pdb.set_trace()
#hmrv = np.where((data.phot_g_mean_mag[best] < 11.0))[0]
#pickle.dump((stardat[best[hmrv]],data[best[hmrv]],prob[best[hmrv]],prob2[best[hmrv]]),open('hmrv_sco_followup.pkl','wb'))


#keep = np.where(prob >= 0.2)[0]
#pickle.dump((stardat[keep],data[keep],prob[keep],prob2[keep]),open('scocen_20percenters.pkl','wb'))

# 
# b40=readcol('bhac_bts_40.txt',asRecArray=True)
# b40gr = b40.G[::-1]-b40.G_RP[::-1]
# b40g  = b40.G[::-1]
# 
# b40num = np.interp(data.phot_g_mean_mag-data.phot_rp_mean_mag,b40gr,b40g)
# #qwe = np.where(b40num >= data.phot_g_mean_mag+5-5*np.log10(1000.0/data.parallax))[0]
# 
# keep = np.where((prob >= 0.05)| (prob2>= 0.05)| (age<=200))[0]
# >>>>>>> ca92bda2f529a8ad3e53979ed2efbf2fecf65b01
# 
# #keep = np.where((((prob >= 0.01)| (prob2>= 0.01))  | (b40num >= data.phot_g_mean_mag+5-5*np.log10(1000.0/data.parallax))) & (data.phot_g_mean_mag <  18.001))[0]
# keep = np.where(((prob>0.01) | (prob2>0.01)) | (b40num >= data.phot_g_mean_mag+5-5*np.log10(1000.0/data.parallax)))[0]
# k2= np.where(data.phot_g_mean_mag[keep]<18.0001)[0]
# keep=keep[k2]
# 
# 
# 
# p15 = readcol('parsec1.2s_15Myr_Solar_evansgaia.txt',asRecArray=True)
# kk = np.where((np.isnan(data.phot_rp_mean_mag[best])==False) & (np.isnan(data.phot_bp_mean_mag[best])==False))[0]
# br =data.phot_bp_mean_mag -data.phot_rp_mean_mag
# mstar = np.interp(br[best[kk]],p15.G_BPmag[0:90][::-1]-p15.G_RPmag[0:90][::-1],p15.Mass[0:90][::-1])
# 
# pickle.dump((stardat[keep],data[keep],prob[keep],prob2[keep],age[keep],sig_age[keep],mass[keep],sig_mass[keep],b40num[keep]),open('scocen_1percenters_bhac40.pkl','wb'))
# 
# pdb.set_trace()





plt.plot(data.phot_bp_mean_mag-data.phot_rp_mean_mag,data.phot_g_mean_mag+5-5*np.log10(1000.0/data.parallax),'k,')
plt.plot(data.phot_bp_mean_mag[best]-data.phot_rp_mean_mag[best],data.phot_g_mean_mag[best]+5-5*np.log10(1000.0/data.parallax[best]),'rx')
plt.plot(data.phot_bp_mean_mag[best2]-data.phot_rp_mean_mag[best2],data.phot_g_mean_mag[best2]+5-5*np.log10(1000.0/data.parallax[best2]),'bx')
#plt.plot(data.phot_bp_mean_mag[flyers]-data.phot_rp_mean_mag[flyers],data.phot_g_mean_mag[flyers]+5-5*np.log10(1000.0/data.parallax[flyers]),'go')
plt.ylim([17,0]) 
plt.show()


pdb.set_trace()

known = readcol(os.getenv('HOME')+'/python/projects/K2pipe/membership/'+'ScoCenAll_forpython.csv',fsep=',',asRecArray=True)
knownmem = np.zeros(len(stardat),dtype='|S21')
recov    = np.zeros(len(known),dtype=int)
for i in range(len(known)):
    dist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,known.radeg[i],known.decdeg[i])
    close = np.argmin(dist)
    print str(i) + ' / ' + str(len(known))
   # pdb.set_trace()
    if dist[close] < 10.0:
        knownmem[close] = known.mem[i]
        recov[i] = 1
#    pdb.set_trace()
        
pickle.dump(knownmem,open('sco_knownmem_cross.pkl','wb'))[0]


pdb.set_trace()




pdb.set_trace()
print 'Done'