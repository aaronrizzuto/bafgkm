import numpy as np
import os,glob,sys,pdb,time,pickle
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
sys.path.append(os.getenv("HOME")+'/BAFGKM')
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from scipy.special import erf as scipyerf
from scipy.stats import truncnorm
from readcol import readcol
from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE

##define the communicator
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
ncor = size
jobID = str(sys.argv[1])
subg = str(sys.argv[2])

##first identify the directory structure for output
homedir = os.getenv("HOME")
if homedir == '/Users/acr2877':
    machinename = 'laptop'
    basedir = '/Users/acr2877/python/BAFGKM/gaiadr2_scripts/'
if homedir == '/Users/arizz':
    machinename = 'laptop'
    basedir = '/Users/arizz/python/BAFGKM/gaiadr2_scripts/'
if homedir == '/home1/03602/arizzuto':
    machinename = 'lonestar'
    basedir     = '/work/03602/arizzuto/lonestar/'
datestamp    = time.strftime('%Y%m%d-%H:%M:%S', time.localtime(time.time())) ##output with starting timestamp so all runs are distinguishable

outdir = basedir+'bafgkm_out/ScoCen/' + jobID + '_'+datestamp+'/'
if rank == 0:
    if os.path.exists(outdir): 
        print 'Failed Due to Pre-existing output!!' ##SHOULD NEVER HAPPEN!!
        comm.Abort()
    else: os.makedirs(outdir)

##we now have input data to be run, and field sample parallaxes all in the standard format 
##for the selection:
if subg == 'US' : datafile = 'gaia_dr2_usco_inputformat_20180508.pkl'
if subg == 'UCL': datafile = 'gaia_dr2_ucl_inputformat_20180511.pkl'
if subg == 'LCC': datafile = 'gaia_dr2_lcc_inputformat_20180511.pkl'
if (subg != 'LCC') & (subg !='UCL') & (subg != 'US'): 
    print 'which subgroup is this mate!!'
    comm.Abort()
(stardat,galah_id,rvsource,rposvel_field) = pickle.load(open(datafile,'rb'))
rf_field   = np.sqrt(rposvel_field[:,0]**2+rposvel_field[:,1]**2 + rposvel_field[:,2]**2)
#rposvel_field = rposvel_field[:,3:6] ##just for testing!!!!    
##Bin the field parallaxes to produce a weighted histogram:
prf_mf,rf_binedges = np.histogram(rf_field,bins=200)
prf_mf = prf_mf*1.0/len(rf_field)
rf_bins  = rf_binedges[0:len(rf_binedges)-1] + (rf_binedges[1]-rf_binedges[0])/2.0

#pdb.set_trace()

#bmin= np.min(stardat.b)*180/np.pi
#bmax= np.max(stardat.b)*180/np.pi
bmin=-10.0
bmax = 40.0

##for testing use these to target specific objects
#aaa = np.where((stardat.id == 6235747125966268928) | (stardat.id == 6233740654686482560	))[0]
#aaa = np.where(stardat.id == 6266179271356066688)[0]
#aaa = np.where(stardat.id == 6007264520906861824)[0]
#stardat = stardat[0:1]
#stardat  = stardat[aaa]

nfsamp = len(rposvel_field)
nsamp  = 1000000

if rank==0: ##write down the name of the datafile and particular information for logging purposes.
    infofile = open(outdir + 'infolog.txt','wb')
    infofile.write(datafile + ',' + str(nsamp)+','+str(nfsamp) +' \n')
    infofile.flush()

#run the membership selection, first figure out who is doing what and divvy up the input dataset
nstars = len(stardat) ##the number of input stars to run
s   = nstars/ncor
nx  = (s+1)*ncor - nstars
ny  = nstars - s*ncor
if rank <= nx-1: 
    mynumber = s
    myrange = [(rank)*s,(rank+1)*s]
if rank > nx-1: 
    mynumber = s+1
    myrange = [nx*s+(rank-nx)*mynumber,nx*s+(rank-nx+1)*mynumber]
##cut out my subset:
stardat = stardat[myrange[0]:myrange[1]]
#print rank,myrange


##now it's clear how many test are running on each processor, tell the user
if rank == 0: print 'Using ' + str(nx) + ' cores of size ' + str(s) + ' and ' +str(ny) + ' cores of size ' + str(s+1)

#pdb.set_trace()
sig_rg_bar  = 25.0
posvel = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
cposvel = np.diag([sig_rg_bar**2,0.0,0.0,9.0,9.0,9.0])
distparams = np.array([0.0058992004,-0.0072425234])
uvw_a      = np.array([-0.13389093  ,    0.0089457638   ,     0.019123222])
uvw_b      = np.array([50.863992    ,    -20.810681     ,     -12.262665])
sigbf = 40.0
bslope = 0.20952391
bint = -54.98
sigbslope = -0.097688882
sigbint = 45.612
chunks = 3

pdmf = np.zeros(len(stardat))
pdmg = np.zeros(len(stardat))
pdmg_nrv = np.zeros(len(stardat))
pdmf_nrv = np.zeros(len(stardat))
for i in range(len(stardat)):

    thisuvw = uvw_a*stardat.l[i]*180/np.pi + uvw_b
    thisdist = (distparams[0]*np.cos(stardat.l[i])+ distparams[1]*np.sin(stardat.l[i]))**(-1)
    thisposvel = np.array([thisdist,0.0,0.0,thisuvw[0],thisuvw[1],thisuvw[2]])
    rposvel = build_sample_group(thisposvel,cposvel,0.0,nsamp,trace_forward=False,observables=False)
    
##the field and group samples are now build, do the integrals

##Sky b position:
    thisb     = stardat.l[i]*180/np.pi*bslope + bint
    this_sigb = stardat.l[i]*180/np.pi*sigbslope + sigbint
    tnormg = truncnorm.pdf(stardat.b[i]*180/np.pi,bmin,bmax,loc=thisb,scale=this_sigb)
    tnormf = truncnorm.pdf(stardat.b[i]*180/np.pi,bmin,bmax,loc=0.0,scale=sigbf)

##DISTANCE INTEGRAL
#FIELD:
    pD_mf = np.sum(1/np.sqrt(2*np.pi*stardat.sig_plx[i]**2)*np.exp(-(1000.0/rf_bins - stardat.plx[i])**2/2/stardat.sig_plx[i]**2)*prf_mf)
#GROUP:
    prg_mg = np.exp(-(rf_bins-thisdist)**2/2/sig_rg_bar**2)*prf_mf
    prg_mg = prg_mg/np.sum(prg_mg)
    pD_rg  = 1/np.sqrt(2*np.pi*stardat.sig_plx[i]**2)*np.exp(-(1000.0/rf_bins-stardat.plx[i])**2/2/stardat.sig_plx[i]**2)
    pD_mg  = np.sum(prg_mg*pD_rg)
   # pdb.set_trace()
    #parallax_factor = pD_mg/pD_mf

##VELOCITY
##FIELD INTEGRAL:
    pdmf[i] = dataset_looper(stardat[i:i+1],rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False,mode='distanced')*tnormf*pD_mf
##GROUP INTEGRAL
##field == True means not using positions in the integral
    pdmg[i] = dataset_looper(stardat[i:i+1],rposvel,chunksize=chunks,field=True,verbose=True,doall=False,mode='distanced')*tnormg*pD_mg
    ##print stardat.sig_rv[i]
    oldsrv = stardat.sig_rv[i]*1.0
##GROUP INTERVAL WITH NO RV:
    if stardat.sig_rv[i] < 299.999:
        stardat.sig_rv[i] = 300.0
        pdmg_nrv[i] = dataset_looper(stardat[i:i+1],rposvel,chunksize=chunks,field=True,verbose=True,doall=False,mode='distanced')*tnormg*pD_mg
        pdmf_nrv[i] = dataset_looper(stardat[i:i+1],rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False,mode='distanced')*tnormf*pD_mf
        stardat.sig_rv[i] = oldsrv*1.0
    else: 
        pdmf_nrv[i],pdmg_nrv[i] = pdmf[i],pdmg[i]
    
    
#pdb.set_trace()
#pdb.set_trace()
pickle.dump((stardat,pdmg,pdmf,pdmg_nrv,pdmf_nrv),open(outdir + 'result_'+str(rank)+'_'+datestamp+'.pkl','wb'))


print 'Im Done ' + str(rank)