import numpy as np
import os,glob,sys,pdb,time,pickle
sys.path.append(os.getenv("HOME")+'/BAFGKM')
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from scipy.special import erf as scipyerf
from scipy.stats import truncnorm
from readcol import readcol

from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE

##define the communicator
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
ncor = size
jobID = str(sys.argv[1])

##first identify the directory structure for output
homedir = os.getenv("HOME")
if homedir == '/Users/acr2877':
    machinename = 'laptop'
    basedir = '/Users/acr2877/python/BAFGKM/gaiadr2_scripts/'
if homedir == '/Users/arizz':
    machinename = 'laptop'
    basedir = '/Users/arizz/python/BAFGKM/gaiadr2_scripts/'
if homedir == '/home1/03602/arizzuto':
    machinename = 'lonestar'
    basedir     = '/work/03602/arizzuto/lonestar/'
datestamp    = time.strftime('%Y%m%d-%H:%M:%S', time.localtime(time.time())) ##output with starting timestamp so all runs are distinguishable

outdir = basedir+'bafgkm_out/Taurus/' + jobID + '_'+datestamp+'/'
if rank == 0:
    if os.path.exists(outdir): 
        print 'Failed Due to Pre-existing output!!' ##SHOULD NEVER HAPPEN!!
        comm.Abort()
    else: os.makedirs(outdir)

##we now have input data to be run, and field sample parallaxes all in the standard format 
##for the selection:
datafile = 'gaia_dr2_tauaur_inputformat_20180504.pkl'
(stardat,galah_id,rvsource,rposvel_field) = pickle.load(open(datafile,'rb'))

##for testing use these to target specific objects
#aaa = np.where((stardat.id == '145213192171159552') | (stardat.id == '151374202498079872') | (stardat.id == '147869784062378624'))[0]
#stardat  = stardat[aaa]

nfsamp = len(rposvel_field)
nsamp  = 100000

if rank==0: ##write down the name of the datafile and particular information for logging purposes.
    infofile = open(outdir + 'infolog.txt','wb')
    infofile.write(datafile + ',' + str(nsamp)+','+str(nfsamp) +' \n')
    infofile.flush()

#uvw_g,  = np.array([ 15.7, -11.3, -10.1])
##build random group sampling
sig_int,dsig = 3.0,15.0
posvel = np.array([0.0,0.0,145.0, 15.7, -11.3, -10.1])    
cposvel =  np.zeros([6,6])
cvel = np.array([[sig_int**2,0.0,0.0],[0.0,sig_int**2,0.0],[0.0,0.0,sig_int**2]])
cpos = np.array([[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,dsig**2]])
cposvel[0:3,0:3] = cpos*1.0
cposvel[3:6,3:6] = cvel*1.0
if rank == 0 : 
    infofile.write(np.array_str(posvel) + '\n')
    infofile.write(np.array_str(cposvel) + '\n')
    infofile.flush()
rposvel = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=False,observables=False)


#run the membership selection, first figure out who is doing what and divvy up the input dataset
nstars = len(stardat) ##the number of input stars to run
s   = nstars/ncor
nx  = (s+1)*ncor - nstars
ny  = nstars - s*ncor
if rank <= nx-1: 
    mynumber = s
    myrange = [(rank)*s,(rank+1)*s]
if rank > nx-1: 
    mynumber = s+1
    myrange = [nx*s+(rank-nx)*mynumber,nx*s+(rank-nx+1)*mynumber]
##cut out my subset:
stardat = stardat[myrange[0]:myrange[1]]
#print rank,myrange


##now it's clear how many test are running on each processor, tell the user
if rank == 0: print 'Using ' + str(nx) + ' cores of size ' + str(s) + ' and ' +str(ny) + ' cores of size ' + str(s+1)


##the field and group samples are now build, do the integrals
chunks = 3
#stardat = stardat[0:2]##testing
##field integral
pdmodf = dataset_looper(stardat,rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False)

##GROUP INTEGRAL
##field == True means not using positions in the integral
pdmodg = dataset_looper(stardat,rposvel,chunksize=chunks,field=True,verbose=True,doall=False)
stardat.sig_rv = 300.0
pdmodg_nrv = dataset_looper(stardat,rposvel,chunksize=chunks,field=True,verbose=True,doall=False)

pickle.dump((stardat,pdmodg,pdmodf,pdmodg_nrv),open(outdir + 'result_'+str(rank)+'_'+datestamp+'.pkl','wb'))


print 'Im Done ' + str(rank)