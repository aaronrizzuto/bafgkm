import numpy as np
import os,glob,sys,pdb,time,pickle
sys.path.append(os.getenv("HOME")+'/BAFGKM')
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from scipy.special import erf as scipyerf
from scipy.stats import truncnorm
from readcol import readcol
from ymg_paramgen import ymg_paramgen
from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE
from rejsamp import rejsamp
from rejsamp import rejsamp_opt
from gal_xyz import gal_xyz
from circular_stats import circmean
from circular_stats import circvar
##define the communicator
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
ncor = size
jobID = str(sys.argv[1])

##first identify the directory structure for output
homedir = os.getenv("HOME")
if homedir == '/Users/acr2877':
    machinename = 'laptop'
    basedir = '/Users/acr2877/python/BAFGKM/gaiadr2_scripts/'
if homedir == '/Users/arizz':
    machinename = 'laptop'
    basedir = '/Users/arizz/python/BAFGKM/gaiadr2_scripts/'
if homedir == '/home1/03602/arizzuto':
    machinename = 'lonestar'
    basedir     = '/work/03602/arizzuto/lonestar/'
datestamp    = time.strftime('%Y%m%d-%H:%M:%S', time.localtime(time.time())) ##output with starting timestamp so all runs are distinguishable

outdir = basedir+'bafgkm_out/tuchor/' + jobID + '_'+datestamp+'/'
if rank == 0:
    if os.path.exists(outdir): 
        print 'Failed Due to Pre-existing output!!' ##SHOULD NEVER HAPPEN!!
        comm.Abort()
    else: os.makedirs(outdir)

##we now have input data to be run, and field sample parallaxes all in the standard format 
##for the selection:
datafile = 'gaia_dr2_tuchor_inputformat_20180806.pkl'
(stardat,galah_id,rvsource,rposvel_field) = pickle.load(open(datafile,'rb'))
#pdb.set_trace()







nfsamp = 1000000
nsamp  = 1000000


#regen the field sample, dont trust input
hist,binedges = np.histogram(1000.0/stardat.plx,bins=200,range=(np.min(1000.0/stardat.plx),np.max(1000.0/stardat.plx)),density=True)
fdist         = rejsamp(binedges[0:200],hist,nfsamp)
uvw_field     = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = fdist

#rposvel_field = rposvel_field[:,3:6] ##just for testing!!!!    
##Bin the field parallaxes to produce a weighted histogram:
rf_field   = np.sqrt(rposvel_field[:,0]**2+rposvel_field[:,1]**2 + rposvel_field[:,2]**2)

prf_mf,rf_binedges = np.histogram(rf_field,bins=200)
prf_mf = prf_mf*1.0/len(rf_field)
rf_bins  = rf_binedges[0:len(rf_binedges)-1] + (rf_binedges[1]-rf_binedges[0])/2.0



##for testing use these to target specific objects
#aaa = np.where((stardat.id == 6410889501381373440))[0]
#aaa = np.where((stardat.id == 4742040513540707072))[0] ##af hor
#aaa = np.where((stardat.id == 2163592940776952832))[0]
#aaa = np.where(stardat.id == 2625620710428582272)[0]
#stardat  = stardat[aaa]
#pdb.set_trace()

#4662475625916613376, 4721443877613450240, 4876399082108508416,
#       6350316615532992000, 6410889501381373440, 6458256943341776000


if rank==0: ##write down the name of the datafile and particular information for logging purposes.
    infofile = open(outdir + 'infolog.txt','wb')
    infofile.write(datafile + ',' + str(nsamp)+','+str(nfsamp) +' \n')
    infofile.flush()


##build random group sampling
sig_int,dsig = 2.0,15.0
posvel,spv,rposvel = ymg_paramgen('tuchor',num=nsamp)
rgd = np.sqrt(rposvel[:,0]**2 + rposvel[:,1]**2 + rposvel[:,2]**2)
rg_bar = np.mean(rgd)
sig_rg_bar = np.std(rgd)
rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
lmean      = circmean(rgl)
if lmean < 0.0: lmean += 2*np.pi
lsig       = np.sqrt(circvar(rgl))
bmean      = circmean(rgb)
if bmean > np.pi/2: bmean =  bmean-2*np.pi
bsig       = np.sqrt(circvar(rgb))
#import pdb
#pdb.set_trace()

if rank == 0 : 
    infofile.write(np.array_str(posvel) + '\n')
    infofile.write(np.array_str(spv) + '\n')
    infofile.flush()


#run the membership selection, first figure out who is doing what and divvy up the input dataset
nstars = len(stardat) ##the number of input stars to run
s   = nstars/ncor
nx  = (s+1)*ncor - nstars
ny  = nstars - s*ncor
if rank <= nx-1: 
    mynumber = s
    myrange = [(rank)*s,(rank+1)*s]
if rank > nx-1: 
    mynumber = s+1
    myrange = [nx*s+(rank-nx)*mynumber,nx*s+(rank-nx+1)*mynumber]
##cut out my subset:
stardat = stardat[myrange[0]:myrange[1]]
#print rank,myrange


##now it's clear how many test are running on each processor, tell the user
if rank == 0: print 'Using ' + str(nx) + ' cores of size ' + str(s) + ' and ' +str(ny) + ' cores of size ' + str(s+1)

pdmodf = np.zeros(len(stardat.id))
pdmodg  = np.zeros(len(stardat.id))
pdmodg_np = np.zeros(len(stardat.id))
for i in range(len(stardat.id)):
    ##the field and group samples are now build, do the integrals
    chunks = 1
    
    ##distance integral
    pD_mf = np.sum(1/np.sqrt(2*np.pi*stardat.sig_plx[i]**2)*np.exp(-(1000.0/rf_bins - stardat.plx[i])**2/2/stardat.sig_plx[i]**2)*prf_mf)
    #GROUP:
    prg_mg = np.exp(-(rf_bins-rg_bar)**2/2/sig_rg_bar**2)*prf_mf
    prg_mg = prg_mg/np.sum(prg_mg)
    pD_rg  = 1/np.sqrt(2*np.pi*stardat.sig_plx[i]**2)*np.exp(-(1000.0/rf_bins-stardat.plx[i])**2/2/stardat.sig_plx[i]**2)
    pD_mg  = np.sum(prg_mg*pD_rg)

    ##now do the position for the group
    pl_mg = np.max([1/np.sqrt(2*np.pi*lsig**2)*np.exp(-(lmean-stardat.l[i])**2/2./lsig**2),1/np.sqrt(2*np.pi*lsig**2)*np.exp(-(lmean-stardat.l[i]+2*np.pi)**2/2./lsig**2)])
    pb_mg = 1/np.sqrt(2*np.pi*bsig**2)*np.exp(-(bmean-stardat.b[i])**2/2./bsig**2)

    ##field integral
    pdmodf[i] = dataset_looper(stardat[i:i+1],rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False,mode='distanced')*pD_mf

    ##GROUP INTEGRAL
    ##field == True means not using positions in the integral
    pdmodg_np[i] = dataset_looper(stardat[i:i+1],rposvel,chunksize=chunks,field=False,verbose=True,doall=False,mode='distanced')*pD_mg
    pdmodg[i] = pdmodg_np[i]*pl_mg*pb_mg
    #pdb.set_trace()






##output to file
pickle.dump((stardat,pdmodg,pdmodf,pdmodg_np),open(outdir + 'result_'+str(rank)+'_'+datestamp+'.pkl','wb'))


print 'Im Done ' + str(rank)