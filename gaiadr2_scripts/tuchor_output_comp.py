import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys,time,os,glob,pdb,pickle
from gcirc import gcirc
from readcol import readcol
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
import helpers
from numpy.lib.recfunctions import stack_arrays
import pandas as pd
uvw = np.array([9.9,-20.9,-1.4])
mpl.rcParams['lines.linewidth']   = 1.5
mpl.rcParams['axes.linewidth']    = 2
mpl.rcParams['xtick.major.width'] =2
mpl.rcParams['ytick.major.width'] =2
mpl.rcParams['ytick.labelsize'] = 15
mpl.rcParams['xtick.labelsize'] = 15
mpl.rcParams['axes.labelsize'] = 15
mpl.rcParams['legend.numpoints'] = 1
mpl.rcParams['axes.labelweight']='semibold'
mpl.rcParams['axes.titlesize']=15
mpl.rcParams['axes.titleweight']='semibold'
mpl.rcParams['font.weight'] = 'semibold'
def idcheck(stardat,prob,prob2,gaiaid):
    qwe = np.where(stardat.id == gaiaid)[0]
    if len(qwe) == 0:
        print 'not in stardat'
        return -1,-1
    return prob[qwe],prob2[qwe]
    


def read_data(datadir,dirnames,paired_files):
    ##read everything in in the right order:
    
    for i in range(len(dirnames)):
        datafiles = glob.glob(datadir+dirnames[i]+'/*pkl')
        ordering = np.argsort([int(ff.split('/')[-1].split('_')[1]) for ff in datafiles])
        #pdb.set_trace()
        for ii in range(len(ordering)):
            file = datafiles[ordering[ii]]
            if (ii==0) & (i==0): 

                stardat,pdmg,pdmf,pdmg2 = pickle.load(open(file,'rb'))
            else:
                sd,pg,pf,pg2 = pickle.load(open(file,'rb'))
                stardat  = stack_arrays((stardat,sd),asrecarray=True,usemask=False)
                pdmg     = np.concatenate((pdmg,pg))
                pdmf     = np.concatenate((pdmf,pf))
                pdmg2  = np.concatenate((pdmg2,pg2))
        if i == 0: 
            data = pickle.load(open(paired_files[i],'rb'))
        else:
            data = stack_arrays((data, pickle.load(open(paired_files[i],'rb'))),asrecarray=True,usemask=False)
        
        ##NOW TEST THE EVERYTHING LINES UP CORRECTLY
        if np.sum(data.source_id - stardat.id) != 0: print 'datafile ' + str(i) + ' not matching supposed input'
    
    return stardat,pdmg,pdmf,pdmg2,data
        
            


##TUCHOR ##laptop setup
dfiles = ['TUCHOR1_20180807-16:31:20']
datalocation = '/Volumes/UT2/bafgkm_out_tacc/tuchor/'
pairfiles = ['gaia_dr2_tuchor_inputformat_20180806pair.pkl']


stardat,pdmg,pdmf,pdmg2, data = read_data(datalocation,dfiles,pairfiles)
print 'loading done'

#pdb.set_trace()


flag =np.zeros(len(pdmg),dtype=int)
flag2 =np.zeros(len(pdmg),dtype=int)

flyers = np.where((pdmf==0 ) & (pdmg == 0))[0]
flyers2 = np.where((pdmf==0 ) & (pdmg2 == 0))[0]
pdmf[flyers] = 1.0
pdmg[flyers] = 1.0
pdmg2[flyers2] = 1.0
flag[flyers] = 1
flag2[flyers2] = 1
bfact = pdmg/pdmf*1.0
bfact2 = pdmg2/pdmf*1.0
qwe = np.where(np.isinf(bfact))[0]
qwe2 = np.where(np.isinf(bfact2))[0]
bfact[qwe]= 1.0
bfact2[qwe]= 1.0
flag[qwe] = 2
flag2[qwe2] = 2

prior = np.zeros(101)+0.01
prior2 = np.zeros(101)+0.01
reg = np.where((stardat.dec*180/np.pi < -15) & (stardat.plx >0))[0]
for i in range(100): prior[i+1] = np.sum(bfact[reg]*prior[i]/(1+prior[i]*bfact[reg]))/(len(reg))
for i in range(100): prior2[i+1] = np.sum(bfact2[reg]*prior2[i]/(1+prior2[i]*bfact2[reg]))/(len(reg))
prior[100]=0.01
prior2[100]=0.01

prob = bfact*prior[100]/(bfact*prior[100]+1)
prob2 = bfact2*prior2[100]/(bfact2*prior2[100]+1)
best  = np.where(prob >= 0.5)[0]
best2 = np.where(prob >=0.9)[0]
best3 = np.where(prob2 >=0.9)[0]
best4 = np.where(prob2 >=0.5)[0]
#pdb.set_trace()


##make a HR diagram:
bb = data.phot_bp_mean_flux_over_error
rr = data.phot_rp_mean_flux_over_error
gg = data.phot_g_mean_flux_over_error

br = data.phot_bp_mean_mag - data.phot_rp_mean_mag
absg = data.phot_g_mean_mag +5.-5.*np.log10(1000.0/data.parallax)

samp = np.where((prob > 0.5) & (np.isnan(bb) == False) & (np.isnan(rr) ==False) & (bb > 10) & (rr>10))[0]
samp2 = np.where((prob2 > 0.5) & (np.isnan(bb) == False) & (np.isnan(rr) ==False) & (bb > 10) & (rr>10))[0]
samp3 = np.where((prob > 0.9) & (np.isnan(bb) == False) & (np.isnan(rr) ==False) & (bb > 10) & (rr>10))[0]
samp4 = np.where((prob > 0.2) & (np.isnan(bb) == False) & (np.isnan(rr) ==False) & (bb > 10) & (rr>10))[0]


fig,ax = plt.subplots()
ax.plot(br[samp2],absg[samp2],'k.',label='No Position >50%')
ax.plot(br[samp4],absg[samp4],'r.',label='>20%')
ax.plot(br[samp],absg[samp],'b.',label='>50%')
ax.plot(br[samp3],absg[samp3],'gx',label='>90%')
ax.set_ylim([17.5,0])
plt.legend()
ax.set_ylabel('Absolute G (mag)')
ax.set_xlabel('BP-RP (mag)')
fig.tight_layout()
fig.savefig('tuchor_select.pdf')
plt.close(fig)
#plt.show()

##now cross with all of Adams list
rerun_check=False
#rerun_check=True
if rerun_check == True:
    import pandas as pd
    from astroquery.vizier import Vizier
    import astropy.units as u
    import astropy.coordinates as coord
    kt2 = pd.read_csv(os.getenv('HOME') +'/data_local/kraus_tuchor/kraustuc_table2.txt',delimiter='|').to_records()
    Vizier.columns=['all']

    kt2ids = np.zeros(len(kt2),dtype=int)-99
    for i in range(len(kt2.tmass)):
        print str(i+1) + ' out of ' + str(len(kt2))
        stuff = Vizier.query_region('2MASS ' +kt2.tmass[i],radius="0d0m3s",catalog='I/345/gaia2')
        if len(stuff) > 1: pdb.set_trace()
        if len(stuff) == 1:
            rrr = stuff[0]['_r']
            qwe = np.argmin(rrr)
            kt2ids[i] = stuff[0]['Source'][qwe]
    pickle.dump((kt2,kt2ids),open('Gaia_ids_kraus_table2.pkl','wb'))
kt2,kt2ids = pickle.load(open('Gaia_ids_kraus_table2.pkl','rb'))

xkt = np.zeros(len(kt2ids),dtype=int)-99 
for i in range(len(kt2ids)):
    mm = np.where(stardat.id == kt2ids[i])[0]
    if len(mm) >1 :pdb.set_trace()
    if len(mm) > 0:
        xkt[i] = mm[0]

zz = np.where(xkt != -99)[0]
xk = xkt[zz]
ym = np.where(kt2.mem[zz] == 'Y')[0]
nm = np.where(kt2.mem[zz] == 'N')[0]

uvw = np.array([9.9,-20.9,-1.4])


##make a list:
from nice_radec import wrap_nice_radec
of = open('tuchor_selection_180808.csv','wb')
of.write('gaiaID,ra,dec,rad,decd,G,RP,BP,prob1,prob2,plx,pmra,pmdec,kraus14mem \n')
for i in range(len(stardat)):
    if (prob[i] > 0.1) | (prob2[i] > 0.1):
        thisnr = wrap_nice_radec(stardat.ra[i:i+1]*180/np.pi,stardat.dec[i:i+1]*180/np.pi,spc=' ',mspc=',')
        qwe = np.where(kt2ids == stardat.id[i])[0]
        thismem = ''

        if len(qwe) > 0: thismem = kt2.mem[qwe[0]]
        thisline = 'G' + str(stardat.id[i]) + ',' + thisnr[0] + ',' + str(stardat.ra[i]*180/np.pi)+ ',' +str(stardat.dec[i]*180/np.pi) +','+ str(data.phot_g_mean_mag[i])+','+ str(data.phot_bp_mean_mag[i])+','+ str(data.phot_rp_mean_mag[i]) +','+ str(prob[i]) +','+str(prob2[i])+','+str(stardat.plx[i])+','+str(stardat.pmra[i])+','+str(stardat.pmdec[i])+','+thismem+' \n'
        of.write(thisline)
of.flush()
of.close()









#pdb.set_trace()
print 'Done'