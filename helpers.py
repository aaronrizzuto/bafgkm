import numpy as np
import pdb
import time

from bayes_code import multi_star_integral
from bayes_code import multi_star_integral_distanced
from cp_math import find_cp
##UTILS IMPORTS
from gal_xyz import gal_xyz


##CURRENTLY THERE IS ONLY ONE PREFUNCTION

##DATASET LOOPER
#This function will iteratively call multi_star_integral() on chunks of the 
##data. 
##For use when there are too many stars to do in one go
## dataset_looper will take star data (rundat np.array([nstars,14])), 
##the random group samples (rposvel,np.array([nsamp,6])), and a 
##chunking size (chunksize, int) as inputs 
## and output the prob(data|model) for each star in rundat for the group random sample.
#@profile
def dataset_looper(rundat,rposvel,chunksize=1000,field=False,verbose=True,doall=False,mode='none'):
	##option to run everything at once without thinking about chunksize
	if doall==True:
		chunksize = rundat.ra.shape[0]
		
	##figure out how many loops we need
	numloops = rundat.ra.shape[0]/chunksize
	if np.mod(rundat.ra.shape[0],chunksize) != 0:
		numloops = numloops+1

	## Transform the random group positions into l,b,plx just once (only for non distanced mode)
	if mode != 'distanced' : rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
	## Find Convergent Points just once
	galcon,eqcon  = find_cp(rposvel[:,3:6],equatorial=True) 

	##appending is slow, initialise the output variable
	output = np.zeros(rundat.ra.shape[0])

	##now the loop
	for ix in range(0,numloops):
		lt1 = time.clock()
		thischunk = rundat[ix*chunksize:(ix+1)*chunksize]
		if mode == 'distanced':output[ix*chunksize:(ix+1)*chunksize] = multi_star_integral_distanced(thischunk,rposvel,galcon,eqcon)
		else: output[ix*chunksize:(ix+1)*chunksize] = multi_star_integral(thischunk,rposvel,galcon,eqcon,rgl,rgb,rgplx,field=field)

		if (verbose == True) & (doall==False):
			tneed = (time.clock()-lt1)*(numloops-ix)/60.0/60.0
			print "Time Remaining: " +str(np.round(tneed,1)) + " hours"
		
	return output	

def arrow_plotter(ra,dec,pmra,pmdec,outfilename='',galco=False,cols=None,scalenumber=100.0,sclocation='br',plotlimits=None):
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    mpl.rcParams['lines.linewidth']   =3
    mpl.rcParams['axes.linewidth']    = 2
    mpl.rcParams['xtick.major.width'] =2
    mpl.rcParams['ytick.major.width'] =2    
    mpl.rcParams['ytick.labelsize'] = 10
    mpl.rcParams['xtick.labelsize'] = 10
    mpl.rcParams['axes.labelsize'] = 12
    mpl.rcParams['legend.numpoints'] = 1
    mpl.rcParams['axes.labelweight']='semibold'
    mpl.rcParams['mathtext.fontset']='stix'
    mpl.rcParams['font.weight'] = 'semibold'
    if cols == None:   cols=['k','b','r','g','m','c','y','teal']
    ###if labels == None: labels = ['1','2','3','4','5','6','7','8','9','10']

    if len(pmra.shape) == 1: 
        seriesnum = 1
        pmra  = np.expand_dims(pmra, axis=1)
        pmdec = np.expand_dims(pmdec,axis=1)
    else: seriesnum = pmra.shape[1]

    if galco == True:
        from ACRastro.glactc import glactc
        x,y   = glactc(ra,dec,degree=True)
        x2,y2 =  pmra*0.0,pmdec*0.0
        dx,dy =  pmra*0.0,pmdec*0.0
        for i in range(seriesnum):
            x2[:,i],y2[:,i] = glactc(ra+pmra[:,i]/3600.0,dec+pmdec[:,i]/3600.0,degree=True)
            dx[:,i],dy[:,i] = (x2[:,i]-x)*3600,(y2[:,i]-y)*3600
    else:
        x,y=ra,dec
        dx,dy=pmra,pmdec
    
    fig,ax=plt.subplots()
    ww = 0.003

    for i in range(seriesnum): 
        usealph = 1.0
        if i > 0 :usealph = 0.2
        ax.quiver(x,y,dx[:,i]/3600.*scalenumber,dy[:,i]/3600*scalenumber,width=ww,color=cols[i],angles='xy', scale_units='xy',
                    scale=1,headlength=1.5,headwidth=1.5,alpha=usealph)
    
    #pdb.set_trace()

    if sclocation == 'br': xs,ys,ys2 = np.max(x)-10,np.min(y)+5,np.min(y)+5+1.5
    if sclocation == 'tr': xs,ys,ys2 = np.max(x)-10,np.max(y)-5,np.max(y)-51.5
    if sclocation == 'bl': xs,ys,ys2 = np.min(x)+5,np.min(y)+5,np.min(y)+5+1.5
    if sclocation == 'tl': xs,ys,ys2 = np.min(x)+5,np.max(y)-5,np.max(y)-5-1.5
    
    sizer = np.round(0.1*np.median(np.sqrt(dx**2+dy**2)))*10*5
    if plotlimits != None: sizer*=(plotlimits[0][1]-plotlimits[0][0])/np.absolute(plotlimits[0][1]-plotlimits[0][0])
    sizerstr = int(np.round(np.absolute(sizer)))
    ax.quiver(xs,ys,sizer/3600*scalenumber,0.0,width=ww,color=cols[i],angles='xy', scale_units='xy', scale=1,headlength=1.5,headwidth=1.5)
    ax.text(xs,ys2,str(sizerstr)+' mas/yr',size=5)
    if galco == False:
        ax.set_xlabel(r'R.A. ($^{\circ}$)')
        ax.set_ylabel(r'Decl. ($^{\circ}$)')
    else:
        ax.set_xlabel(r'$\mathbf{l}$ ($^{\circ}$)')
        ax.set_ylabel(r'$\mathbf{b}$ ($^{\circ}$)')
            
    if plotlimits != None:
        ax.set_xlim(plotlimits[0])
        ax.set_ylim(plotlimits[1])    
    #ax.legend()
    if outfilename != '': 
        plt.savefig(outfilename)
    else: plt.show()
    plt.clf()
    plt.close(fig)
    return -1
    
def gaia_HRD(bp,rp,g,plx,cols=None,syms=None,plotlimits=None,outfilename='',isos=None,isolabels=None):
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    mpl.rcParams['lines.linewidth']   =3
    mpl.rcParams['axes.linewidth']    = 2
    mpl.rcParams['xtick.major.width'] =2
    mpl.rcParams['ytick.major.width'] =2    
    mpl.rcParams['ytick.labelsize'] = 10
    mpl.rcParams['xtick.labelsize'] = 10
    mpl.rcParams['axes.labelsize'] = 12
    mpl.rcParams['legend.numpoints'] = 1
    mpl.rcParams['axes.labelweight']='semibold'
    mpl.rcParams['mathtext.fontset']='stix'
    mpl.rcParams['font.weight'] = 'semibold'
    if cols == None:   cols=['k','b','r','g','m','c','y','teal']
    if syms == None:   syms=['.','x','d','+','D','h']

    BR,Gabs = bp-rp,g+5-5*np.log10(1000.0/plx)
    
    if len(BR.shape) == 1: 
        seriesnum = 1
        BR    = np.expand_dims(BR, axis=1)
        Gabs  = np.expand_dims(Gabs, axis=1)
    else: seriesnum = BR.shape[1]
       
    fig,ax=plt.subplots()
    
    for i in range(seriesnum):
        ax.plot(BR[:,i],Gabs[:,i],syms[i]+cols[i])
    
    ax.set_ylim([np.max(Gabs),np.min(Gabs)])
    ax.set_xlabel('BP - RP (mags)')
    ax.set_ylabel('Absolute G (mags)')
    
    if isos != None:
        isocols = ['r','b','m']
        for i in range(len(isos)):
            ax.plot(isos[i,0],isos[i,1],isocols[i]+'--',label=isolabels[i])
    ax.legend()
    
    if outfilename == '':
        plt.show()
    else: plt.savefig(outfilename)
    
    plt.clf()
    plt.close(fig)
    return -1
    
    
    
  
    