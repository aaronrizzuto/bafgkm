## central script to run the new bayes codes.
## test data will be hipparcos catalog

import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time
import emcee

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp

from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from hyades_paramgen import hyades_params
from hyades_paramgen import hyades_fastparams



dpi = np.pi

## So far I am assuming the data catalog is a numpy redarray where each part of the array
## is a numpy array. I guess the basic data input must contain ra, dec, plx, sig_plx
## pmra,sig_pmra,pmdec,sig_pmdec,rv, sig_rv and some ID number/name for the stars
##General rule: ALL ANGLES ASSUMED IN RADIANS!!!!!!!!


file = open("hip_main.pkl","rb")
datacat = pickle.load(file)
pdb.set_trace()


##define a recarray of all the things that go into the bayes code (plus an identifier)
##and an auxilliary array of corresponding other data that might be needed in the preamble 
##e.g. photometry
##ra,dec,l,b,prma,spmra,pmdec,spmdec,plx,sigplx,vr,sig_vr,id,pmcov
##nams = (('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',long),('pmcov',float))
##dts  = [float,float,float,float,float,float,float,float,float,float,float,float,int,float]
stardat = np.recarray(len(datacat.hip),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',long),('pmcov',float)])
stardat.id       = datacat.hip
stardat.ra        = datacat.ra 
stardat.dec       = datacat.dec
stardat.pmra      = datacat.pmra
stardat.pmdec     = datacat.pmdec
stardat.sig_pmra  = datacat.sig_pmra
stardat.sig_pmdec = datacat.sig_pmdec
stardat.plx       = datacat.plx
stardat.sig_plx   = datacat.sig_plx
stardat.rv        = 0.0
stardat.sig_rv    = 300.0
gl,gb     = glactc(datacat.ra*180/dpi,datacat.dec*180/dpi,2000,degree=True,fk4=True)
stardat.l         = gl*np.pi/180.0
stardat.b         = gb*np.pi/180.0

##Next: Cutting the inputs to some range 
##
ing       = np.where((stardat.ra*180/np.pi>20) & (stardat.ra*180/np.pi<100) & (stardat.dec*180/np.pi>-30) & (stardat.dec*180/np.pi<60) & (stardat.plx > 0.1))
stardat  = stardat[ing]
pdb.set_trace()

##data is all in and sky region cut out now do the actually selection code

##get the group parameeters
posvel,cposvel = hyades_fastparams()
nsamp=10000
rposvel = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=True,observables=False)

pdb.set_trace()
##first do the field calculations independently
distforhist = 1000.0/stardat.plx
##build field sample
nfsamp = 100000
hist,binedges = np.histogram(distforhist,bins=100,range=(0.0,3000.0),density=True)
fdist         = rejsamp(binedges[0:100],hist,nfsamp)
fplx          = 1000.0/fdist
uvw_field = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = fdist
chunks=1000
pdmodf = dataset_looper(stardat,rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False)

##now do the hyades group calculation

pdmodg = dataset_looper(stardat,rposvel,chunksize=chunks,field=False,verbose=True,doall=False)
pdb.set_trace()
outname = "hyades_hip_151119"
outputs=  np.zeros((stardat.shape[0],3))
outputs[:,0] = stardat.id ##THE STAR ID
outputs[:,1] = pdmodf        ##FIELD INTEGRAL VALUE
outputs[:,2] = pdmodg        ##GROUP INTEGRAL VALUE

pickle.dump(outputs, open(outname+".pkl", "wb" ))

pdb.set_trace()


print "Im Here"
















