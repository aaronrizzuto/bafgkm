import numpy as np
from gal_xyz import gal_xyz
import pdb
from ACRastro.gal_uvw import gal_uvw
import emcee
from ACRastro.glactc import glactc
import time
import matplotlib.pyplot as plt
#import triangle
import pickle
import os

def posprob(p,mmm,icov):
	ll,bb,dist=gal_xyz(p[0],p[1],p[2],reverse=True,plx=False,radec=True)
	rr,dd = glactc(np.array([ll*180/np.pi]),np.array([bb*180/np.pi]),2000,reverse=True,degree=True,fk4=True)
	input = np.array([rr[0],dd[0],dist])
	##pdb.set_trace()
	diff = input-mmm
	return -np.dot(diff,np.dot(icov,diff))/2.0

def hyades_params():
	ra = 66.5
	dec= 16.9
	sig_ra = 10.0
	sig_dec = 10.0
	plx = 21.53
	sig_plx = 2.76
	dist = 1000.0/21.53
	sig_dist = 6.0
	##van leeuwen numbers
	x = 43.1
	y = 0.7
	z = -17.3
	u = +41.1
	v = -19.2
	w = -1.4
	sig_u = np.sqrt(2.**2 + 0.9**2)
	sig_v = np.sqrt(2.**2 + 0.2**2)
	sig_w = np.sqrt(2.**2 + 0.4**2)
	##
	
	##make the 6d posvel array and covariance matrix
	##we don't really care about the on sky position for the 
	##target selection so jsut make sure the distance is captured properly
	upper = np.array(gal_xyz(ra,dec,plx+sig_plx,radec=True))
	lower = np.array(gal_xyz(ra,dec,plx-sig_plx,radec=True))
	#pdb.set_trace()
	sig_xyz = (upper-lower)/2
	vel = np.array([u,v,w])
	pos = np.array([x,y,z])
	##standard window of hyades in ra dec taken as 4 sigma wide
	cov = np.array([[sig_ra**2,0.0,0.0],[0.0,sig_dec**2,0.0],[0.0,0.0,sig_dist**2]])
	icov = np.linalg.inv(cov)
	mns = np.array([ra,dec,dist])
	nwalkers = 100
	ndim     = 3
	p0 = np.random.multivariate_normal(pos,cov,(nwalkers))
	
	sampler = emcee.EnsembleSampler(nwalkers,ndim,posprob,args=[mns,icov])
	
	##burn in 100
	time1 = time.clock()
	pos,prob,state = sampler.run_mcmc(p0,100)
	sampler.reset()
	timeneeded = 10*time.clock()-time1
	##pdb.set_trace()


	timeneeded = 10*time.clock()-time1
	print "Running MCMC sampler"
	print "Time needed: " + str(timeneeded) +"seconds"
	
	sampler.run_mcmc(pos,1000)
	##remove the first 50 as burn in again
	samples = sampler.chain[:,50:,:].reshape((-1,ndim))
	#pdb.set_trace()
	##the position means
	pos = np.mean(samples,axis=0)
	##the xyz covariance matrix
	cpos = np.cov(samples,rowvar=0)
	## the velocity covariance matrix
	cvel = np.array([[sig_u**2,0.0,0.0],[0.0,sig_v**2,0.0],[0.0,0.0,sig_w**2]])
	
	## combine the velocity and position into a single array (and the covariance mats too)
	posvel = np.append(pos,vel)
	cposvel = np.zeros((6,6))
	cposvel[0:3,0:3] = cpos
	cposvel[3:6,3:6] = cvel
	
	
	#print "im here"
	pickle.dump(posvel,  open( "hyades_posvel.pkl", "wb" ))
	pickle.dump(cposvel,open( "hyades_cposvel.pkl", "wb" ))

	return posvel,cposvel

def hyades_fastparams():
	if os.path.isfile("hyades_posvel.pkl")==True & os.path.isfile("hyades_cposvel.pkl")==True:
		file = open("hyades_posvel.pkl","rb")
		file2 = open("hyades_cposvel.pkl","rb")
		posvel =  pickle.load(file)	
		cposvel =  pickle.load(file2)
		return posvel,cposvel
	else:
		print "Hold up! Hyades Paramater Files Not Created Yet, Doing It Now..."
		posvel,cposvel = hyades_params()
		return posvel,cposvel



