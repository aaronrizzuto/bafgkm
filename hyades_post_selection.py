import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from scipy.io.idl import readsav as idlrsave
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol

where = np.where


fileh = open('hyades_hip_151119.pkl','rb')
results = pickle.load(fileh)
pdmodg = results[:,2]
pdmodf = results[:,1]
bfact = pdmodg/pdmodf
bad = np.where(bfact !=bfact)[0]
bfact[bad] = 0.0
prob = 0.01*bfact/(0.01*bfact+1)
ing = np.where(bfact >1)[0]
inf = np.where(bfact <1)[0]
mixg = float(len(ing))/(len(inf)+len(ing))
mixf = float(len(inf))/(len(inf)+len(ing))
pstarg = 



pdb.set_trace()




##data of stars run through the bayes selection:ra,dec,l,b,prma,spmra,pmdec,spmdec,plx,sigplx,vr,sig_vr,id,pmcov
file1 = open('hyades_ucac4.pkl','rb')
ucac = pickle.load(file1)
file2 = open('Hyades_151117.pkl','rb')
results = pickle.load(file2)





##file = open("taurus_ucac4.pkl","rb")
##datacat = pickle.load(file)
bfact = results[:,2]/results[:,1]
bad = np.where(bfact != bfact)[0] ##these tend to be stars with crazy proper motions, reasonable to set to zero
bfact[bad] = 0.0
prob = 0.01*bfact/(0.01*bfact+1)
pdb.set_trace()


run_xmatch = 1
if run_xmatch ==1:
##cross match with ucac to get the full data entries
	xm = np.zeros(len(prob),long)
	for i in range(0,len(prob)):
		xmm = np.where(ucac.id2m == results[i,0])[0]
		print i
		if len(xmm) > 1: xmm = xmm[0]
		xm[i] = xmm
	pdb.set_trace()
	ucacs = ucac[xm]
	pickle.dump(ucacs, open( "hyades_run_ucac_151117"+".pkl", "wb" ))

pdb.set_trace()
fileucac = open("hyades_run_ucac_151117"+".pkl", "rb")
ucacs = pickle.load(fileucac)
pdb.set_trace()





pdb.set_trace()
print 'im here'







