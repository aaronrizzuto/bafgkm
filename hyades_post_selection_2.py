import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from scipy.io.idl import readsav as idlrsave
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol

where = np.where


##fileh = open('Hyades_150205_2_stardat.pkl','rb')
fileh = open('Hyades_150205_2_nopos_stardat.pkl','rb')

stardat = pickle.load(fileh)
##file2 = open('Hyades_150205_2.pkl','rb')
file2 = open('Hyades_150205_2_nopos.pkl','rb')
results = pickle.load(file2)
pdb.set_trace()
##file = open("taurus_ucac4.pkl","rb")
##datacat = pickle.load(file)
prior = 1.0/1000.0
bfact = results[:,1]/results[:,0]
bad = np.where(bfact != bfact)[0] ##these tend to be stars with crazy proper motions, reasonable to set to zero
bfact[bad] = 0.0
prob =prior*bfact/(prior*bfact+1)


crosscat = 0
if crosscat == 1:
	tt = readcol('hylist.txt')
	rat = tt[:,0]*15.0+tt[:,1]*15/60.0 +tt[:,2]*15/3600.0
	det = tt[:,3]+tt[:,4]/60.0 +tt[:,5]/3600.0
	xm = np.zeros(len(rat),dtype=int)
	for i in range(0,len(rat)):
		dd = gcirc(rat[i],det[i],stardat.ra*180/np.pi,stardat.dec*180/np.pi)
		close = where(dd == min(dd))[0]
		#print(i)
		#pdb.set_trace()
		
		if len(close) != 1: pdb.set_trace()
		if dd[close] < 10.0: xm[i] = close[0]
		if dd[close] > 10.0: xm[i] = -1
			
	
	pdb.set_trace()


good = np.where(prob > 0.5)[0]

pdb.set_trace()
ax = stardat[good].ra*180/np.pi
ay = stardat[good].dec*180/np.pi
au = stardat[good].pmra/3600.0*40.0*7.0
av = stardat[good].pmdec/3600.0*40.0*7.0
plt.quiver(ax,ay,au,av)
plt.xlabel('R.A. (Degrees)',fontsize=18)
plt.ylabel('Decl. (Degrees)',fontsize=18)
plt.savefig('hyades_arrow.eps',format = 'eps',dpi=900)
#plt.show()



pdb.set_trace()



output_it = 1
if output_it == 1:
	outfile = open("hyades_output_nopos.txt","wb")
	headline = "RA  DEC 2MID PMRA PMDEC PLX BFACT PROB \n"
	outfile.write(headline)
	outfile.flush
	headline2 = "(deg) (deg) (2mass) (mas) (mas) (mas) (XX) (prior=.001) \n" 
	outfile.write(headline2)
	outfile.flush
	for i in range(0,stardat.shape[0]):
		outline = str(stardat[i].ra*180/np.pi) +' '+ str(stardat[i].dec*180/np.pi) +' ' +str(stardat[i].id) +' ' +str(stardat[i].pmra) +' '+str(stardat[i].pmdec)+' '+str(stardat[i].plx)+' ' +str(bfact[i]) +' '+ str(prob[i])+' \n'
		#pdb.set_trace()
		outfile.write(outline)
		outfile.flush()

	outfile.close()




pdb.set_trace()
print 'im here'







