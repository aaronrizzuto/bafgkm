import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time
import emcee

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp

from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from phot_dist import phot_dist_interp
from hyades_paramgen import hyades_params
from hyades_paramgen import hyades_fastparams





outname = "Hyades_150205_2_nopos_fulldist"
dpi = np.pi
#file = open("hyades_ucac4.pkl","rb")
file = open("K2C13_full_cross.pkl","rb")  ##ucac hyades field crossed with ALKs K2C13 and ppmxl
#file = open("hyades_corsscat.pkl","rb")
datacat = pickle.load(file)
##pdb.set_trace()
stardat = np.recarray(len(datacat.id2m),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',long),('pmcov',float)])
stardat.id        = datacat.id2m
stardat.ra        = datacat.ra 
stardat.dec       = datacat.dec
stardat.pmra      = datacat.pmra
stardat.pmdec     = datacat.pmdec
stardat.sig_pmra  = datacat.sig_pmra
stardat.sig_pmdec = datacat.sig_pmdec
stardat.plx       = 0.0
stardat.sig_plx   = 300.0
stardat.rv        = 0.0
stardat.sig_rv    = 300.0
gl,gb     = glactc(datacat.ra*180/dpi,datacat.dec*180/dpi,2000,degree=True,fk4=True)
stardat.l         = gl*np.pi/180.0
stardat.b         = gb*np.pi/180.0
pdb.set_trace()

jmag     = datacat.j
hmag     = datacat.h
kmag     = datacat.k
bap      = datacat.bap
vap      = datacat.vap
gap      = datacat.gap
iap      = datacat.iap
rap      = datacat.rap
#rus      = datacat.r
##auxilliary data that the velocity integral doesn't need, but might be useful in 
##excluding non-members or calculating other numbers for input to the velocity
##integrals
auxdat = np.transpose(np.array([jmag,hmag,kmag,bap,vap,gap,iap,rap]))
#pdb.set_trace()
##check which stars have different types of photometry
hasap = np.where((bap <20) & (vap <20))[0]
hask = np.where((kmag <20))[0]
hjk = np.zeros(len(jmag),'int')-1
hap = np.zeros(len(jmag),'int')-1
hjk[hask] = 1
hap[hasap] = 1


#((jmag-kmag<1.5) | (hjk==-1)) & ((vap-kmag<8.0) | ((hjk==-1) & (hap==-1)))
##now make some color cuts and ##remove everything with K>13
incolmag = np.where(((jmag-kmag<1.5) | (hjk==-1)) & ((bap-vap<2.0) | (hap==-1)) & ((vap-kmag<9.8) | ((hjk==-1) | (hap==-1))) & (kmag < 13) & (kmag > 4))[0]
auxdat  = auxdat[incolmag,:]
stardat = stardat[incolmag]
#pdb.set_trace()

##now do photometric distances
pdb.set_trace()
jkd_ms,vkd_ms,bvd_ms,dist_ms,sig_dist_ms = phot_dist_interp(auxdat[:,3],auxdat[:,4],auxdat[:,0],auxdat[:,2],nodist_val = 200.0)
pdb.set_trace()

distforhist = dist_ms[np.where(sig_dist_ms<499.9)[0]]
stardat.plx = 1000.0/dist_ms
stardat.sig_plx= 1000.0/dist_ms*sig_dist_ms/dist_ms
pdb.set_trace()
##remove anything with incorrect distance or proper motion
indist = np.where((stardat.plx>4) & (stardat.plx<10000) & (stardat.pmra<400) & (stardat.pmra>0) & (stardat.pmdec<300) & (stardat.pmdec>-300))[0] 
#indist = np.where((stardat.plx>45) & (stardat.pmra<300) & (stardat.pmra>0) & (stardat.pmdec<150) & (stardat.pmdec>-200))[0] 
stardat=stardat[indist]
auxdat  = auxdat[indist,:]

pdb.set_trace()

pdb.set_trace()

##build field sample
nfsamp = 100000
hist,binedges = np.histogram(distforhist,bins=100,range=(0.0,3000.0),density=True)
fdist         = rejsamp(binedges[0:100],hist,nfsamp)
fplx          = 1000.0/fdist
uvw_field = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = fdist

##get hyades group params
posvel,cposvel = hyades_fastparams()
nsamp=10000
pdb.set_trace()

rposvel = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=True,observables=False)
rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)


#test = np.where(stardat.id == 1303344734)[0]
#stardat=stardat[test]

##Field integral
pdb.set_trace()
chunks=1000
dude = time.clock()
pdmodf = dataset_looper(stardat,rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False)
#pdmodf = np.zeros(len(stardat.ra))
fieldtime = time.clock() - dude
##GROUP INTEGRAL
dude = time.clock()
pdmodg  = dataset_looper(stardat,rposvel,chunksize=chunks,field=False,verbose=True,doall=False)
pdmodgf = dataset_looper(stardat,rposvel,chunksize=chunks,field=True,verbose=True,doall=False)
grouptime = time.clock() - dude



print grouptime
print fieldtime
##DO SOME SMART PICKLES TO SAVE IMPORTANT AND REUSABLE VARIABLES
##FIRST SAVE PDPAR AND 2MASS ID IN SAME VARIABLE
outputs=  np.zeros((stardat.shape[0],3))
outputs[:,0] = pdmodf        ##FIELD INTEGRAL VALUE
outputs[:,1] = pdmodg        ##GROUP INTEGRAL VALUE
outputs[:,2] = pdmodgf
pickle.dump(outputs, open( outname+".pkl", "wb" ))
pickle.dump(stardat,open(outname+"_stardat.pkl", "wb" ))




pdb.set_trace()
print "im here"
