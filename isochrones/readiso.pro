iso1='pad625myr.txt'
iso2='p5gyr.txt'


readcol, iso1,    Z,	loga, M_ini ,  	M_act,	logL,	logTe,	logG,	mbol,    UX,      BX   ,   B1   ,    V1    ,   R  ,     I   ,    J1    ,   H1   ,    K1     ,  L    ,   Lp ,     M,	int_IMF,	stage, skipline=1

readcol, iso2,    Z,	loga, M_ini ,  	M_act,	logL,	logTe,	logG,	mbol,    UX,      BX   ,   B2   ,    V2    ,   R  ,     I   ,    J2    ,   H2   ,    K2     ,  L    ,   Lp ,     M,	int_IMF,	stage, skipline=1

stop
end
