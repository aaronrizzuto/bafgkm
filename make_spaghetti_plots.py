from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import numpy as np
from scipy.interpolate import interp1d
from matplotlib import cm
from matplotlib import pyplot as plt
step = 0.04
maxval = 1.0
fig = plt.figure()
ax = Axes3D(fig)  


x = np.array([0,1,2,3,4,5,6,7,8,9])
y = np.array([0,1,2,3,4,5,6,7,8,9])
z = np.array([0,1,2,3,4,5,6,7,8,9])

x2 =  np.array([0,1,2,3,4,5,6,7,8,9]) + 1
y2 = np.array([0,1,2,3,4,5,6,7,8,9])+1
z2 = np.array([0,1,2,3,4,5,6,7,8,9])

x3 = np.array([0,1,2,3,4,5,6,7,8,9])
y3 = np.array([9,8,7,6,5,4,3,2,1,0])-1
z3 = np.array([0,1,2,3,4,5,6,7,8,9])-1

ax.plot(x,y,z,linewidth=5)
ax.plot(x2,y2,z2,linewidth=5)
ax.plot(x3,y3,z3,linewidth=5)


plt.show()

pdb.set_trace()
print 'im here'