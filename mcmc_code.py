##general imports
import emcee
import numpy as np
import matplotlib.pyplot as plt
import pdb

##my codes
from helpers import dataset_looper
from sample_generation import build_sample_group
from sample_generation import build_uvw_field

def group_lnprob_fixcmat(aposvel,cposvel,stardat,chunks,sum_pdmodf,nsamp,prior):
	posvel = aposvel[0:5]
	age    = aposvel[6]
	##run the group selection process for the posvel input
	rposvel = build_sample_group(posvel,cposvel,age,nsamp,trace_forward=True,observables=False)
	pdmodg = dataset_looper(stardat,rposvel,chunksize=chunks,field=False,verbose=False,doall=False)
	
	bfactg = np.sum(pdmodg)/sum_pdmodf
	prob   = bfactg*prior/(1+prior_bfactg)
	lnprob = np.log(prob)
	return lnprob
	


	