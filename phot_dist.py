##function to estimate main sequence distances from an isochrone

import numpy as np
import pdb
from readcol import readcol
import matplotlib.pyplot as plt


def phot_dist_interp(bm,v,jm,k,age=-1,binbias=0.2,nodist_val=200.0):
	magok = find_okmags(bm,v,jm,k)
	kj=k-jm
	vb=v-bm
	kv=k-v
	
	if age==-1:  
	    theiso = "isochrones/p1gyr.dat"
	    lll = 60
	if age== 5:  theiso = "isochrones/p5.01myr.dat"
	if age== 2: theiso = "isochrones/p2.00myr.dat"
	if age==10:  
	    theiso = "isochrones/p10.00myr.dat" ##150
	    lll = 150
	#if age==16:  theiso = "isochrones/p15.83myr.dat"
	if age==20:  theiso = "isochrones/p19.95myr.dat"
	if age==100: 
	    theiso = "isochrones/pad100myr.dat" ##60
        lll = 60
	iso = readcol(theiso,twod=True,names=True,asStruct=True)
	#pdb.set_trace()
	limjk = iso.J[0]-iso.K[0]
	limbv = iso.B[0]-iso.V[0]
	limvk = iso.V[0]-iso.K[0]
	dojk = np.where((magok[:,2]==1) & (magok[:,3]==1) & (-kj<limjk))[0]
	dovk = np.where((magok[:,2]==1) & (magok[:,1]==1) & (-kv<limvk))[0]
	dobv = np.where((magok[:,0]==1) & (magok[:,1]==1) & (-vb<limbv))[0]
	
	
	jkdist = np.zeros(len(jm))
	vkdist = np.zeros(len(jm))
	bvdist = np.zeros(len(jm))

	interpk  = np.interp(kj[dojk],iso.K[0:lll]-iso.J[0:lll],iso.K[0:lll])
	interpvk = np.interp(kv[dovk],iso.K[0:lll]-iso.V[0:lll],iso.V[0:lll])
	interpvb = np.interp(vb[dobv],iso.V[0:lll]-iso.B[0:lll],iso.V[0:lll])
	djk  =  10.0**((k[dojk]+binbias-interpk )/5.0+1.0)
	dvk  =  10.0**((v[dovk]+binbias-interpvk)/5.0+1.0)
	dbv  =  10.0**((v[dobv]+binbias-interpvb)/5.0+1.0)

	jkdist[dojk] = djk
	vkdist[dovk] = dvk
	bvdist[dobv] = dbv
		
	
	##combine the distances somehow, maybe a mean over the available distances
	combdist = np.zeros(len(jm)) -1.0
	combdist[dojk] = djk
	combdist[dobv] = dbv
	combdist[dovk] = dvk	 
	#pdb.set_trace()
	##error based on 0.5 mags uncertainty in the models
	#sig_combdist = combdist
	#sig_combdist[dojk]   = combdist*np.log(10.0)/5.0*0.7
	sig_combdist         = combdist*np.log(10.0)/5.0*0.5 
	#sig_combdist[dobv]   = combdist*np.log(10.0)/5.0*0.5 
	
	#pdb.set_trace()
	nodist = np.where(combdist < 0.0)[0]
	#
	#pdb.set_trace()
	if nodist.shape != 0:
		sig_combdist[nodist] = 1001.0
		combdist[nodist]     = nodist_val
	return jkdist,vkdist,bvdist,combdist,sig_combdist
	
def find_okmags(b,v,j,k):
	okmag  = np.zeros((len(j),4),'int')-1
	badv   = np.where(v < 20.0)
	badb   = np.where(b < 20.0)
	badj   = np.where(j < 20.0)
	badk   = np.where(k < 20.0)
	okmag[badj,2]=1
	okmag[badk,3]=1
	okmag[badb,0]=1
	okmag[badv,1]=1
	return okmag