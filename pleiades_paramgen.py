import numpy as np
from gal_xyz import gal_xyz
import pdb
from ACRastro.gal_uvw import gal_uvw
import emcee
from ACRastro.glactc import glactc
import time
import matplotlib.pyplot as plt
import corner
import pickle
import os

def posprob(p,mmm,icov):
	ll,bb,dist=gal_xyz(p[0],p[1],p[2],reverse=True,plx=False,radec=True)
	rr,dd = glactc(np.array([ll*180/np.pi]),np.array([bb*180/np.pi]),2000,reverse=True,degree=True,fk4=True)
	input = np.array([rr[0],dd[0],dist])
	##pdb.set_trace()
	diff = input-mmm
	return -np.dot(diff,np.dot(icov,diff))/2.0

def pleiades_params():
	ra = 56.5
	dec= 24.09
	sig_ra = 3.##was 5
	sig_dec = 3.##was 5
	plx = 7.3
	sig_plx = 0.2
	dist = 1000.0/plx
	sig_dist = 4.5 ##2.0 5pc depth corresponds to 2 degrees radius scale on sky
	##van leeuwen numbers
	x = 107.
	y = 25.9
	z = -48.3
	u = +6.81
	v = -28.71
	w = -14.18
	sig_u = np.sqrt(1.**2 + 0.9**2)
	sig_v = np.sqrt(1.**2 + 0.5**2)
	sig_w = np.sqrt(1.**2+ 0.5**2)
	##

	##make the 6d posvel array and covariance matrix
	##we don't really care about the on sky position for the 
	##target selection so jsut make sure the distance is captured properly
	upper = np.array(gal_xyz(ra,dec,plx+sig_plx,radec=True))
	lower = np.array(gal_xyz(ra,dec,plx-sig_plx,radec=True))
	#pdb.set_trace()
	sig_xyz = (upper-lower)/2
	vel = np.array([u,v,w])
	pos = np.array([x,y,z])
	##standard window of pleiades in ra dec
	cov = np.array([[sig_ra**2,0.0,0.0],[0.0,sig_dec**2,0.0],[0.0,0.0,sig_dist**2]])
	icov = np.linalg.inv(cov)
	mns = np.array([ra,dec,dist])
	nwalkers = 100
	ndim     = 3
	p0 = np.random.multivariate_normal(pos,cov,(nwalkers))
	
	sampler = emcee.EnsembleSampler(nwalkers,ndim,posprob,args=[mns,icov])
	
	##burn in 100
	time1 = time.time()
	pos,prob,state = sampler.run_mcmc(p0,100)
	sampler.reset()
	timeneeded = 10*(time.time()-time1)
	##pdb.set_trace()

	steps = 2000
	timeneeded = steps/100.0*time.clock()-time1
	print "Running MCMC sampler"
	print "Time needed: " + str(timeneeded) +"seconds"
	
	sampler.run_mcmc(pos,steps)
	##remove the first 50 as burn in again
	samples = sampler.chain[:,50:,:].reshape((-1,ndim))
	#pdb.set_trace()
	##the position means
	pos = np.mean(samples,axis=0)
	##the xyz covariance matrix
	cpos = np.cov(samples,rowvar=0)
	## the velocity covariance matrix
	cvel = np.array([[sig_u**2,0.0,0.0],[0.0,sig_v**2,0.0],[0.0,0.0,sig_w**2]])
	
	## combine the velocity and position into a single array (and the covariance mats too)
	posvel = np.append(pos,vel)
	cposvel = np.zeros((6,6))
	cposvel[0:3,0:3] = cpos
	cposvel[3:6,3:6] = cvel
	
	import pdb
	pdb.set_trace()
	corner.corner(samples)
	plt.show()
	#print "im here"
	pickle.dump(posvel,  open( "pleiades_posvel.pkl", "wb" ))
	pickle.dump(cposvel,open( "pleiades_cposvel.pkl", "wb" ))

	return posvel,cposvel

def pleiades_fastparams():
	if os.path.isfile("pleiades_posvel.pkl")==True & os.path.isfile("pleiades_cposvel.pkl")==True:
		file = open("pleiades_posvel.pkl","rb")
		file2 = open("pleiades_cposvel.pkl","rb")
		posvel =  pickle.load(file)	
		cposvel =  pickle.load(file2)
		return posvel,cposvel
	else:
		print "Hold up! pleiades Paramater Files Not Created Yet, Doing It Now..."
		posvel,cposvel = pleiades_params()
		return posvel,cposvel



