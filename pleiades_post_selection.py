import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from scipy.io.idl import readsav as idlrsave
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol

where = np.where


fileh = open('pleiades_150210_stardat.pkl','rb')
stardat = pickle.load(fileh)
file2 = open('pleiades_150210.pkl','rb')
results = pickle.load(file2)

##file = open("taurus_ucac4.pkl","rb")
##datacat = pickle.load(file)
#prior = 1.0/1000.0
prior=1/1000.
bfact = results[:,1]/results[:,0]
bad = np.where(bfact != bfact)[0] ##these tend to be stars with crazy proper motions, reasonable to set to zero
bfact[bad] = 0.0
prob =prior*bfact/(prior*bfact+1)
good=np.where(prob > 0.495)[0]

pdb.set_trace()

#arrows plots
these = np.random.random_integers(0,len(stardat.ra),200)
#good = these
ax = stardat[good].ra*180/np.pi
ay = stardat[good].dec*180/np.pi
au = stardat[good].pmra/3600.0*40.0*10.0
av = stardat[good].pmdec/3600.0*40.0*10.0
plt.quiver(ax,ay,au,av)
plt.show()

pdb.set_trace()
output_it = 1
if output_it == 1:
	outfile = open("pleiades_output_150210.txt","wb")
	headline = "RA  DEC 2MID PMRA PMDEC PLX BFACT PROB \n"
	outfile.write(headline)
	outfile.flush
	headline2 = "(rad) (rad) (2mass) (mas) (mas) (mas) (XX) (prior=.001) \n" 
	outfile.write(headline2)
	outfile.flush
	for i in range(0,stardat.shape[0]):
		outline = str(stardat[i].ra) +' '+ str(stardat[i].dec) +' ' +str(stardat[i].id) +' ' +str(stardat[i].pmra) +' '+str(stardat[i].pmdec)+' '+str(stardat[i].plx)+' ' +str(bfact[i]) +' '+ str(prob[i])+' \n'
		#pdb.set_trace()
		outfile.write(outline)
		outfile.flush()

	outfile.close()




pdb.set_trace()
print 'im here'







