import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time
import emcee

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp

from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from phot_dist import phot_dist_interp
from pleiades_paramgen import pleiades_params
from pleiades_paramgen import pleiades_fastparams
from full_uvw_to_observable import gal_to_obs_full




outname = "pleiades_150210"
dpi = np.pi
file = open("K2C13_full_cross.pkl","rb")
datacat = pickle.load(file)
region = np.where((datacat.ra*180/np.pi < 70) & (datacat.ra*180/np.pi > 45)  & (datacat.dec*180/np.pi < 40) & (datacat.ra*180/np.pi > 10) ) 

datacat = datacat[region]
pdb.set_trace()

stardat = np.recarray(len(datacat.id2m),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',long),('pmcov',float)])
stardat.id        = datacat.id2m
stardat.ra        = datacat.ra 
stardat.dec       = datacat.dec
stardat.pmra      = datacat.pmra
stardat.pmdec     = datacat.pmdec
stardat.sig_pmra  = datacat.sig_pmra
stardat.sig_pmdec = datacat.sig_pmdec
stardat.plx       = 0.0
stardat.sig_plx   = 300.0
stardat.rv        = 0.0
stardat.sig_rv    = 300.0
gl,gb     = glactc(datacat.ra*180/dpi,datacat.dec*180/dpi,2000,degree=True,fk4=True)
stardat.l         = gl*np.pi/180.0
stardat.b         = gb*np.pi/180.0
stardat_field = stardat.copy()
#pdb.set_trace()
qwe = np.where(stardat.id == 	1281947259)[0]
stardat[qwe[0]].rv = 7.4
stardat[qwe[0]].sig_rv = 0.2

pdb.set_trace()

jmag     = datacat.j
hmag     = datacat.h
kmag     = datacat.k
bap      = datacat.bap
vap      = datacat.vap
gap      = datacat.gap
iap      = datacat.iap
rap      = datacat.rap
##auxilliary data that the velocity integral doesn't need, but might be useful in 
##excluding non-members or calculating other numbers for input to the velocity
##integrals
auxdat = np.transpose(np.array([jmag,hmag,kmag,bap,vap,gap,iap,rap]))
#pdb.set_trace()
##check which stars have different types of photometry
hasap = np.where((bap <20) & (vap <20))
hask = np.where((kmag <20))
hjk = np.zeros(len(jmag),'int')-1
hap = np.zeros(len(jmag),'int')-1
hjk[hask] = 1
hap[hasap] = 1


#((jmag-kmag<1.5) | (hjk==-1)) & ((vap-kmag<8.0) | ((hjk==-1) & (hap==-1)))
##now make some color cuts and ##remove everything with K>13
incolmag = np.where(((jmag-kmag<1.5) | (hjk==-1)) & ((bap-vap<2.0) | (hap==-1)) & ((vap-kmag<9.8) | ((hjk==-1) | (hap==-1))) & (kmag < 13) & (kmag > 4))[0]
auxdat  = auxdat[incolmag,:]
stardat = stardat[incolmag]
stardat_field = stardat_field[incolmag]
pdb.set_trace()

##now do photometric distances
jkd_100,vkd_100,bvd_100,dist_100,sig_dist_100 = phot_dist_interp(auxdat[:,3],auxdat[:,4],auxdat[:,0],auxdat[:,2],age=100)
jkd_ms,vkd_ms,bvd_ms,dist_ms,sig_dist_ms = phot_dist_interp(auxdat[:,3],auxdat[:,4],auxdat[:,0],auxdat[:,2])

distforhist = dist_ms[np.where(sig_dist_ms<499.9)[0]]
stardat.plx = 1000.0/dist_100
stardat.sig_plx= 1000.0/dist_100*sig_dist_100/dist_100
stardat_field.plx = 1000.0/dist_ms
stardat_field.sig_plx= 1000.0/dist_ms*sig_dist_ms/dist_ms

##remove anything with incorrect distance or proper motion
indist = np.where((stardat.plx>4) & (stardat.plx<45))[0] 
stardat = stardat[indist]
stardat_field = stardat_field[indist]
auxdat  = auxdat[indist,:]


pdb.set_trace()

##build field sample
nfsamp = 100000
hist,binedges = np.histogram(distforhist,bins=100,range=(0.0,3000.0),density=True)
fdist         = rejsamp(binedges[0:100],hist,nfsamp)
fplx          = 1000.0/fdist
uvw_field = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = fdist

##get pleiades group params
posvel,cposvel = pleiades_fastparams()
nsamp=10000
testmean,testcov = gal_to_obs_full(posvel,cposvel)
pdb.set_trace()

rposvel = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=True,observables=False)
rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)

#run = np.where(stardat.id == 	1281947259)[0]


##Field integral
pdb.set_trace()
chunks=1000
dude = time.clock()
pdmodf = dataset_looper(stardat_field,rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False)
fieldtime = time.clock() - dude
##GROUP INTEGRAL
dude = time.clock()
pdmodg = dataset_looper(stardat,rposvel,chunksize=chunks,field=False,verbose=True,doall=False)
grouptime = time.clock() - dude
#pdb.set_trace()
print grouptime
print fieldtime
##DO SOME SMART PICKLES TO SAVE IMPORTANT AND REUSABLE VARIABLES
##FIRST SAVE PDPAR AND 2MASS ID IN SAME VARIABLE
outputs=  np.zeros((stardat.shape[0],3))
outputs[:,0] = pdmodf        ##FIELD INTEGRAL VALUE
outputs[:,1] = pdmodg        ##GROUP INTEGRAL VALUE
pickle.dump(outputs, open( outname+".pkl", "wb" ))
pickle.dump(stardat,open(outname+"_stardat.pkl", "wb" ))




pdb.set_trace()
print "im here"
