import numpy as np
from gal_xyz import gal_xyz
import pdb
from ACRastro.gal_uvw import gal_uvw
import emcee
from ACRastro.glactc import glactc
import time
import matplotlib.pyplot as plt
import corner
import pickle
import os

def posprob(p,mmm,icov):
	ll,bb,dist=gal_xyz(p[0],p[1],p[2],reverse=True,plx=False,radec=True)
	rr,dd = glactc(np.array([ll*180/np.pi]),np.array([bb*180/np.pi]),2000,reverse=True,degree=True,fk4=True)
	input = np.array([rr[0],dd[0],dist])
	##pdb.set_trace()
	diff = input-mmm
	return -np.dot(diff,np.dot(icov,diff))/2.0

def praesepe_params():
	ra = 130.0
	dec= 19.7
	sig_ra  =1.5 ##4.5 ##should really be 1.0
	sig_dec =1.5 ##4.5 ##should really be 1.0
	plx = 5.5
	sig_plx = 0.18
	dist = 181.5
	sig_dist = 4.8 ##6.0 ##1 degree in ra corresponds to 3 pc
	##van leeuwen numbers
	x = 138.4
	y = -67.0
	z = 97.6
	u = +41.5
	v = -19.8
	w = -9.7
	sig_u = np.sqrt(1.**2 + 0.9**2)
	sig_v = np.sqrt(1.**2 + 0.5**2)
	sig_w = np.sqrt(1.**2 + 1.1**2)
	##
	
	##make the 6d posvel array and covariance matrix
	##we don't really care about the on sky position for the 
	##target selection so jsut make sure the distance is captured properly
	upper = np.array(gal_xyz(ra,dec,plx+sig_plx,radec=True))
	lower = np.array(gal_xyz(ra,dec,plx-sig_plx,radec=True))
	#pdb.set_trace()
	sig_xyz = (upper-lower)/2
	vel = np.array([u,v,w])
	pos = np.array([x,y,z])
	##standard window of praesepe in ra dec taken as 4 sigma wide
	cov = np.array([[sig_ra**2,0.0,0.0],[0.0,sig_dec**2,0.0],[0.0,0.0,sig_dist**2]])
	icov = np.linalg.inv(cov)
	mns = np.array([ra,dec,dist])
	nwalkers = 100
	ndim     = 3
	p0 = np.random.multivariate_normal(pos,cov,(nwalkers))
	
	print mns
	sampler = emcee.EnsembleSampler(nwalkers,ndim,posprob,args=[mns,icov])
	
	##burn in 100
	time1 = time.clock()
	pos,prob,state = sampler.run_mcmc(p0,100)
	sampler.reset()
	timeneeded = 10*time.clock()-time1
	##pdb.set_trace()


	timeneeded = 10*time.clock()-time1
	print "Running MCMC sampler"
	print "Time needed: " + str(timeneeded) +"seconds"
	
	sampler.run_mcmc(pos,1000)
	##remove the first 50 as burn in again
	samples = sampler.chain[:,50:,:].reshape((-1,ndim))
	#pdb.set_trace()
	##the position means
	pos = np.mean(samples,axis=0)
	##the xyz covariance matrix
	cpos = np.cov(samples,rowvar=0)
	## the velocity covariance matrix
	cvel = np.array([[sig_u**2,0.0,0.0],[0.0,sig_v**2,0.0],[0.0,0.0,sig_w**2]])
	
	## combine the velocity and position into a single array (and the covariance mats too)
	posvel = np.append(pos,vel)
	cposvel = np.zeros((6,6))
	cposvel[0:3,0:3] = cpos
	cposvel[3:6,3:6] = cvel
	
	import pdb
	pdb.set_trace()
	corner.corner(samples)
	plt.show()
	#print "im here"
	pickle.dump(posvel,  open( "praesepe_posvel.pkl", "wb" ))
	pickle.dump(cposvel,open( "praesepe_cposvel.pkl", "wb" ))

	return posvel,cposvel

def praesepe_fastparams():
	if os.path.isfile("praesepe_posvel.pkl")==True & os.path.isfile("praesepe_cposvel.pkl")==True:
		file = open("praesepe_posvel.pkl","rb")
		file2 = open("praesepe_cposvel.pkl","rb")
		posvel =  pickle.load(file)	
		cposvel =  pickle.load(file2)
		return posvel,cposvel
	else:
		print "Hold up! praesepe Paramater Files Not Created Yet, Doing It Now..."
		posvel,cposvel = praesepe_params()
		return posvel,cposvel



