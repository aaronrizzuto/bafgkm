import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys,time,os,glob,pdb,pickle
from gcirc import gcirc
from readcol import readcol
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
import helpers
from numpy.lib.recfunctions import stack_arrays
import pandas as pd
import bspline_acr as bspline
from gal_xyz import gal_xyz

##Taurus
#datadir   = '/Volumes/UT2/bafgkm_out_tacc/Taurus/tau_1_20180505-07:33:18/'
#data = readcol('/Volumes/UT1/gaia_dr2_tauaur.csv',fsep=',',asRecArray=True,nullval='') ##note that this will be out of order compared to starat once compiled
#inputfile = open(glob.glob(datadir + '*txt')[0],'rb').readline().split(',')[0]
#known = readcol('/Users/arizz/python/projects/K2pipe/membership/Tau_known_forpy.csv',fsep=',',asRecArray=True)
uvw_a = np.array([-1.1046809264874493,0.07893754795501393,0.03321024914480979,-0.01760714835660724,-0.0037768780667496757])
uvw_b = np.array([70.46518175402045 ,-103.93523750916036 ,7.67826738010928 , -3.2696518972792186 , -17.929178850978552 ])
#USco:
#datadir = os.getenv('HOME')+'/data_local/lonestar_outputs/bafgkm_out/ScoCen/US_1_20180508-19:02:11/'
datadir = 'PscEri/psceri1_20190312-17:33:55/'

##this is reallly slow...
# data = pickle.load(open('psceri_inputformat_20190311pair.pkl','rb'))
# 
# 
# 
# 
# 
# #pdb.set_trace()
# ##combine the output files in the correct ordering
# datafiles = glob.glob(datadir+'*pkl') 
# indexes = [int(ff.split('/')[-1].split('_')[1]) for ff in datafiles]
# ordering = np.argsort(indexes)
# 
# 
# print 'Reading ' + datadir
# for ii in range(len(ordering)):
#     print(ii)
#     file = datafiles[ordering[ii]]
#     if ii == 0: stardat, pdmg,pdmf,pdmg_nrv,pdmf_nrv = pickle.load(open(file,'rb'))
#     if ii != 0: 
#         sd,pg,pf,pg2,pf2 = pickle.load(open(file,'rb'))
#         stardat  = stack_arrays((stardat,sd),asrecarray=True,usemask=False)
#         pdmg     = np.concatenate((pdmg,pg))
#         pdmf     = np.concatenate((pdmf,pf))
#         pdmg_nrv = np.concatenate((pdmg_nrv,pg2))
#         pdmf_nrv = np.concatenate((pdmf_nrv,pf2))
# print 'Data loaded'
# if np.sum(stardat.id - data.source_id) >0:
#     print 'in/out mismatch!!??!!'
#     pdb.set_trace()
# 
# pd.DataFrame.from_records(data).to_csv('psceri_combined_data_1.csv')
# pd.DataFrame.from_records(stardat).to_csv('psceri_combined_stardata_1.csv')
# pickle.dump((pdmg,pdmf,pdmg_nrv,pdmf_nrv),open('psceri_combined_results_1.pkl','wb'))

data = pd.read_csv('psceri_combined_data_1.csv').to_records()
stardat =pd.read_csv('psceri_combined_stardata_1.csv').to_records()
pdmg,pdmf,pdmg_nrv,pdmf_nrv = pickle.load(open('psceri_combined_results_1.pkl','rb'))
pdmg2,pdmf2,pdmg_nrv2,pdmf_nrv2 = pickle.load(open('psceri_combined_results.pkl','rb'))

known  = pd.read_csv('PscEri-FullList-20190302.txt',delimiter=' ').to_records()


##xmatch the known
xk = np.zeros(len(stardat),dtype=int)-1
for i in range(len(known)):
    print('xmatching known ' + str(i+1) + ' out of ' + str(len(known)))
    qwe = np.where(stardat.id == known.DR2Name[i])[0]
    if len(qwe) > 1:
        print('woah')
        pdb.set_trace()
    if len(qwe) == 1: xk[qwe] = i
kk = np.where(xk>-1)[0]
#x,y,z = gal_xyz(data.ra,data.dec,data.parallax,radec=True,plx=True)
x,y,z = pickle.load(open('psceri_results_xyz.pkl','rb'))
#pdb.set_trace()

model = uvw_a*x[:,np.newaxis]+uvw_b
ll,bb,pp = gal_xyz(x,model[:,0],model[:,1],reverse=True)
from ACRastro.glactc import glactc
rr,dd = glactc(ll*180/np.pi,bb*180/np.pi,2000,reverse=True,fk4=True)


##lindegren cleanup
u_param = data.astrometric_chi2_al/(data.astrometric_n_good_obs_al - 5)
b_r = data.phot_bp_mean_mag-data.phot_rp_mean_mag
ef = data.phot_bp_rp_excess_factor
g = data.phot_g_mean_mag
gexp = np.exp(-0.2*(g-19.5))
qwe = np.where(gexp < 1.0)[0]
gexp[qwe] = 1.0
goodrow = np.where((u_param < 1.2*gexp) & (ef >1.+0.015*b_r**2) & (ef < 1.3+0.06*b_r**2) & (np.absolute(data.b-bb*180/np.pi)<200))[0]
gr = data.phot_g_mean_mag-data.phot_rp_mean_mag
absg = g+5-5*np.log10(1000.0/data.parallax)
qwe = np.where((absg[goodrow] > 9.6) & (gr[goodrow] < 0.85))[0]
masker = np.zeros(len(goodrow),dtype=int)
masker[qwe] = 1
goodgood = np.where(masker ==0 )[0]
goodrow = goodrow[goodgood]
qwe = np.where((absg > 9.6) & (gr < 0.85))[0]
pdmg[qwe] = 0.0


#pdb.set_trace()
flyers = np.where((pdmg==0) & (pdmf==0))[0]
pdmf[flyers] = 1.0
pdmg[flyers] = 0.0
pdmg_nrv[flyers] = 0.0
pdmf_nrv[flyers] = 1.0

flyers = np.where((pdmg2==0) & (pdmf2==0))[0]
pdmf2[flyers] = 1.0
pdmg2[flyers] = 0.0
pdmg_nrv2[flyers] = 0.0
pdmf_nrv2[flyers] = 1.0


bfact = pdmg/pdmf*1.0
bf2 = pdmg_nrv/pdmf_nrv
reg = goodrow
hist,edge = np.histogram(gr,bins=15,range=[-0.2,1.5])
binassign = np.digitize(gr,edge[0:len(edge)-1])
qwe = np.where(binassign >= len(edge)-1)[0]
binassign[qwe] = len(edge)-2
qwe = np.where(binassign <= edge[0])[0]
binassign[qwe] = 0
prior = np.zeros((101,len(edge)-1))+10.0

for i in range(len(prior)-1):     prior[i+1] = np.histogram(gr,bins=15,range=[-.2,1.5],weights=prior[i,binassign]*bfact/(prior[i,binassign]*bfact+1))[0]/np.histogram(gr[goodrow],bins=15,range=[-.2,1.5])[0]

    #pdb.set_trace()
    #print i
    #pdb.set_trace()
    #theseprobs = prior[i,binassign]*bfact/(prior[i,binassign]*bfact+1)
    #prior[i+1] = np.sum(pdmg[reg]/pdmf[reg]*prior[i]/(1+prior[i]*pdmg[reg]/pdmf[reg]))/(len(reg)-np.sum(pdmg[reg]/pdmf[reg]*prior[i]/(1+prior[i]*pdmg[reg]/pdmf[reg])))

prob = prior[-1,binassign]*bfact/(prior[-1,binassign]*bfact+1)

oldprior = np.zeros(50)+10
for i in range(len(oldprior)-1): oldprior[i+1] = np.sum(oldprior[i]*bfact/(1+oldprior[i]*bfact))/len(goodrow)
#pdb.set_trace()
prob = oldprior[-1]*bfact/(1+oldprior[-1]*bfact)
pdb.set_trace()
#prob = pdmg/pdmf*prior[100]/(pdmg/pdmf*prior[100]+1)
#prob = 0.01*bfact/(1+0.01*bfact)

bfact2 = pdmg2/pdmf2*1.0
bf22 = pdmg_nrv2/pdmf_nrv2
reg = goodrow
prior2 = np.zeros(101)+10.0
for i in range(100): prior2[i+1] = np.sum(pdmg2[reg]/pdmf2[reg]*prior2[i]/(1+prior2[i]*pdmg2[reg]/pdmf2[reg]))/(len(reg))
prob2 = pdmg2/pdmf2*prior2[100]/(pdmg2/pdmf2*prior2[100]+1)




centerdist = data.dec-dd
qwe = np.where((xk > -1) & (stardat.sig_rv > 10.) & (data.phot_g_mean_mag < 11))[0]

best = qwe
#best  = np.where((prob >= 0.2) & (np.absolute(data.dec-dd)<1000))[0]



data= data[best]
out = pd.DataFrame.from_records(data)
out['prob'] = prob[best]
out['centerdistance'] = centerdist[best]
pdb.set_trace()
out.to_csv('psceri_curtis_brightG11_norv.csv',columns = ['ra','dec','phot_g_mean_mag','prob'])
#
pdb.set_trace()

best3 =np.where((prob >= 0.5) & (np.abs(data.b-bb*180/np.pi)<10))[0]
best2 = np.where(prob >=0.9)[0]
asd  = np.where((pdmg < 1e-8) & (prob > 0.9))[0]
qwe = np.where((pdmg > 1e-6) & (prob > 0.9))[0]

pdb.set_trace()
# 
# plt.show()
pdb.set_trace()
knownmem = np.zeros(len(stardat),dtype='|S21')
recov    = np.zeros(len(known),dtype=int)
for i in range(len(known)):
    dist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,known.radeg[i],known.decdeg[i])
    close = np.argmin(dist)
    print str(i) + ' / ' + str(len(known))
   # pdb.set_trace()
    if dist[close] < 10.0:
        knownmem[close] = known.mem[i]
        recov[i] = 1
#    pdb.set_trace()
        



pdb.set_trace()

#datafile = 'gaia_dr2_tauaur_inputformat_20180504.pkl'
#(stardatooo,galah_idooo,rvsourceooo,rposvel_field) = pickle.load(open(datafile,'rb'))

##5317
##uvw=(10.05964498792061, -19.226151993492238, -7.1195458163785865)

x,y,z = gal_xyz(stardat[i].l*180/np.pi,stardat[i].b*180/np.pi,stardat[i].plx,plx=True)
thism = uvw_a*x + uvw_b






pdb.set_trace()
print 'Done'