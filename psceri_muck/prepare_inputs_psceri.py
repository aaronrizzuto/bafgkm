import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from astropy.io import fits
import os,glob,sys,pdb,time,pickle
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
import idlsave
sys.path.append(os.getenv("HOME")+'/python/BAFGKM')
from gcirc import gcirc
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from taurus_paramgen import taurus_paramgen
from taurus_paramgen import taurus_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from scipy.special import erf as scipyerf
from scipy.stats import truncnorm
#np.set_printoptions(suppress=True,precision=2)
from readcol import readcol
import pandas as pd
from numpy.lib.recfunctions import stack_arrays


datestamp = time.strftime('%Y%m%d', time.localtime(time.time()))
#datadir = os.getenv("HOME") + '/data_local/'
datadir = '/Volumes/UT1/'
#dfile = 'gaia_dr2_tauaur.csv'
#data = pd.read_csv(datadir + dfile, sep=',').to_records()
#pdb.set_trace()

#dfile = 'gaia_dr2_usco.csv'
#dfile = 'gaia_dr2_ucl.csv'
#dfile = 'gaia_dr2_lcc.csv'
#dfiles = ['gaia_psceri_ra0-50decm50-20plx4-13.csv','gaia_psceri_ra50-100decm50-20plx4-13.csv','gaia_psceri_ra300-360decm50-20plx4-13.csv']
dfiles = ['gaia_psceri_ra250_300decm50_20plx4-13.csv']#,'gaia_psceri_ra100_150decm50-20plx4-13.csv']
#outname = dfile.split('.')[0] + '_inputformat_' + datestamp + '.pkl'
#outname2 = dfile.split('.')[0] + '_inputformat_' + datestamp + 'pair.pkl'
outname = 'psceri_more' + '_inputformat_' + datestamp + '.pkl'
outname2 = 'psceri_more' + '_inputformat_' + datestamp + 'pair.pkl'
###!!!!NOTE MAKE SURE TO OUTPUT A FULL GAIA DR2 file for the prepared inputs to use for analysis later
##get the main gaia dr2 file 
#data = readcol(datadir + dfile,fsep=',',asRecArray = False,names=True,nullval='')
for i in range(len(dfiles)):
    print('reading file ' + str(i+1) + ' out of ' + str(len(dfiles)))
    if i == 0:
        data=pd.read_csv(datadir+dfiles[i], sep=',').to_records()
    if i >  0:
        thisdata = pd.read_csv(datadir+dfiles[i], sep=',').to_records()
        data = stack_arrays((data,thisdata),asrecarray=True,usemask=False)


#pdb.set_trace()
##apply some cuts here if you like, dummy for now
keep =np.where((data.ra >= -100) & (data.ra > -100))[0]


print len(keep),len(data)
#data = data[keep]

##now produce the stardat structure for running the mem selection

stardat = np.recarray(len(data.ra),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',int),('pmcov',float),('plxpmra_cov',float),('plxpmdec_cov',float)])
stardat.id        = data.source_id
stardat.ra        = data.ra*np.pi/180
stardat.dec       = data.dec*np.pi/180
stardat.pmra      = data.pmra
stardat.pmdec     = data.pmdec
stardat.sig_pmra  = data.pmra_error
stardat.sig_pmdec = data.pmdec_error
stardat.plx       = data.parallax
stardat.sig_plx   = data.parallax_error
stardat.rv        = data.radial_velocity
stardat.sig_rv    = data.radial_velocity_error
badrv = np.where(np.isnan(stardat.rv))[0]
stardat.rv[badrv] = 0.0
stardat.sig_rv[badrv] = 300.0
stardat.l         = data.l*np.pi/180.0
stardat.b         = data.b*np.pi/180.0
stardat.pmcov = data.pmra_pmdec_corr*data.pmra_error*data.pmdec_error
stardat.plxpmra_cov = data.parallax_pmra_corr*data.pmra_error*data.parallax_error
stardat.plxpmdec_cov = data.parallax_pmdec_corr*data.pmdec_error*data.parallax_error
#pdb.set_trace()


##generate Field sample information ##this is bad inside of ~10 parsecs
fplx = data.parallax.copy()
zxc = np.where((fplx > 10))[0]
fplx = fplx[zxc]
nfsamp=1000000
hist,binedges = np.histogram(stardat.plx,bins=200,range=(np.min(stardat.plx),np.max(stardat.plx)),density=True)
fplx          = rejsamp(binedges[0:200],hist,nfsamp)
uvw_field     = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = 1000.0/fplx


#pdb.set_trace()




##this is so ridiculously slow its stupid...
pickle.dump((stardat,rposvel_field),open(outname,'wb'))

#pickle.dump(data,open(outname2,'wb'))
pd.DataFrame.from_records(data).to_csv( path_or_buf=outname2,index=False)


print 'Done, output file is: ' + outname





