import numpy as np
import os,sys,glob,pdb,pickle
sys.path.append(os.getenv("HOME") + '/python/BAFGKM/')
sys.path.append(os.getenv("HOME") + '/python/projects/K2pipe')
import bspline_acr as bspline
import time
import matplotlib.pyplot as plt
import matplotlib as mpl
from astroquery.vizier import Vizier
import astropy.units as u
import astropy.coordinates as coord

##my imports
from gcirc import gcirc
from readcol import readcol
from uvwtopm import uvwtopm
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from gcirc import gcirc
import cluster_membership_tools
from rejsamp import rejsamp_opt
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from helpers import dataset_looper
from gal_xyz import gal_xyz
import emcee
import corner
mpl.rcParams['lines.linewidth']   = 3
mpl.rcParams['axes.linewidth']    = 2
mpl.rcParams['xtick.major.width'] =2
mpl.rcParams['ytick.major.width'] =2
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['legend.numpoints'] = 1
mpl.rcParams['axes.labelweight']='semibold'
mpl.rcParams['mathtext.fontset']='stix'
mpl.rcParams['font.weight'] = 'bold'
import pandas as pd
from uvwmc import uvwmc
from uvwmc import uvw6D

def slx(p,mu,cov,model=False):

    y = mu[:,0]*p[1]+p[0]
    lnlike = np.sum(-0.5*(mu[:,1]-y)**2/(cov + p[2]**2)-0.5*np.log(cov + p[2]**2))
    if model == True:
        return y,lnlike
    
    return lnlike

    
    


# xx =pd.read_csv('PscEri-FullList-20190302.txt',sep=' ')
# data = xx.to_records()
# missed = np.zeros(len(data),dtype=int)
# Vizier.columns=['all']
# for i in range(len(data)):
#     print(i)
#     thisname = 'Gaia DR2 '+str(data[i].DR2Name)
#     stuff = Vizier.query_region(coord.SkyCoord(ra=data[i].RA,dec=data[i].Dec,unit=(u.deg, u.deg),frame='icrs'),radius="0d0m10s",catalog='I/345/gaia2')
#     dude = stuff[0].to_pandas()
#     match = np.where(dude.DR2Name.values == thisname)[0]
#     
#     if len(match) < 1: 
#         print('something missing')
#         missed[i] = 1
#         pdb.set_trace()
#     if len(match) ==1:
#         if i == 0: 
#             thedf = dude[match[0]:match[0]+1].copy()
#         
#         if i > 0:
#             thedf = thedf.append(dude[match[0]:match[0]+1])
#             #print('here')
#             #pdb.set_trace()
# 
# thedf.to_csv(path_or_buf= 'psceri_curtis_fullgaia.csv')




udata = pd.read_csv('psceri_curtis_fullgaia.csv').to_records()
ra = udata.RAJ2000
dec = udata.DEJ2000
qwe = np.where(np.isnan(udata.RV)==False)[0]
data = udata[qwe]
u = np.zeros(len(data))-99999
v = np.zeros(len(data))-99999
w = np.zeros(len(data))-99999
su = np.zeros(len(data))-99999
sv = np.zeros(len(data))-99999
sw = np.zeros(len(data))-99999
thiscorr = np.diag([data])
psvect = np.zeros((len(data),6))
pscov  = np.zeros((len(data),6,6))
# for i in range(len(qwe)):
#     thiscorr = np.diag([data[i].e_RA_ICRS**2,data[i].e_DE_ICRS**2,data[i].e_Plx**2,data[i].e_pmRA**2,data[i].e_pmDE**2,data[i].e_RV])
#     thisvect = np.array([data[i].RA_ICRS,data[i].DE_ICRS,data[i].Plx,data[i].pmRA,data[i].pmDE,data[i].RV])
#     print i
#     ##construct covariance matrix:
#     thiscorr[0,1] = data[i].RADEcor*data[i].e_RA_ICRS*data[i].e_DE_ICRS
#     thiscorr[1,0] = data[i].RADEcor*data[i].e_RA_ICRS*data[i].e_DE_ICRS
#     thiscorr[2,1] = data[i].DEPlxcor*data[i].e_Plx*data[i].e_DE_ICRS
#     thiscorr[1,2] = data[i].DEPlxcor*data[i].e_Plx*data[i].e_DE_ICRS
#     thiscorr[2,0] = data[i].RAPlxcor*data[i].e_Plx*data[i].e_RA_ICRS
#     thiscorr[0,2] = data[i].RAPlxcor*data[i].e_Plx*data[i].e_RA_ICRS
#     thiscorr[3,2] = data[i].PlxpmRAcor*data[i].e_Plx*data[i].e_pmRA
#     thiscorr[2,3] = data[i].PlxpmRAcor*data[i].e_Plx*data[i].e_pmRA
#     thiscorr[3,1] = data[i].DEpmRAcor*data[i].e_DE_ICRS*data[i].e_pmRA
#     thiscorr[1,3] = data[i].DEpmRAcor*data[i].e_DE_ICRS*data[i].e_pmRA
#     thiscorr[3,0] = data[i].RApmRAcor*data[i].e_RA_ICRS*data[i].e_pmRA
#     thiscorr[0,3] = data[i].RApmRAcor*data[i].e_RA_ICRS*data[i].e_pmRA    
#     thiscorr[4,3] = data[i].pmRApmDEcor*data[i].e_pmDE*data[i].e_pmRA
#     thiscorr[3,4] = data[i].pmRApmDEcor*data[i].e_pmDE*data[i].e_pmRA
#     thiscorr[4,2] = data[i].PlxpmDEcor*data[i].e_pmDE*data[i].e_Plx
#     thiscorr[2,4] = data[i].PlxpmDEcor*data[i].e_pmDE*data[i].e_Plx
#     thiscorr[4,1] = data[i].DEpmDEcor*data[i].e_pmDE*data[i].e_DE_ICRS
#     thiscorr[1,4] = data[i].DEpmDEcor*data[i].e_pmDE*data[i].e_DE_ICRS
#     thiscorr[4,0] = data[i].RApmDEcor*data[i].e_pmDE*data[i].e_RA_ICRS
#     thiscorr[0,4] = data[i].RApmDEcor*data[i].e_pmDE*data[i].e_RA_ICRS
#     #no correlation to RV's?
#     
#     
#     psvect[i],pscov[i] = uvw6D(thisvect,thiscorr)
# 
#    # psvect[i] = stuff0.copy()
#    # pscov[i]   = stuff1[1].copy()
#    # pdb.set_trace()
#     
# pickle.dump((psvect,pscov),open('psceri_curtis_6Dphase.pkl','wb'))

psvect,pscov = pickle.load(open('psceri_curtis_6Dphase.pkl','rb'))



##now we want to make a model of UVW+XYZ for Psc-Eri
startpars = [55.0,-.9,1.0]
startsig = [10.,3.,5.0]
##define the starting positions for the walkers as a gaussian around some starting reasonable answer
##now x-y
use = np.where(psvect[:,1]>-999999999)[0]
for i in range(10):
    modelpars = np.polyfit(psvect[use,0],psvect[use,1],1)
    model = np.polyval(modelpars,psvect[use,0])
    rmsoff = np.sqrt(np.mean((model-psvect[use,1])**2))
    zxc= np.where(np.absolute(model-psvect[use,1])-2*rmsoff <= 0)[0]
    if i < 9:use=use[zxc]

model = np.polyval(modelpars,psvect[:,0])
ypars= modelpars*1.0
plt.plot(psvect[:,0],psvect[:,1],'.')
plt.plot(psvect[use,0],psvect[use,1],'.r')
plt.plot(psvect[:,0],model,'k')
plt.suptitle('Y')

use = np.where(psvect[:,1]>-999999999)[0]
for i in range(10):
    modelpars = np.polyfit(psvect[use,0],psvect[use,2],1)
    model = np.polyval(modelpars,psvect[use,0])
    rmsoff = np.sqrt(np.mean((model-psvect[use,2])**2))
    zxc= np.where(np.absolute(model-psvect[use,2])-2*rmsoff <= 0)[0]
    if i < 9:use=use[zxc]

model = np.polyval(modelpars,psvect[:,0])
zpars= modelpars*1.0
plt.figure()
plt.plot(psvect[:,0],psvect[:,2],'.')
plt.plot(psvect[use,0],psvect[use,2],'.r')
plt.plot(psvect[:,0],model,'k')
plt.suptitle('Z')

use = np.where(psvect[:,3]>-999)[0]
for i in range(10):
    modelpars = np.polyfit(psvect[use,0],psvect[use,3],1)
    model = np.polyval(modelpars,psvect[use,0])
    rmsoff = np.sqrt(np.mean((model-psvect[use,3])**2))
    zxc= np.where(np.absolute(model-psvect[use,3])-2*rmsoff <= 0)[0]
    if i < 9:use=use[zxc]

model = np.polyval(modelpars,psvect[:,0])
upars= modelpars*1.0
plt.figure()
plt.plot(psvect[:,0],psvect[:,3],'.')
plt.plot(psvect[use,0],psvect[use,3],'.r')
plt.plot(psvect[:,0],model,'k')
plt.suptitle('U')

use = np.where(psvect[:,4]>-999)[0]
for i in range(10):
    modelpars = np.polyfit(psvect[use,0],psvect[use,4],1)
    model = np.polyval(modelpars,psvect[use,0])
    rmsoff = np.sqrt(np.mean((model-psvect[use,4])**2))
    zxc= np.where(np.absolute(model-psvect[use,4])-2*rmsoff <= 0)[0]
    if i < 9:use=use[zxc]

model = np.polyval(modelpars,psvect[:,0])
vpars= modelpars*1.0
plt.figure()
plt.plot(psvect[:,0],psvect[:,4],'.')
plt.plot(psvect[use,0],psvect[use,4],'.r')
plt.plot(psvect[:,0],model,'k')
plt.suptitle('V')

use = np.where(psvect[:,5]>-999)[0]
for i in range(10):
    modelpars = np.polyfit(psvect[use,0],psvect[use,5],1)
    model = np.polyval(modelpars,psvect[use,0])
    rmsoff = np.sqrt(np.mean((model-psvect[use,5])**2))
    zxc= np.where(np.absolute(model-psvect[use,5])-2*rmsoff <= 0)[0]
    if i < 9:use=use[zxc]

model = np.polyval(modelpars,psvect[:,0])
wpars= modelpars*1.0
plt.figure()
plt.plot(psvect[:,0],psvect[:,5],'.')
plt.plot(psvect[use,0],psvect[use,5],'.r')
plt.plot(psvect[:,0],model,'k')
plt.suptitle('W')

plt.show()

outfile = open('psceri_linepars.txt','wb')
print('slope intercept \n')
outfile.write(str(ypars[0])+' ' + str(ypars[1]) +' \n')
outfile.write(str(zpars[0])+' ' + str(zpars[1]) +' \n')
outfile.write(str(upars[0])+' ' + str(upars[1]) +' \n')
outfile.write(str(vpars[0])+' ' + str(vpars[1]) +' \n')
outfile.write(str(wpars[0])+' ' + str(wpars[1]) +' \n')
outfile.flush()
outfile.close()




pdb.set_trace()
pdb.set_trace()