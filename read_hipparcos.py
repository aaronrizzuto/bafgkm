import numpy as np
from readcol import readcol
import pdb
import pickle

filename = "/Users/aaron/data_local/catalogs/hipparcos07/rawfiles/hip2.dat"

rfile    = "/Users/arizz/data_local/catalogs/hipparcos07/rawfiles/ReadMe"

##read the header names first
#readcol(filename,skipline=0,skipafter=0,names=False,fsep=None,twod=True,
#       fixedformat=None,asdict=False,comment='#',verbose=True,nullval=None,
#        asStruct=False,namecomment=True,removeblanks=False,header_badchars=None,
#        asRecArray=False):


#heads = readcol(rfile,skipline=68,names=False)

#pdb.set_trace()
##read the data
rawdata = readcol(filename,names=False)
size = rawdata.shape
#pdb.set_trace()
names = ['hip','ra','dec','plx','pmra','pmdec','sig_ra','sig_dec','sig_plx','sig_pmra','sig_pmdec','Hpmag','sig_hpmag','bv','sig_bv']
spots = [0,4,5,6,7,8,9,10,11,12,13,19,20,23,24]
type  = [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
#data = []
#for i in range(0,size[0]-1):
#	source = {}
#	for j in range(0,len(spots)-1):
#		if type[j] == 0:
#			source[names[j]] = int(rawdata[i,spots[j]])
#		else:
#			source[names[j]] = rawdata[i,spots[j]]
#	data.append(source)
##try with recarray
##make data types are aliased titles 
dts = []
#pdb.set_trace()
for i in range(0,len(type)):
	if type[i] == 0:
		dts.append((names[i],long))
	if type[i] == 1:
		dts.append((names[i],float))
pdata = rawdata[:,spots]
data = np.recarray((size[0],),dtype=dts)
#now fill in the data

dnames = data.dtype.names

for i in range(0,len(dnames)):
	data[dnames[i]] = rawdata[:,spots[i]]

pickle.dump(data, open( "hip_main.pkl", "wb" ) )

#pdb.set_trace()


