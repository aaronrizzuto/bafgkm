import pdb
import pickle
from readcol import readcol 
import numpy as np
import numpy.lib.recfunctions as rcf

np.set_printoptions(suppress=True,precision=5)
file = "/Users/arizz/data_local/catalogs/hipparcos07/rawfiles/hip2.dat"

hip = readcol(file, asRecArray=True)
pdb.set_trace()
hip.ra = hip.ra*180/np.pi
hip.dec = hip.dec*180/np.pi

## now build the covariance matrices
uwmat = np.zeros((len(hip.ra),5,5))

uwmat[:,0,0] = hip.u1
uwmat[:,0,1] = hip.u3
uwmat[:,0,2] = hip.u4
uwmat[:,0,3] = hip.u7
uwmat[:,0,4] = hip.u11
uwmat[:,1,1] = hip.u3
uwmat[:,1,2] = hip.u5
uwmat[:,1,3] = hip.u8
uwmat[:,1,4] = hip.u12
uwmat[:,2,2] = hip.u6
uwmat[:,2,3] = hip.u9
uwmat[:,2,4] = hip.u13
uwmat[:,3,3] = hip.u10
uwmat[:,3,4] = hip.u14
uwmat[:,4,4] = hip.u15

covmat = np.zeros((len(hip.ra),5,5))
cc  = 1.0/3600./1000.0
convmat = np.array([[cc**2,cc**2,cc,cc,cc],[cc**2,cc**2,cc,cc,cc],[cc,cc,1.,1.,1.],[cc,cc,1.,1.,1.],[cc,cc,1.,1.,1.]])
for i in range(76348,len(hip.ra)):
    cm = np.linalg.inv(np.dot(np.transpose(uwmat[i]),uwmat[i]))
    cm2 = np.dot(np.transpose(uwmat[i]),uwmat[i])
    covmat[i,:,:] = np.linalg.inv(np.dot(np.transpose(uwmat[i]),uwmat[i]))*convmat
    xvect = np.array([hip.ra[i],hip.dec[i],hip.plx[i],hip.pmra[i],hip.pmdec[i]])
    pdb.set_trace()
    #covmat2       = np.linalg.inv(np.dot(np.linalg.inv(uwmat[i])
    if np.mod(i,1000) == 0: print "up to " + str(i+1) + " out of " +str(len(hip.ra)) 
    pdb.set_trace()     
##now make the output structure

##save them as two complementary variables
pickle.dump(hip,open("hip_main2.pkl","wb"))
pickle.dump(covmat,open("hip_main2_covmat.pkl","wb"))



pdb.set_trace()
print 'im here'