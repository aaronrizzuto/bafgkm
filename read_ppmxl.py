import numpy as np
import pdb
import pickle
#import subprocess
import os 
import glob
from readcol import readcol
from numpy.lib.recfunctions import stack_arrays
def ppmxl_readfile(fname):
	with open(fname) as f:
		#pdb.set_trace()
		lines = f.readlines()
	lines = lines[1:]
	data = np.recarray((len(lines),),dtype=[('name',int),('ra',float),('dec',float),('pmra',float),('pmdec',float),('sig_pmra',float),('sig_pmdec',float)])
	for i in range(0,len(lines)):
		line = lines[i]
		data[i].name = int(line[0:19])
		data[i].ra     = float(line[20:30]) 
		data[i].dec     =  float(line[30:40]) 
		data[i].pmra   =  float(line[40:50]) 
		data[i].pmdec    = float(line[50:58]) 
		data[i].sig_pmra = float(line[82:87]) 
		data[i].sig_pmdec = float(line[87:92]) 
	#pdb.set_trace()
	return data

##set limits on what to scoop up
#ramin  = 20.0
#ramax  = 100.0 
#decmin = -30.0
#decmax = 60.0
#outname = 'hyades_ppmxl'
ramin   = 45.
ramax   = 70.
decmin  = 10.
decmax  = 40.
outname = "pleiades_ppmxl"


## find all the ppmxl data files in the ppmxl directory
os.chdir("/Volumes/UTRAID/ppmxl")
flist = ['first']
for file in glob.glob("*.dat"):
	flist.append(file)
flist = flist[1:]
#flist=flist[0:2]

##now loop through all the files and get appropriate entires.
pdb.set_trace()

doneone=0
for i in range(0,len(flist)):
	thisfile = "/Volumes/UTRAID/ppmxl/"+flist[i]
	fdat = ppmxl_readfile(thisfile)
	inlims = np.where((fdat.ra < ramax) & (fdat.ra > ramin) & (fdat.dec>decmin) &(fdat.dec<decmax))[0]
	if len(inlims) > 0: 
		fdat = fdat[inlims]
		if doneone==0:
			ppdat = fdat.copy()
			doneone = 1 
		if doneone==1: ppdat=stack_arrays((ppdat,fdat),asrecarray=True,usemask=False)
	plines = "done " + str(i) + " out of " + str(len(flist))
	print plines
pickle.dump(ppdat, open(outname+".pkl", "wb" ))


pdb.set_trace()


print 'im here'
