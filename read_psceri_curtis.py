import numpy as np
import os,sys,glob,pdb,pickle
sys.path.append(os.getenv("HOME") + '/python/BAFGKM/')
sys.path.append(os.getenv("HOME") + '/python/projects/K2pipe')
import bspline_acr as bspline

import matplotlib.pyplot as plt
import matplotlib as mpl
from astroquery.vizier import Vizier
import astropy.units as u
import astropy.coordinates as coord

##my imports
from gcirc import gcirc
from readcol import readcol
from uvwtopm import uvwtopm
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from gcirc import gcirc
import cluster_membership_tools
from rejsamp import rejsamp_opt
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from helpers import dataset_looper
from gal_xyz import gal_xyz
mpl.rcParams['lines.linewidth']   = 3
mpl.rcParams['axes.linewidth']    = 2
mpl.rcParams['xtick.major.width'] =2
mpl.rcParams['ytick.major.width'] =2
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['legend.numpoints'] = 1
mpl.rcParams['axes.labelweight']='semibold'
mpl.rcParams['mathtext.fontset']='stix'
mpl.rcParams['font.weight'] = 'bold'
import pandas as pd
from uvwmc import uvwmc


# xx =pd.read_csv('PscEri-FullList-20190302.txt',sep=' ')
# data = xx.to_records()
# missed = np.zeros(len(data),dtype=int)
# Vizier.columns=['all']
# for i in range(len(data)):
#     print(i)
#     thisname = 'Gaia DR2 '+str(data[i].DR2Name)
#     stuff = Vizier.query_region(coord.SkyCoord(ra=data[i].RA,dec=data[i].Dec,unit=(u.deg, u.deg),frame='icrs'),radius="0d0m10s",catalog='I/345/gaia2')
#     dude = stuff[0].to_pandas()
#     match = np.where(dude.DR2Name.values == thisname)[0]
#     
#     if len(match) < 1: 
#         print('something missing')
#         missed[i] = 1
#         pdb.set_trace()
#     if len(match) ==1:
#         if i == 0: 
#             thedf = dude[match[0]:match[0]+1].copy()
#         
#         if i > 0:
#             thedf = thedf.append(dude[match[0]:match[0]+1])
#             #print('here')
#             #pdb.set_trace()
# 
# thedf.to_csv(path_or_buf= 'psceri_curtis_fullgaia.csv')


data = pd.read_csv('psceri_curtis_fullgaia.csv').to_records()
ra = data.RAJ2000
dec = data.DEJ2000
qwe = np.where(np.isnan(data.RV)==False)[0]
# u = np.zeros(len(data))-99999
# v = np.zeros(len(data))-99999
# w = np.zeros(len(data))-99999
# su = np.zeros(len(data))-99999
# sv = np.zeros(len(data))-99999
# sw = np.zeros(len(data))-99999
# for i in range(len(qwe)):
#     print i
#     u[qwe[i]],v[qwe[i]],w[qwe[i]],su[qwe[i]],sv[qwe[i]],sw[qwe[i]] = uvwmc(data[qwe[i]].RAJ2000,data[qwe[i]].DEJ2000,data[qwe[i]].Plx,data[qwe[i]].pmRA,data[qwe[i]].pmDE,data[qwe[i]].RV,data[qwe[i]].e_Plx,data[qwe[i]].e_pmRA,data[qwe[i]].e_pmDE,data[qwe[i]].e_RV)
# 
# pickle.dump((u,v,w,su,sv,sw),open('psceri_curtis_uvw.pkl','wb'))

u,v,w,su,sv,sw = pickle.load(open('psceri_curtis_uvw.pkl','rb'))

x,y,z = gal_xyz(data.GLON,data.GLAT,data.Plx,radec=False,plx=True)







pdb.set_trace()