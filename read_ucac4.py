import numpy as np
from readcol import readcol
import pdb
import pickle
import matplotlib.pyplot as plt

##read an individual ucac4 file
##filenum is an integer
def ucac4_readfile(filenum):
	read_dir = "/Users/arizz/data_local/catalogs/ucac4/raw/ascii/"
	num = str(filenum)
	if filenum <100:
		num = '0'+str(filenum)
	if filenum <10:
		num = '00'+str(filenum)
	
	file_to_read = read_dir+"z"+num+".asc"
	thedata = readcol(file_to_read,twod=True)
	##rescale things to make sense
	##RADEC TO RADIANS FROM MAS
	thedata[:,0:2] = thedata[:,0:2]/3600.0/1000.0*np.pi/180.0
	thedata[:,1]   = thedata[:,1]-np.pi/2.0
	##fix mags from mmag
	thedata[:,2:4] = thedata[:,2:4]/1000.0
	thedata[:,19:22]= thedata[:,19:22]/1000.0
	thedata[:,28:33]= thedata[:,28:33]/1000.0
	##fix mag errors from 1/100 mag
	thedata[:,4] = thedata[:,4]/100.0
	thedata[:,25:28] = thedata[:,25:28]/100.0
	thedata[:,33:39] = thedata[:,33:39]/100.0
	#fix proper motions
	thedata[:,14:18] = thedata[:,14:18]/10.0
	return thedata
	
	
#def ucac4_getcut(ramin,ramax,decmin,decmax,outname):
#ramin  = 55.0
#ramax  = 75.0 
#decmin = 15.0
#decmax = 35.0
#outname = "taurus_ucac4"
#ramin  = 20.0
#ramax  = 100.0 
#decmin = -30.0
#decmax = 60.0
#outname = "hyades_ucac4"
#ramin   = 45.
#ramax   = 70.
#decmin  = 10.
#decmax  = 40.
#outname = "pleiades_ucac4"
#ramin   = 130.
#ramax   = 270.
#decmin  = -62.
#decmax  = -4.
#outname = "sco_ucac4"

ramin = 225.0
ramax = 242.0
decmin = -28.0
decmax = -12.0
outname = "K2C15_ucac4"


	##convert to radians
ramin  = ramin*np.pi/180
ramax  = ramax*np.pi/180
decmin = decmin*np.pi/180
decmax = decmax*np.pi/180

##which files do i read???
spdist_min = decmin*180/np.pi + 90.0
spdist_max = decmax*180/np.pi + 90.0
startfile  = int(round(spdist_min/0.2) - 2)
endfile    = int(round(spdist_max/0.2) + 2)
numbers = range(startfile,endfile+1)
pdb.set_trace()

for num in range(0,len(numbers)):
	fnum = numbers[num]
	thedata  = ucac4_readfile(fnum)
	space_cut = np.where((thedata[:,0]>ramin) & (thedata[:,0]<ramax) & (thedata[:,1]>decmin) &(thedata[:,1]<decmax))
	thiscdat  = thedata[space_cut,:][0,:,:]
	#pdb.set_trace()
		##append if thats the case
	if num !=0:
		cdat = np.append(cdat,thiscdat,axis=0)
	if num ==0:
		cdat = thiscdat
	#pdb.set_trace()
	print "up to " + str(fnum)
	
	##make into recarray, dont keep everything I guess
#pdb.set_trace()	
names = ['ra','dec','sig_ra','sig_dec','pmra','pmdec','sig_pmra','sig_pmdec','id2m','j','h','k','sig_j','sig_h','sig_k','bap','vap','gap','rap','iap','sig_bap','sig_vap','sig_gap','sig_rap','sig_iap','x2m']
fields = len(names)
dts   = ['float','float','float','float','float','float','float','float','int','float','float','float','float','float','float','float','float','float','float','float','float','float','float','float','float','int']
spots = [0,1,7,8,14,15,16,17,18,19,20,21,25,26,27,28,29,30,31,32,33,34,35,36,37,49]
size  = cdat.shape	
odat  = np.recarray((size[0],),formats = dts,names=names)
#pdb.set_trace()
for i in range(0,fields):
	odat[names[i]] = cdat[:,spots[i]]

#pdb.set_trace()	
pickle.dump(odat, open( outname+".pkl", "wb" ))

	
