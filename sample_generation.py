##IMPORTS
import numpy as np
import matplotlib.pyplot as plt
##UTILS imports:
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz


##ASTROLIB imports:
from ACRastro.glactc import glactc


##function that builds a sample of xyzuvw values in a 6d gaussian
##posvel  =    (X,Y,Z,U,V,W)
##cposvel = COV(X,Y,Z,U,V,W)
##nsamp = number of random samples you want from the distribution
##age   = trace-forward age
##OPTIONS:
##trace_forward = do/donot run trace_forward option 
##observables   = do/donot convert xyzuvw sample into observables (gall,galb,plx,pmra,pmdec,vrad)
def build_sample_group(posvel,cposvel,age,nsamp,trace_forward=False,observables=False):
	##Holy Crap this function exists!
	##This should be a nsamp-by-6 array
	##means using mcmc to sample this is pointless I think
	#pdb.set_trace()
	rposvel = np.random.multivariate_normal(posvel,cposvel,nsamp)
	if trace_forward == True:
		trpos = linear_trace_forward(age,rposvel[:,0:3],rposvel[:,3:6])
		rposvel[:,0:3] = np.array(trpos)
	if observables == False:
		return rposvel
	if observables == True:
		ll,bb,plx  = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
		ra,dec     = glactc(ll*180/np.pi,bb*180/np.pi,2000,degree=True,reverse=True,fk4=True) ##use fk4 for speed (slightly inaccurate but not huge)
		pmvect     = uvwtopm(rposvel[:,3:6],ra*np.pi/180,dec*np.pi/180,plx) ##array with (vrad,pmra,pmdec)
		return  ra*np.pi/180.0,dec*np.pi/180.0,ll,bb,plx,pmvect
	
##Function to make the field sample velocities/positions input is number of samples wanted
def build_uvw_field(nsamp):
	V_ad       = 3.1     ##asymmetric drift of galaxy in lsr
	sig_u_f    = 19.8    ##in lsr
	sig_v_f    = 12.8    ##in lsr
	sig_w_f    = 8.0     ##in lsr
	##LSR conversion values
	uvw_sun    = np.array([-9.0,12.0,7.0])
	##the actual uvw for the field not in lsr
	uvw_field  = np.array([0.0,V_ad,0.0])-uvw_sun
	cov_field  = np.array([[sig_u_f**2,0.0,0.0],[0.0,sig_v_f**2,0.0],[0.0,0.0,sig_w_f**2]])
	uvw_field   = np.random.multivariate_normal(uvw_field,cov_field,nsamp)
	return uvw_field
	
##Gaussian 6D probability for a single position in xyzuvw space with trace forward
##Currently not used at all
def sample_lnprob(pos,posvel,cposvel,age):
	newpos = linear_trace_forward(age,posvel[0:3],posvel[3:6])
	posvel[0:3] = newpos[0:3]
	diff=pos-posvel
	icposvel = np.linalg.inv(cposvel)	
	return -np.dot(diff,np.dot(icposvel,diff))/2.0
	
#Python function to run a basic linear trace-forward on a set of X,Y,W,U,V,W numbers 
#to a given age in the future.
#Age is an age in Myr, should be input as float
#pos/vel are numpy.ndarrays with size (#stars,3) with each row containing X,Y,Z/U,V,W numbers 
#for a single star
#output is formatted in the same way as pos/vel

##TODO: make an option that does more fancy trace forwards
#import pdb
def linear_trace_forward(age,pos,vel):
	pcmyr  = 1.022712165 #pc/km/myr converter
	#newpos = pos + vel*age*pcmyr
	#return newpos
	##pdb.set_trace()
	return pos + vel*age*pcmyr