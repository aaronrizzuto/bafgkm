import numpy as np
import pickle,os
import pdb
import matplotlib.pyplot as plt
import time
#import emcee
from scipy.io.idl import readsav as idlrsave

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
import idlsave

from gcirc import gcirc
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from taurus_paramgen import taurus_paramgen
from taurus_paramgen import taurus_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
#np.set_printoptions(suppress=True,precision=2)
import K2fov
from readcol import readcol



uvw_a      = np.array([-0.13389093  ,    0.0089457638   ,     0.019123222])
uvw_b      = np.array([50.863992    ,    -20.810681     ,     -12.262665])
#vel = uvw_a*stardat[qwe[0]].l*180/np.pi+uvw_b
distparams = np.array([0.0058992004,-0.0072425234])
outname = "scocen_k2c15_161214"

dpi = np.pi
##load just the taurus patch of sky for speed
#datacat    = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/taurus_tgas.pkl','rb'))
#datacat    = pickle.load(open(os.getenv("HOME")+'/data_local/catalogs/hipparcos07/hip_main.pkl','rb'))

datacat = pickle.load(open('K2C15_full_cross.pkl','rb'))


stardat = np.recarray(len(datacat.ra),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id','|S22'),('pmcov',float)])
stardat.id = datacat.id2m
stardat.ra        = datacat.ra
stardat.dec       = datacat.dec
stardat.pmra      = datacat.pmra
stardat.pmdec     = datacat.pmdec
stardat.sig_pmra  = datacat.sig_pmra
stardat.sig_pmdec = datacat.sig_pmdec
stardat.plx       = 0.0
stardat.sig_plx   = 0.0
#stardat.rv        = rvcat
#stardat.sig_rv    = sig_rv
stardat.rv        = 0.0
stardat.sig_rv    = 300.0

gl,gb     = glactc(datacat.ra*180/np.pi,datacat.dec*180/np.pi,2000,degree=True,fk4=True)
stardat.l         = gl*np.pi/180.0
stardat.b         = gb*np.pi/180.0
stardat.pmcov = 0.0

jmag     = datacat.j
hmag     = datacat.h
kmag     = datacat.k
bap      = datacat.bap
vap      = datacat.vap
gap      = datacat.gap
iap      = datacat.iap
rap      = datacat.rap
##auxilliary data that the velocity integral doesn't need, but might be useful in 
##excluding non-members or calculating other numbers for input to the velocity
##integrals
auxdat = np.transpose(np.array([jmag,hmag,kmag,bap,vap,gap,iap,rap]))
#pdb.set_trace()
###remove everything with colors outside some limits
hasap = np.where((bap <20) & (vap <20))
hask = np.where((kmag <20))

hjk = np.zeros(len(jmag),'int')-1
hap = np.zeros(len(jmag),'int')-1
hjk[hask]  = 1
hap[hasap] = 1

#pdb.set_trace()
#expected propoer motions
# exp_pmra  = np.zeros(len(stardat.ra))
# exp_pmdec = np.zeros(len(stardat.ra))
# exp_dist  = np.zeros(len(stardat.ra))
# exp_rv    = np.zeros(len(stardat.ra))
# for i in range(len(stardat.ra)):
#     thisvel  = uvw_a*stardat.l[i]*180/np.pi + uvw_b
#     exp_dist[i] = (distparams[0]*np.cos(stardat.l[i])+ distparams[1]*np.sin(stardat.l[i]))**(-1)
#     #pdb.set_trace()
#     thispmv  = uvwtopm(thisvel,stardat.ra[i],stardat.dec[i],1000.0/exp_dist[i])
#     exp_pmra[i] = thispmv[1]
#     exp_pmdec[i] = thispmv[2]
#     exp_rv[i] = thispmv[0]
#     print "up to " + str(i+1) + " out of " + str(len(stardat.id))
# pickle.dump((exp_pmra,exp_pmdec,exp_rv,exp_dist),open('expected_val_k2c15_scocen.pkl','wb'))
exp_pmra,exp_pmdec,exp_rv,exp_dist = pickle.load(open('expected_val_k2c15_scocen.pkl','rb'))
dpmra  = exp_pmra-stardat.pmra
dpmdec = exp_pmdec-stardat.pmdec

##crossmatch with known
known = readcol('ScoCenAll_forpython.csv',fsep=',',asRecArray=True)
# sx = np.zeros(len(stardat.id),dtype=int)-1
# for i in range(0,len(stardat.id)):
#     dist = gcirc(known.radeg,known.decdeg,stardat.ra[i]*180/np.pi,stardat.dec[i]*180/np.pi,format=2)
#     close = np.argmin(dist)
#     if dist[close] < 10.0: sx[i] = close
#     print "up to " + str(i+1) + " out of " + str(len(stardat.id))
# pickle.dump(sx,open('cross_scocen_k2c15_known.pkl','wb'))
sx = pickle.load(open('cross_scocen_k2c15_known.pkl','rb'))
sxk = np.where(sx != -1)[0]
aaa = np.where(known.mem[sx[sxk]] == 'Y')[0]
nnn = np.where(known.mem[sx[sxk]] == 'N')[0]
sxka = sxk[aaa]
sxkn = sxk[nnn]

cpmra = np.zeros(len(sxka))
cpmde = np.zeros(len(sxka))
vel = np.array([6.4,-15.9,-7.4])
for i in range(len(sxka)):
    expd = (distparams[0]*np.cos(stardat.l[sxka[i]])+ distparams[1]*np.sin(stardat.l[sxka[i]]))**(-1)

    thispmv = uvwtopm(vel,stardat.ra[sxka[i]],stardat.dec[sxka[i]],1000.0/expd)
    cpmra[i] = thispmv[1]
    cpmde[i] = thispmv[2]
    
#pdb.set_trace()




#pdb.set_trace()
##color cut
#incolor = np.where(((jmag-kmag<2.0) | (hjk==-1)) & ((vap-kmag<10.0) | ((hjk==-1) | (hap==-1))) & ((bap-vap<3.0) | (hap==-1)) & ((bap-vap>0.1) | (hap==-1)) & ((jmag-kmag>0.1) | (hjk==-1)) & ((hap != -1) | (hjk != -1)))[0]
incolor = np.where((hap == 1) | (hjk == 1))[0] 
auxdat  =  auxdat[incolor,:]
stardat = stardat[incolor]
datacat = datacat[incolor]
vap     = vap[incolor]
dpmra   = dpmra[incolor]
dpmdec  = dpmdec[incolor]
sx = sx[incolor]
#pdb.set_trace()
##magnitude cut
#inmag = np.where((vap <= 13.0) & (vap >= 11.0))[0]
#auxdat = auxdat[inmag,:]
#stardat = stardat[inmag]
#datacat = datacat[inmag]
#pdb.set_trace()


jkd_ms,vkd_ms,bvd_ms,dist_ms,sig_dist_ms = phot_dist_interp(auxdat[:,3],auxdat[:,4],auxdat[:,0],auxdat[:,2])
jkd_10,vkd_10,bvd_10,dist_10,sig_dist_10 = phot_dist_interp(auxdat[:,3],auxdat[:,4],auxdat[:,0],auxdat[:,2],age=5)
distforhist = dist_ms[np.where(sig_dist_ms<499.9)[0]]



##throw away anything that isn't above the main sequence at 145.0 pc, this includes anything too 
##red to be in isochrone range, also insert the isochrone distances into the stardat array
##also get rid of things with group parallaxes less than 3.0 and gt 30.0
#pdb.set_trace()
sxk = np.where(sx != -1)[0]
aaa = np.where(known.mem[sx[sxk]] == 'Y')[0]
nnn = np.where(known.mem[sx[sxk]] == 'N')[0]
sxka = sxk[aaa]
sxkn = sxk[nnn]
#pdb.set_trace()

ams = np.where((1000.0/dist_10 > 3.0) & (1000.0/dist_10<40.0) & (np.absolute(dpmra) < 15.) & (np.absolute(dpmdec) < 15.))[0]
sigma_pmra = np.sqrt(stardat.sig_pmra**2 + 4.7**2)
sigma_pmde = np.sqrt(stardat.sig_pmdec**2 + 4.7**2)
pmrchi2 = (dpmra**2/sigma_pmra**2 + dpmdec**2/sigma_pmde**2)/2
# file = open('tmp2file.csv','wb')
# 
# for i in range(len(ams)):
#     file.write(str(stardat.ra[ams[i]]*180/np.pi)+','+str(stardat.dec[ams[i]]*180/np.pi)+','+str(10.0)+'\n')
#     file.flush()
# 
# 
# 
# scheck = K2fov.K2onSilicon('tmp2file.csv',15)
##now read the silico flag file
silfile = readcol('targets_siliconFlag.csv',fsep=',')
silflag = silfile[:,3].astype(int)*1
onsil = np.where(silflag == 2)[0]
ons    = ams[onsil]



x = datacat.j-datacat.k
kpmag = 0.42443603 + 3.7937617*x - 2.3267277*x**2 + 1.4602553*x**3 + datacat.k

b = datacat.bap
v = datacat.vap
r = -0.44*b + 1.44*v+0.12
g = 0.54*b + 0.46*v - 0.07
x=g-r
kpmag2 = 0.2*g+0.8*r
qwe = np.where(x > 8)[0]
if len(qwe) > 0: kpmag2[qwe] = 0.1*g[qwe] + 0.9*r[qwe]


qwe = np.where((silflag == 2) & ((kpmag2[ams] < 20) | (kpmag[ams] < 20)) & (stardat.ra[ams]*180/np.pi < 232.1) & (1000.0/dist_ms[ams] > 4) )[0]
pdb.set_trace()
ams = ams[qwe]


###
#pdb.set_trace()

stardat = stardat[ams]
auxdat  = auxdat[ams]
datacat = datacat[ams]
dist_10 = dist_10[ams]
sig_dist_10 = sig_dist_10[ams]
dist_ms = dist_ms[ams]
sig_dist_ms = sig_dist_ms[ams]
stardat.plx = 1000.0/dist_10
stardat.sig_plx = 1000.0/dist_10*sig_dist_10/dist_10
sx = sx[ams]
sxk = np.where(sx != -1)[0]
aaa = np.where(known.mem[sx[sxk]] == 'Y')[0]
nnn = np.where(known.mem[sx[sxk]] == 'N')[0]
sxka = sxk[aaa]
sxkn = sxk[nnn]



#pdb.set_trace()
#qwe = np.where((stardat.plx > 4.0) & (stardat.plx < 15.0) & (stardat.pmra < 0.0))[0]
##build the field samples
nfsamp = 100000 ##seems like 1-million samples is the ideal number for less ~1-2% pmem variation (and 100000 for group)
hist,binedges = np.histogram(stardat.plx,bins=200,range=(0.0,100.0),density=True)
fplx          = rejsamp(binedges[0:200],hist,nfsamp)
uvw_field     = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = 1000.0/fplx
rfl,rfb,rfplx = gal_xyz(rposvel_field[:,0],rposvel_field[:,1],rposvel_field[:,2],plx=True,reverse=True)
#pdb.set_trace()


#pdb.set_trace()
##remove stars we don't want to run the selection on 
#torun = np.where((stardat.plx > 4.0) & (stardat.plx < 15.0) & (stardat.pmra < 0.0))[0]
#pdb.set_trace()
#torun = np.array([1200,1201,1202,1203,1204,1205])
#torun = np.where(datacat.hip == '77900')[0]
#stardat.rv = 11.9
stardat_field = stardat.copy()
stardat_field.plx = 1000.0/dist_ms
stardat_field.sig_plx = sig_dist_ms/dist_ms*1000.0/dist_ms
pdb.set_trace()
pdb.set_trace()
#stardat = stardat[sxk]
#datacat = datacat[sxk]
#stardat_field = stardat_field[sxk]

sig_rg_bar  = 25.0

posvel = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
cposvel = np.diag([sig_rg_bar**2,0.0,0.0,9.0,9.0,9.0])
#rg_bar    = (distparams[0]*cos(l_star)+ distparams[1]*sin(l_star))^(-1)
distparams = np.array([0.0058992004,-0.0072425234])
uvw_a      = np.array([-0.13389093  ,    0.0089457638   ,     0.019123222])
uvw_b      = np.array([50.863992    ,    -20.810681     ,     -12.262665])
nsamp=10000

pmem = np.zeros(len(stardat.ra))
pdmg = pmem*1.0
pdmf = pmem*1.0
for i in range(len(stardat)):
    thisuvw = uvw_a*stardat.l[i]*180/np.pi + uvw_b
 #   thisuvw = np.array([6.4,-15.9,-7.4])
    thisdist = (distparams[0]*np.cos(stardat.l[i])+ distparams[1]*np.sin(stardat.l[i]))**(-1)
    thisposvel = np.array([thisdist,0.0,0.0,thisuvw[0],thisuvw[1],thisuvw[2]])
    rposvel = build_sample_group(thisposvel,cposvel,0.0,nsamp,trace_forward=False,observables=False)
    rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
    #pdb.set_trace()


    ##the field and group samples are now build, do the integrals
    chunks = 1
    
    ##field integral
    pdmodf = dataset_looper(stardat_field[i:i+1],rposvel_field,chunksize=chunks,field=True,verbose=False,doall=False)

    ##GROUP INTEGRAL
    ##field == True means not using positions in the integral
    pdmodg = dataset_looper(stardat[i:i+1],rposvel,chunksize=chunks,field=True,verbose=False,doall=False)
    tt = pdmodg/pdmodf
    pmem[i] = 0.02*tt/(1+0.02*tt)
    pdmf[i] = pdmodf*1.0
    pdmg[i] = pdmodg*1.0
    #pdb.set_trace()
    if np.mod(i,10)==0: print 'Up to ' + str(i+1) + ' out of ' + str(len(stardat.ra))
    #pdb.set_trace()

pickle.dump((stardat,pdmf,pdmg),open(outname+'_all.pkl','wb'))


pdb.set_trace()
print "im here"