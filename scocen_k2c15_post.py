import numpy as np
import pickle
import pdb,os
import matplotlib.pyplot as plt
import time
import idlsave
from gal_xyz import gal_xyz
import matplotlib as mpl
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from scipy.io.idl import readsav as idlrsave
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol
from k2_stuff import kpm_jhk
from phot_dist import phot_dist_interp
from subprocess import call
from taurus_paramgen import taurus_fastparams
from sample_generation import build_sample_group
from uvwtopm import uvwtopm
import K2fov


stardat,pdmf,pdmg = pickle.load(open('scocen_k2c15_161214_all.pkl','rb'))
datacat = pickle.load(open('scocen_k2c15_161214datacat.pkl','rb'))

prior = 0.015
bfact = pdmg/pdmf
prob = pdmg/pdmf*prior/(1+pdmg/pdmf*prior)

##just choose things where adams selection doesn't cover
qwe = np.where((prob > 0.04) & (stardat.ra*180/np.pi < 232.) & (stardat.plx != -100))[0]

##just output everything for now
file = open('scocenk2c15_out.csv','wb')
file.write('name,ra,dec,pmra,pmdec,plx,b,v,j,k,pmem \n')
file.flush
for i in range(len(qwe)):
    line = str(stardat.id[qwe[i]])+','+str(stardat.ra[qwe[i]]*180/np.pi)+','+str(stardat.dec[qwe[i]]*180/np.pi)+','+str(stardat.pmra[qwe[i]])+','+str(stardat.pmdec[qwe[i]])+','+str(stardat.plx[qwe[i]])+','+str(datacat.bap[qwe[i]])+','+str(datacat.vap[qwe[i]])+','+str(datacat.j[qwe[i]])+','+str(datacat.k[qwe[i]])+','+str(np.floor(prob[qwe[i]]*100.0).astype(int)) + ' \n'
    file.write(line)
    file.flush()
file.close()



pdb.set_trace()
print 'im here'