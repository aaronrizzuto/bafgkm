import numpy as np
import pickle,os
import pdb
import matplotlib.pyplot as plt
import time
#import emcee
from scipy.io.idl import readsav as idlrsave

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
import idlsave

from gcirc import gcirc
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from taurus_paramgen import taurus_paramgen
from taurus_paramgen import taurus_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from scipy.special import erf as scipyerf
from scipy.stats import truncnorm
#np.set_printoptions(suppress=True,precision=2)
from readcol import readcol

outname = "scocen_tgas_171002"

dpi = np.pi
##load just the taurus patch of sky for speed
#datacat    = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/taurus_tgas.pkl','rb'))
#datacat    = pickle.load(open(os.getenv("HOME")+'/data_local/catalogs/hipparcos07/hip_main.pkl','rb'))

datacat = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_scocen.pkl','rb'))
#gl,gb      = glactc(datacat.ra,datacat.dec,2000,degree=True,fk4=True)
#pdb.set_trace()
##this should get all of sco-cen plus the region covered by the K2C15 field <360 degrees

#region = np.where((gl > 280) & (gb < 45) & (gb > -10))[0]
#datacat = datacat[region]
#pickle.dump(datacat,open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_scocen.pkl','wb'))
#pdb.set_trace()
##crossmatch radial velocities here:
#pulk = readcol(os.getenv("HOME")+'/data_local/catalogs/pulkovo_rv/pulkovo.csv',fsep=',',asRecArray=True)
#gcrv = pickle.load(open(os.getenv("HOME")+'/data_local/catalogs/gcvr/gcrv.pkl','rb'))




# rvcat = np.zeros(len(datacat))
# sig_rv = np.zeros(len(datacat))+300.0
# for i in range(len(datacat)):
#     print 'up to ' + str(i)
#     if datacat.hip[i] != '':
#         mmm = np.where(pulk.hip == int(datacat.hip[i]))[0]
#         if len(mmm) != 0:
#             rvcat[i] = pulk.rv[mmm]
#             sig_rv[i] = pulk.erv[mmm]
#     wdist =  gcirc(datacat.ra[i]*180/np.pi,datacat.dec[i]*180/np.pi,gcrv.RA,gcrv.DEC,format=2)
#     close = np.argmin(wdist)
#     if wdist[close] < 10.0: 
#         rvcat[i] = gcrv.RV[close]
#         sig_rv[i] = gcrv.SIG_RV[close]
# pickle.dump((rvcat,sig_rv),open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_rvcross.pkl','wb'))    
rvcat,sig_rv = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_rvcross.pkl','rb'))              
                
#pdb.set_trace()

stardat = np.recarray(len(datacat.ra),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id','|S22'),('pmcov',float)])
for i in range(len(datacat.ra)): 
    if datacat.hip[i] != '': 
        thisid = datacat.hip[i] 
    else: thisid = datacat.tycho[i]
    stardat.id[i]        = thisid
stardat.ra        = datacat.ra*np.pi/180
stardat.dec       = datacat.dec*np.pi/180
stardat.pmra      = datacat.pmra
stardat.pmdec     = datacat.pmdec
stardat.sig_pmra  = datacat.sig_pmra
stardat.sig_pmdec = datacat.sig_pmdec
stardat.plx       = datacat.plx
stardat.sig_plx   = datacat.sig_plx
stardat.rv        = rvcat
stardat.sig_rv    = sig_rv
gl,gb     = glactc(datacat.ra,datacat.dec,2000,degree=True,fk4=True)
stardat.l         = gl*np.pi/180.0
stardat.b         = gb*np.pi/180.0
bmax = np.max(gb)
bmin = np.min(gb)
stardat.pmcov = 0.0
for i in range(len(datacat.ra)): stardat.pmcov[i] = datacat.covar[i][3,4]


#pdb.set_trace()
#qwe = np.where((stardat.plx > 4.0) & (stardat.plx < 15.0) & (stardat.pmra < 0.0))[0]
##build the field samples
nfsamp = 1000000 ##seems like 1-million samples is the ideal number for less ~1-2% pmem variation (and 100000 for group)
hist,binedges = np.histogram(stardat.plx,bins=200,range=(0.0,100.0),density=True)
fplx          = rejsamp(binedges[0:200],hist,nfsamp)
uvw_field     = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = 1000.0/fplx
rfl,rfb,rfplx = gal_xyz(rposvel_field[:,0],rposvel_field[:,1],rposvel_field[:,2],plx=True,reverse=True)
#pdb.set_trace()


#pdb.set_trace()
##remove stars we don't want to run the selection on 
torun = np.where((stardat.plx > 4.0) & (stardat.plx < 20.0) & (stardat.pmra < 0.0))[0]
#pdb.set_trace()
#torun = np.where(datacat.hip == '77900')[0]
#stardat.rv = 11.9
stardat = stardat[torun]
datacat = datacat[torun]


#pdb.set_trace()
sig_rg_bar  = 25.0
posvel = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
cposvel = np.diag([sig_rg_bar**2,0.0,0.0,9.0,9.0,9.0])
distparams = np.array([0.0058992004,-0.0072425234])
uvw_a      = np.array([-0.13389093  ,    0.0089457638   ,     0.019123222])
uvw_b      = np.array([50.863992    ,    -20.810681     ,     -12.262665])

bslope = 0.20952391
bint = -54.98
sigbslope = -0.097688882
sigbint = 45.612


nsamp=100000

pmem = np.zeros(len(stardat.ra))
pdmg = pmem*1.0
pdmf = pmem*1.0
for i in range(len(torun)):
    thisuvw = uvw_a*stardat.l[i]*180/np.pi + uvw_b
    thisdist = (distparams[0]*np.cos(stardat.l[i])+ distparams[1]*np.sin(stardat.l[i]))**(-1)
    thisposvel = np.array([thisdist,0.0,0.0,thisuvw[0],thisuvw[1],thisuvw[2]])
    rposvel = build_sample_group(thisposvel,cposvel,0.0,nsamp,trace_forward=False,observables=False)
    rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)


    ##the field and group samples are now build, do the integrals
    chunks = 1
    
    ##field integral
    pdmodf = dataset_looper(stardat[i:i+1],rposvel_field,chunksize=chunks,field=True,verbose=False,doall=False)    
    tnormf = truncnorm.pdf(stardat.b[i]*180/np.pi,bmin,bmax,0.0, 37.317092)
    pdmodf *= tnormf
    ##GROUP INTEGRAL
    ##field == True means not using positions in the integral
    pdmodg    = dataset_looper(stardat[i:i+1],rposvel,chunksize=chunks,field=True,verbose=False,doall=False)
    thisb     = stardat.l[i]*180/np.pi*bslope + bint
    this_sigb = stardat.l[i]*180/np.pi*sigbslope + sigbint
    tnormg = truncnorm.pdf(stardat.b[i]*180/np.pi,bmin,bmax,thisb,this_sigb)
    pdmodg *= tnormg    

    tt = pdmodg/pdmodf
    pmem[i] = 0.08*tt/(1+0.08*tt)
    pdmf[i] = pdmodf*1.0
    pdmg[i] = pdmodg*1.0
    if np.mod(i,10)==0: print 'Up to ' + str(i+1) + ' out of ' + str(len(stardat.ra))


pickle.dump((stardat,datacat,pdmf,pdmg),open(outname+'_all.pkl','wb'))


pdb.set_trace()
print "im here"