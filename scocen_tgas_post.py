import numpy as np
import pickle
import pdb,os
import matplotlib.pyplot as plt
import time
import idlsave
from gal_xyz import gal_xyz
from numpy.lib.recfunctions import stack_arrays
import matplotlib as mpl
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from scipy.io.idl import readsav as idlrsave
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol
from k2_stuff import kpm_jhk
from phot_dist import phot_dist_interp
from subprocess import call
from taurus_paramgen import taurus_fastparams
from sample_generation import build_sample_group
from uvwtopm import uvwtopm
import tic_query as tic
import tvguide

##set matplotlibglobal params
mpl.rcParams['lines.linewidth']   = 1.5
mpl.rcParams['axes.linewidth']    = 2
mpl.rcParams['xtick.major.width'] =2
mpl.rcParams['ytick.major.width'] =2
mpl.rcParams['ytick.labelsize'] = 18
mpl.rcParams['xtick.labelsize'] = 18
mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['legend.numpoints'] = 1
mpl.rcParams['axes.labelweight']='bold'
mpl.rcParams['axes.titlesize']=18
mpl.rcParams['axes.titleweight']='semibold'
mpl.rcParams['font.weight'] = 'bold'

#known = readcol('ScoCenAll_forpython.csv',fsep=',',asRecArray=True)
#get hip stars
#khip = np.zeros(len(known),dtype=int) -1 
#for i in range(len(known)): 
#    if known.name[i][0:3] == 'HIP' : khip[i] = 1



#np.set_printoptions(suppress=True,precision=5)


uvw_a      = np.array([-0.13389093  ,    0.0089457638   ,     0.019123222])
uvw_b      = np.array([50.863992    ,    -20.810681     ,     -12.262665])
#vel = uvw_a*stardat[qwe[0]].l*180/np.pi+uvw_b
distparams = np.array([0.0058992004,-0.0072425234])
#thisdist = (distparams[0]*np.cos(stardat.l[qwe[0]])+ distparams[1]*np.sin(stardat.l[qwe[0]]))**(-1)

##data of stars run through the bayes selection:ra,dec,l,b,prma,spmra,pmdec,spmdec,plx,sigplx,vr,sig_vr,id,pmcov
file1 = open('scocen_tgas_171002_all.pkl','rb')
stardat,datacat,pdmf,pdmg = pickle.load(file1)
#datacat = pickle.load(open('scocen_tgas_161201_datacat.pkl','rb'))
#rvcat,sig_rv = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_rvcross.pkl','rb'))              
##make a cross match of the original datacat
#datacat = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_scocen.pkl','rb'))
#dcat_index = np.zeros(len(stardat.id),dtype=int)-1



#bayes factor components
bfact = pdmg/pdmf
bad = np.where(np.isnan(bfact))[0] ##these tend to be stars with large proper motions that dont fit the group of the field
bfact[bad] = 0.001
bad2 = np.where(np.isinf(bfact))[0]##these tend to be stars with large proper motions that dont fit the group of the field
bfact[bad2] = 0.001
prior = 0.015
prob = prior*bfact/(prior*bfact+1)
#bbb = np.where((pdmg < 1e-7) & (prob > 0.5))[0]
#prob[bbb] = 0.0


#pdb.set_trace()
good = np.where((prob > 0.8) & (stardat.dec*180/np.pi > -25))[0]
good2 = np.where(prob >=0.8)[0]
#pdb.set_trace()
rah,ram,ras,ded,dem,des=radec(stardat.ra[good]*180/np.pi,stardat.dec[good]*180/np.pi)
nrd = nice_radec(rah,ram,ras,ded,dem,des,spc=' ',mspc=' & ')

for i in range(len(good)):
    print datacat.tycho[good[i]] + ' & ' + nrd[i] + ' & ' + str(np.around(datacat.v[good[i]],decimals=1)) + ' & ' + str(np.around(prob[good[i]]*100).astype(int)) + '\\' + '\\'
 

pdb.set_trace()



# exp_pmra  = np.zeros(len(stardat.ra))
# exp_pmdec = np.zeros(len(stardat.ra))
# exp_dist  = np.zeros(len(stardat.ra))
# exp_rv    = np.zeros(len(stardat.ra))
# for i in range(len(stardat.ra)):
#     thisvel  = uvw_a*stardat.l[i]*180/np.pi + uvw_b
#     exp_dist[i] = (distparams[0]*np.cos(stardat.l[i])+ distparams[1]*np.sin(stardat.l[i]))**(-1)
#     thispmv  = uvwtopm(thisvel,stardat.ra[i],stardat.dec[i],stardat.plx[i])
#     exp_pmra[i] = thispmv[1]
#     exp_pmdec[i] = thispmv[2]
#     exp_rv[i] = thispmv[0]
#     print i
# pickle.dump((exp_pmra,exp_pmdec,exp_rv,exp_dist),open('expected_val_tgas_scocen_171002.pkl','wb'))
exp_pmra,exp_pmdec,exp_rv,exp_dist = pickle.load(open('expected_val_tgas_scocen_171002.pkl','rb'))

##crossmatch with known
# sx = np.zeros(len(stardat.id),dtype=int)-1
# for i in range(0,len(stardat.id)):
#     dist = gcirc(known.radeg,known.decdeg,stardat.ra[i]*180/np.pi,stardat.dec[i]*180/np.pi,format=2)
#     close = np.argmin(dist)
#     if dist[close] < 20.0: sx[i] = close
#     print i
# pickle.dump(sx,open('cross_scocen_tgas_known_171002.pkl','wb'))
#sx = pickle.load(open('cross_scocen_tgas_known_171002.pkl','rb'))
#sxk = np.where(sx != -1)[0]
#aaa = np.where(known.mem[sx[sxk]] == 'Y')[0]
#nnn = np.where(known.mem[sx[sxk]] == 'N')[0]
#sxka = sxk[aaa]
#sxkn = sxk[nnn]

#qwe=  np.where((khip[sx[sxk]] == 1) & (known.mem[sx[sxk]] == 'Y'))[0]

##make a selection here:
cut1 = np.where(prob > 0.2)[0]

##now search for the tic info for these objects

start = 0
foundflag = np.ones(len(cut1),dtype=int)
inctl     = np.zeros(len(cut1),dtype=int)
obsable   = np.zeros(len(cut1),dtype=int)
outname = 'scocen_tgas_chose'
# start,foundflag,fulltic,inctl,obsable=pickle.load(open(outname + '_tmp.pkl','rb'))
# #pdb.set_trace()
# 
# for i in range(start,len(cut1)):
#     thisra = stardat.ra[cut1[i]]*180/np.pi
#     thisde = stardat.dec[cut1[i]]*180/np.pi
#     print 'up to ' + str(i+1) + ' out of ' + str(len(cut1))
#     
#     thistic = tic.tic_radec(thisra,thisde,10.0,closest=True)
#     if len(thistic) == 0: 
#         print 'no data found in TIC'
#         foundflag[i] = 0   
# 
#     if i == 0: fulltic = thistic.copy()
#     if i != 0: fulltic = stack_arrays((fulltic,thistic), asrecarray=True, usemask=False)
#     
#     thisctl = tic.ctl_tic(thisra,thisde,1.0)
#     if thisctl == 1:inctl[i] = 1
#    
#    
#     obsy = tvguide.check_observable(thisra,thisde,silent=True)
#     if type(obsy) == type(0): 
#         obsable[i] = 0
#     else: obsable[i] = 1
#     
# 
#     
#     if np.mod(i,100) == 0:
#         pickle.dump((i+1,foundflag,fulltic,inctl,obsable),open(outname + '_tmp.pkl','wb'))
#         
#         
#         
#         
# ##remove all the things I cant find
# found = np.where(foundflag == 1)[0]
# pickle.dump((fulltic,prob[cut1[found]],stardat[cut1[found]],inctl[found],obsable[found]),open(outname+'.pkl','wb'))

fulltic,pmem,sdat,inctl,obsle = pickle.load(open(outname+'.pkl','rb'))
#pdb.set_trace()

##read in the known members stuff:
ktic,memflag2,mem2 = pickle.load(open(os.getenv("HOME") + '/manuscripts/proposals/tessGO1/target_selection/scocen_members_tic.pkl','rb'))
kctl = pickle.load(open(os.getenv("HOME") + '/manuscripts/proposals/tessGO1/target_selection/scoctl.pkl','rb'))
kobsable = pickle.load(open(os.getenv("HOME") + '/manuscripts/proposals/tessGO1/target_selection/fulltic_obsable.pkl','rb'))
ttt = np.unique(ktic.ID,return_index = True)[1]
ktic = ktic[ttt]
memflag2 = memflag2[ttt]
mem2 = mem2[ttt]
kctl = kctl[ttt]
kobsable = kobsable[ttt]

#pdb.set_trace()
##kill all known members from tgas tables
kkeep = np.ones(len(fulltic),dtype=int)
for i in range(len(fulltic)):
    match = np.where(ktic.ID == fulltic[i].ID)[0]
    if len(match) > 0 : kkeep[i]= 0
    if len(match) > 1: 
        print 'waaat!!??!?!?!?'
        #pdb.set_trace()

keep = np.where(kkeep == 1)[0]
fulltic = fulltic[keep]
obsle = obsle[keep]
sdat = sdat[keep]
pmem=pmem[keep]
inctl=inctl[keep]

##now find all the targets:
mcut = [7.5,13]
kin = np.where((kobsable == 1) & (ktic.contratio <= 2)& (ktic.Tmag <= mcut[1]) & (ktic.Tmag > mcut[0]) & ((ktic.plx > 5) | (ktic.plx <0)) & (ktic.plx < 15.0))[0]
nin   = np.where((obsle == 1) & (fulltic.contratio <= 2) & (pmem > 0.5) & (fulltic.Tmag <= mcut[1]) & (fulltic.Tmag > mcut[0]))[0]

##make a target list from here
tlist = open('target_list_tessGI.csv','wb')
tlist.write('TIC,RA,DEC,PMRA,PMDEC,Tmag,Name,Ext,Spec,remark \n')
for i in range(len(kin)):
    thispmra = ktic.pmRA[kin[i]]
    thispmde = ktic.pmDEC[kin[i]]
    if thispmra < -1000.0: thispmra = 0.0 
    if thispmde < -1000.0: thispmde = 0.0 
    thisname = mem2[kin[i]].Name
    line = ktic.ID[kin[i]] + ',' + str(ktic.ra[kin[i]]) + ',' + str(ktic.dec[kin[i]])+ ','+ str(thispmra)+ ','+ str(thispmde) + ',' + str(ktic.Tmag[kin[i]]) + ',' + thisname+','+'N'+','+'N'+','+' \n' 
    tlist.write(line)
    
tlist.flush()
for i in range(len(nin)):
    thispmra = sdat[nin[i]].pmra
    thispmde = sdat[nin[i]].pmdec
    if thispmra < -1000.0: thispmra = 0.0 
    if thispmde < -1000.0: thispmde = 0.0 
    idstr = sdat[nin[i]].id
    thisname = 'TYC ' + idstr
    if len(idstr.split('-')) == 1: thisname = 'HIP'+idstr
    line = fulltic.ID[nin[i]] + ',' + str(fulltic.ra[nin[i]]) + ',' + str(fulltic.dec[nin[i]])+ ','+ str(thispmra)+ ','+ str(thispmde) + ',' + str(fulltic.Tmag[nin[i]]) + ',' + thisname+','+'N'+','+'N'+','+' \n' 
    tlist.write(line)
tlist.flush()
tlist.close()

zzz = np.where(pmem[nin] > 0.9)[0]
plt.plot(fulltic[nin].GAIAmag - fulltic[nin].Jmag,fulltic[nin].GAIAmag+5-5*np.log10(1000.0/sdat.plx[nin]),'.k')
qwe = np.where(ktic[kin].plx >0)[0]
plt.plot(ktic[kin[qwe]].GAIAmag - ktic[kin[qwe]].Jmag,ktic[kin[qwe]].GAIAmag+5-5*np.log10(1000.0/ktic.plx[kin[qwe]]),'.k')
plt.ylim([7.0,1.2])
plt.xlim([0.0,2.0])
plt.xlabel('Gaia G - J (mag)')
plt.ylabel('G (mag)')
plt.tight_layout()
plt.savefig('tgas_tessgisco_gjg.pdf')
plt.show()
#pdb.set_trace()
gjk = ktic[kin[qwe]].GAIAmag - ktic[kin[qwe]].Jmag
gk = ktic[kin[qwe]].GAIAmag+5-5*np.log10(1000.0/ktic.plx[kin[qwe]])
qwe2 =np.where((gjk > 0.5) & (gk < 2))[0]

gj  = fulltic[nin].GAIAmag - fulltic[nin].Jmag
gg = fulltic[nin].GAIAmag+5-5*np.log10(1000.0/sdat.plx[nin])
asd = np.where((gj > 1.0) & (gg < 2.5))[0] 


qwe = np.where(ktic[kin].plx >0)[0]
plt.plot(fulltic[nin].Bmag - fulltic[nin].Vmag,fulltic[nin].Vmag+5-5*np.log10(1000.0/sdat.plx[nin]),'.k')
plt.plot(ktic[kin[qwe]].Bmag - ktic[kin[qwe]].Vmag,ktic[kin[qwe]].Vmag+5-5*np.log10(1000.0/ktic.plx[kin[qwe]]),'.k')
plt.ylim([7.5,1.0])
plt.xlim([0,1.5])
plt.xlabel('B - V (mag)')
plt.ylabel('V (mag)')
plt.tight_layout()
plt.savefig('tgas_tessgisco_bvv.pdf')

plt.show()

qwe = np.where(ktic[kin].plx >0)[0]
plt.plot(fulltic[nin].Jmag - fulltic[nin].Kmag,fulltic[nin].Kmag+5-5*np.log10(1000.0/sdat.plx[nin]),'.b')

pdb.set_trace()

print 'Done'
pdb.set_trace()






