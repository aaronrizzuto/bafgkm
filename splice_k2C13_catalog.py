import numpy as np
import pdb
import matplotlib.pyplot as plt
import pickle
import time
import numpy.lib.recfunctions as nprec ##stack_arrays and append_fields



from wtmn  import swtmn
from gcirc import gcirc
##read hyades ucac + PPMXL
#hy_ucac =  pickle.load(open("K2C15_ucac4.pkl","rb")) 

##read in the ALK cross block
hy_ucac       = pickle.load(open("K2C15_alk_crosscat.pkl","rb"))
#pdb.set_trace()
##read in the ALK new stars
alknew         = pickle.load(open("K2C15_alk_new.pkl","rb"))
qwe = np.where((alknew.Kmag >0.001) & (alknew.Jmag > 0.001))[0]
alknew=alknew[qwe]
#pdb.set_trace()
##have to do 2 things: 
#1 splice alkblock into hy_ucac, should be a bijection
#2 add alknew to the array, making sure the right bits are preserved

##start with #1#alkblock.ra.shape[0]
dostartbit = 0
if dostartbit == 1:
    for i in range(alkblock.ra.shape[0]):
    ##if the star has no 2mass id then ignore it
        if alkblock.id2m[i] !=0: 
            match = np.where(hy_ucac.id2m == alkblock.id2m[i])[0]
    
   # pdb.set_trace()
            if len(match) > 1 : 
                best = np.where(hy_ucac[match].ra == alkblock.ra[i])[0]
                match = match[best]
            if len(match) == 0: pdb.set_trace()
    
            hy_ucac[match[0]].pmra      = alkblock.pmra[i]     
            hy_ucac[match[0]].pmdec     = alkblock.pmdec[i]     
            hy_ucac[match[0]].sig_pmra  = alkblock.sig_pmra[i]     
            hy_ucac[match[0]].sig_pmdec = alkblock.sig_pmdec[i] 
            #pdb.set_trace()
            print "matching " + str(i+1) + " out of " +str(alkblock.ra.shape[0])    
#pdb.set_trace()

##now onto #2
#first, add a new column to the hy_ucac recarray, fill it with nonsense
hy_ucac = nprec.append_fields(hy_ucac,'r',data=hy_ucac.k*99.0,usemask=False,asrecarray=True)
pdb.set_trace()
##now make a similar recarray to stack onto hy_ucac, that will contain the new entries
nnn  = nprec.stack_arrays((hy_ucac,hy_ucac),asrecarray=True,usemask=False)
newblock = nnn[0:alknew.shape[0]].copy()
newblock[:] = 9999.0 ##set everything to nonsense
for i in range(alknew.shape[0]):
    thisid = -i-1 ##give then negative ID's, seems reasonable and unique
    newblock.ra[i] = alknew.ra[i]*np.pi/180.0
    newblock.dec[i] = alknew.dec[i]*np.pi/180.0
    newblock.pmra[i] = alknew.pmra[i]
    newblock.sig_pmra[i] = alknew.sig_pm[i]
    newblock.pmdec[i] = alknew.pmdec[i]
    newblock.sig_pmdec[i] = alknew.sig_pm[i]    
    newblock.j[i] = alknew.Jmag[i]
    newblock.k[i] = alknew.Kmag[i]
    newblock.r[i] = alknew.R2mag[i]    
    newblock.bap[i] = alknew.B2mag[i]
    newblock.id2m[i] = thisid
    print "up to " + str(i+1) + " out of " +str(alknew.shape[0])

theout = nprec.stack_arrays((hy_ucac,newblock),asrecarray=True,usemask=False)
#pdb.set_trace()
outfile = "K2C15_full_cross.pkl"

pickle.dump(theout,open(outfile,"wb"))
#'ra', 'dec', 'sig_ra', 'sig_dec', 'pmra', 'pmdec', 'sig_pmra', 'sig_pmdec', 'id2m', 'j', 'h', 'k', 'sig_j', 
#'sig_h', 'sig_k', 'bap', 'vap', 'gap', 'rap', 'iap', 'sig_bap', 'sig_vap', 'sig_gap', 'sig_rap', 'sig_iap', 'x2m'


pdb.set_trace()
print "Im Here"