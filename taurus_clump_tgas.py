import numpy as np
import pickle,os
import pdb
import matplotlib.pyplot as plt
import time
#import emcee

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
import idlsave

from gcirc import gcirc
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
#from taurus_paramgen import taurus_paramgen
from taurus_clump_paramgen import taurus_clump_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
#np.set_printoptions(suppress=True,precision=2)
from readcol import readcol

##some testing of the parameters 
#posvel,cposvel = taurus_fastparams()
#mnf,cf = gal_to_obs_full(posvel,cposvel)
#test = readcol('clustering/betapic_fixed.csv',names=True,twod=True,asRecArray=True)
#wt = readcol('WTTS.csv',fsep=',',names=True,twod=True,asRecArray=True)
#ok = np.where((wt.FINAL2 != 'N') & (wt.FINAL2 != 'N?'))[0]

#pdb.set_trace()

outname = "Taurusclump_161010_tgas_nopos"



# datacat    = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/tgas_recarray.pkl','rb'))
# qwe = np.where((datacat.ra < 140) & (datacat.ra > 50) & (datacat.dec > 10) & (datacat.dec < 45))[0]
# datacat = datacat[qwe]
# pickle.dump(datacat,open(os.getenv("HOME")+'/data_local/tgas/taurus_clump_tgas.pkl','wb'))

#pdb.set_trace()
dpi = np.pi
##load just the taurus patch of sky for speed
datacat    = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/taurus_clump_tgas.pkl','rb'))
base_check = np.where((datacat.plx > 0.0))[0]
datacat    = datacat[base_check]

##crossmatch radial velocities here:
#pdb.set_trace()
pulk = readcol(os.getenv("HOME")+'/data_local/catalogs/pulkovo_rv/pulkovo.csv',fsep=',',asRecArray=True)
gcrv = pickle.load(open(os.getenv("HOME")+'/data_local/catalogs/gcvr/gcrv.pkl','rb'))
nearme = np.where((gcrv.ra < 140) & (gcrv.ra > 50) & (gcrv.dec > 10) & (gcrv.dec < 45))[0]
gcrv = gcrv[nearme]


#pdb.set_trace()
##cross match rvs in
rvcat = np.zeros(len(datacat))
sig_rv = np.zeros(len(datacat))+300.0
for i in range(len(datacat)):
    print 'up to ' + str(i)
    if datacat.hip[i] != '':
        mmm = np.where(pulk.hip == int(datacat.hip[i]))[0]
        if len(mmm) != 0:
            rvcat[i] = pulk.rv[mmm]
            sig_rv[i] = pulk.erv[mmm]
    wdist =  gcirc(datacat.ra[i],datacat.dec[i],gcrv.RA,gcrv.DEC,format=2)
    close = np.argmin(wdist)
    if wdist[close] < 10.0: 
        rvcat[i] = gcrv.RV[close]
        sig_rv[i] = gcrv.SIG_RV[close]
        
                
            

stardat = np.recarray(len(datacat.id),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',long),('pmcov',float)])
stardat.id        = datacat.id
stardat.ra        = datacat.ra*np.pi/180.
stardat.dec       = datacat.dec*np.pi/180.
stardat.pmra      = datacat.pmra
stardat.pmdec     = datacat.pmdec
stardat.sig_pmra  = datacat.sig_pmra
stardat.sig_pmdec = datacat.sig_pmdec
stardat.plx       = datacat.plx
stardat.sig_plx   = datacat.sig_plx
stardat.rv        = rvcat
stardat.sig_rv    = sig_rv
gl,gb     = glactc(datacat.ra,datacat.dec,2000,degree=True,fk4=True)
stardat.l         = gl*np.pi/180.0
stardat.b         = gb*np.pi/180.0
for i in range(len(datacat.id)): stardat.pmcov[i] = datacat.covar[i][3,4]


#pdb.set_trace()

##build the field samples
nfsamp = 100000
hist,binedges = np.histogram(stardat.plx,bins=200,range=(0.0,100.0),density=True)
fplx          = rejsamp(binedges[0:200],hist,nfsamp)
uvw_field     = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = 1000.0/fplx
rfl,rfb,rfplx = gal_xyz(rposvel_field[:,0],rposvel_field[:,1],rposvel_field[:,2],plx=True,reverse=True)
#pdb.set_trace()


#pdb.set_trace()
##remove stars we don't want to run the selection on 
torun = np.where((stardat.plx > 3.0) & (stardat.plx < 20.0))[0]
stardat = stardat[torun]

##get Taurus group parameters
posvel,cposvel = taurus_clump_fastparams()
nsamp=10000
rposvel = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=True,observables=False)
rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
##pmvect = uvwtopm(np.mean(rposvel[:,3:6],axis=0),stardat[:,0],stardat[:,1],7.0)



##the field and group samples are now build, do the integrals
chunks = 1000
#stardat = stardat[0:2]##testing
##field integral
pdmodf = dataset_looper(stardat,rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False)

##GROUP INTEGRAL
##field == True means not using positions in the integral
pdmodg = dataset_looper(stardat,rposvel,chunksize=chunks,field=True,verbose=True,doall=False)

##DO SOME SMART PICKLES TO SAVE IMPORTANT AND REUSABLE VARIABLES
##FIRST SAVE PDPAR AND 2MASS ID IN SAME VARIABLE
outputs=  np.zeros((stardat.shape[0],2))
#outputs[:,0] = stardat[:,12] ##THE STAR ID
outputs[:,0] = pdmodf        ##FIELD INTEGRAL VALUE
outputs[:,1] = pdmodg        ##GROUP INTEGRAL VALUE

pickle.dump(outputs, open(outname+".pkl", "wb" ))
pickle.dump(stardat,open(outname+"_stardat.pkl", "wb" ))



pdb.set_trace()
print "im here"