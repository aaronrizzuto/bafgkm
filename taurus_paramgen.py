import numpy as np
from gal_xyz import gal_xyz
import pdb
from ACRastro.gal_uvw import gal_uvw
import emcee
from ACRastro.glactc import glactc
import time
import matplotlib.pyplot as plt
#import triangle
import pickle
import os

def posprob(p,mmm,icov):
	ll,bb,dist=gal_xyz(p[0],p[1],p[2],reverse=True,plx=False,radec=True)
	rr,dd = glactc(np.array([ll*180/np.pi]),np.array([bb*180/np.pi]),2000,reverse=True,degree=True,fk4=True)
	input = np.array([rr[0],dd[0],dist])
	##pdb.set_trace()
	diff = input-mmm
	return -np.dot(diff,np.dot(icov,diff))/2.0
		
def taurus_paramgen():
	ra  = 4.0*15.0+32.0/60.0*15.0 + 10.0*15.0/3600.0
	dec = 25.0+51.0/60.0 + 40.0/3600.0
	dist = 145.0
	sig_dist = 15.0
	#u = 16.0
	#v = -11.0
	#w = -10.0
	pmra = 6.1
	pmdec = -21.0
	rv    = 16.3
	sig_rv = 0.3
	sig_int = 3.0
	u,v,w=gal_uvw(ra=ra,dec=dec,pmra=pmra,pmdec=pmdec,vrad=rv,distance=dist)
	x,y,z = gal_xyz(ra,dec,dist,radec=True,plx=False,reverse=False)
	ux,uy,uz = gal_xyz(ra,dec,dist+sig_dist,radec=True,plx=False,reverse=False)
	lx,ly,lz = gal_xyz(ra,dec,dist-sig_dist,radec=True,plx=False,reverse=False)
	sig_x = np.absolute((ux-lx)/2)
	sig_y = np.absolute((uy-ly)/2)
	sig_z = np.absolute((uz-lz)/2)
	vel = np.array([u,v,w])
	pos = np.array([x,y,z])
	pos = np.array([145.0,0.0,0.0])
	sig_pos = np.array([sig_x,sig_y,sig_z])
	##standard window of taurus in ra dec taken as 4 sigma wide
	cov = np.array([[15.0**2,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]])
	icov = np.linalg.inv(cov)
	mns = np.array([ra,dec,dist])
	nwalkers = 100
	ndim     = 3
	dummy = np.array([[15.**2,0.0,0.0],[0.0,15.0**2,0.0],[0.0,0.0,15.0**2]])
	p0 = np.random.multivariate_normal(pos,cov,(nwalkers))
	#pdb.set_trace()
	
			
	sampler = emcee.EnsembleSampler(nwalkers,ndim,posprob,args=[mns,icov])
	
	##burn in 100
	time1 = time.clock()
	pos,prob,state = sampler.run_mcmc(p0,100)
	sampler.reset()
	timeneeded = 10*time.clock()-time1
	##pdb.set_trace()
	print "Running MCMC sampler"
	print "Time needed: " + str(timeneeded) +"seconds"
	
	sampler.run_mcmc(pos,1000)
	##remove the first 50 as burn in again
	samples = sampler.chain[:,50,:].reshape((-1,ndim))
	#pdb.set_trace()
	##the position means
	pos = np.mean(samples,axis=0)
	##the xyz covariance matrix
	cpos = np.cov(samples,rowvar=0)
	## the velocity covariance matrix
	cvel = np.array([[sig_int**2,0.0,0.0],[0.0,sig_int**2,0.0],[0.0,0.0,sig_int**2]])
	
	## combine the velocity and position into a single array (and the covariance mats too)
	posvel = np.append(pos,vel)
	cposvel = np.zeros((6,6))
	cposvel[0:3,0:3] = cpos
	cposvel[3:6,3:6] = cvel
	
	
	#print "im here"
	pickle.dump(posvel,  open( "taurus_posvel.pkl", "wb" ))
	pickle.dump(cposvel,open( "taurus_cposvel.pkl", "wb" ))

	return posvel,cposvel
	
def taurus_fastparams():
	if os.path.isfile("taurus_posvel.pkl")==True & os.path.isfile("taurus_cposvel.pkl")==True:
		file = open("taurus_posvel.pkl","rb")
		file2 = open("taurus_cposvel.pkl","rb")
		posvel =  pickle.load(file)	
		cposvel =  pickle.load(file2)
		return posvel,cposvel
	else:
		print "Taurus Paramater Files Not Created Yet, Doing It Now..."
		posvel,cposvel = taurus_paramgen()
		return posvel,cposvel