import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time
import idlsave

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from scipy.io.idl import readsav as idlrsave
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol
from k2_stuff import kpm_jhk
from phot_dist import phot_dist_interp
from subprocess import call
from taurus_paramgen import taurus_fastparams
where = np.where

##data of stars run through the bayes selection:ra,dec,l,b,prma,spmra,pmdec,spmdec,plx,sigplx,vr,sig_vr,id,pmcov
file1 = open('Taurus_NovTS2_d10_nopos_stardat.pkl','rb')
stardat = pickle.load(file1)
#bayes factor components
file2 = open('Taurus_NovTS2_d10_nopos.pkl','rb')
results = pickle.load(file2)
##read in the original data catalog for the stars processed
datacat = pickle.load(open("Taurus_NovTS2_d10_noposdatacat.pkl","rb"))



##compute probabilites
##stanard 6D mode
bfact = results[:,1]/results[:,0]
bad = np.where(bfact != bfact)[0] ##these tend to be stars with large proper motions
bfact[bad] = 1.0
bad2 = np.where(np.isfinite(bfact)==False)[0]
bfact[bad2] = 1e6
prob = 0.01*bfact/(0.01*bfact+1)
  
sampra= datacat.ra*180/np.pi
sampdec = datacat.dec*180/np.pi




##filter out obvious stars that probably aren't members
#flt = np.where((stardat[:,4] < 30) & (stardat[:,4]>-10) & (stardat[:,6]<0) & (stardat[:,6]>-40) & (stardat[:,8]>6) & (stardat[:,8]<30))[0]
#best = np.where(prob[flt] > 0.8)[0]

##read in the taurus known star lists
tmem  = idlrsave('known_taurus.idlsave').taumems
twtts = idlrsave('tauwtts.idlsave').tauwtts
#pdb.set_trace()
##now do a cross match for these 
# xw = np.zeros(len(twtts.ra[0]),'int')-1
# for i in range(0,len(twtts.ra[0])):
#  #   pdb.set_trace()
#     wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,twtts.ra[0][i],twtts.dec[0][i],format=2)
#     close = np.argmin(wdist)
# 	#pdb.set_trace()
#     if len(close.shape) > 1: pdb.set_trace()
#     if wdist[close] < 10.0: xw[i] = close
#     print 'up to ' + str(i) + ' out of ' + str(len(twtts.ra[0]))
# 
# xk = np.zeros(len(tmem.rad[0]),'int')-1
# ##pdb.set_trace()
# for i in range(0,len(tmem.rad[0])):
#     wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,tmem.rad[0][i],tmem.decd[0][i],format=2)
#     close = np.argmin(wdist)
#     if len(close.shape) > 1: pdb.set_trace()
#     if wdist[close] < 10.0: xk[i] = close
#     print 'up to ' + str(i) + ' out of ' + str(len(tmem.rad[0]))
# 
# pickle.dump(xw,open("tmp/xw_novts10.pkl","wb"))
# pickle.dump(xk,open("tmp/xk_novts10.pkl","wb"))
# 
xw = pickle.load(open("tmp/xw_novts10.pkl","rb"))
xk = pickle.load(open("tmp/xk_novts10.pkl","rb"))

sxk = xk[np.where(xk != -1)[0]]
sxw = xw[np.where(xw != -1)[0]]
sxwa = xw[np.where((xw != -1) & (twtts.status[0] == 'Accept'))[0]]
qwe = np.where((prob > 0.8) & (stardat.plx != 5.0) & (stardat.ra < 80*np.pi/180) & (stardat.ra > 56*np.pi/180) & (stardat.dec < 35*np.pi/180) & (stardat.dec > 13*np.pi/180.))[0]
test = np.where((prob > 0.8)  & (stardat.ra < 80*np.pi/180) & (stardat.ra > 56*np.pi/180) & (stardat.dec < 35*np.pi/180) & (stardat.dec > 13*np.pi/180.))[0]
rad = stardat.ra*180/np.pi
ded = stardat.dec*180/np.pi
plt.plot(rad[sxk],ded[sxk],'ro')
plt.plot(rad[sxw],ded[sxw],'go')
plt.plot(rad[qwe],ded[qwe],'b.')



#pdb.set_trace()

reject_known = np.zeros(len(stardat.ra),int)
reject_known[sxw] = 1
reject_known[sxk] = 1

##cross match with galex and rass fsc
#gcat = readcol("/Users/aaron/data_local/galex/galex_k2c13.csv",asRecArray=True,names=True,fsep=",")
#rass = readcol("/Users/aaron/data_local/rass/rass_fsc.csv",asRecArray=True,names=True,fsep=",")
#rass_in = np.where((rass.ra > 64) & (rass.ra < 82) & (rass.dec > 12) & (rass.dec < 29))[0]
#rass = rass[rass_in]

#xr = np.zeros(rass.shape[0],int)-1
# for i in range(rass.shape[0]):
#     rdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,rass.ra[i],rass.dec[i],format=2)
#     close = np.argmin(rdist)
#     if len(close.shape) > 1: pdb.set_trace()
#     if rdist[close] < 10.0: xr[i] = close
#     print 'up to ' + str(i) + ' out of ' + str(len(rass.ra))
# pickle.dump(xr,open("tmp/xr_novts2.pkl","wb"))
#xr = pickle.load(open("tmp/xr_novts2.pkl","rb"))
#sxr = xr[np.where(xr != -1)[0]]
has_rosat = np.zeros(len(stardat.ra),int)
#has_rosat[sxr] = 1
# 
#xg = np.zeros(len(gcat),int)-1
# for i in range(len(gcat)):
#     rough = np.where((np.absolute(stardat.ra*180/np.pi - gcat.ra[i]) < 0.01) & (np.absolute(stardat.dec*180/np.pi - gcat.dec[i]) < 0.1))[0]
#     if len(rough) == 0: rough = np.array([0,1,2],int)
#    # pdb.set_trace()
#     gdist = gcirc(stardat.ra[rough]*180/np.pi,stardat.dec[rough]*180/np.pi,gcat.ra[i],gcat.dec[i],format=2)
#     close = np.argmin(gdist)
#     if len(close.shape) > 1: pdb.set_trace()
#     if gdist[close] < 10.0: xg[i] = rough[close]
#     print 'up to ' + str(i) + ' out of ' + str(len(gcat.ra))
#pickle.dump(xg,open("tmp/xg_novts2.pkl","wb"))
#xg = pickle.load(open("tmp/xg_novts2.pkl","rb"))
#sxg = xg[np.where(xg != -1)[0]]
has_galex = np.zeros(len(stardat.ra),int)
#has_galex[sxg] = 1


# lclouds = readcol('luhman_clouds.txt')
# lcra    = (lclouds[:,0]+lclouds[:,1]/60.0+lclouds[:,2]/3600.0)*15.0
# lcdec    = (lclouds[:,3]+lclouds[:,4]/60.0+lclouds[:,5]/3600.0)
# lcra = lcra[0:11]
# lcdec = lcdec[0:11]
# #find the nearest cloud centre for each candidate
# cdist = np.zeros(len(stardat.ra))
# for i in range(len(stardat.ra)):
#    # pdb.set_trace()
#     cdist[i] = np.min(np.sqrt((lcra-180/np.pi*stardat.ra[i])**2+(lcdec-180/np.pi*stardat.dec[i])**2))
#     if np.mod(i+1,1000) == 0: print "up to "+str(i+1) + " out of " +str(len(stardat.ra))
#     
# pickle.dump(cdist,open("tmp/cdist_novts2.pkl","wb"))

cdist = pickle.load(open("tmp/cdist_novts2.pkl","rb"))

donemask = np.zeros(len(stardat.ra))
dlist = readcol('donelist_ts23nov.txt')[:,0]
for i in range(len(dlist)):
    this = np.where(stardat.id == int(dlist[i]))[0]
    if len(this) != 0: donemask[this] = 1


##define the observations sample
# best = np.where((prob > 0.5) & (sampra < 85.) & (sampra > 59) & (sampdec > 14.) & (sampdec < 35.))[0]
# test = np.where((prob > 0.9) & (sampra < 85.) & (sampra > 59) & (sampdec > 14.) & (sampdec < 35.) & (cdist < 3))[0]
# vel = np.array([15.7,-11.3,-10.1])
# best = np.where(prob >-1)[0]
# exp_pmra =np.zeros(len(best))
# exp_pmdec =np.zeros(len(best))
# from uvwtopm import uvwtopm
# for i in range(len(best)):
#     pmvect = uvwtopm(vel, stardat.ra[best[i]],stardat.dec[best[i]],1000.0/145.0)
#     print i+1
#     exp_pmra[i] = pmvect[1]
#     exp_pmdec[i] = pmvect[2]
# pickle.dump((exp_pmra,exp_pmdec),open('exp_tauUCACNovT2s.pkl','wb'))
exp_pmra,exp_pmdec = pickle.load(open('exp_tauUCACNovT2s.pkl','rb'))
qwe = np.where((prob > 0.5) & (stardat.plx != 5.0) & (stardat.ra < 80*np.pi/180) & (stardat.ra > 56*np.pi/180) & (stardat.dec < 35*np.pi/180) & (stardat.dec > 13*np.pi/180.) & (stardat.pmdec-exp_pmdec < 6.5) & (reject_known == 0) & (stardat.pmra > -2.))[0]
asd = np.where(prob[sxwa] < 0.1)[0]
from phot_dist import phot_dist_interp


plt.plot(stardat.pmra[sxk]-exp_pmra[sxk],stardat.pmdec[sxk]-exp_pmdec[sxk],'ro',markersize=8)
plt.plot(stardat.pmra[sxwa]-exp_pmra[sxwa],stardat.pmdec[sxwa]-exp_pmdec[sxwa],'go',markersize=8)
plt.plot(stardat.pmra[qwe]-exp_pmra[qwe],stardat.pmdec[qwe]-exp_pmdec[qwe],'.')
plt.plot([0],[0],'*r',markersize=20)
plt.show()




#qwe = np.where((np.absolute(exp_pmra-stardat.pmra)<10.0) & (np.absolute(exp_pmdec-stardat.pmdec)<10.0) & (prob > 0.5) & (sampra < 85.) & (sampra > 59) & (sampdec > 14.) & (sampdec < 35.))[0]

pdb.set_trace()
rah,ram,ras,ded,dem,des = radec(sampra[best],sampdec[best])
nrd = nice_radec(rah,ram,ras,ded,dem,des,spc=' ',mspc=' ')
nrd2 = nice_radec(rah,ram,ras,ded,dem,des,spc=' ',mspc=',')

##make the lists
wfile = open('tauUCAC_test.wrk','wb')
lfile = open('tauUCAC_test.csv','wb')
lfile.write('n,name,ra,dec,radeg,decdeg,V,VK,Pmem,CoreDist,inGalex \n')
lfile.flush()
for i in range(len(best)):
    if reject_known[best[i]] != 1:
        wline = str(i+1) +'"' + str(stardat.id[best[i]])+'" ' + nrd[i]+' 2000 0.0 0.0 V='+str(datacat.vap[best[i]]) +' \n'
        wfile.write(wline)
        wfile.flush()
        line = str(i+1)+','+str(stardat.id[best[i]])+','+nrd2[i]+','+str(sampra[best[i]])+','+str(sampdec[best[i]])+','+str(datacat.vap[best[i]])+','+str(datacat.vap[best[i]]-datacat.k[best[i]])+','+str(np.round(round(100*prob[best[i]])))+','+str(np.round(cdist[best[i]]))+','+str(has_galex[best[i]])+' \n'        
        lfile.write(line)
        lfile.flush()

        
pdb.set_trace()
print 'im here'







