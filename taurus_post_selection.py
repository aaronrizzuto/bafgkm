import numpy as np
import pickle
import pdb
import matplotlib.pyplot as plt
import time
import idlsave

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from scipy.io.idl import readsav as idlrsave
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol
from k2_stuff import kpm_jhk
from phot_dist import phot_dist_interp
from subprocess import call

where = np.where

##data of stars run through the bayes selection:ra,dec,l,b,prma,spmra,pmdec,spmdec,plx,sigplx,vr,sig_vr,id,pmcov
file1 = open('Taurus_160216_stardat.pkl','rb')
stardat = pickle.load(file1)
#bayes factor components
file2 = open('Taurus_160216.pkl','rb')
results = pickle.load(file2)
##read in the original data catalog for the stars processed
datacat = pickle.load(open("Taurus_160216datacat_run.pkl","rb"))

results2 = pickle.load(open("Taurus_160216_nopos.pkl","rb"))


##compute probabilites
##stanard 6D mode
bfact = results[:,1]/results[:,0]
bad = np.where(bfact != bfact)[0] ##these tend to be stars with large proper motions
bfact[bad] = 0.0
prob = 0.01*bfact/(0.01*bfact+1)
prior = 0.001
prob2 = prior*bfact/(prior*bfact+1)

##field mode
bfact3 = results2[:,1]/results[:,0]
bad = np.where(bfact3 != bfact3)[0] ##these tend to be stars with large proper motions
bfact3[bad] = 0.0
prob3 = 0.01*bfact3/(0.01*bfact3+1)

##calculate kepler Kp_JHK mags 
kp_jk = kpm_jhk(datacat.j,datacat.k)
##print a csv file for getting on/off silicon numbers
# silfile = open("tmp/silicon_input.csv","wb")
# for i in range(len(stardat.ra)):
#     thisline = str(stardat.ra[i]*180/np.pi) + ',' + str(stardat.dec[i]*180/np.pi)+','+str(kp_jk[i])+' \n'
#     silfile.write(thisline)
#     #silfile.flush()
# 
# silfile.close()


##filter out obvious stars that probably aren't members
#flt = np.where((stardat[:,4] < 30) & (stardat[:,4]>-10) & (stardat[:,6]<0) & (stardat[:,6]>-40) & (stardat[:,8]>6) & (stardat[:,8]<30))[0]
#best = np.where(prob[flt] > 0.8)[0]

##read in the taurus known star lists
tmem  = idlrsave('known_taurus.idlsave').taumems
twtts = idlrsave('tauwtts.idlsave').tauwtts
#pdb.set_trace()
##now do a cross match for these 
# xw = np.zeros(len(twtts.ra[0]),'int')-1
# for i in range(0,len(twtts.ra[0])):
#  #   pdb.set_trace()
#     wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,twtts.ra[0][i],twtts.dec[0][i],format=2)
#     close = np.argmin(wdist)
# 	#pdb.set_trace()
#     if len(close.shape) > 1: pdb.set_trace()
#     if wdist[close] < 10.0: xw[i] = close
#     print 'up to ' + str(i) + ' out of ' + str(len(twtts.ra[0]))
# 
# xk = np.zeros(len(tmem.rad[0]),'int')-1
# ##pdb.set_trace()
# for i in range(0,len(tmem.rad[0])):
#     wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,tmem.rad[0][i],tmem.decd[0][i],format=2)
#     close = np.argmin(wdist)
#     if len(close.shape) > 1: pdb.set_trace()
#     if wdist[close] < 10.0: xk[i] = close
#     print 'up to ' + str(i) + ' out of ' + str(len(tmem.rad[0]))
# 
# 
# pickle.dump(xw,open("tmp/xw.pkl","wb"))
# pickle.dump(xk,open("tmp/xk.pkl","wb"))

xw = pickle.load(open("tmp/xw.pkl","rb"))
xk = pickle.load(open("tmp/xk.pkl","rb"))

sxk = xk[np.where(xk != -1)[0]]
sxw = xw[np.where(xw != -1)[0]]
sxwa = xw[np.where((xw != -1) & (twtts.status[0] == 'Accept'))[0]]

reject_known = np.zeros(len(stardat.ra),int)
reject_known[sxw] = 1
reject_known[sxk] = 1

##cross match with galex and rass fsc
gcat = readcol("/Users/aaron/data_local/galex/galex_k2c13.csv",asRecArray=True,names=True,fsep=",")
rass = readcol("/Users/aaron/data_local/rass/rass_fsc.csv",asRecArray=True,names=True,fsep=",")
rass_in = np.where((rass.ra > 64) & (rass.ra < 82) & (rass.dec > 12) & (rass.dec < 29))[0]
rass = rass[rass_in]

# xr = np.zeros(rass.shape[0],int)-1
# for i in range(rass.shape[0]):
#     rdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,rass.ra[i],rass.dec[i],format=2)
#     close = np.argmin(rdist)
#     if len(close.shape) > 1: pdb.set_trace()
#     if rdist[close] < 10.0: xr[i] = close
#     print 'up to ' + str(i) + ' out of ' + str(len(rass.ra))
# pickle.dump(xr,open("tmp/xr.pkl","wb"))
xr = pickle.load(open("tmp/xr.pkl","rb"))
sxr = xr[np.where(xr != -1)[0]]
has_rosat = np.zeros(len(stardat.ra),int)
has_rosat[sxr] = 1

# xg = np.zeros(len(gcat),int)-1
# for i in range(len(gcat)):
#     rough = np.where((np.absolute(stardat.ra*180/np.pi - gcat.ra[i]) < 0.01) & (np.absolute(stardat.dec*180/np.pi - gcat.dec[i]) < 0.1))[0]
#     if len(rough) == 0: rough = np.array([0,1,2],int)
#    # pdb.set_trace()
#     gdist = gcirc(stardat.ra[rough]*180/np.pi,stardat.dec[rough]*180/np.pi,gcat.ra[i],gcat.dec[i],format=2)
#     close = np.argmin(gdist)
#     if len(close.shape) > 1: pdb.set_trace()
#     if gdist[close] < 10.0: xg[i] = rough[close]
#     print 'up to ' + str(i) + ' out of ' + str(len(gcat.ra))
# pickle.dump(xg,open("tmp/xg.pkl","wb"))
xg = pickle.load(open("tmp/xg.pkl","rb"))
sxg = xg[np.where(xg != -1)[0]]
has_galex = np.zeros(len(stardat.ra),int)
has_galex[sxg] = 1

lclouds = readcol('luhman_clouds.txt')
lcra    = (lclouds[:,0]+lclouds[:,1]/60.0+lclouds[:,2]/3600.0)*15.0
lcdec    = (lclouds[:,3]+lclouds[:,4]/60.0+lclouds[:,5]/3600.0)
lcra = lcra[0:11]
lcdec = lcdec[0:11]
#find the nearest cloud centre for each candidate
# cdist = np.zeros(len(stardat.ra))
# for i in range(len(stardat.ra)):
#    # pdb.set_trace()
#     cdist[i] = np.min(np.sqrt((lcra-180/np.pi*stardat.ra[i])**2+(lcdec-180/np.pi*stardat.dec[i])**2))
#     if np.mod(i+1,1000) == 0: print "up to "+str(i+1) + " out of " +str(len(stardat.ra))
#     
# pickle.dump(cdist,open("tmp/cdist.pkl","wb"))

cdist = pickle.load(open("tmp/cdist.pkl","rb"))

##calculate Main sequence height at 140 parsecs.

jkd_ms,vkd_ms,bvd_ms,dist_ms,sig_dist_ms = phot_dist_interp(datacat.bap,datacat.vap,datacat.j,datacat.k,nodist_val=0)


rrr = np.where((stardat.ra*180/np.pi < 82) & (stardat.ra*180/np.pi > 64) & (stardat.dec*180/np.pi>12) & (stardat.dec*180/np.pi < 29))
##find candidate targets
#ct = np.where((stardat.ra*180/np.pi < 82) & (stardat.ra*180/np.pi > 64) & (stardat.dec*180/np.pi>12) & (stardat.dec*180/np.pi < 29) & (prob2 > 0.7) & (kp_jk < 15) & (datacat.k < 14) & (datacat.k > 6) & (dist_ms < 145) & (reject_known == 0))[0]

##a more full list for plots
#ct = np.where((prob2 > 0.2) & (kp_jk < 15) & (datacat.k < 14) & (datacat.k > 6) & (dist_ms < 145) )[0]
#ct2 = np.where((prob3 > 0.2) & (kp_jk < 15) & (datacat.k < 14) & (datacat.k > 6) & (dist_ms < 145) & (cdist < 10))[0]

ct = np.where((prob3 > 0.2)& (datacat.k > 6)   & (cdist<8)& (reject_known !=3))[0]
#ct2 = np.where((prob3 > 0.5) & (kp_jk < 18) & (datacat.k < 14) & (datacat.k > 6) & (dist_ms < 145) & (reject_known ==0) & (cdist<8))[0]
#ct = np.where((prob3 > 0.2) & (cdist<8))[0]

qwe = np.where((prob3 > 0.2) & (cdist<8) & (stardat.plx ==5) & (kp_jk<19) & (datacat.k<14))[0]

#& (datacat.k < 14)
#& (reject_known == 0)
#& (dist_ms < 145)
pdb.set_trace()
silfile = open("tmp/silicon_input_part.csv","wb")
for i in range(len(stardat.ra[ct])):
    thisline = str(stardat.ra[ct[i]]*180/np.pi) + ',' + str(stardat.dec[ct[i]]*180/np.pi)+','+str(kp_jk[ct[i]])+' \n'
    silfile.write(thisline)
    #silfile.flush()
silfile.close()
pdb.set_trace()
##call the k2fov function to get on-silicon numbets
call(["K2onsilicon","tmp/silicon_input_part.csv","13"])
##read in the output
k2fov = readcol("targets_siliconFlag.csv",names=False,fsep=',')
ct_onsil = k2fov[:,3]
qwe = np.where(ct_onsil==2)[0]
print str(qwe.shape[0]) + " on silicon"

##output another csv file for reading into a spreadsheet for final manipulation
outfile = open("tmp/tau_k2_select_onsil.csv","wb")
fullfile= open("tmp/tau_k2_select.csv","wb")
headline = "ra,dec,Kp_JK,OnSil,Prob,pmra,pmdec,sig_pmra,sig_pmdec,plx,sig_plx,J,K,B,V,g,r,i,galex,rass \n"
outfile.write(headline)
fullfile.write(headline)
for i in range(len(stardat.ra[ct])):
    thisline = str(stardat.ra[ct[i]]*180/np.pi) + ',' + str(stardat.dec[ct[i]]*180/np.pi)+','+str(kp_jk[ct[i]])+','+str(ct_onsil[i])+','+ str(prob3[ct[i]]*100)+','+str(stardat.pmra[ct[i]])+','+str(stardat.pmdec[ct[i]])+','+str(stardat.sig_pmra[ct[i]])+','+str(stardat.sig_pmdec[ct[i]])+','+str(stardat.plx[ct[i]])+','+str(stardat.sig_plx[ct[i]])+',' +str(datacat.j[ct[i]])+','+str(datacat.k[ct[i]])+','+str(datacat.bap[ct[i]])+','+str(datacat.vap[ct[i]])+','+str(datacat.gap[ct[i]])+','+str(datacat.rap[ct[i]])+','+str(datacat.iap[ct[i]])+','+str(has_galex[ct[i]])+','+str(has_rosat[ct[i]])+' \n'
    if ct_onsil[i] == 2: outfile.write(thisline)
    fullfile.write(thisline)
outfile.close()
fullfile.close()
pdb.set_trace()



# xg   = np.zeros(len(stardat.ra[ct]),int)-1
# hasg = np.zeros(len(stardat.ra[ct]),int)
# for i in range(len(stardat.ra[ct])):
#     rough = np.where((np.absolute(stardat.ra[i]*180/np.pi - gcat.ra) < 1) & (np.absolute(stardat.dec[i]*180/np.pi - gcat.dec) < 1))[0]
#     if len(rough) == 0: rough = np.array([0,1,2],int)
#     #pdb.set_trace()
#     gdist = gcirc(stardat.ra[i]*180/np.pi,stardat.dec[i]*180/np.pi,gcat.ra[rough],gcat.dec[rough],format=2)
#     close = np.argmin(gdist)
#     if len(close.shape) > 1: pdb.set_trace()
#     if gdist[close] < 10.0: 
#         xg[i]   = rough[close]
#         hasg[i] = 1
#     print 'up to ' + str(i) + ' out of ' + str(len(stardat.ra[ct]))
# #pickle.dump(xg,open("tmp/xg.pkl","wb"))
# 




pdb.set_trace()
au = stardat.pmra/3600.0*40.0*10.0
av = stardat.pmdec/3600.0*40.0*10.0
plt.quiver(stardat.ra[ct]*180/np.pi,stardat.dec[ct]*180/np.pi,au[ct],av[ct],scale=1,units='xy',width=0.05,headlength=3,headwidth=2)
plt.quiver(stardat.ra[sxk]*180/np.pi,stardat.dec[sxk]*180/np.pi,au[sxk],av[sxk],scale=1,units='xy',width=0.05,headlength=3,headwidth=2)
plt.quiver(stardat.ra[sxwa]*180/np.pi,stardat.dec[sxwa]*180/np.pi,au[sxwa],av[sxwa],scale=1,units='xy',width=0.05,headlength=3,headwidth=2)



pdb.set_trace()



















##first sort out which stars in the Taurus WTTS candidate list need followup with IGRINS
#ifqwe = np.where((twtts.status[0] == 'NIRgravityindicator') | (twtts.status[0] == 'RVMonitoring')  | (twtts.status[0] == 'NIRRV') | (twtts.status[0] == 'OpticalRV') | (twtts.status[0] == 'Opticalgravityindicator'))[0]
#ifpmem = prob[xw[ifqwe]]
#qwe = np.where(xw[ifqwe] == -1)[0]
#ifpmem[qwe] = -1
#twrah,twram,twras,twded,twdem,twdes = radec(twtts.ra[0],twtts.dec[0])
#nradec = nice_radec(twrah,twram,twras,twded,twdem,twdes,mspc=",")
#twtts_file = open("twtts_targs.csv","wb")
#names = twtts.name[0][ifqwe]
#status = twtts.status[0][ifqwe]
#for i in range(0,len(ifqwe)):
#	line = names[i] + "," + nradec[ifqwe[i]] + "," + str(ifpmem[i]) + "," + status[i]+ " \n"
#	twtts_file.write(line)
#twtts_file.flush()
#twtts_file.close()





pdb.set_trace()



##make an arrow plot if nessecary
#these = flt[best]
#these = np.random.random_integers(0,len(stardat[:,0]),1000)
#ax = stardat[these,0]*180/np.pi
#ay = stardat[these,1]*180/np.pi
#au = stardat[these,4]/3600.0*40.0
#av = stardat[these,6]/3600.0*40.0

#pmdir = np.arctan2(stardat[:,6],stardat[:,4])*180/np.pi
#plt.quiver(ax,ay,au,av)

pdb.set_trace()
print 'im here'







