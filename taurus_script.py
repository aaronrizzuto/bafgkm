import numpy as np
import pickle
import pdb
#import emcee

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from scipy.io.idl import readsav as idlrsave

from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from taurus_paramgen import taurus_paramgen
from taurus_paramgen import taurus_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from readcol import readcol

##some testing of the parameters 
#posvel,cposvel = taurus_fastparams()
#mnf,cf = gal_to_obs_full(posvel,cposvel)
#test = readcol('clustering/betapic_fixed.csv',names=True,twod=True,asRecArray=True)
#wt = readcol('WTTS.csv',fsep=',',names=True,twod=True,asRecArray=True)
#ok = np.where((wt.FINAL2 != 'N') & (wt.FINAL2 != 'N?'))[0]

#pdb.set_trace()

outname = "Taurus_DECTS2_d10_nopos"


dpi = np.pi
file = open("K2C13_full_cross.pkl","rb")

datacat = pickle.load(file)

##unpack the data to make things general:
stardat = np.recarray(len(datacat.id2m),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',long),('pmcov',float)])
stardat.id        = datacat.id2m
stardat.ra        = datacat.ra 
stardat.dec       = datacat.dec
stardat.pmra      = datacat.pmra
stardat.pmdec     = datacat.pmdec
stardat.sig_pmra  = datacat.sig_pmra
stardat.sig_pmdec = datacat.sig_pmdec
stardat.plx       = 0.0
stardat.sig_plx   = 300.0
stardat.rv        = 0.0
stardat.sig_rv    = 300.0
gl,gb     = glactc(datacat.ra*180/dpi,datacat.dec*180/dpi,2000,degree=True,fk4=True)
stardat.l         = gl*np.pi/180.0
stardat.b         = gb*np.pi/180.0
stardat.pmcov = 1.0*0.0
#qwe = np.where(stardat.id == 	1281947259)
#pdb.set_trace()

jmag     = datacat.j
hmag     = datacat.h
kmag     = datacat.k
bap      = datacat.bap
vap      = datacat.vap
gap      = datacat.gap
iap      = datacat.iap
rap      = datacat.rap
##auxilliary data that the velocity integral doesn't need, but might be useful in 
##excluding non-members or calculating other numbers for input to the velocity
##integrals
auxdat = np.transpose(np.array([jmag,hmag,kmag,bap,vap,gap,iap,rap]))
#pdb.set_trace()
###remove everything with colors outside some limits
hasap = np.where((bap <20) & (vap <20))
hask = np.where((kmag <20))

hjk = np.zeros(len(jmag),'int')-1
hap = np.zeros(len(jmag),'int')-1
hjk[hask]  = 1
hap[hasap] = 1

#pdb.set_trace()
##color cut
incolor = np.where(((jmag-kmag<2.0) | (hjk==-1)) & ((vap-kmag<10.0) | ((hjk==-1) | (hap==-1))) & ((bap-vap<3.0) | (hap==-1)) & ((bap-vap>0.1) | (hap==-1)) & ((jmag-kmag>0.1) | (hjk==-1)) & ((hap != -1) | (hjk != -1)))[0]
auxdat  =  auxdat[incolor,:]
stardat = stardat[incolor]
datacat = datacat[incolor]
vap     = vap[incolor]
#pdb.set_trace()
##magnitude cut
inmag = np.where((vap <= 13.0) & (vap >= 11.0))[0]
auxdat = auxdat[inmag,:]
stardat = stardat[inmag]
datacat = datacat[inmag]
#pdb.set_trace()


jkd_ms,vkd_ms,bvd_ms,dist_ms,sig_dist_ms = phot_dist_interp(auxdat[:,3],auxdat[:,4],auxdat[:,0],auxdat[:,2])
jkd_10,vkd_10,bvd_10,dist_10,sig_dist_10 = phot_dist_interp(auxdat[:,3],auxdat[:,4],auxdat[:,0],auxdat[:,2],age=10)
distforhist = dist_ms[np.where(sig_dist_ms<499.9)[0]]




##throw away anything that isn't above the main sequence at 145.0 pc, this includes anything too 
##red to be in isochrone range, also insert the isochrone distances into the stardat array
##also get rid of things with group parallaxes less than 3.0 and gt 30.0
ams = np.where((1000.0/dist_10 > 3.0) & (1000.0/dist_10<40.0) & (stardat.ra*180/np.pi < 80) & (stardat.ra*180/np.pi >55) & (stardat.dec*180/np.pi < 40) & (stardat.dec*180/np.pi > 10))[0]
stardat = stardat[ams]
auxdat  = auxdat[ams]
datacat = datacat[ams]
dist_10 = dist_10[ams]
sig_dist_10 = sig_dist_10[ams]
dist_ms = dist_ms[ams]
sig_dist_ms = sig_dist_ms[ams]
stardat.plx = 1000.0/dist_10
stardat.sig_plx = 1000.0/dist_10*sig_dist_10/dist_10

##save the datacat of the objects that were run
#pdb.set_trace()

tmem  = idlrsave('known_taurus.idlsave').taumems
twtts = idlrsave('tauwtts.idlsave').tauwtts
#pdb.set_trace()
##now do a cross match for these 
xw = np.zeros(len(twtts.ra[0]),'int')-1
for i in range(0,len(twtts.ra[0])):
 #   pdb.set_trace()
    wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,twtts.ra[0][i],twtts.dec[0][i],format=2)
    close = np.argmin(wdist)
	#pdb.set_trace()
    if len(close.shape) > 1: pdb.set_trace()
    if wdist[close] < 10.0: xw[i] = close
    print 'up to ' + str(i) + ' out of ' + str(len(twtts.ra[0]))

xk = np.zeros(len(tmem.rad[0]),'int')-1
##pdb.set_trace()
for i in range(0,len(tmem.rad[0])):
    wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,tmem.rad[0][i],tmem.decd[0][i],format=2)
    close = np.argmin(wdist)
    if len(close.shape) > 1: pdb.set_trace()
    if wdist[close] < 10.0: xk[i] = close
    print 'up to ' + str(i) + ' out of ' + str(len(tmem.rad[0]))
sxk = xk[np.where(xk != -1)[0]]
sxw = xw[np.where(xw != -1)[0]]
sxwa = xw[np.where((xw != -1) & (twtts.status[0] == 'Accept'))[0]]
#pdb.set_trace()

pickle.dump(datacat,open(outname+'datacat.pkl','wb'))


#distforhist = dist_ms[np.where(sig_dist_ms<499.0)]
#pdb.set_trace()

##build field sample
nfsamp = 100000
hist,binedges = np.histogram(distforhist,bins=100,range=(0.0,3000.0),density=True)
fdist         = rejsamp(binedges[0:100],hist,nfsamp)
fplx          = 1000.0/fdist
uvw_field = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = fdist

##get Taurus group parameters
#posvel,cposvel = taurus_fastparams()
posvel = np.array([145.0,0.0,0.0,16.0,-11.0,-10.0])
cposvel = np.diag(np.array([15**2,0.0,0.0,9.0,9.0,9.0]))
nsamp=10000
rposvel = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=True,observables=False)
rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
##pmvect = uvwtopm(np.mean(rposvel[:,3:6],axis=0),stardat[:,0],stardat[:,1],7.0)
##the field and group samples are now build, do the integrals
#pdb.set_trace()


chunks = 50

######stardat = stardat[0:10000,:]
##LKCA1 mem (tested, large BF)
#teststar = np.where(stardat[:,12] ==	137433001)[0]
##DFTAU
#teststar = np.where(stardat[:,12] ==1302752376	)[0]	
##RRXJ0433.7+1823 nonmem (tested, BF=0)




##FIELD INTEGRAL
stardat_field = stardat.copy()
stardat_field.plx = 1000.0/dist_ms
stardat_field.sig_plx = sig_dist_ms/dist_ms*1000.0/dist_ms
#pdb.set_trace()

#stardat = stardat[sxwa]
#stardat_field = stardat_field[sxwa]

#teststar = np.where(stardat.id ==	112520676)[0]	
#stardat=stardat[teststar]
#stardat_field = stardat_field[teststar]




pdmodf = dataset_looper(stardat_field,rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False)
#pdmodf = np.zeros(len(stardat.ra))
##GROUP INTEGRAL
pdmodg = dataset_looper(stardat,rposvel,chunksize=chunks,field=True,verbose=True,doall=False)

#tt = pdmodg/pdmodf
#ppp = 0.01*tt/(1+0.01*tt)
#pdb.set_trace()

#pdb.set_trace()
##DO SOME SMART PICKLES TO SAVE IMPORTANT AND REUSABLE VARIABLES
##FIRST SAVE PDPAR AND 2MASS ID IN SAME VARIABLE
outputs=  np.zeros((stardat.shape[0],2))
#outputs[:,0] = stardat[:,12] ##THE STAR ID
outputs[:,0] = pdmodf        ##FIELD INTEGRAL VALUE
outputs[:,1] = pdmodg        ##GROUP INTEGRAL VALUE

pickle.dump(outputs, open( outname+".pkl", "wb" ))
pickle.dump(stardat,open(outname+"_stardat.pkl", "wb" ))





#pdb.set_trace()
print "im here"