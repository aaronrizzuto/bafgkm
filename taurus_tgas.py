import numpy as np
import pickle,os
import pdb
import matplotlib.pyplot as plt
import time
#import emcee
from scipy.io.idl import readsav as idlrsave

from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
import idlsave

from gcirc import gcirc
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from taurus_paramgen import taurus_paramgen
from taurus_paramgen import taurus_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
#np.set_printoptions(suppress=True,precision=2)
from readcol import readcol

##some testing of the parameters 
#posvel,cposvel = taurus_fastparams()
#mnf,cf = gal_to_obs_full(posvel,cposvel)
#test = readcol('clustering/betapic_fixed.csv',names=True,twod=True,asRecArray=True)
#wt = readcol('WTTS.csv',fsep=',',names=True,twod=True,asRecArray=True)
#ok = np.where((wt.FINAL2 != 'N') & (wt.FINAL2 != 'N?'))[0]

#SOME testing
vel = np.array([ 15.7, -11.3, -10.1])
rr = 1.066722987719300
dd = 0.38282985059787017
pp = 8.61
nnn = 1000
uuu= np.random.normal(vel[0],3.,nnn)
vvv= np.random.normal(vel[1],3.,nnn)
www= np.random.normal(vel[2],3.,nnn)
pra = np.zeros(nnn)
pde = np.zeros(nnn)
prv = np.zeros(nnn)
for i in range(nnn):
    this = uvwtopm(np.array([uuu[i],vvv[i],www[i]]),rr,dd,pp)
    pra[i] = this[1]
    pde[i] = this[2]
    prv[i] = this[0]
pdb.set_trace()


outname = "Taurus_160928_tgas_nopos_deep"


dpi = np.pi
##load just the taurus patch of sky for speed
datacat    = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/taurus_tgas.pkl','rb'))
base_check = np.where((datacat.plx > 0.0))[0]
datacat    = datacat[base_check]

##crossmatch radial velocities here:
#pdb.set_trace()
pulk = readcol(os.getenv("HOME")+'/data_local/catalogs/pulkovo_rv/pulkovo.csv',fsep=',',asRecArray=True)
gcrv = pickle.load(open(os.getenv("HOME")+'/data_local/catalogs/gcvr/gcrv.pkl','rb'))
nearme = np.where((gcrv.ra < 90) & (gcrv.ra > 50) & (gcrv.dec > 10) & (gcrv.dec < 45))[0]
gcrv = gcrv[nearme]


#pdb.set_trace()
rvcat = np.zeros(len(datacat))
sig_rv = np.zeros(len(datacat))+300.0
for i in range(len(datacat)):
    print 'up to ' + str(i)
    if datacat.hip[i] != '':
        mmm = np.where(pulk.hip == int(datacat.hip[i]))[0]
        if len(mmm) != 0:
            rvcat[i] = pulk.rv[mmm]
            sig_rv[i] = pulk.erv[mmm]
    wdist =  gcirc(datacat.ra[i],datacat.dec[i],gcrv.RA,gcrv.DEC,format=2)
    close = np.argmin(wdist)
    if wdist[close] < 10.0: 
        rvcat[i] = gcrv.RV[close]
        sig_rv[i] = gcrv.SIG_RV[close]
        
                
            

stardat = np.recarray(len(datacat.id),dtype=[('ra',float),('dec',float),('l',float),('b',float),('pmra',float),('sig_pmra',float),('pmdec',float),('sig_pmdec',float),('plx',float),('sig_plx',float),('rv',float),('sig_rv',float),('id',long),('pmcov',float)])
stardat.id        = datacat.id
stardat.ra        = datacat.ra*np.pi/180.
stardat.dec       = datacat.dec*np.pi/180.
stardat.pmra      = datacat.pmra
stardat.pmdec     = datacat.pmdec
stardat.sig_pmra  = datacat.sig_pmra
stardat.sig_pmdec = datacat.sig_pmdec
stardat.plx       = datacat.plx
stardat.sig_plx   = datacat.sig_plx
stardat.rv        = rvcat
stardat.sig_rv    = sig_rv
stardat.pmcov = 1.0*0.0
#stardat.rv        = 0.0
##stardat.sig_rv    = 300.0

gl,gb     = glactc(datacat.ra,datacat.dec,2000,degree=True,fk4=True)
stardat.l         = gl*np.pi/180.0
stardat.b         = gb*np.pi/180.0
for i in range(len(datacat.id)): stardat.pmcov[i] = datacat.covar[i][3,4]


#pdb.set_trace()

##build the field samples
nfsamp = 1000000
disto = 1000.0/stardat.plx
use = np.where(disto<4000)[0]
disto= disto[use]
hist,binedges = np.histogram(disto,bins=500,density=True)
fdsamp        = rejsamp(binedges[0:500]/2.0+binedges[1:]/2,hist,nfsamp)
fplx = 1000.0/fdsamp
uvw_field     = build_uvw_field(nfsamp)
rposvel_field = np.zeros((nfsamp,6),dtype='float')
rposvel_field[:,3:6]=uvw_field
rposvel_field[:,2] = 1000.0/fplx
#rfplx = fplx*1.0
#rfl,rfb,rfplx = gal_xyz(rposvel_field[:,0],rposvel_field[:,1],rposvel_field[:,2],plx=True,reverse=True)
#pdb.set_trace()


#pdb.set_trace()
##remove stars we don't want to run the selection on 
#torun = np.where((stardat.plx > 3.0) & (stardat.plx < 15.0) & (stardat.pmra > -20) & (stardat.pmdec<20))[0]
torun = np.where(stardat.id == 159965197946584320)[0]
stardat = stardat[torun]
stardat.sig_rv = 300.0
#pdb.set_trace()
# tmem  = idlrsave('known_taurus.idlsave').taumems
# xk = np.zeros(len(tmem.rad[0]),'int')-1
# for i in range(0,len(tmem.rad[0])):
#     wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,tmem.rad[0][i],tmem.decd[0][i],format=2)
#     close = np.argmin(wdist)
#     if len(close.shape) > 1: pdb.set_trace()
#     if wdist[close] < 10.0: xk[i] = close
#    # print 'up to ' + str(i) + ' out of ' + str(len(tmem.rad[0]))
# 
# kx  = np.where(xk != -1)[0]
# sxk = xk[np.where(xk != -1)[0]]
# 
# pickle.dump(sxk,open('tttt_tmp.pkl','wb'))

#sxk = pickle.load(open('tttt_tmp.pkl','rb'))
#stardat = stardat[sxk]
#torun = np.where(stardat.id == 156917489152834688)[0]
#pdb.set_trace()
#stardat=stardat[torun]


##get Taurus group parameters
posvel,cposvel = taurus_fastparams()
#posvel = np.array([145.0,0.0,0.0,16.0,-11.0,-10.0])
#cposvel = np.diag(np.array([15**2,0.0,0.0,9.0,9.0,9.0]))

nsamp=100000
rposvel = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=False,observables=False)
rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
##pmvect = uvwtopm(np.mean(rposvel[:,3:6],axis=0),stardat[:,0],stardat[:,1],7.0)



##the field and group samples are now build, do the integrals
chunks = 2
#stardat = stardat[0:2]##testing
##field integral
pdmodf = dataset_looper(stardat,rposvel_field,chunksize=chunks,field=True,verbose=True,doall=False)

##GROUP INTEGRAL
##field == True means not using positions in the integral
pdmodg = dataset_looper(stardat,rposvel,chunksize=chunks,field=True,verbose=True,doall=False)
tt = pdmodg/pdmodf
prob = 0.01*tt/(1+0.01*tt)
print prob,pdmodg,pdmodf
pdb.set_trace()
##DO SOME SMART PICKLES TO SAVE IMPORTANT AND REUSABLE VARIABLES
##FIRST SAVE PDPAR AND 2MASS ID IN SAME VARIABLE
outputs=  np.zeros((stardat.shape[0],2))
#outputs[:,0] = stardat[:,12] ##THE STAR ID
outputs[:,0] = pdmodf        ##FIELD INTEGRAL VALUE
outputs[:,1] = pdmodg        ##GROUP INTEGRAL VALUE

pickle.dump(outputs, open(outname+".pkl", "wb" ))
pickle.dump(stardat,open(outname+"_stardat.pkl", "wb" ))

#standard
# 0.04362804] [  7.72865465e-07] [  1.69420154e-07]
#[ 0.07359811] [  5.29643422e-06] [  6.66678317e-07]
#[ 0.92719139] [  2.73353035e-05] [  2.14653158e-08]

#x10
#[ 0.41207018] [  3.00389774e-05] [  4.28587449e-07]
#[ 0.17881102] [  2.09779121e-05] [  9.63409870e-07]
#[ 0.48090133] [  1.59259436e-05] [  1.71909194e-07]
#[ 0.31411179] [  3.18605604e-05] [  6.95700818e-07]
#[ 0.20477863] [  1.22991914e-05] [  4.77617221e-07]
#[ 0.19443234] [  2.00576322e-05] [  8.31023240e-07]
#[ 0.31839163] [  2.02331176e-05] [  4.33147767e-07]
#
#


#pdb.set_trace()
print "im here"