import numpy as np
import pickle
import pdb,os
import matplotlib.pyplot as plt
import time
import idlsave
from gal_xyz import gal_xyz
import matplotlib as mpl
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from scipy.io.idl import readsav as idlrsave
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol
from k2_stuff import kpm_jhk
from phot_dist import phot_dist_interp
from subprocess import call
from taurus_paramgen import taurus_fastparams
from sample_generation import build_sample_group
from uvwtopm import uvwtopm

##set matplotlibglobal params
mpl.rcParams['lines.linewidth']   = 1.5
mpl.rcParams['axes.linewidth']    = 2
mpl.rcParams['xtick.major.width'] =2
mpl.rcParams['ytick.major.width'] =2
mpl.rcParams['ytick.labelsize'] = 13
mpl.rcParams['xtick.labelsize'] = 13
mpl.rcParams['axes.labelsize'] = 15


np.set_printoptions(suppress=True,precision=5)
nsamp=10000
posvel,cposvel = taurus_fastparams()
rposvel = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=True,observables=False)
rr,dd,ll,bb,plx,pmvect = build_sample_group(posvel,cposvel,0.0,nsamp,trace_forward=False,observables=True)
rgl,rgb,rgplx = gal_xyz(rposvel[:,0],rposvel[:,1],rposvel[:,2],plx=True,reverse=True)
rra,rdec     = glactc(rgl*180/np.pi,rgb*180/np.pi,2000,degree=True,fk4=True,reverse=True)
#pdb.set_trace()
#datacat    = pickle.load(open('/Users/aaron/data_local/tgas/taurus_tgas.pkl','rb'))
ty2 = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/tychoraw/tycho2_min.pkl','rb'))
nearme = np.where((ty2.ra < 90) & (ty2.ra > 50) & (ty2.dec > 10) & (ty2.dec < 45))[0]
ty2 = ty2[nearme]

hip = pickle.load(open(os.getenv("HOME")+'/data_local/catalogs/hipparcos07/hip_main.pkl','rb'))




##data of stars run through the bayes selection:ra,dec,l,b,prma,spmra,pmdec,spmdec,plx,sigplx,vr,sig_vr,id,pmcov
file1 = open('Taurusclump_161010_tgas_nopos_stardat.pkl','rb')
stardat = pickle.load(file1)
#bayes factor components
file2 = open('Taurusclump_161010_tgas_nopos.pkl','rb')
results = pickle.load(file2)
##read in the original data catalog for the stars processed
#datacat = pickle.load(open("Taurus_160216datacat_run.pkl","rb"))
#results2 = pickle.load(open("Taurus_160216_nopos.pkl","rb"))


##compute probabilites
##stanard 6D mode
bfact = results[:,1]/results[:,0]
bad = np.where(np.isnan(bfact))[0] ##these tend to be stars with large proper motions
bfact[bad] = 1.0
#bad2 = np.where(np.isinf(bfact))[0]
#bfact[bad2] = 1.0e6
prior = 0.01
prob = prior*bfact/(prior*bfact+1)


##read in the taurus known star lists
tmem  = idlrsave('known_taurus.idlsave').taumems
twtts = readcol('WTTS2_working.csv',fsep=',',asRecArray=True)
#pdb.set_trace()

##do a cross match
xw = np.zeros(len(twtts.ra),'int')-1
for i in range(0,len(twtts.ra)):
 #   pdb.set_trace()

    wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,float(twtts.ra[i]),float(twtts.dec[i]),format=2)
    close = np.argmin(wdist)
	#pdb.set_trace()
    if len(close.shape) > 1: pdb.set_trace()
    if wdist[close] < 10.0: xw[i] = close
   # print 'up to ' + str(i) + ' out of ' + str(len(twtts.ra[0]))

xk = np.zeros(len(tmem.rad[0]),'int')-1
for i in range(0,len(tmem.rad[0])):
    wdist = gcirc(stardat.ra*180/np.pi,stardat.dec*180/np.pi,tmem.rad[0][i],tmem.decd[0][i],format=2)
    close = np.argmin(wdist)
    if len(close.shape) > 1: pdb.set_trace()
    if wdist[close] < 10.0: xk[i] = close
   # print 'up to ' + str(i) + ' out of ' + str(len(tmem.rad[0]))


kx  = np.where(xk != -1)[0]
wx  = np.where(xw != -1)[0]
sxk = xk[np.where(xk != -1)[0]]
sxw = xw[np.where(xw != -1)[0]]
sxwa = xw[np.where((xw != -1) & ((twtts.mem == 'Y?') | (twtts.mem == 'Y')))[0]]


# ##cross with tycho2
# sxt = np.zeros(len(stardat),'int')-1
# sxh = np.zeros(len(stardat),'int')-1
# for i in range(len(stardat.ra)):
#     print 'up to  '+ str(i)
#     wdist = gcirc(ty2.ra,ty2.dec,stardat[i].ra*180/np.pi,stardat[i].dec*180/np.pi,format=2)
#     close = np.argmin(wdist)
#     #if len(close) > 1: pdb.set_trace()
#    # if wdist[close] > 10.0: pdb.set_trace()
#     if wdist[close] < 10.0: sxt[i] = close
#     
#     wdist = gcirc(hip.ra*180/np.pi,hip.dec*180/np.pi,stardat[i].ra*180/np.pi,stardat[i].dec*180/np.pi,format=2)
#     close = np.argmin(wdist)
#     if wdist[close] < 10.0: sxh[i] = close
# 
# pickle.dump(sxt,open('tycho_tgas_tauclumpcross.pkl','wb'))
# pickle.dump(sxh,open('hip_tgas_tauclumpcross.pkl','wb'))
# pdb.set_trace()
sxt = pickle.load(open('tycho_tgas_tauclumpcross.pkl','rb'))
sxh = pickle.load(open('hip_tgas_tauclumpcross.pkl','rb'))

bad = np.where((sxt == -1) & (sxh == -1))[0]
bv = np.zeros(len(stardat))
vm = np.zeros(len(stardat))

hh = np.where(sxh != -1)[0]
bv[hh] = hip.bv[sxh[hh]]
vm[hh] = hip.Hpmag[sxh[hh]]

tt = np.where(sxt != -1)[0]
bv[tt] = ty2.bv[sxt[tt]]
vm[tt] = ty2.v[sxt[tt]]

bv[bad] = -99.99
vm[bad] = -99.99



p1g = readcol('isochrones/p1gyr.dat',asRecArray=True)
p5m = readcol('isochrones/p5.01myr.dat',asRecArray=True)
p20 = readcol('isochrones/p19.95myr.dat',asRecArray=True)

mv = vm+5-5*np.log10(1000.0/stardat.plx)

good=  np.where((prob > 0.5) & (stardat.ra*180/np.pi < 95.) & (stardat.ra*180/np.pi > 75.) & (stardat.dec*180/np.pi > 17) & (stardat.dec*180/np.pi<27.))[0]
vgood=  np.where(prob > 0.8)[0]


##expected motions
ppm = np.zeros((len(good),3))*0.0
for i in range(len(good)):
    pmvect     = uvwtopm(posvel[3:6],stardat.ra[good[i]],stardat.dec[good[i]],stardat.plx[good[i]])
    ppm[i,:] = pmvect*1.0
    #pdb.set_trace()
    
ppw = np.zeros((len(sxw),3))*0.0
for i in range(len(sxw)):
    pmvect =  uvwtopm(posvel[3:6],stardat.ra[sxw[i]],stardat.dec[sxw[i]],stardat.plx[sxw[i]])
    ppw[i,:] = pmvect*1.0

ppk = np.zeros((len(sxk),3))*0.0
for i in range(len(sxk)):
    pmvect =  uvwtopm(posvel[3:6],stardat.ra[sxk[i]],stardat.dec[sxk[i]],stardat.plx[sxk[i]])
    ppk[i,:] = pmvect*1.0

pdb.set_trace()







#plt.plot(bv,mv,',')
gmask = np.zeros(len(good),dtype='int')
giant = np.where((bv[good] > 0.4) & (mv[good] < 2))[0]
gmask[giant]= 1
oks = np.where(gmask == 0)[0]
plt.plot(p5m.B-p5m.V,p5m.V,'r',label='5 Myr')
plt.plot(p20.B-p20.V,p20.V,'m',label = '20 Myr')
##plt.plot(p1g.B-p1g.V,p1g.V,'k')
plt.plot(bv[good[oks]],mv[good[oks]],'ko',label='Pmem > 50%')
plt.plot(bv[sxwa],mv[sxwa],'m^',label='Disk-Free')
plt.plot(bv[sxk],mv[sxk],'rs',label = 'Disk-Bearing')
plt.xlim([-.3,2])

plt.ylim([6,-1])
ax = plt.subplot(111)
ax.set_xlabel('B-V (mag)')
ax.set_ylabel('M$_\mathbf{V}$ (mag)')
plt.legend(numpoints=1)
plt.savefig('taurusclump_tgas_HRD.pdf')


#pdb.set_trace()


#plt.plot(bv[vgood],mv[vgood],'.r')

#plt.plot(p1g.B-p1g.V,p1g.V,'r')
#plt.plot(bv[sxk],mv[sxk],'go')
#plt.plot(bv[sxwa],mv[sxwa],'mo')

plt.figure()
#plt.quiver(stardat.ra[vgood]*180/np.pi,stardat.dec[vgood]*180/np.pi,stardat.pmra[vgood]/3600.0*40.0,stardat.pmdec[vgood]/3600.0*40.0)
#plt.quiver(stardat.ra[sxk]*180/np.pi,stardat.dec[sxk]*180/np.pi,stardat.pmra[sxk]/3600.0*40.0,stardat.pmdec[sxk]/3600.0*40.0,color='r')


ww = 0.003
plt.quiver(stardat.ra[good]*180/np.pi,stardat.dec[good]*180/np.pi,ppm[:,1]/3600.0*40.0,ppm[:,2]/3600.0*40.0,scale=5,width=ww,color='r')
plt.quiver(stardat.ra[good]*180/np.pi,stardat.dec[good]*180/np.pi,stardat.pmra[good]/3600.0*40.0-ppm[:,1]/3600.0*40.0,stardat.pmdec[good]/3600.0*40.0-ppm[:,2]/3600.0*40.0,color='b',scale=5,width=ww)
plt.quiver(stardat.ra[good]*180/np.pi,stardat.dec[good]*180/np.pi,stardat.pmra[good]/3600.0*40.0,stardat.pmdec[good]/3600.0*40.0,scale=5,width=ww*2)

plt.quiver(60.0,38.0,20.0/3600*40.0,0.0,scale=5)
plt.text(60.0,36.5,'20 mas/yr')
ax = plt.subplot(111)
ax.set_xlabel('R.A. (Degrees)')
ax.set_ylabel('Decl. (Degrees)')
plt.savefig('taurusclump_tgas_arrows.pdf')

#plt.quiver(stardat.ra[sxw]*180/np.pi,stardat.dec[sxw]*180/np.pi,ppw[:,1]/3600.0*40.0,ppw[:,2]/3600.0*40.0,scale=5)
#plt.quiver(stardat.ra[sxw]*180/np.pi,stardat.dec[sxw]*180/np.pi,stardat.pmra[sxw]/3600.0*40.0,stardat.pmdec[sxw]/3600.0*40.0,color='m',scale=5)
#plt.quiver(stardat.ra[sxw]*180/np.pi,stardat.dec[sxw]*180/np.pi,stardat.pmra[sxw]/3600.0*40.0-ppw[:,1]/3600.0*40.0,stardat.pmdec[sxw]/3600.0*40.0-ppw[:,2]/3600.0*40.0,color='g',scale=5)


#plt.quiver(stardat.ra[sxk]*180/np.pi,stardat.dec[sxk]*180/np.pi,ppk[:,1]/3600.0*40.0,ppk[:,2]/3600.0*40.0,scale=5)
#plt.quiver(stardat.ra[sxk]*180/np.pi,stardat.dec[sxk]*180/np.pi,stardat.pmra[sxk]/3600.0*40.0,stardat.pmdec[sxk]/3600.0*40.0,color='m',scale=5)
#plt.quiver(stardat.ra[sxk]*180/np.pi,stardat.dec[sxk]*180/np.pi,stardat.pmra[sxk]/3600.0*40.0-ppk[:,1]/3600.0*40.0,stardat.pmdec[sxk]/3600.0*40.0-ppk[:,2]/3600.0*40.0,color='g',scale=5)



listfile = open('list_clump.csv','w')
import ACRastro.radec as radec
from nice_radec import nice_radec
rah,ram,ras,ded,dem,des = radec.radec(stardat.ra[good]*180/np.pi,stardat.dec[good]*180/np.pi)
nrd = nice_radec(rah,ram,ras,ded,dem,des,spc=' ',mspc=',')
good=good[oks]
listfile.write('GaiaID,RA,DEC,B-V,Mv,Plx,Pmem,hasRV? \n')
for i in range(len(good)):
    rvstr = 'N'
    if (stardat.sig_rv[good[i]]<290.0): rvstr = 'Y'
    line = str(stardat.id[good[i]]) + ',' + nrd[oks[i]] +','+str(bv[good[i]]) +','+ str(vm[good[i]]) + ','+str(stardat.plx[good[i]]) +','+ str(np.floor(prob[good[i]]*100.0).astype(int)) +','+rvstr+ ' \n'
    qwe = np.where((sxk == good[i]))[0]
    qwe2 = np.where(sxw == good[i])[0]
    if (len(qwe) ==0 )& (len(qwe2)==0):
        listfile.write(line)
        listfile.flush()
listfile.close()

pdb.set_trace()
print 'im here'







