import matplotlib.pyplot as plt

import numpy as np
import pdb
import triangle

from full_uvw_to_observable import gal_to_obs
from full_uvw_to_observable import gal_to_obs_full
from cov2cor import cov2cor

np.set_printoptions(suppress=2)
np.set_printoptions(precision=3)


x = 43.1
y = 0.7
z = -17.3
sig_x = 10.0
sig_y =	10.0
sig_z = 10.0

u = +41.1
v = -19.2
w = -1.4
sig_u = np.sqrt(2.**2 + 0.9**2)
sig_v = np.sqrt(2.**2 + 0.2**2)
sig_w = np.sqrt(2.**2 + 0.4**2)


posvel  = np.array([x,y,z,u,v,w])
cposvel=np.array([[sig_x**2,1,1,0,0,0],[1,sig_y**2,1,0,0,0],[1,1,sig_z**2,0,0,0],[0,0,0,sig_u**2,0,0],[0,0,0,0,sig_v**2,0],[0,0,0,0,0,sig_w**2]])
pdb.set_trace()
means,covmat = gal_to_obs_full(posvel,cposvel)

rsamp = np.random.multivariate_normal(means,covmat,1000)
pdb.set_trace()
figure = triangle.corner(rsamp,labels = ('ra','dec','plx','pmra','pmdec','rv'))

plt.show()

pdb.set_trace()
V_ad       = 3.1     ##asymmetric drift of galaxy in lsr
sig_u_f    = 19.8    ##in lsr
sig_v_f    = 12.8    ##in lsr
sig_w_f    = 8.0     ##in lsr
##LSR conversion values
uvw_sun    = np.array([-9.0,12.0,7.0])
#posvel = np.array([0.0,0.0,0.0,])



pdb.set_trace()
print 'here'