from scipy.io.idl import readsav as idlrsave
import matplotlib.pyplot as plt
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from rejsamp import rejsamp
import idlsave

from gcirc import gcirc
from bayes_code import single_star_integral
from bayes_code import multi_star_integral
from sample_generation import build_sample_group
from sample_generation import build_uvw_field
from cp_math import find_cp
from helpers import dataset_looper
from cp_math import rotate_pm_to_cp
from cp_math import rotate_pm_to_cp_multistar
from cp_math import project_uvw_to_cp_multistar
from cp_math import project_uvw_to_cp
from taurus_paramgen import taurus_paramgen
from taurus_paramgen import taurus_fastparams
from phot_dist import phot_dist_interp
from full_uvw_to_observable import gal_to_obs_full
from scipy.special import erf as scipyerf
from scipy.stats import truncnorm
#np.set_printoptions(suppress=True,precision=2)
from readcol import readcol
import pdb,pickle,os,sys
import numpy as np
from astroquery.vizier import Vizier
import astropy.units as u
import astropy.coordinates as coord
import idlsave



#dcat1 = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz.pkl','rb'))

#pdb.set_trace()

datacat = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz.pkl','rb'))
#rvdata  = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_rvcross.pkl','rb'))
#incvz = np.where((datacat.ra >= 270-13) & (datacat.ra <= 270+13) & (datacat.dec >= 67.2-13) & (datacat.dec <= 67.2 + 13))[0]
#datacat = datacat[incvz]
#pickle.dump(datacat,open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz.pkl','wb'))

##make a distance cut:
distcut = np.where(datacat.plx >= 5.0)[0] ##200 pc

datacat = datacat[distcut]
close = np.where(datacat.plx >=10.0)[0]
rosat = np.zeros((len(datacat),6))-9999999.9
galex = np.zeros((len(datacat),5))-9999999.9
##find rosat/galex entries


# for i in range(0,len(datacat)):
#         print i+1
#         stuff = Vizier.query_region(coord.SkyCoord(ra=datacat.ra[i],dec=datacat.dec[i],unit=(u.deg, u.deg),frame='icrs'),radius="0d0m10s",catalog='II/312/mis')
#         if len(stuff) > 1: pdb.set_trace()
#         if len(stuff) == 1:
#             #pdb.set_trace()
#             #rrr = stuff[0]['_r']
#             #qwe = np.argmin(rrr)
#             galex[i] = np.array([stuff[0]['FUV'][0],stuff[0]['e_FUV'][0],stuff[0]['NUV'][0],stuff[0]['e_NUV'][0],stuff[0]['b'][0]])
# 
# pickle.dump(galex,open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz_galex.pkl','wb'))
# 
# 
# 
# for i in range(0,len(datacat)):
#         print i+1
#         stuff = Vizier.query_region(coord.SkyCoord(ra=datacat.ra[i],dec=datacat.dec[i],unit=(u.deg, u.deg),frame='icrs'),radius="0d0m10s",catalog='IX/29/rass_fsc')
#         if len(stuff) > 1: pdb.set_trace()
#         if len(stuff) == 1:
#             #pdb.set_trace()
#             #rrr = stuff[0]['_r']
#             #qwe = np.argmin(rrr)
#             rosat[i] = np.array([stuff[0]['HR1'][0],stuff[0]['e_HR1'][0],stuff[0]['HR2'][0],stuff[0]['e_HR2'][0],stuff[0]['Count'][0],stuff[0]['e_Count'][0]])
# 
# pickle.dump(rosat,open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz_rosat.pkl','wb'))
rosat = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz_rosat.pkl','rb'))
galex = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz_galex.pkl','rb'))


##crossmatch 2mass because its messed up:
# tmass = np.zeros((len(datacat),3))-99999.9
# for i in range(0,len(datacat)):
#     print i+1
#     stuff = Vizier.query_region(coord.SkyCoord(ra=datacat.ra[i],dec=datacat.dec[i],unit=(u.deg, u.deg),frame='icrs'),radius="0d0m10s",catalog='II/246/out')
#     if len(stuff) > 1: pdb.set_trace()
#     if len(stuff) == 1:
#         dis = gcirc(datacat.ra[i],datacat.dec[i],stuff[0]['RAJ2000'],stuff[0]['DEJ2000'])
#         besto = np.argmin(dis)
#         tmass[i] = np.array([stuff[0]['Jmag'][besto],stuff[0]['Hmag'][besto],stuff[0]['Kmag'][besto]])
# 
# pickle.dump(tmass,open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz_2mass.pkl','wb'))
tmass = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/usethese/tgas_x_2m_x_tyc_tesscvz_2mass.pkl','rb'))

gr = np.where((rosat[:,0] > -100) | (galex[:,2]>-100))[0]

Vmag = datacat.v +5 -5*np.log10(1000.0/datacat.plx)
Bmag = datacat.b +5 -5*np.log10(1000.0/datacat.plx)
Jmag = tmass[:,0] +5 -5*np.log10(1000.0/datacat.plx)
Hmag = tmass[:,1] +5 -5*np.log10(1000.0/datacat.plx)
Kmag = tmass[:,2] +5 -5*np.log10(1000.0/datacat.plx)
Gmag = datacat.gmag +5 -5*np.log10(1000.0/datacat.plx)

p1g = readcol('isochrones/p1gyr.dat',asRecArray=True)
p10m = readcol('isochrones/p10.00myr.dat',asRecArray=True)
p100m = readcol('isochrones/p19.95myr.dat',asRecArray=True)

bh10 = readcol('isochrones/bhac15_10myr.txt',asRecArray=True)[::-1]
bh20 = readcol('isochrones/bhac15_20myr.txt',asRecArray=True)[::-1]
bh100 = readcol('isochrones/bhac15_100myr.txt',asRecArray=True)[::-1]
bh1000 = readcol('isochrones/bhac15_1000myr.txt',asRecArray=True)[::-1]
bhg10 = readcol('isochrones/bhac15g_10myr.txt',asRecArray=True)[::-1]
bhg20 = readcol('isochrones/bhac15g_20myr.txt',asRecArray=True)[::-1]
bhg100 = readcol('isochrones/bhac15g_100myr.txt',asRecArray=True)[::-1]
bhg1000 = readcol('isochrones/bhac15g_1000myr.txt',asRecArray=True)[::-1]
bh50  = readcol('isochrones/bhac15_50myr.txt',asRecArray=True)[::-1]
bhg50  = readcol('isochrones/bhac15g_50myr.txt',asRecArray=True)[::-1]

gj = datacat.gmag - tmass[:,0]
gdiff = -Gmag + np.interp(gj,bhg20.G[0:20]-bh20.J[0:20],bhg20.G[0:20])

qwe = np.where((gdiff>=0) & (gj > 1.5))[0]
plt.plot(gj[qwe],Gmag[qwe],'xk')
plt.plot(gj,Gmag,'.')
plt.plot(gj[gr],Gmag[gr],'ko')
plt.plot(gj[close],Gmag[close],'.r')
plt.plot(bhg1000.G-bh1000.J,bhg1000.G,'k')
plt.plot(bhg50.G-bh50.J,bhg50.G,'m-')
plt.plot(bhg10.G-bh10.J,bhg10.G,'g')
plt.plot(bhg20.G-bh20.J,bhg20.G,'r')
plt.ylim([10,0])
plt.xlim([-6,10])


pdb.set_trace()
print 'done'