import numpy as np
from gal_xyz import gal_xyz
import pdb
from ACRastro.gal_uvw import gal_uvw
import emcee
from ACRastro.glactc import glactc
import time
import matplotlib.pyplot as plt
#import triangle
import pickle
import os

def ymg_paramgen(group,num=10000):
    
    glist = np.array(['bpic','tuchor','col','car','twhya','echa','oct','argus','abdor'])
    mm    = np.where(glist == group)[0]
    if len(mm) == 0:
        print 'You asked for group I dont know!!'
        pdb.set_trace()
    
    posvel = np.array([[20.,3.,-42.,14.,15.,50.,22.,5.,-6.],[-5,-24.,-56.,-94.,-44.,-92.,-106.,-80.0,-14.],
            [-15.,-35.,-47.,-17.,21.,-28.,-68.,-18.,-20.],[10.1,9.9,13.2,10.2,10.5,11.0,14.5,22.0,6.8],
            [-15.9,-20.9,-21.8,-23.0,-18.0,-19.9,-3.6,-14.4,-27.2],[-9.2,-1.4,-5.9,-4.4,-4.9,-10.4,-11.2,-5.0,-13.3]])
    
    
    sig_posvel = np.array([[50.0,50.0,50.0,15.0,15.0,15.0,100.0,50.0,80.0],[25.0,25.0,80.0,60.0,20.0,15.0,30.0,80.0,90.0],
                [15.0,10.0,50.0,15.0,10.0,15.0,30.0,40.0,40.0],[2.1,1.5,1.3,0.4,0.9,1.2,0.9,0.3,1.3],
                [0.8,0.8,0.8,0.8,1.5,1.2,1.6,1.3,1.2],[1.0,0.9,1.2,1.5,0.9,1.6,1.4,1.3,1.6]])

    #pdb.set_trace()
    posvel = posvel[:,mm].squeeze()
    sig_posvel = sig_posvel[:,mm].squeeze()
    sig_posvel[0:3] = sig_posvel[0:3]/3.0
    sig_posvel[3:] = sig_posvel[3:]#*2.0
    spv = np.diag(sig_posvel**2)
    
    rposvel = np.random.multivariate_normal(posvel,spv,num)
    
    return posvel,spv,rposvel

    
    
    
    
    
    
    
    
#stuff = ymg_paramgen('bpic',num=10000)
    
    
    