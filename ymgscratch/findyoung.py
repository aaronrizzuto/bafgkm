import numpy as np
import pickle,os,glob
import pdb
import matplotlib.pyplot as plt
from readcol import readcol
import scipy.interpolate
import mpfit
import emcee
import corner

def build_iso_pad(age,mass,mage,massp,modgrid):


    ##find the closest ages
    dage  = mage-age
    less  = np.where(dage <= 0.00000)[0]
    spotl = less[np.argmin(np.absolute(dage[less]))]
    more  = np.where(dage >= 0.00000)[0]
    spotm = more[np.argmin(np.absolute(dage[more]))]
    agel  = mage[spotl]
    agem  = mage[spotm]
    mod_agediff = agem-agel

    if agem == agel: 
        aslope = 1.0 
    else: 
        aslope = (age - agel)/(agem - agel)

    ##read the lower model and interpolate the the required mass
    modell = modgrid[spotl]
    
   
    interpofunct_l = scipy.interpolate.interp1d(massp,modell,axis=0,fill_value='extrapolate')
    lvalues        = interpofunct_l(mass)
    
    ##do the same for the upper model
    modelm = modgrid[spotm]
    interpofunct_m = scipy.interpolate.interp1d(massp,modelm,axis=0,fill_value='extrapolate')
    mvalues        = interpofunct_m(mass)
    finaljohn      = lvalues + (mvalues-lvalues)*aslope

    namevect = np.array(['Teff','Lum','Logg','B','V','J','H','K'])
    
  
    return namevect,finaljohn

def readall_isos_pad():
    isodir = './padova_isos/'
    ifiles = glob.glob(isodir+'p*.dat')
    mage = np.zeros(len(ifiles))
    for i in range(len(ifiles)): mage[i] = float(ifiles[i].split('/')[2].split('myr')[0].split('p')[1])
    ssss =  np.argsort(mage)
    mage = mage[ssss]
    ifiles = np.array(ifiles)[ssss]

    #now can read them all into a big array for use later
    masspoints = np.arange(0.1,4.1,0.1)
    grid = np.zeros((len(mage),len(masspoints),8))
    for i in range(len(ifiles)):
        thismodel = readcol(ifiles[i],verbose=False,skipline=1)
        mass = thismodel[:,2]
        b    = np.interp(masspoints,mass,thismodel[:,9])
        v    = np.interp(masspoints,mass,thismodel[:,10])       
        j    = np.interp(masspoints,mass,thismodel[:,13])       
        h    = np.interp(masspoints,mass,thismodel[:,14])       
        k    = np.interp(masspoints,mass,thismodel[:,15])   
        lum  = np.interp(masspoints,mass,thismodel[:,4])   
        teff = np.interp(masspoints,mass,thismodel[:,5])   
        logg = np.interp(masspoints,mass,thismodel[:,6])   
        
        grid[i,:,0] = teff*1.0
        grid[i,:,1] = lum*1.0
        grid[i,:,2] = logg*1.0
        grid[i,:,3] = b*1.0
        grid[i,:,4] = v*1.0
        grid[i,:,5] = j*1.0
        grid[i,:,6] = h*1.0
        grid[i,:,7] = k*1.0    
    return mage,masspoints,grid

def lotsplot(yy,err,iso1,iso2):
    plt.plot(iso1.B-iso1.V,iso1.V,'b')
    plt.plot(iso2.B-iso2.V,iso2.V,'g')    
    plt.errorbar(yy[0]-yy[1],yy[1],xerr=np.sqrt(err[1]**2+err[0]**2),yerr=err[1],fmt='or')
    plt.title('B-V vs V')
    plt.figure()
    plt.plot(iso1.J-iso1.K,iso1.K,'b')
    plt.plot(iso2.J-iso2.K,iso2.K,'g')    
    plt.errorbar(yy[2]-yy[4],yy[4],xerr=np.sqrt(err[4]**2+err[2]**2),yerr=err[4],fmt='or')
    plt.title('J-K vs K')
    plt.figure()
    plt.plot(iso1.V-iso1.J,iso1.J,'b')
    plt.plot(iso2.V-iso2.J,iso2.J,'g')    
    plt.errorbar(yy[1]-yy[2],yy[2],xerr=np.sqrt(err[1]**2+err[2]**2),yerr=err[2],fmt='or')
    plt.title('V-J vs J')
    plt.show()
    
    
##simple mpfit style residual function for BVJHK for a given model age/mass

def iso_resid(p,y=None,err=None,fjac=None,model=False,agep=None,massp=None,modgrid=None):
#    age  = p[0]
#    mass = p[1]
    mname,mvals = build_iso_pad(p[0],p[1],agep,massp,modgrid)
    if model == False:    return [0,(y-mvals[3:])/err]
    if model == True: return mname,mvals
    
def iso_lnlike(p,y,err,agep,massp,modgrid,model=False):
    if ((p[0] < 1.001) | (p[0] > 999.99)) : return -np.inf
    if ((p[1] < 0.02)  | (p[1] > 4.0))    : return -np.inf 
    mname,mvals = build_iso_pad(p[0],p[1],agep,massp,modgrid)
    lnlike = np.sum(-0.5*(mvals[3:]-y)**2/err**2 - 0.0*np.log(np.sqrt(2*np.pi*err**2)))
    if model == False: return lnlike
    if model == True: return mname,mvals
    
    


agepoints,masspoints,gridpoints = readall_isos_pad()

#pdb.set_trace()   
    
    
datacat    = pickle.load(open(os.getenv("HOME")+'/data_local/tgas/tgas_x_2m_x_tyc_100pc.pkl','rb'))

##read some isochrones
p1g = readcol('padova_isos/p1gyr.txt',asRecArray=True)
p31 = readcol('padova_isos/p31.6myr.txt',asRecArray=True)

#plot everything on a hrd

sig_v    = np.sqrt(datacat.evt**2+datacat.ebt**2)
sig_b    = sig_v*1.0
sig_dist = 1000.0/datacat.plx*np.sqrt(datacat.sig_plx/datacat.plx)**2
dist     = 1000.0/datacat.plx
MV       = datacat.v+5.0-5.0*np.log10(dist)
sig_MV   = np.sqrt(sig_v**2 + (5.0*sig_dist/dist/np.log(10.0))**2)
MB       = datacat.b+5.0-5.0*np.log10(dist)
sig_MB   = np.sqrt(sig_b**2 + (5.0*sig_dist/dist/np.log(10.0))**2)
BV       = datacat.b-datacat.v
sig_BV   = np.sqrt(sig_b**2+sig_v**2)
MJ       = datacat.j+5-5.0*np.log10(dist)
MH       = datacat.h+5-5.0*np.log10(dist)
MK       = datacat.k+5-5.0*np.log10(dist)
sig_MJ   = np.sqrt(datacat.sig_j**2 + (5.0*sig_dist/dist/np.log(10.0))**2)
sig_MH   = np.sqrt(datacat.sig_h**2 + (5.0*sig_dist/dist/np.log(10.0))**2)
sig_MK   = np.sqrt(datacat.sig_k**2 + (5.0*sig_dist/dist/np.log(10.0))**2)

##more cleaning
qwe = np.where((datacat.j < -90) | (datacat.j ==0.0))[0]
sig_MJ[qwe] = 1000000.0
qwe = np.where((datacat.h < -90) | (datacat.j ==0.0))[0]
sig_MH[qwe] = 1000000.0
qwe = np.where((datacat.k < -90) | (datacat.j ==0.0))[0]
sig_MK[qwe] = 1000000.0
qwe = np.where((datacat.b < -90))[0]
datacat.sig_MB = 1000000.0
qwe = np.where((datacat.v < -90))[0]
datacat.sig_MV = 1000000.0




#plt.plot(BV,MV,',k')
#plt.plot(p1g.B-p1g.V,p1g.V,'r')
#plt.plot(p20.B-p20.V,p20.V,'g')
#plt.ylim([15,0])

#whats above the main sequence?
testbv = BV

MSBV,MSV,MSLT,MSM = p1g.B[0:60]-p1g.V[0:60],p1g.V[0:60],p1g.logTe[0:60],p1g.M_act[0:60]


init_mass = np.interp(testbv,MSBV[::-1],MSM[::-1])



##isofile ages:
isodir = './padova_isos/'
ifiles = glob.glob(isodir+'p*.dat')
agegrid = np.zeros(len(ifiles))
for i in range(len(ifiles)): agegrid[i] = float(ifiles[i].split('/')[2].split('myr')[0].split('p')[1])
agegrid = np.sort(agegrid)
agegrid[0] = 1.1
agegrid[-1] =999.99

agegrid = [2.0,5.0,20.0,30.0 ,50.0,100.0,200.0,300.0,400.0,500.0,600.0,700.0,800.0,900.0]

# 29,   219,   433,   511,   711,   772,  1632,  1655,  1849,
 #        2001,  2164,  2458,  3457,  3550,  4370,  5348,  5591,  5809,
#         5977,  6401,  6841,  7146,  7399,  7453,  7620,  8512,  9448,
#         9847, 10111, 10557, 11064, 11910, 12163, 12934, 13215, 13452,
#        14242, 14488, 15252, 16018, 16466, 16778, 16787, 16799, 17466,
#        18332, 18365, 19510, 19737, 19757, 19790, 20394, 20580, 20976,
#        22032, 22145, 22427, 22472, 22707, 23454, 24387, 24545, 25449,
#        25467, 26103, 26421, 26876, 27316, 27621, 27975, 28237, 28727,
#        28871, 28971, 30650, 30825, 31003, 31140, 31858, 32590, 32717,
#        32904, 33211, 33448, 33820, 33866, 34568, 34898, 34988,


##storage for model fit values, ordering: age,mass,Lum,teff,logg,mbol
modelvals = np.zeros((len(datacat),2))
sig_modelvals = np.zeros((len(datacat),2))
sig_modelvalsl = np.zeros((len(datacat),2))
sig_modelvalsu = np.zeros((len(datacat),2))

modelextra = np.zeros((len(datacat),3))
#for i in range(len(datacat)):
for jj in range(0,len(datacat)):
    if np.mod(jj,100) == 0: print 'Up to ' + str(jj+1) + ' out of ' + str(len(datacat))
    print jj
    yy  = np.array([MB[jj],MV[jj],MJ[jj],MH[jj],MK[jj]])
    err = np.array([sig_MB[jj],sig_MV[jj],sig_MJ[jj],sig_MH[jj],sig_MK[jj]])
    #err = np.array([sig_MB[jj],sig_MV[jj],1000.0,1000.0,1000.0])
   # pdb.set_trace()
    pstart = np.array([30.0,init_mass[jj]])
    fa      = {'y':yy, 'err':err,'agep':agepoints,'massp':masspoints,'modgrid':gridpoints}
    parinfo = [{'fixed':0, 'limited':[0,0], 'limits':[0.,0.]} for dddd in range(pstart.shape[0])]

    parinfo[0]['limited'] = [1,1]
    parinfo[0]['limits']  = [1.001,999.99]
    parinfo[1]['limited'] = [1,1]
    parinfo[1]['limits']  = [0.05,4.0]
    parinfo[0]['fixed'] = 1
    
    current_chi2 = np.inf
    for ii in range(len(agegrid)):
        pstart[0] = agegrid[ii]
        thisfit = mpfit.mpfit(iso_resid,pstart,functkw=fa,quiet=True,parinfo=parinfo)
        if thisfit.fnorm < current_chi2:
            current_chi2 = thisfit.fnorm*1.0
            bestindex = ii*1
            bestpars = thisfit.params*1.0

            
 
     
    parinfo[0]['fixed']=0
    pstart = bestpars*1.0

    thisfit = mpfit.mpfit(iso_resid,pstart,functkw=fa,quiet=True,parinfo=parinfo,maxiter=20)
    fitpars = thisfit.params
    fitcov  = thisfit.covar
    
    
    

   # pdb.set_trace()
   #  ##now set up an emcee version
    #for now don't run this
    if fitpars[0] < -100000.0:
        nwalkers = 10
        ndim     = 2
        startsig = thisfit.perror*10.0
        startsig[0] = np.max([startsig[0],20.0])
        startpars = thisfit.params*1.0
        p0 = [np.random.randn(ndim)*startsig+startpars for ddd in range(nwalkers)]
       # ssss = np.array([thisfit.params[0],100.0,200.0,300.0,400.0,500.0,600.0,700.0,800.0,900.0])
        for i in range(nwalkers):
            #p0[i][0] = ssss[i]*1.0
            if (p0[i][0] < 1.001) | (p0[i][0] > 999.999): p0[i][0] = 500.0
            if (p0[i][1] < 0.01) | (p0[i][1] > 4.0): p0[i][1] = 1.0
        
        burn = 4000
        nsamps = 5000
        sampler = emcee.EnsembleSampler(nwalkers,ndim,iso_lnlike,args=[yy,err,agepoints,masspoints,gridpoints])
        
    
        pos,prob,state = sampler.run_mcmc(p0,nsamps)
        samples = sampler.chain[:,burn:,:].reshape((-1,ndim))

        result = np.zeros((ndim,3),float)
        for stuff in range(ndim): result[stuff,:]=np.percentile(samples[:,stuff],[16,60,84])
        ##the parameter results and errors
        sig_lower = result[:,1]-result[:,0]
        sig_upper = result[:,2]-result[:,1]
        fitpars   = result[:,1]
        pdb.set_trace()
        lotsplot(yy,err,p1g,p31)
        corner.corner(samples,labels = ['Age (Myr)','Mass (Msun)'])

    mnames,mvals = build_iso_pad(fitpars[0],fitpars[1],agepoints,masspoints,gridpoints)
    modelvals[jj,0] = fitpars[0]*1.0
    modelvals[jj,1] = fitpars[1]*1.0
    sig_modelvals[jj,0] = thisfit.perror[0]*1.0
    sig_modelvals[jj,1] = thisfit.perror[1]*1.0   
   ## sig_modelvalsu[jj,0] = sig_upper[0]*1.0
    ##sig_modelvalsu[jj,1] = sig_upper[1]*1.0  
    modelextra[jj,:]    = mvals[0:3]*1.0
    #pdb.set_trace()

pickle.dump((modelvals,sig_modelvals,modelextra),open('agefitter_tgas_100px.pkl','wb'))


pdb.set_trace()
print 'im here'