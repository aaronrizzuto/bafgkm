;;johnson
jfile = 'compiles_johnson.dat'
sfile = 'compiled_sdss.dat'

readcol, jfile, Z ,loga,	M_ini   ,	M_act,	logL,	logTe	,logG	,mbol   , UX   ,   BX  ,    B  ,     V  ,     R      , Im     ,  Jm     ,  H     ,  K   ,    L,       Lp ,     M,	intimfstage,dummy
close,/all
ages = loga[uniq(loga)]
for i=0,n_elements(ages)-1 do begin
   thisfile = 'p'+nform(10^ages[i]/1e6,dec=2)+'myr.dat'
   lines = where(loga eq ages[i])
   openw,1,thisfile
   printf,1,'Z ,loga,	M_ini   ,	M_act,	logL,	logTe	,logG	,mbol   , UX   ,    B  ,     V  ,     R      , I     ,  J     ,  H     ,  K   ,    L,       Lp ,     M, int_inf stage'
   for j=0,n_elements(lines)-1 do begin
      ii = lines[j]
      thisline = nform(z[ii],dec=5) + ' ' + nform(loga[ii],dec=4) +' '+ nform(m_ini[ii],dec=4) +' '+ nform(m_act[ii],dec=4)+' '+nform(logl[ii],dec=4)+' '+nform(logte[ii],dec=4)+' '+nform(logg[ii],dec=4)+' '+nform(mbol[ii],dec=4)+' '+nform(UX[ii],dec=4)+' '+nform(B[ii],dec=4)+' '+nform(V[ii],dec=4)+' '+nform(R[ii],dec=4)+' '+nform(Im[ii],dec=4)+' '+nform(Jm[ii],dec=4)+' '+nform(H[ii],dec=4)+' '+nform(K[ii],dec=4)+' '+nform(L[ii],dec=4)+' '+nform(Lp[ii],dec=4)+' '+nform(M[ii],dec=4) +' '+nform(intimfstage[ii],dec=4)+' '+nform(dummy[ii],dec=4)
      printf,1,thisline
   endfor
   close,1   
endfor
stop
readcol, sfile, Z ,loga,	M_ini   ,	M_act,	logL,	logTe	,logG	,mbol,u,g,rm,im,z,intimfstage,dummy
close,/all
ages = loga[uniq(loga)]
for i=0,n_elements(ages)-1 do begin
   thisfile = 'psdss'+nform(10^ages[i]/1e6,dec=2)+'myr.dat'
   lines = where(loga eq ages[i])
   openw,1,thisfile
   printf,1,'Z ,loga,	M_ini   ,	M_act,	logL,	logTe	,logG	,mbol   ,u,g,r,i,z,int_inf stage'
   for j=0,n_elements(lines)-1 do begin
      ii = lines[j]
      thisline = nform(z[ii],dec=5) + ' ' + nform(loga[ii],dec=4) +' '+ nform(m_ini[ii],dec=4) +' '+ nform(m_act[ii],dec=4)+' '+nform(logl[ii],dec=4)+' '+nform(logte[ii],dec=4)+' '+nform(logg[ii],dec=4)+' '+nform(mbol[ii],dec=4)+' '+nform(u[ii],dec=4)+' '+nform(g[ii],dec=4)+' '+nform(rm[ii],dec=4)+' '+nform(im[ii],dec=4)+' '+nform(z[ii],dec=4)+' '+nform(intimfstage[ii],dec=4)+' '+nform(dummy[ii],dec=4)
      printf,1,thisline
   endfor
   close,1   
endfor



stop
end
