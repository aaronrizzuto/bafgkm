import numpy as np
import pickle
import pdb,os,glob
import matplotlib.pyplot as plt
import time
from gal_xyz import gal_xyz
import matplotlib as mpl
from ACRastro.glactc import glactc
from ACRastro.gal_uvw import gal_uvw
from gcirc import gcirc
from ACRastro.radec import radec
from nice_radec import nice_radec
from readcol import readcol
from k2_stuff import kpm_jhk
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from matplotlib.pyplot import cm

##set matplotlibglobal params
mpl.rcParams['lines.linewidth']   = 1.5
mpl.rcParams['axes.linewidth']    = 2
mpl.rcParams['xtick.major.width'] =2
mpl.rcParams['ytick.major.width'] =2
mpl.rcParams['ytick.labelsize'] = 13
mpl.rcParams['xtick.labelsize'] = 13
mpl.rcParams['axes.labelsize'] = 15
mpl.rcParams['legend.numpoints'] = 1
listfile = '/Users/aaron/data_local/catalogs/bell15ymg/bell_ymg15_TGAS.csv'
ymglist = readcol(listfile,fsep=',',asRecArray=True)

msh = readcol('bowler_msheight.txt',asRecArray=True)  ##brendans main-sequence height

#pdb.set_trace()
##The run dataset file
file1 = 'bpic_tgas_20161103_stardat.pkl'
datafile = '/Users/aaron/data_local/tgas/tgas_x_2m_x_tyc__pulk_gcrv_100pc.pkl'
datacat  = pickle.load(open(datafile,'rb'))
stardat  = pickle.load(open(file1,'rb'))

fullrv = readcol('tgas_100pc_rv.csv',asRecArray=True,fsep=',')
datacat.rv = fullrv.rv
datacat.sig_rv = fullrv.sig_rv
datacat.rvsource = fullrv.rvsource

##xmatch brendan's catalog
hms = np.zeros(len(datacat.ra),dtype=float)+2.0
for i in range(len(datacat.ra)):
    print i
    if datacat.tycho[i] != '':
        mm = np.where(msh.tyc == datacat.tycho[i])[0]
        if len(mm) > 1: pdb.set_trace()
        if len(mm) != 0 : hms[i] = msh.mvresid[mm]
        
    else:
        mm = np.where(str(msh.hip) == datacat.hip[i])[0]
        if len(mm) > 1 : pdb.set_trace()
        if len(mm) != 0 : hms[i] = msh.mvresid[mm]

#pdb.set_trace()

files  = glob.glob('*tgas_2016*[0-9].pkl')
tmparr = np.zeros((len(datacat),len(files)),dtype=float)
groups = np.zeros(len(files),dtype='|S22')

for i in range(len(files)):
    groups[i] = files[i].split('_')[0]
    results = pickle.load(open(files[i],'rb'))
    bfact = results[:,1]/results[:,0]
    bad = np.where((np.isnan(bfact)))[0] ##these tend to be stars with large proper motions
    bfact[bad] = 1.0
    bad2 = np.where(np.isinf(bfact))[0]
    bfact[bad2] = 1.0e6
    tmparr[:,i] = bfact
    
##convert to probabilities
prior = 0.01
probarr = tmparr*prior/(tmparr*prior+1.0)

##convert to a nice recarray
dts  = [(gg,float) for gg in groups]
prob = np.recarray(probarr.shape[0],dtype=dts)
fieldnames= prob.dtype.names
for i in range(len(groups)): prob[groups[i]] = probarr[:,i]


#pdb.set_trace()

##cross with YMG list from Bell 2015
xymg = np.zeros(len(ymglist),dtype=int)-1
for i in range(len(ymglist)):
    
    if ymglist.tyc[i] == '': mm = np.where(datacat.hip == ymglist.hip[i])[0]
    if ymglist.tyc[i] != '': mm = np.where(datacat.tycho == ymglist.tyc[i])[0]
    if len(mm) !=1:
        #pdb.set_trace()
        print 'missed one'
    else:
        xymg[i] = mm[0]
    
kkk     = np.where(xymg >= 0)[0]
xymg    = xymg[kkk]
ymglist = ymglist[kkk]


pdb.set_trace()

#ymggroups = array(['32', 'AB', 'Ar', 'BP', 'Ca', 'Co', 'EC', 'TH', 'TW'])

gnames = ('abdor', 'argus', 'bpic', 'car', 'col', 'echa', 'oct', 'tuchor', 'twhya')
ymgn   = ('AB','Ar','BP','Ca','Co','EC','OCT','TH','TW')
group_names = np.array(gnames)
myg = np.where(prob.echa > 0.9)[0]
kn  = np.where(ymglist.grp=='EC')[0]

#plt.plot(datacat.ra[myg],datacat.dec[myg],'ob')
#plt.plot(datacat.ra[xymg[kn]],datacat.dec[xymg[kn]],'.r')
#good = np.where(prob >= 0.5)[0]
test = np.max(probarr,axis=1)
best = np.where(test > 0.9)[0]

ti = np.argmax(probarr,axis=1)
bestgroup = group_names[ti]


plt.plot(datacat.j-datacat.k,datacat.k+5-5*np.log10(1000.0/datacat.plx),',k')
plt.plot(datacat.j[xymg]-datacat.k[xymg],datacat.k[xymg]+5-5*np.log10(1000.0/datacat.plx[xymg]),'ok')

color=iter(cm.jet(np.linspace(0,1,len(groups))))
for i in range(len(groups)):
    best = np.where((test > 0.9) & (ti == i))[0]
    c=next(color)
    plt.plot(datacat.j[best]-datacat.k[best],datacat.k[best]+5-5*np.log10(1000.0/datacat.plx[best]),'.',c=c,label=groups[i])

plt.legend()
plt.xlim([-.25,1.3])
plt.ylim([10,-2])
plt.savefig('sample_overlap_cmd.pdf')
#plt.show()
plt.clf()
##make probability list
#file = open('tgas_group_probs_100pc.csv','wb')

#for i in range(len(datacat)):
    
#pdb.set_trace()

##recovery histograms for each group
for i in range(len(gnames)):
    if (gnames[i] != 'oct') | (gnames[i] != 'echa'):
        this = np.where(ymglist.grp == ymgn[i])[0]
        if len(this) > 2:
            plt.hist(prob[gnames[i]][xymg[this]])
            plt.savefig('histo_recover_'+gnames[i]+'pdf')
            plt.clf()



#pdb.set_trace()
##make a complete list for use later
# pfile = open('group_probs_datacat.csv','wb')
# pfile.write('hip,tyc,abdor,argus,bpic,car,col,echa,oct,tuchor,twhya /n')
# for i in range(len(datacat)):
#     line = datacat.hip[i]+','+datacat.tycho[i]+','+str(prob.abdor[i])+','+str(prob.argus[i])+','+str(prob.bpic[i])+','+str(prob.car[i])+','+str(prob.col[i])+','+str(prob.echa[i])+','+str(prob.oct[i])+','+str(prob.tuchor[i])+','+str(prob.twhya[i])+', /n'
#     pfile.write(line)
#     pfile.flush()
#     print 'up to ' + str(i+1)
# pfile.close()


#pdb.set_trace()
##now make the observing lists
vkcol = datacat.v-datacat.k
gbflag = np.zeros(len(datacat),dtype=int)
linex = np.array([1.75,2.7])
liney = np.array([-1.15,-3])
lim = np.zeros(len(datacat),dtype=float)
qwe=  np.where(vkcol < linex[0])[0]
lim = np.interp(vkcol,linex,liney)
lim[qwe] = liney[0]*1.0

gb = np.where((hms < lim) & (vkcol < linex[1]))[0]
plt.plot(vkcol,hms,',k')
plt.plot(vkcol[gb],hms[gb],',r')
gbflag[gb] = 1
amsflag = np.zeros(len(datacat),dtype=int)+1
below = np.where((vkcol >2.0) & (hms>0.0))[0]
amsflag[below] = 0
hhh = np.where(hms == 2.0)[0]
amsflag[hhh] = 1
knownflag = np.zeros(len(datacat),dtype=int)
knownflag[xymg] = 1

mikelist = readcol('mikelist.txt',asRecArray=True)
mikeflag = np.zeros(len(datacat),dtype=int)
for i in range(len(mikelist.ra)):
    mdist = gcirc(datacat.ra,datacat.dec,mikelist.ra[i],mikelist.dec[i],format=2)
    close = np.argmin(mdist)
    if mdist[close] < 10.0:
        mikeflag[i] = 1



obs = np.where((test > 0.5) & (amsflag == 1) & (vkcol <6.0) & (vkcol > -0.5) & (gbflag ==0) & (knownflag == 0) & (mikeflag ==0))[0]



##position plot:
plt.clf()
#color=iter(cm.jet(np.linspace(0,1,len(groups))))
for i in range(len(groups)):
    best = np.where((ti[obs] == i))[0]
    this = np.where(ymglist.grp == ymgn[i])[0]
    if len(this) > 2:
        plt.plot(datacat[xymg[this]].ra,datacat[xymg[this]].dec,'ok')
    #c=next(color)
    plt.plot(datacat.ra[obs[best]],datacat.dec[obs[best]],'.r',c=c,label=groups[i])
    plt.savefig('Candidate_pos' + groups[i]+'.pdf')
    plt.clf()

#plt.legend()
plt.clf()

##make observing lists:
#1 "36862089393979904" 03 52 24.76 +12 22 43.4 2015.0 0.0 0.0 V=9.8258 Type=Tau 
wrkfile = open('TGAS_YMG.wrk','wb')
tlist   = open('TGAS_YMG.csv','wb')
tlist.write('hip,tyc,ra,dec,v,vk,hms,bestprob,bestgroup,hasrv,bpic,tuchor,abdor,argus,car,col,echa,oct,twhya' + ' \n')
tlist.flush()

rah,ram,ras,ded,dem,des = radec(datacat.ra[obs],datacat.dec[obs])
nrd = nice_radec(rah,ram,ras,ded,dem,des,spc=' ',mspc=' ')
nrd2 = nice_radec(rah,ram,ras,ded,dem,des,spc=' ',mspc=',')
for i in range(len(obs)):
    if datacat[obs[i]].hip != '':thisname = datacat[obs[i]].hip
    if datacat[obs[i]].tycho != '':thisname = datacat[obs[i]].tycho
    wline = str(i+1)+ ' ' + '"'+thisname+'" '+nrd[i] + ' 2000 0.0 0.0 V='+str(datacat.v[obs[i]])+' \n'
    wrkfile.write(wline)
    wrkfile.flush()
    rvcat = datacat.rvsource[obs[i]]
    if rvcat != '':
        if datacat.sig_rv[obs[i]] >1.0: rvcat = ''
    
    line = str(i+1) +','+thisname+','+nrd2[i]+','+str(datacat.v[obs[i]])+','+str(vkcol[obs[i]])+','+str(hms[obs[i]])+','+str(np.round(100*test[obs[i]]))+','+bestgroup[obs[i]]+','+rvcat+','+str(np.round(100*prob.bpic[obs[i]]))+','+str(np.round(100*prob.tuchor[obs[i]]))+','+str(np.round(100*prob.abdor[obs[i]]))+','+str(np.round(100*prob.argus[obs[i]]))+','+str(np.round(100*prob.car[obs[i]]))+','+str(np.round(100*prob.col[obs[i]]))+','+str(np.round(100*prob.echa[obs[i]]))+','+str(np.round(100*prob.oct[obs[i]]))+','+str(np.round(100*prob.twhya[obs[i]]))+' \n'
    tlist.write(line)
    tlist.flush()
tlist.close()
wrkfile.close()
    
    

# plt.quiver(stardat.ra[good]*180/np.pi,stardat.dec[good]*180/np.pi,stardat.pmra[good]/3600.0*40.0,stardat.pmdec[good]/3600.0*40.0,scale=5,width=ww*2)
# 
# ax = plt.subplot(111)
# ax.set_xlabel('R.A. (Degrees)')
# ax.set_ylabel('Decl. (Degrees)')

#plt.quiver(stardat.ra[sxw]*180/np.pi,stardat.dec[sxw]*180/np.pi,ppw[:,1]/3600.0*40.0,ppw[:,2]/3600.0*40.0,scale=5)
#plt.quiver(stardat.ra[sxw]*180/np.pi,stardat.dec[sxw]*180/np.pi,stardat.pmra[sxw]/3600.0*40.0,stardat.pmdec[sxw]/3600.0*40.0,color='m',scale=5)
#plt.quiver(stardat.ra[sxw]*180/np.pi,stardat.dec[sxw]*180/np.pi,stardat.pmra[sxw]/3600.0*40.0-ppw[:,1]/3600.0*40.0,stardat.pmdec[sxw]/3600.0*40.0-ppw[:,2]/3600.0*40.0,color='g',scale=5)


#plt.quiver(stardat.ra[sxk]*180/np.pi,stardat.dec[sxk]*180/np.pi,ppk[:,1]/3600.0*40.0,ppk[:,2]/3600.0*40.0,scale=5)
#plt.quiver(stardat.ra[sxk]*180/np.pi,stardat.dec[sxk]*180/np.pi,stardat.pmra[sxk]/3600.0*40.0,stardat.pmdec[sxk]/3600.0*40.0,color='m',scale=5)
#plt.quiver(stardat.ra[sxk]*180/np.pi,stardat.dec[sxk]*180/np.pi,stardat.pmra[sxk]/3600.0*40.0-ppk[:,1]/3600.0*40.0,stardat.pmdec[sxk]/3600.0*40.0-ppk[:,2]/3600.0*40.0,color='g',scale=5)



pdb.set_trace()
print 'im here'







